/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.movirtu.ussd.thrift;

import java.util.Map;

import com.movirtu.vsp.menu.menu.UssdMenuHandlerService;

/**
 *
 * @author Mlungisi Sincuba
 * @since 1.0
 */
public class VspUssdHandlerImpl implements UssdHandler {

	private UssdMenuHandlerService ussdMenuHandlerService;

	public VspUssdHandlerImpl(String baseUrl) {
		ussdMenuHandlerService = UssdMenuHandlerService.getInstance();
	}

	public String startUssdSession(int serviceId, long sessionId, String encoding, String data, String msisdn, Map<String, String> extension) {
		return null;
	}

	public String stopUssdSession(int serviceId, long sessionId, String encoding, String data, String msisdn, int error, Map<String, String> extension) {
		return null;
	}

	public String requestUssd(int serviceId, long sessionId, String encoding, String data, String msisdn, Map<String, String> extension) {
		return null;
	}

	public String respondUssd(int serviceId, long sessionId, String encoding, String data, String msisdn, Map<String, String> extension) {
		return ussdMenuHandlerService.process(msisdn, data);
	}

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.movirtu.ussd.thrift;

import java.util.Map;

/**
 *
 * @author Mlungisi Sincuba
 * @since 1.0
 */
public interface UssdHandler {
	
    String startUssdSession(int serviceId, long sessionId, String encoding, String data, String msisdn, Map<String, String> extension);
    String stopUssdSession(int serviceId, long sessionId, String encoding, String data, String msisdn, int error, Map<String, String> extension);
    String requestUssd(int serviceId, long sessionId, String encoding, String data, String msisdn, Map<String, String> extension);
    String respondUssd(int serviceId, long sessionId, String encoding, String data, String msisdn, Map<String, String> extension);
}

package com.movirtu.thrift.generated;

import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MapJavagw {

  public interface Iface {

    public void reqMissedCallDetails(int service_id, long session_id, String msisdn, Map<String,String> extension) throws org.apache.thrift.TException;

    public void respUpdateVLRDetails(int service_id, long session_id, String hlr_number, short error, Map<String,String> extension) throws org.apache.thrift.TException;

    public void reqvMSRN(int service_id, long session_id, String imsi, Map<String,String> extension) throws org.apache.thrift.TException;

    public void reqInitiateUSSDSession(int service_id, long session_id, String coding_scheme, String data_string, String msisdn, Map<String,String> extension) throws org.apache.thrift.TException;

    public void respUSSD(int service_id, long session_id, String coding_scheme, String data_string, String msisdn, Map<String,String> extension) throws org.apache.thrift.TException;

    public void reqIncommingSmsData(int service_id, long session_id, String imsi, String coding_scheme, String data_string, String orig_party_addr, String timestamp, Map<String,String> extension) throws org.apache.thrift.TException;

    public void respOutgoingSmsData(int service_id, long session_id, short error, Map<String,String> extension) throws org.apache.thrift.TException;

    public void registerNodeRequest(Map<String,String> extension) throws org.apache.thrift.TException;

    public void registerNodeResponse(Map<String,String> extension) throws org.apache.thrift.TException;

    public void serviceTypeRequest(int service_id, long session_id, String msisdn, String imsi, Map<String,String> extension) throws org.apache.thrift.TException;

    public void subscriberInfoResp(int service_id, long session_id, String msisdn, Map<String,String> extension) throws org.apache.thrift.TException;

    public void respSriSm(int service_id, long session_id, String imsi, Map<String,String> extension) throws org.apache.thrift.TException;

  }

  public interface AsyncIface {

    public void reqMissedCallDetails(int service_id, long session_id, String msisdn, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<AsyncClient.reqMissedCallDetails_call> resultHandler) throws org.apache.thrift.TException;

    public void respUpdateVLRDetails(int service_id, long session_id, String hlr_number, short error, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<AsyncClient.respUpdateVLRDetails_call> resultHandler) throws org.apache.thrift.TException;

    public void reqvMSRN(int service_id, long session_id, String imsi, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<AsyncClient.reqvMSRN_call> resultHandler) throws org.apache.thrift.TException;

    public void reqInitiateUSSDSession(int service_id, long session_id, String coding_scheme, String data_string, String msisdn, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<AsyncClient.reqInitiateUSSDSession_call> resultHandler) throws org.apache.thrift.TException;

    public void respUSSD(int service_id, long session_id, String coding_scheme, String data_string, String msisdn, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<AsyncClient.respUSSD_call> resultHandler) throws org.apache.thrift.TException;

    public void reqIncommingSmsData(int service_id, long session_id, String imsi, String coding_scheme, String data_string, String orig_party_addr, String timestamp, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<AsyncClient.reqIncommingSmsData_call> resultHandler) throws org.apache.thrift.TException;

    public void respOutgoingSmsData(int service_id, long session_id, short error, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<AsyncClient.respOutgoingSmsData_call> resultHandler) throws org.apache.thrift.TException;

    public void registerNodeRequest(Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<AsyncClient.registerNodeRequest_call> resultHandler) throws org.apache.thrift.TException;

    public void registerNodeResponse(Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<AsyncClient.registerNodeResponse_call> resultHandler) throws org.apache.thrift.TException;

    public void serviceTypeRequest(int service_id, long session_id, String msisdn, String imsi, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<AsyncClient.serviceTypeRequest_call> resultHandler) throws org.apache.thrift.TException;

    public void subscriberInfoResp(int service_id, long session_id, String msisdn, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<AsyncClient.subscriberInfoResp_call> resultHandler) throws org.apache.thrift.TException;

    public void respSriSm(int service_id, long session_id, String imsi, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<AsyncClient.respSriSm_call> resultHandler) throws org.apache.thrift.TException;

  }

  public static class Client implements org.apache.thrift.TServiceClient, Iface {
    public static class Factory implements org.apache.thrift.TServiceClientFactory<Client> {
      public Factory() {}
      public Client getClient(org.apache.thrift.protocol.TProtocol prot) {
        return new Client(prot);
      }
      public Client getClient(org.apache.thrift.protocol.TProtocol iprot, org.apache.thrift.protocol.TProtocol oprot) {
        return new Client(iprot, oprot);
      }
    }

    public Client(org.apache.thrift.protocol.TProtocol prot)
    {
      this(prot, prot);
    }

    public Client(org.apache.thrift.protocol.TProtocol iprot, org.apache.thrift.protocol.TProtocol oprot)
    {
      iprot_ = iprot;
      oprot_ = oprot;
    }

    protected org.apache.thrift.protocol.TProtocol iprot_;
    protected org.apache.thrift.protocol.TProtocol oprot_;

    protected int seqid_;

    public org.apache.thrift.protocol.TProtocol getInputProtocol()
    {
      return this.iprot_;
    }

    public org.apache.thrift.protocol.TProtocol getOutputProtocol()
    {
      return this.oprot_;
    }

    public void reqMissedCallDetails(int service_id, long session_id, String msisdn, Map<String,String> extension) throws org.apache.thrift.TException
    {
      send_reqMissedCallDetails(service_id, session_id, msisdn, extension);
    }

    public void send_reqMissedCallDetails(int service_id, long session_id, String msisdn, Map<String,String> extension) throws org.apache.thrift.TException
    {
      oprot_.writeMessageBegin(new org.apache.thrift.protocol.TMessage("reqMissedCallDetails", org.apache.thrift.protocol.TMessageType.CALL, ++seqid_));
      reqMissedCallDetails_args args = new reqMissedCallDetails_args();
      args.setService_id(service_id);
      args.setSession_id(session_id);
      args.setMsisdn(msisdn);
      args.setExtension(extension);
      args.write(oprot_);
      oprot_.writeMessageEnd();
      oprot_.getTransport().flush();
    }

    public void respUpdateVLRDetails(int service_id, long session_id, String hlr_number, short error, Map<String,String> extension) throws org.apache.thrift.TException
    {
      send_respUpdateVLRDetails(service_id, session_id, hlr_number, error, extension);
    }

    public void send_respUpdateVLRDetails(int service_id, long session_id, String hlr_number, short error, Map<String,String> extension) throws org.apache.thrift.TException
    {
      oprot_.writeMessageBegin(new org.apache.thrift.protocol.TMessage("respUpdateVLRDetails", org.apache.thrift.protocol.TMessageType.CALL, ++seqid_));
      respUpdateVLRDetails_args args = new respUpdateVLRDetails_args();
      args.setService_id(service_id);
      args.setSession_id(session_id);
      args.setHlr_number(hlr_number);
      args.setError(error);
      args.setExtension(extension);
      args.write(oprot_);
      oprot_.writeMessageEnd();
      oprot_.getTransport().flush();
    }

    public void reqvMSRN(int service_id, long session_id, String imsi, Map<String,String> extension) throws org.apache.thrift.TException
    {
      send_reqvMSRN(service_id, session_id, imsi, extension);
    }

    public void send_reqvMSRN(int service_id, long session_id, String imsi, Map<String,String> extension) throws org.apache.thrift.TException
    {
      oprot_.writeMessageBegin(new org.apache.thrift.protocol.TMessage("reqvMSRN", org.apache.thrift.protocol.TMessageType.CALL, ++seqid_));
      reqvMSRN_args args = new reqvMSRN_args();
      args.setService_id(service_id);
      args.setSession_id(session_id);
      args.setImsi(imsi);
      args.setExtension(extension);
      args.write(oprot_);
      oprot_.writeMessageEnd();
      oprot_.getTransport().flush();
    }

    public void reqInitiateUSSDSession(int service_id, long session_id, String coding_scheme, String data_string, String msisdn, Map<String,String> extension) throws org.apache.thrift.TException
    {
      send_reqInitiateUSSDSession(service_id, session_id, coding_scheme, data_string, msisdn, extension);
    }

    public void send_reqInitiateUSSDSession(int service_id, long session_id, String coding_scheme, String data_string, String msisdn, Map<String,String> extension) throws org.apache.thrift.TException
    {
      oprot_.writeMessageBegin(new org.apache.thrift.protocol.TMessage("reqInitiateUSSDSession", org.apache.thrift.protocol.TMessageType.CALL, ++seqid_));
      reqInitiateUSSDSession_args args = new reqInitiateUSSDSession_args();
      args.setService_id(service_id);
      args.setSession_id(session_id);
      args.setCoding_scheme(coding_scheme);
      args.setData_string(data_string);
      args.setMsisdn(msisdn);
      args.setExtension(extension);
      args.write(oprot_);
      oprot_.writeMessageEnd();
      oprot_.getTransport().flush();
    }

    public void respUSSD(int service_id, long session_id, String coding_scheme, String data_string, String msisdn, Map<String,String> extension) throws org.apache.thrift.TException
    {
      send_respUSSD(service_id, session_id, coding_scheme, data_string, msisdn, extension);
    }

    public void send_respUSSD(int service_id, long session_id, String coding_scheme, String data_string, String msisdn, Map<String,String> extension) throws org.apache.thrift.TException
    {
      oprot_.writeMessageBegin(new org.apache.thrift.protocol.TMessage("respUSSD", org.apache.thrift.protocol.TMessageType.CALL, ++seqid_));
      respUSSD_args args = new respUSSD_args();
      args.setService_id(service_id);
      args.setSession_id(session_id);
      args.setCoding_scheme(coding_scheme);
      args.setData_string(data_string);
      args.setMsisdn(msisdn);
      args.setExtension(extension);
      args.write(oprot_);
      oprot_.writeMessageEnd();
      oprot_.getTransport().flush();
    }

    public void reqIncommingSmsData(int service_id, long session_id, String imsi, String coding_scheme, String data_string, String orig_party_addr, String timestamp, Map<String,String> extension) throws org.apache.thrift.TException
    {
      send_reqIncommingSmsData(service_id, session_id, imsi, coding_scheme, data_string, orig_party_addr, timestamp, extension);
    }

    public void send_reqIncommingSmsData(int service_id, long session_id, String imsi, String coding_scheme, String data_string, String orig_party_addr, String timestamp, Map<String,String> extension) throws org.apache.thrift.TException
    {
      oprot_.writeMessageBegin(new org.apache.thrift.protocol.TMessage("reqIncommingSmsData", org.apache.thrift.protocol.TMessageType.CALL, ++seqid_));
      reqIncommingSmsData_args args = new reqIncommingSmsData_args();
      args.setService_id(service_id);
      args.setSession_id(session_id);
      args.setImsi(imsi);
      args.setCoding_scheme(coding_scheme);
      args.setData_string(data_string);
      args.setOrig_party_addr(orig_party_addr);
      args.setTimestamp(timestamp);
      args.setExtension(extension);
      args.write(oprot_);
      oprot_.writeMessageEnd();
      oprot_.getTransport().flush();
    }

    public void respOutgoingSmsData(int service_id, long session_id, short error, Map<String,String> extension) throws org.apache.thrift.TException
    {
      send_respOutgoingSmsData(service_id, session_id, error, extension);
    }

    public void send_respOutgoingSmsData(int service_id, long session_id, short error, Map<String,String> extension) throws org.apache.thrift.TException
    {
      oprot_.writeMessageBegin(new org.apache.thrift.protocol.TMessage("respOutgoingSmsData", org.apache.thrift.protocol.TMessageType.CALL, ++seqid_));
      respOutgoingSmsData_args args = new respOutgoingSmsData_args();
      args.setService_id(service_id);
      args.setSession_id(session_id);
      args.setError(error);
      args.setExtension(extension);
      args.write(oprot_);
      oprot_.writeMessageEnd();
      oprot_.getTransport().flush();
    }

    public void registerNodeRequest(Map<String,String> extension) throws org.apache.thrift.TException
    {
      send_registerNodeRequest(extension);
    }

    public void send_registerNodeRequest(Map<String,String> extension) throws org.apache.thrift.TException
    {
      oprot_.writeMessageBegin(new org.apache.thrift.protocol.TMessage("registerNodeRequest", org.apache.thrift.protocol.TMessageType.CALL, ++seqid_));
      registerNodeRequest_args args = new registerNodeRequest_args();
      args.setExtension(extension);
      args.write(oprot_);
      oprot_.writeMessageEnd();
      oprot_.getTransport().flush();
    }

    public void registerNodeResponse(Map<String,String> extension) throws org.apache.thrift.TException
    {
      send_registerNodeResponse(extension);
    }

    public void send_registerNodeResponse(Map<String,String> extension) throws org.apache.thrift.TException
    {
      oprot_.writeMessageBegin(new org.apache.thrift.protocol.TMessage("registerNodeResponse", org.apache.thrift.protocol.TMessageType.CALL, ++seqid_));
      registerNodeResponse_args args = new registerNodeResponse_args();
      args.setExtension(extension);
      args.write(oprot_);
      oprot_.writeMessageEnd();
      oprot_.getTransport().flush();
    }

    public void serviceTypeRequest(int service_id, long session_id, String msisdn, String imsi, Map<String,String> extension) throws org.apache.thrift.TException
    {
      send_serviceTypeRequest(service_id, session_id, msisdn, imsi, extension);
    }

    public void send_serviceTypeRequest(int service_id, long session_id, String msisdn, String imsi, Map<String,String> extension) throws org.apache.thrift.TException
    {
      oprot_.writeMessageBegin(new org.apache.thrift.protocol.TMessage("serviceTypeRequest", org.apache.thrift.protocol.TMessageType.CALL, ++seqid_));
      serviceTypeRequest_args args = new serviceTypeRequest_args();
      args.setService_id(service_id);
      args.setSession_id(session_id);
      args.setMsisdn(msisdn);
      args.setImsi(imsi);
      args.setExtension(extension);
      args.write(oprot_);
      oprot_.writeMessageEnd();
      oprot_.getTransport().flush();
    }

    public void subscriberInfoResp(int service_id, long session_id, String msisdn, Map<String,String> extension) throws org.apache.thrift.TException
    {
      send_subscriberInfoResp(service_id, session_id, msisdn, extension);
    }

    public void send_subscriberInfoResp(int service_id, long session_id, String msisdn, Map<String,String> extension) throws org.apache.thrift.TException
    {
      oprot_.writeMessageBegin(new org.apache.thrift.protocol.TMessage("subscriberInfoResp", org.apache.thrift.protocol.TMessageType.CALL, ++seqid_));
      subscriberInfoResp_args args = new subscriberInfoResp_args();
      args.setService_id(service_id);
      args.setSession_id(session_id);
      args.setMsisdn(msisdn);
      args.setExtension(extension);
      args.write(oprot_);
      oprot_.writeMessageEnd();
      oprot_.getTransport().flush();
    }

    public void respSriSm(int service_id, long session_id, String imsi, Map<String,String> extension) throws org.apache.thrift.TException
    {
      send_respSriSm(service_id, session_id, imsi, extension);
    }

    public void send_respSriSm(int service_id, long session_id, String imsi, Map<String,String> extension) throws org.apache.thrift.TException
    {
      oprot_.writeMessageBegin(new org.apache.thrift.protocol.TMessage("respSriSm", org.apache.thrift.protocol.TMessageType.CALL, ++seqid_));
      respSriSm_args args = new respSriSm_args();
      args.setService_id(service_id);
      args.setSession_id(session_id);
      args.setImsi(imsi);
      args.setExtension(extension);
      args.write(oprot_);
      oprot_.writeMessageEnd();
      oprot_.getTransport().flush();
    }

  }
  public static class AsyncClient extends org.apache.thrift.async.TAsyncClient implements AsyncIface {
    public static class Factory implements org.apache.thrift.async.TAsyncClientFactory<AsyncClient> {
      private org.apache.thrift.async.TAsyncClientManager clientManager;
      private org.apache.thrift.protocol.TProtocolFactory protocolFactory;
      public Factory(org.apache.thrift.async.TAsyncClientManager clientManager, org.apache.thrift.protocol.TProtocolFactory protocolFactory) {
        this.clientManager = clientManager;
        this.protocolFactory = protocolFactory;
      }
      public AsyncClient getAsyncClient(org.apache.thrift.transport.TNonblockingTransport transport) {
        return new AsyncClient(protocolFactory, clientManager, transport);
      }
    }

    public AsyncClient(org.apache.thrift.protocol.TProtocolFactory protocolFactory, org.apache.thrift.async.TAsyncClientManager clientManager, org.apache.thrift.transport.TNonblockingTransport transport) {
      super(protocolFactory, clientManager, transport);
    }

    public void reqMissedCallDetails(int service_id, long session_id, String msisdn, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<reqMissedCallDetails_call> resultHandler) throws org.apache.thrift.TException {
      checkReady();
      reqMissedCallDetails_call method_call = new reqMissedCallDetails_call(service_id, session_id, msisdn, extension, resultHandler, this, protocolFactory, transport);
      this.currentMethod = method_call;
      manager.call(method_call);
    }

    public static class reqMissedCallDetails_call extends org.apache.thrift.async.TAsyncMethodCall {
      private int service_id;
      private long session_id;
      private String msisdn;
      private Map<String,String> extension;
      public reqMissedCallDetails_call(int service_id, long session_id, String msisdn, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<reqMissedCallDetails_call> resultHandler, org.apache.thrift.async.TAsyncClient client, org.apache.thrift.protocol.TProtocolFactory protocolFactory, org.apache.thrift.transport.TNonblockingTransport transport) throws org.apache.thrift.TException {
        super(client, protocolFactory, transport, resultHandler, true);
        this.service_id = service_id;
        this.session_id = session_id;
        this.msisdn = msisdn;
        this.extension = extension;
      }

      public void write_args(org.apache.thrift.protocol.TProtocol prot) throws org.apache.thrift.TException {
        prot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("reqMissedCallDetails", org.apache.thrift.protocol.TMessageType.CALL, 0));
        reqMissedCallDetails_args args = new reqMissedCallDetails_args();
        args.setService_id(service_id);
        args.setSession_id(session_id);
        args.setMsisdn(msisdn);
        args.setExtension(extension);
        args.write(prot);
        prot.writeMessageEnd();
      }

      public void getResult() throws org.apache.thrift.TException {
        if (getState() != org.apache.thrift.async.TAsyncMethodCall.State.RESPONSE_READ) {
          throw new IllegalStateException("Method call not finished!");
        }
        org.apache.thrift.transport.TMemoryInputTransport memoryTransport = new org.apache.thrift.transport.TMemoryInputTransport(getFrameBuffer().array());
        org.apache.thrift.protocol.TProtocol prot = client.getProtocolFactory().getProtocol(memoryTransport);
      }
    }

    public void respUpdateVLRDetails(int service_id, long session_id, String hlr_number, short error, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<respUpdateVLRDetails_call> resultHandler) throws org.apache.thrift.TException {
      checkReady();
      respUpdateVLRDetails_call method_call = new respUpdateVLRDetails_call(service_id, session_id, hlr_number, error, extension, resultHandler, this, protocolFactory, transport);
      this.currentMethod = method_call;
      manager.call(method_call);
    }

    public static class respUpdateVLRDetails_call extends org.apache.thrift.async.TAsyncMethodCall {
      private int service_id;
      private long session_id;
      private String hlr_number;
      private short error;
      private Map<String,String> extension;
      public respUpdateVLRDetails_call(int service_id, long session_id, String hlr_number, short error, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<respUpdateVLRDetails_call> resultHandler, org.apache.thrift.async.TAsyncClient client, org.apache.thrift.protocol.TProtocolFactory protocolFactory, org.apache.thrift.transport.TNonblockingTransport transport) throws org.apache.thrift.TException {
        super(client, protocolFactory, transport, resultHandler, true);
        this.service_id = service_id;
        this.session_id = session_id;
        this.hlr_number = hlr_number;
        this.error = error;
        this.extension = extension;
      }

      public void write_args(org.apache.thrift.protocol.TProtocol prot) throws org.apache.thrift.TException {
        prot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("respUpdateVLRDetails", org.apache.thrift.protocol.TMessageType.CALL, 0));
        respUpdateVLRDetails_args args = new respUpdateVLRDetails_args();
        args.setService_id(service_id);
        args.setSession_id(session_id);
        args.setHlr_number(hlr_number);
        args.setError(error);
        args.setExtension(extension);
        args.write(prot);
        prot.writeMessageEnd();
      }

      public void getResult() throws org.apache.thrift.TException {
        if (getState() != org.apache.thrift.async.TAsyncMethodCall.State.RESPONSE_READ) {
          throw new IllegalStateException("Method call not finished!");
        }
        org.apache.thrift.transport.TMemoryInputTransport memoryTransport = new org.apache.thrift.transport.TMemoryInputTransport(getFrameBuffer().array());
        org.apache.thrift.protocol.TProtocol prot = client.getProtocolFactory().getProtocol(memoryTransport);
      }
    }

    public void reqvMSRN(int service_id, long session_id, String imsi, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<reqvMSRN_call> resultHandler) throws org.apache.thrift.TException {
      checkReady();
      reqvMSRN_call method_call = new reqvMSRN_call(service_id, session_id, imsi, extension, resultHandler, this, protocolFactory, transport);
      this.currentMethod = method_call;
      manager.call(method_call);
    }

    public static class reqvMSRN_call extends org.apache.thrift.async.TAsyncMethodCall {
      private int service_id;
      private long session_id;
      private String imsi;
      private Map<String,String> extension;
      public reqvMSRN_call(int service_id, long session_id, String imsi, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<reqvMSRN_call> resultHandler, org.apache.thrift.async.TAsyncClient client, org.apache.thrift.protocol.TProtocolFactory protocolFactory, org.apache.thrift.transport.TNonblockingTransport transport) throws org.apache.thrift.TException {
        super(client, protocolFactory, transport, resultHandler, true);
        this.service_id = service_id;
        this.session_id = session_id;
        this.imsi = imsi;
        this.extension = extension;
      }

      public void write_args(org.apache.thrift.protocol.TProtocol prot) throws org.apache.thrift.TException {
        prot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("reqvMSRN", org.apache.thrift.protocol.TMessageType.CALL, 0));
        reqvMSRN_args args = new reqvMSRN_args();
        args.setService_id(service_id);
        args.setSession_id(session_id);
        args.setImsi(imsi);
        args.setExtension(extension);
        args.write(prot);
        prot.writeMessageEnd();
      }

      public void getResult() throws org.apache.thrift.TException {
        if (getState() != org.apache.thrift.async.TAsyncMethodCall.State.RESPONSE_READ) {
          throw new IllegalStateException("Method call not finished!");
        }
        org.apache.thrift.transport.TMemoryInputTransport memoryTransport = new org.apache.thrift.transport.TMemoryInputTransport(getFrameBuffer().array());
        org.apache.thrift.protocol.TProtocol prot = client.getProtocolFactory().getProtocol(memoryTransport);
      }
    }

    public void reqInitiateUSSDSession(int service_id, long session_id, String coding_scheme, String data_string, String msisdn, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<reqInitiateUSSDSession_call> resultHandler) throws org.apache.thrift.TException {
      checkReady();
      reqInitiateUSSDSession_call method_call = new reqInitiateUSSDSession_call(service_id, session_id, coding_scheme, data_string, msisdn, extension, resultHandler, this, protocolFactory, transport);
      this.currentMethod = method_call;
      manager.call(method_call);
    }

    public static class reqInitiateUSSDSession_call extends org.apache.thrift.async.TAsyncMethodCall {
      private int service_id;
      private long session_id;
      private String coding_scheme;
      private String data_string;
      private String msisdn;
      private Map<String,String> extension;
      public reqInitiateUSSDSession_call(int service_id, long session_id, String coding_scheme, String data_string, String msisdn, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<reqInitiateUSSDSession_call> resultHandler, org.apache.thrift.async.TAsyncClient client, org.apache.thrift.protocol.TProtocolFactory protocolFactory, org.apache.thrift.transport.TNonblockingTransport transport) throws org.apache.thrift.TException {
        super(client, protocolFactory, transport, resultHandler, true);
        this.service_id = service_id;
        this.session_id = session_id;
        this.coding_scheme = coding_scheme;
        this.data_string = data_string;
        this.msisdn = msisdn;
        this.extension = extension;
      }

      public void write_args(org.apache.thrift.protocol.TProtocol prot) throws org.apache.thrift.TException {
        prot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("reqInitiateUSSDSession", org.apache.thrift.protocol.TMessageType.CALL, 0));
        reqInitiateUSSDSession_args args = new reqInitiateUSSDSession_args();
        args.setService_id(service_id);
        args.setSession_id(session_id);
        args.setCoding_scheme(coding_scheme);
        args.setData_string(data_string);
        args.setMsisdn(msisdn);
        args.setExtension(extension);
        args.write(prot);
        prot.writeMessageEnd();
      }

      public void getResult() throws org.apache.thrift.TException {
        if (getState() != org.apache.thrift.async.TAsyncMethodCall.State.RESPONSE_READ) {
          throw new IllegalStateException("Method call not finished!");
        }
        org.apache.thrift.transport.TMemoryInputTransport memoryTransport = new org.apache.thrift.transport.TMemoryInputTransport(getFrameBuffer().array());
        org.apache.thrift.protocol.TProtocol prot = client.getProtocolFactory().getProtocol(memoryTransport);
      }
    }

    public void respUSSD(int service_id, long session_id, String coding_scheme, String data_string, String msisdn, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<respUSSD_call> resultHandler) throws org.apache.thrift.TException {
      checkReady();
      respUSSD_call method_call = new respUSSD_call(service_id, session_id, coding_scheme, data_string, msisdn, extension, resultHandler, this, protocolFactory, transport);
      this.currentMethod = method_call;
      manager.call(method_call);
    }

    public static class respUSSD_call extends org.apache.thrift.async.TAsyncMethodCall {
      private int service_id;
      private long session_id;
      private String coding_scheme;
      private String data_string;
      private String msisdn;
      private Map<String,String> extension;
      public respUSSD_call(int service_id, long session_id, String coding_scheme, String data_string, String msisdn, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<respUSSD_call> resultHandler, org.apache.thrift.async.TAsyncClient client, org.apache.thrift.protocol.TProtocolFactory protocolFactory, org.apache.thrift.transport.TNonblockingTransport transport) throws org.apache.thrift.TException {
        super(client, protocolFactory, transport, resultHandler, true);
        this.service_id = service_id;
        this.session_id = session_id;
        this.coding_scheme = coding_scheme;
        this.data_string = data_string;
        this.msisdn = msisdn;
        this.extension = extension;
      }

      public void write_args(org.apache.thrift.protocol.TProtocol prot) throws org.apache.thrift.TException {
        prot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("respUSSD", org.apache.thrift.protocol.TMessageType.CALL, 0));
        respUSSD_args args = new respUSSD_args();
        args.setService_id(service_id);
        args.setSession_id(session_id);
        args.setCoding_scheme(coding_scheme);
        args.setData_string(data_string);
        args.setMsisdn(msisdn);
        args.setExtension(extension);
        args.write(prot);
        prot.writeMessageEnd();
      }

      public void getResult() throws org.apache.thrift.TException {
        if (getState() != org.apache.thrift.async.TAsyncMethodCall.State.RESPONSE_READ) {
          throw new IllegalStateException("Method call not finished!");
        }
        org.apache.thrift.transport.TMemoryInputTransport memoryTransport = new org.apache.thrift.transport.TMemoryInputTransport(getFrameBuffer().array());
        org.apache.thrift.protocol.TProtocol prot = client.getProtocolFactory().getProtocol(memoryTransport);
      }
    }

    public void reqIncommingSmsData(int service_id, long session_id, String imsi, String coding_scheme, String data_string, String orig_party_addr, String timestamp, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<reqIncommingSmsData_call> resultHandler) throws org.apache.thrift.TException {
      checkReady();
      reqIncommingSmsData_call method_call = new reqIncommingSmsData_call(service_id, session_id, imsi, coding_scheme, data_string, orig_party_addr, timestamp, extension, resultHandler, this, protocolFactory, transport);
      this.currentMethod = method_call;
      manager.call(method_call);
    }

    public static class reqIncommingSmsData_call extends org.apache.thrift.async.TAsyncMethodCall {
      private int service_id;
      private long session_id;
      private String imsi;
      private String coding_scheme;
      private String data_string;
      private String orig_party_addr;
      private String timestamp;
      private Map<String,String> extension;
      public reqIncommingSmsData_call(int service_id, long session_id, String imsi, String coding_scheme, String data_string, String orig_party_addr, String timestamp, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<reqIncommingSmsData_call> resultHandler, org.apache.thrift.async.TAsyncClient client, org.apache.thrift.protocol.TProtocolFactory protocolFactory, org.apache.thrift.transport.TNonblockingTransport transport) throws org.apache.thrift.TException {
        super(client, protocolFactory, transport, resultHandler, true);
        this.service_id = service_id;
        this.session_id = session_id;
        this.imsi = imsi;
        this.coding_scheme = coding_scheme;
        this.data_string = data_string;
        this.orig_party_addr = orig_party_addr;
        this.timestamp = timestamp;
        this.extension = extension;
      }

      public void write_args(org.apache.thrift.protocol.TProtocol prot) throws org.apache.thrift.TException {
        prot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("reqIncommingSmsData", org.apache.thrift.protocol.TMessageType.CALL, 0));
        reqIncommingSmsData_args args = new reqIncommingSmsData_args();
        args.setService_id(service_id);
        args.setSession_id(session_id);
        args.setImsi(imsi);
        args.setCoding_scheme(coding_scheme);
        args.setData_string(data_string);
        args.setOrig_party_addr(orig_party_addr);
        args.setTimestamp(timestamp);
        args.setExtension(extension);
        args.write(prot);
        prot.writeMessageEnd();
      }

      public void getResult() throws org.apache.thrift.TException {
        if (getState() != org.apache.thrift.async.TAsyncMethodCall.State.RESPONSE_READ) {
          throw new IllegalStateException("Method call not finished!");
        }
        org.apache.thrift.transport.TMemoryInputTransport memoryTransport = new org.apache.thrift.transport.TMemoryInputTransport(getFrameBuffer().array());
        org.apache.thrift.protocol.TProtocol prot = client.getProtocolFactory().getProtocol(memoryTransport);
      }
    }

    public void respOutgoingSmsData(int service_id, long session_id, short error, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<respOutgoingSmsData_call> resultHandler) throws org.apache.thrift.TException {
      checkReady();
      respOutgoingSmsData_call method_call = new respOutgoingSmsData_call(service_id, session_id, error, extension, resultHandler, this, protocolFactory, transport);
      this.currentMethod = method_call;
      manager.call(method_call);
    }

    public static class respOutgoingSmsData_call extends org.apache.thrift.async.TAsyncMethodCall {
      private int service_id;
      private long session_id;
      private short error;
      private Map<String,String> extension;
      public respOutgoingSmsData_call(int service_id, long session_id, short error, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<respOutgoingSmsData_call> resultHandler, org.apache.thrift.async.TAsyncClient client, org.apache.thrift.protocol.TProtocolFactory protocolFactory, org.apache.thrift.transport.TNonblockingTransport transport) throws org.apache.thrift.TException {
        super(client, protocolFactory, transport, resultHandler, true);
        this.service_id = service_id;
        this.session_id = session_id;
        this.error = error;
        this.extension = extension;
      }

      public void write_args(org.apache.thrift.protocol.TProtocol prot) throws org.apache.thrift.TException {
        prot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("respOutgoingSmsData", org.apache.thrift.protocol.TMessageType.CALL, 0));
        respOutgoingSmsData_args args = new respOutgoingSmsData_args();
        args.setService_id(service_id);
        args.setSession_id(session_id);
        args.setError(error);
        args.setExtension(extension);
        args.write(prot);
        prot.writeMessageEnd();
      }

      public void getResult() throws org.apache.thrift.TException {
        if (getState() != org.apache.thrift.async.TAsyncMethodCall.State.RESPONSE_READ) {
          throw new IllegalStateException("Method call not finished!");
        }
        org.apache.thrift.transport.TMemoryInputTransport memoryTransport = new org.apache.thrift.transport.TMemoryInputTransport(getFrameBuffer().array());
        org.apache.thrift.protocol.TProtocol prot = client.getProtocolFactory().getProtocol(memoryTransport);
      }
    }

    public void registerNodeRequest(Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<registerNodeRequest_call> resultHandler) throws org.apache.thrift.TException {
      checkReady();
      registerNodeRequest_call method_call = new registerNodeRequest_call(extension, resultHandler, this, protocolFactory, transport);
      this.currentMethod = method_call;
      manager.call(method_call);
    }

    public static class registerNodeRequest_call extends org.apache.thrift.async.TAsyncMethodCall {
      private Map<String,String> extension;
      public registerNodeRequest_call(Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<registerNodeRequest_call> resultHandler, org.apache.thrift.async.TAsyncClient client, org.apache.thrift.protocol.TProtocolFactory protocolFactory, org.apache.thrift.transport.TNonblockingTransport transport) throws org.apache.thrift.TException {
        super(client, protocolFactory, transport, resultHandler, true);
        this.extension = extension;
      }

      public void write_args(org.apache.thrift.protocol.TProtocol prot) throws org.apache.thrift.TException {
        prot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("registerNodeRequest", org.apache.thrift.protocol.TMessageType.CALL, 0));
        registerNodeRequest_args args = new registerNodeRequest_args();
        args.setExtension(extension);
        args.write(prot);
        prot.writeMessageEnd();
      }

      public void getResult() throws org.apache.thrift.TException {
        if (getState() != org.apache.thrift.async.TAsyncMethodCall.State.RESPONSE_READ) {
          throw new IllegalStateException("Method call not finished!");
        }
        org.apache.thrift.transport.TMemoryInputTransport memoryTransport = new org.apache.thrift.transport.TMemoryInputTransport(getFrameBuffer().array());
        org.apache.thrift.protocol.TProtocol prot = client.getProtocolFactory().getProtocol(memoryTransport);
      }
    }

    public void registerNodeResponse(Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<registerNodeResponse_call> resultHandler) throws org.apache.thrift.TException {
      checkReady();
      registerNodeResponse_call method_call = new registerNodeResponse_call(extension, resultHandler, this, protocolFactory, transport);
      this.currentMethod = method_call;
      manager.call(method_call);
    }

    public static class registerNodeResponse_call extends org.apache.thrift.async.TAsyncMethodCall {
      private Map<String,String> extension;
      public registerNodeResponse_call(Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<registerNodeResponse_call> resultHandler, org.apache.thrift.async.TAsyncClient client, org.apache.thrift.protocol.TProtocolFactory protocolFactory, org.apache.thrift.transport.TNonblockingTransport transport) throws org.apache.thrift.TException {
        super(client, protocolFactory, transport, resultHandler, true);
        this.extension = extension;
      }

      public void write_args(org.apache.thrift.protocol.TProtocol prot) throws org.apache.thrift.TException {
        prot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("registerNodeResponse", org.apache.thrift.protocol.TMessageType.CALL, 0));
        registerNodeResponse_args args = new registerNodeResponse_args();
        args.setExtension(extension);
        args.write(prot);
        prot.writeMessageEnd();
      }

      public void getResult() throws org.apache.thrift.TException {
        if (getState() != org.apache.thrift.async.TAsyncMethodCall.State.RESPONSE_READ) {
          throw new IllegalStateException("Method call not finished!");
        }
        org.apache.thrift.transport.TMemoryInputTransport memoryTransport = new org.apache.thrift.transport.TMemoryInputTransport(getFrameBuffer().array());
        org.apache.thrift.protocol.TProtocol prot = client.getProtocolFactory().getProtocol(memoryTransport);
      }
    }

    public void serviceTypeRequest(int service_id, long session_id, String msisdn, String imsi, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<serviceTypeRequest_call> resultHandler) throws org.apache.thrift.TException {
      checkReady();
      serviceTypeRequest_call method_call = new serviceTypeRequest_call(service_id, session_id, msisdn, imsi, extension, resultHandler, this, protocolFactory, transport);
      this.currentMethod = method_call;
      manager.call(method_call);
    }

    public static class serviceTypeRequest_call extends org.apache.thrift.async.TAsyncMethodCall {
      private int service_id;
      private long session_id;
      private String msisdn;
      private String imsi;
      private Map<String,String> extension;
      public serviceTypeRequest_call(int service_id, long session_id, String msisdn, String imsi, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<serviceTypeRequest_call> resultHandler, org.apache.thrift.async.TAsyncClient client, org.apache.thrift.protocol.TProtocolFactory protocolFactory, org.apache.thrift.transport.TNonblockingTransport transport) throws org.apache.thrift.TException {
        super(client, protocolFactory, transport, resultHandler, true);
        this.service_id = service_id;
        this.session_id = session_id;
        this.msisdn = msisdn;
        this.imsi = imsi;
        this.extension = extension;
      }

      public void write_args(org.apache.thrift.protocol.TProtocol prot) throws org.apache.thrift.TException {
        prot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("serviceTypeRequest", org.apache.thrift.protocol.TMessageType.CALL, 0));
        serviceTypeRequest_args args = new serviceTypeRequest_args();
        args.setService_id(service_id);
        args.setSession_id(session_id);
        args.setMsisdn(msisdn);
        args.setImsi(imsi);
        args.setExtension(extension);
        args.write(prot);
        prot.writeMessageEnd();
      }

      public void getResult() throws org.apache.thrift.TException {
        if (getState() != org.apache.thrift.async.TAsyncMethodCall.State.RESPONSE_READ) {
          throw new IllegalStateException("Method call not finished!");
        }
        org.apache.thrift.transport.TMemoryInputTransport memoryTransport = new org.apache.thrift.transport.TMemoryInputTransport(getFrameBuffer().array());
        org.apache.thrift.protocol.TProtocol prot = client.getProtocolFactory().getProtocol(memoryTransport);
      }
    }

    public void subscriberInfoResp(int service_id, long session_id, String msisdn, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<subscriberInfoResp_call> resultHandler) throws org.apache.thrift.TException {
      checkReady();
      subscriberInfoResp_call method_call = new subscriberInfoResp_call(service_id, session_id, msisdn, extension, resultHandler, this, protocolFactory, transport);
      this.currentMethod = method_call;
      manager.call(method_call);
    }

    public static class subscriberInfoResp_call extends org.apache.thrift.async.TAsyncMethodCall {
      private int service_id;
      private long session_id;
      private String msisdn;
      private Map<String,String> extension;
      public subscriberInfoResp_call(int service_id, long session_id, String msisdn, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<subscriberInfoResp_call> resultHandler, org.apache.thrift.async.TAsyncClient client, org.apache.thrift.protocol.TProtocolFactory protocolFactory, org.apache.thrift.transport.TNonblockingTransport transport) throws org.apache.thrift.TException {
        super(client, protocolFactory, transport, resultHandler, true);
        this.service_id = service_id;
        this.session_id = session_id;
        this.msisdn = msisdn;
        this.extension = extension;
      }

      public void write_args(org.apache.thrift.protocol.TProtocol prot) throws org.apache.thrift.TException {
        prot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("subscriberInfoResp", org.apache.thrift.protocol.TMessageType.CALL, 0));
        subscriberInfoResp_args args = new subscriberInfoResp_args();
        args.setService_id(service_id);
        args.setSession_id(session_id);
        args.setMsisdn(msisdn);
        args.setExtension(extension);
        args.write(prot);
        prot.writeMessageEnd();
      }

      public void getResult() throws org.apache.thrift.TException {
        if (getState() != org.apache.thrift.async.TAsyncMethodCall.State.RESPONSE_READ) {
          throw new IllegalStateException("Method call not finished!");
        }
        org.apache.thrift.transport.TMemoryInputTransport memoryTransport = new org.apache.thrift.transport.TMemoryInputTransport(getFrameBuffer().array());
        org.apache.thrift.protocol.TProtocol prot = client.getProtocolFactory().getProtocol(memoryTransport);
      }
    }

    public void respSriSm(int service_id, long session_id, String imsi, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<respSriSm_call> resultHandler) throws org.apache.thrift.TException {
      checkReady();
      respSriSm_call method_call = new respSriSm_call(service_id, session_id, imsi, extension, resultHandler, this, protocolFactory, transport);
      this.currentMethod = method_call;
      manager.call(method_call);
    }

    public static class respSriSm_call extends org.apache.thrift.async.TAsyncMethodCall {
      private int service_id;
      private long session_id;
      private String imsi;
      private Map<String,String> extension;
      public respSriSm_call(int service_id, long session_id, String imsi, Map<String,String> extension, org.apache.thrift.async.AsyncMethodCallback<respSriSm_call> resultHandler, org.apache.thrift.async.TAsyncClient client, org.apache.thrift.protocol.TProtocolFactory protocolFactory, org.apache.thrift.transport.TNonblockingTransport transport) throws org.apache.thrift.TException {
        super(client, protocolFactory, transport, resultHandler, true);
        this.service_id = service_id;
        this.session_id = session_id;
        this.imsi = imsi;
        this.extension = extension;
      }

      public void write_args(org.apache.thrift.protocol.TProtocol prot) throws org.apache.thrift.TException {
        prot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("respSriSm", org.apache.thrift.protocol.TMessageType.CALL, 0));
        respSriSm_args args = new respSriSm_args();
        args.setService_id(service_id);
        args.setSession_id(session_id);
        args.setImsi(imsi);
        args.setExtension(extension);
        args.write(prot);
        prot.writeMessageEnd();
      }

      public void getResult() throws org.apache.thrift.TException {
        if (getState() != org.apache.thrift.async.TAsyncMethodCall.State.RESPONSE_READ) {
          throw new IllegalStateException("Method call not finished!");
        }
        org.apache.thrift.transport.TMemoryInputTransport memoryTransport = new org.apache.thrift.transport.TMemoryInputTransport(getFrameBuffer().array());
        org.apache.thrift.protocol.TProtocol prot = client.getProtocolFactory().getProtocol(memoryTransport);
      }
    }

  }

  public static class Processor implements org.apache.thrift.TProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(Processor.class.getName());
    public Processor(Iface iface)
    {
      iface_ = iface;
      processMap_.put("reqMissedCallDetails", new reqMissedCallDetails());
      processMap_.put("respUpdateVLRDetails", new respUpdateVLRDetails());
      processMap_.put("reqvMSRN", new reqvMSRN());
      processMap_.put("reqInitiateUSSDSession", new reqInitiateUSSDSession());
      processMap_.put("respUSSD", new respUSSD());
      processMap_.put("reqIncommingSmsData", new reqIncommingSmsData());
      processMap_.put("respOutgoingSmsData", new respOutgoingSmsData());
      processMap_.put("registerNodeRequest", new registerNodeRequest());
      processMap_.put("registerNodeResponse", new registerNodeResponse());
      processMap_.put("serviceTypeRequest", new serviceTypeRequest());
      processMap_.put("subscriberInfoResp", new subscriberInfoResp());
      processMap_.put("respSriSm", new respSriSm());
    }

    protected static interface ProcessFunction {
      public void process(int seqid, org.apache.thrift.protocol.TProtocol iprot, org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException;
    }

    private Iface iface_;
    protected final HashMap<String,ProcessFunction> processMap_ = new HashMap<String,ProcessFunction>();

    public boolean process(org.apache.thrift.protocol.TProtocol iprot, org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException
    {
      org.apache.thrift.protocol.TMessage msg = iprot.readMessageBegin();
      ProcessFunction fn = processMap_.get(msg.name);
      if (fn == null) {
        org.apache.thrift.protocol.TProtocolUtil.skip(iprot, org.apache.thrift.protocol.TType.STRUCT);
        iprot.readMessageEnd();
        org.apache.thrift.TApplicationException x = new org.apache.thrift.TApplicationException(org.apache.thrift.TApplicationException.UNKNOWN_METHOD, "Invalid method name: '"+msg.name+"'");
        oprot.writeMessageBegin(new org.apache.thrift.protocol.TMessage(msg.name, org.apache.thrift.protocol.TMessageType.EXCEPTION, msg.seqid));
        x.write(oprot);
        oprot.writeMessageEnd();
        oprot.getTransport().flush();
        return true;
      }
      fn.process(msg.seqid, iprot, oprot);
      return true;
    }

    private class reqMissedCallDetails implements ProcessFunction {
      public void process(int seqid, org.apache.thrift.protocol.TProtocol iprot, org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException
      {
        reqMissedCallDetails_args args = new reqMissedCallDetails_args();
        try {
          args.read(iprot);
        } catch (org.apache.thrift.protocol.TProtocolException e) {
          iprot.readMessageEnd();
          org.apache.thrift.TApplicationException x = new org.apache.thrift.TApplicationException(org.apache.thrift.TApplicationException.PROTOCOL_ERROR, e.getMessage());
          oprot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("reqMissedCallDetails", org.apache.thrift.protocol.TMessageType.EXCEPTION, seqid));
          x.write(oprot);
          oprot.writeMessageEnd();
          oprot.getTransport().flush();
          return;
        }
        iprot.readMessageEnd();
        iface_.reqMissedCallDetails(args.service_id, args.session_id, args.msisdn, args.extension);
        return;
      }
    }

    private class respUpdateVLRDetails implements ProcessFunction {
      public void process(int seqid, org.apache.thrift.protocol.TProtocol iprot, org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException
      {
        respUpdateVLRDetails_args args = new respUpdateVLRDetails_args();
        try {
          args.read(iprot);
        } catch (org.apache.thrift.protocol.TProtocolException e) {
          iprot.readMessageEnd();
          org.apache.thrift.TApplicationException x = new org.apache.thrift.TApplicationException(org.apache.thrift.TApplicationException.PROTOCOL_ERROR, e.getMessage());
          oprot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("respUpdateVLRDetails", org.apache.thrift.protocol.TMessageType.EXCEPTION, seqid));
          x.write(oprot);
          oprot.writeMessageEnd();
          oprot.getTransport().flush();
          return;
        }
        iprot.readMessageEnd();
        iface_.respUpdateVLRDetails(args.service_id, args.session_id, args.hlr_number, args.error, args.extension);
        return;
      }
    }

    private class reqvMSRN implements ProcessFunction {
      public void process(int seqid, org.apache.thrift.protocol.TProtocol iprot, org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException
      {
        reqvMSRN_args args = new reqvMSRN_args();
        try {
          args.read(iprot);
        } catch (org.apache.thrift.protocol.TProtocolException e) {
          iprot.readMessageEnd();
          org.apache.thrift.TApplicationException x = new org.apache.thrift.TApplicationException(org.apache.thrift.TApplicationException.PROTOCOL_ERROR, e.getMessage());
          oprot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("reqvMSRN", org.apache.thrift.protocol.TMessageType.EXCEPTION, seqid));
          x.write(oprot);
          oprot.writeMessageEnd();
          oprot.getTransport().flush();
          return;
        }
        iprot.readMessageEnd();
        iface_.reqvMSRN(args.service_id, args.session_id, args.imsi, args.extension);
        return;
      }
    }

    private class reqInitiateUSSDSession implements ProcessFunction {
      public void process(int seqid, org.apache.thrift.protocol.TProtocol iprot, org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException
      {
        reqInitiateUSSDSession_args args = new reqInitiateUSSDSession_args();
        try {
          args.read(iprot);
        } catch (org.apache.thrift.protocol.TProtocolException e) {
          iprot.readMessageEnd();
          org.apache.thrift.TApplicationException x = new org.apache.thrift.TApplicationException(org.apache.thrift.TApplicationException.PROTOCOL_ERROR, e.getMessage());
          oprot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("reqInitiateUSSDSession", org.apache.thrift.protocol.TMessageType.EXCEPTION, seqid));
          x.write(oprot);
          oprot.writeMessageEnd();
          oprot.getTransport().flush();
          return;
        }
        iprot.readMessageEnd();
        iface_.reqInitiateUSSDSession(args.service_id, args.session_id, args.coding_scheme, args.data_string, args.msisdn, args.extension);
        return;
      }
    }

    private class respUSSD implements ProcessFunction {
      public void process(int seqid, org.apache.thrift.protocol.TProtocol iprot, org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException
      {
        respUSSD_args args = new respUSSD_args();
        try {
          args.read(iprot);
        } catch (org.apache.thrift.protocol.TProtocolException e) {
          iprot.readMessageEnd();
          org.apache.thrift.TApplicationException x = new org.apache.thrift.TApplicationException(org.apache.thrift.TApplicationException.PROTOCOL_ERROR, e.getMessage());
          oprot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("respUSSD", org.apache.thrift.protocol.TMessageType.EXCEPTION, seqid));
          x.write(oprot);
          oprot.writeMessageEnd();
          oprot.getTransport().flush();
          return;
        }
        iprot.readMessageEnd();
        iface_.respUSSD(args.service_id, args.session_id, args.coding_scheme, args.data_string, args.msisdn, args.extension);
        return;
      }
    }

    private class reqIncommingSmsData implements ProcessFunction {
      public void process(int seqid, org.apache.thrift.protocol.TProtocol iprot, org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException
      {
        reqIncommingSmsData_args args = new reqIncommingSmsData_args();
        try {
          args.read(iprot);
        } catch (org.apache.thrift.protocol.TProtocolException e) {
          iprot.readMessageEnd();
          org.apache.thrift.TApplicationException x = new org.apache.thrift.TApplicationException(org.apache.thrift.TApplicationException.PROTOCOL_ERROR, e.getMessage());
          oprot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("reqIncommingSmsData", org.apache.thrift.protocol.TMessageType.EXCEPTION, seqid));
          x.write(oprot);
          oprot.writeMessageEnd();
          oprot.getTransport().flush();
          return;
        }
        iprot.readMessageEnd();
        iface_.reqIncommingSmsData(args.service_id, args.session_id, args.imsi, args.coding_scheme, args.data_string, args.orig_party_addr, args.timestamp, args.extension);
        return;
      }
    }

    private class respOutgoingSmsData implements ProcessFunction {
      public void process(int seqid, org.apache.thrift.protocol.TProtocol iprot, org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException
      {
        respOutgoingSmsData_args args = new respOutgoingSmsData_args();
        try {
          args.read(iprot);
        } catch (org.apache.thrift.protocol.TProtocolException e) {
          iprot.readMessageEnd();
          org.apache.thrift.TApplicationException x = new org.apache.thrift.TApplicationException(org.apache.thrift.TApplicationException.PROTOCOL_ERROR, e.getMessage());
          oprot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("respOutgoingSmsData", org.apache.thrift.protocol.TMessageType.EXCEPTION, seqid));
          x.write(oprot);
          oprot.writeMessageEnd();
          oprot.getTransport().flush();
          return;
        }
        iprot.readMessageEnd();
        iface_.respOutgoingSmsData(args.service_id, args.session_id, args.error, args.extension);
        return;
      }
    }

    private class registerNodeRequest implements ProcessFunction {
      public void process(int seqid, org.apache.thrift.protocol.TProtocol iprot, org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException
      {
        registerNodeRequest_args args = new registerNodeRequest_args();
        try {
          args.read(iprot);
        } catch (org.apache.thrift.protocol.TProtocolException e) {
          iprot.readMessageEnd();
          org.apache.thrift.TApplicationException x = new org.apache.thrift.TApplicationException(org.apache.thrift.TApplicationException.PROTOCOL_ERROR, e.getMessage());
          oprot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("registerNodeRequest", org.apache.thrift.protocol.TMessageType.EXCEPTION, seqid));
          x.write(oprot);
          oprot.writeMessageEnd();
          oprot.getTransport().flush();
          return;
        }
        iprot.readMessageEnd();
        iface_.registerNodeRequest(args.extension);
        return;
      }
    }

    private class registerNodeResponse implements ProcessFunction {
      public void process(int seqid, org.apache.thrift.protocol.TProtocol iprot, org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException
      {
        registerNodeResponse_args args = new registerNodeResponse_args();
        try {
          args.read(iprot);
        } catch (org.apache.thrift.protocol.TProtocolException e) {
          iprot.readMessageEnd();
          org.apache.thrift.TApplicationException x = new org.apache.thrift.TApplicationException(org.apache.thrift.TApplicationException.PROTOCOL_ERROR, e.getMessage());
          oprot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("registerNodeResponse", org.apache.thrift.protocol.TMessageType.EXCEPTION, seqid));
          x.write(oprot);
          oprot.writeMessageEnd();
          oprot.getTransport().flush();
          return;
        }
        iprot.readMessageEnd();
        iface_.registerNodeResponse(args.extension);
        return;
      }
    }

    private class serviceTypeRequest implements ProcessFunction {
      public void process(int seqid, org.apache.thrift.protocol.TProtocol iprot, org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException
      {
        serviceTypeRequest_args args = new serviceTypeRequest_args();
        try {
          args.read(iprot);
        } catch (org.apache.thrift.protocol.TProtocolException e) {
          iprot.readMessageEnd();
          org.apache.thrift.TApplicationException x = new org.apache.thrift.TApplicationException(org.apache.thrift.TApplicationException.PROTOCOL_ERROR, e.getMessage());
          oprot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("serviceTypeRequest", org.apache.thrift.protocol.TMessageType.EXCEPTION, seqid));
          x.write(oprot);
          oprot.writeMessageEnd();
          oprot.getTransport().flush();
          return;
        }
        iprot.readMessageEnd();
        iface_.serviceTypeRequest(args.service_id, args.session_id, args.msisdn, args.imsi, args.extension);
        return;
      }
    }

    private class subscriberInfoResp implements ProcessFunction {
      public void process(int seqid, org.apache.thrift.protocol.TProtocol iprot, org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException
      {
        subscriberInfoResp_args args = new subscriberInfoResp_args();
        try {
          args.read(iprot);
        } catch (org.apache.thrift.protocol.TProtocolException e) {
          iprot.readMessageEnd();
          org.apache.thrift.TApplicationException x = new org.apache.thrift.TApplicationException(org.apache.thrift.TApplicationException.PROTOCOL_ERROR, e.getMessage());
          oprot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("subscriberInfoResp", org.apache.thrift.protocol.TMessageType.EXCEPTION, seqid));
          x.write(oprot);
          oprot.writeMessageEnd();
          oprot.getTransport().flush();
          return;
        }
        iprot.readMessageEnd();
        iface_.subscriberInfoResp(args.service_id, args.session_id, args.msisdn, args.extension);
        return;
      }
    }

    private class respSriSm implements ProcessFunction {
      public void process(int seqid, org.apache.thrift.protocol.TProtocol iprot, org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException
      {
        respSriSm_args args = new respSriSm_args();
        try {
          args.read(iprot);
        } catch (org.apache.thrift.protocol.TProtocolException e) {
          iprot.readMessageEnd();
          org.apache.thrift.TApplicationException x = new org.apache.thrift.TApplicationException(org.apache.thrift.TApplicationException.PROTOCOL_ERROR, e.getMessage());
          oprot.writeMessageBegin(new org.apache.thrift.protocol.TMessage("respSriSm", org.apache.thrift.protocol.TMessageType.EXCEPTION, seqid));
          x.write(oprot);
          oprot.writeMessageEnd();
          oprot.getTransport().flush();
          return;
        }
        iprot.readMessageEnd();
        iface_.respSriSm(args.service_id, args.session_id, args.imsi, args.extension);
        return;
      }
    }

  }

  public static class reqMissedCallDetails_args implements org.apache.thrift.TBase<reqMissedCallDetails_args, reqMissedCallDetails_args._Fields>, java.io.Serializable, Cloneable   {
    private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("reqMissedCallDetails_args");

    private static final org.apache.thrift.protocol.TField SERVICE_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("service_id", org.apache.thrift.protocol.TType.I32, (short)1);
    private static final org.apache.thrift.protocol.TField SESSION_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("session_id", org.apache.thrift.protocol.TType.I64, (short)2);
    private static final org.apache.thrift.protocol.TField MSISDN_FIELD_DESC = new org.apache.thrift.protocol.TField("msisdn", org.apache.thrift.protocol.TType.STRING, (short)3);
    private static final org.apache.thrift.protocol.TField EXTENSION_FIELD_DESC = new org.apache.thrift.protocol.TField("extension", org.apache.thrift.protocol.TType.MAP, (short)4);

    public int service_id;
    public long session_id;
    public String msisdn;
    public Map<String,String> extension;

    /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
    public enum _Fields implements org.apache.thrift.TFieldIdEnum {
      SERVICE_ID((short)1, "service_id"),
      SESSION_ID((short)2, "session_id"),
      MSISDN((short)3, "msisdn"),
      EXTENSION((short)4, "extension");

      private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

      static {
        for (_Fields field : EnumSet.allOf(_Fields.class)) {
          byName.put(field.getFieldName(), field);
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, or null if its not found.
       */
      public static _Fields findByThriftId(int fieldId) {
        switch(fieldId) {
          case 1: // SERVICE_ID
            return SERVICE_ID;
          case 2: // SESSION_ID
            return SESSION_ID;
          case 3: // MSISDN
            return MSISDN;
          case 4: // EXTENSION
            return EXTENSION;
          default:
            return null;
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, throwing an exception
       * if it is not found.
       */
      public static _Fields findByThriftIdOrThrow(int fieldId) {
        _Fields fields = findByThriftId(fieldId);
        if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
        return fields;
      }

      /**
       * Find the _Fields constant that matches name, or null if its not found.
       */
      public static _Fields findByName(String name) {
        return byName.get(name);
      }

      private final short _thriftId;
      private final String _fieldName;

      _Fields(short thriftId, String fieldName) {
        _thriftId = thriftId;
        _fieldName = fieldName;
      }

      public short getThriftFieldId() {
        return _thriftId;
      }

      public String getFieldName() {
        return _fieldName;
      }
    }

    // isset id assignments
    private static final int __SERVICE_ID_ISSET_ID = 0;
    private static final int __SESSION_ID_ISSET_ID = 1;
    private BitSet __isset_bit_vector = new BitSet(2);

    public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
    static {
      Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
      tmpMap.put(_Fields.SERVICE_ID, new org.apache.thrift.meta_data.FieldMetaData("service_id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
      tmpMap.put(_Fields.SESSION_ID, new org.apache.thrift.meta_data.FieldMetaData("session_id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
      tmpMap.put(_Fields.MSISDN, new org.apache.thrift.meta_data.FieldMetaData("msisdn", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
      tmpMap.put(_Fields.EXTENSION, new org.apache.thrift.meta_data.FieldMetaData("extension", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.MapMetaData(org.apache.thrift.protocol.TType.MAP, 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING), 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING))));
      metaDataMap = Collections.unmodifiableMap(tmpMap);
      org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(reqMissedCallDetails_args.class, metaDataMap);
    }

    public reqMissedCallDetails_args() {
    }

    public reqMissedCallDetails_args(
      int service_id,
      long session_id,
      String msisdn,
      Map<String,String> extension)
    {
      this();
      this.service_id = service_id;
      setService_idIsSet(true);
      this.session_id = session_id;
      setSession_idIsSet(true);
      this.msisdn = msisdn;
      this.extension = extension;
    }

    /**
     * Performs a deep copy on <i>other</i>.
     */
    public reqMissedCallDetails_args(reqMissedCallDetails_args other) {
      __isset_bit_vector.clear();
      __isset_bit_vector.or(other.__isset_bit_vector);
      this.service_id = other.service_id;
      this.session_id = other.session_id;
      if (other.isSetMsisdn()) {
        this.msisdn = other.msisdn;
      }
      if (other.isSetExtension()) {
        Map<String,String> __this__extension = new HashMap<String,String>();
        for (Map.Entry<String, String> other_element : other.extension.entrySet()) {

          String other_element_key = other_element.getKey();
          String other_element_value = other_element.getValue();

          String __this__extension_copy_key = other_element_key;

          String __this__extension_copy_value = other_element_value;

          __this__extension.put(__this__extension_copy_key, __this__extension_copy_value);
        }
        this.extension = __this__extension;
      }
    }

    public reqMissedCallDetails_args deepCopy() {
      return new reqMissedCallDetails_args(this);
    }

    @Override
    public void clear() {
      setService_idIsSet(false);
      this.service_id = 0;
      setSession_idIsSet(false);
      this.session_id = 0;
      this.msisdn = null;
      this.extension = null;
    }

    public int getService_id() {
      return this.service_id;
    }

    public reqMissedCallDetails_args setService_id(int service_id) {
      this.service_id = service_id;
      setService_idIsSet(true);
      return this;
    }

    public void unsetService_id() {
      __isset_bit_vector.clear(__SERVICE_ID_ISSET_ID);
    }

    /** Returns true if field service_id is set (has been assigned a value) and false otherwise */
    public boolean isSetService_id() {
      return __isset_bit_vector.get(__SERVICE_ID_ISSET_ID);
    }

    public void setService_idIsSet(boolean value) {
      __isset_bit_vector.set(__SERVICE_ID_ISSET_ID, value);
    }

    public long getSession_id() {
      return this.session_id;
    }

    public reqMissedCallDetails_args setSession_id(long session_id) {
      this.session_id = session_id;
      setSession_idIsSet(true);
      return this;
    }

    public void unsetSession_id() {
      __isset_bit_vector.clear(__SESSION_ID_ISSET_ID);
    }

    /** Returns true if field session_id is set (has been assigned a value) and false otherwise */
    public boolean isSetSession_id() {
      return __isset_bit_vector.get(__SESSION_ID_ISSET_ID);
    }

    public void setSession_idIsSet(boolean value) {
      __isset_bit_vector.set(__SESSION_ID_ISSET_ID, value);
    }

    public String getMsisdn() {
      return this.msisdn;
    }

    public reqMissedCallDetails_args setMsisdn(String msisdn) {
      this.msisdn = msisdn;
      return this;
    }

    public void unsetMsisdn() {
      this.msisdn = null;
    }

    /** Returns true if field msisdn is set (has been assigned a value) and false otherwise */
    public boolean isSetMsisdn() {
      return this.msisdn != null;
    }

    public void setMsisdnIsSet(boolean value) {
      if (!value) {
        this.msisdn = null;
      }
    }

    public int getExtensionSize() {
      return (this.extension == null) ? 0 : this.extension.size();
    }

    public void putToExtension(String key, String val) {
      if (this.extension == null) {
        this.extension = new HashMap<String,String>();
      }
      this.extension.put(key, val);
    }

    public Map<String,String> getExtension() {
      return this.extension;
    }

    public reqMissedCallDetails_args setExtension(Map<String,String> extension) {
      this.extension = extension;
      return this;
    }

    public void unsetExtension() {
      this.extension = null;
    }

    /** Returns true if field extension is set (has been assigned a value) and false otherwise */
    public boolean isSetExtension() {
      return this.extension != null;
    }

    public void setExtensionIsSet(boolean value) {
      if (!value) {
        this.extension = null;
      }
    }

    public void setFieldValue(_Fields field, Object value) {
      switch (field) {
      case SERVICE_ID:
        if (value == null) {
          unsetService_id();
        } else {
          setService_id((Integer)value);
        }
        break;

      case SESSION_ID:
        if (value == null) {
          unsetSession_id();
        } else {
          setSession_id((Long)value);
        }
        break;

      case MSISDN:
        if (value == null) {
          unsetMsisdn();
        } else {
          setMsisdn((String)value);
        }
        break;

      case EXTENSION:
        if (value == null) {
          unsetExtension();
        } else {
          setExtension((Map<String,String>)value);
        }
        break;

      }
    }

    public Object getFieldValue(_Fields field) {
      switch (field) {
      case SERVICE_ID:
        return new Integer(getService_id());

      case SESSION_ID:
        return new Long(getSession_id());

      case MSISDN:
        return getMsisdn();

      case EXTENSION:
        return getExtension();

      }
      throw new IllegalStateException();
    }

    /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
    public boolean isSet(_Fields field) {
      if (field == null) {
        throw new IllegalArgumentException();
      }

      switch (field) {
      case SERVICE_ID:
        return isSetService_id();
      case SESSION_ID:
        return isSetSession_id();
      case MSISDN:
        return isSetMsisdn();
      case EXTENSION:
        return isSetExtension();
      }
      throw new IllegalStateException();
    }

    @Override
    public boolean equals(Object that) {
      if (that == null)
        return false;
      if (that instanceof reqMissedCallDetails_args)
        return this.equals((reqMissedCallDetails_args)that);
      return false;
    }

    public boolean equals(reqMissedCallDetails_args that) {
      if (that == null)
        return false;

      boolean this_present_service_id = true;
      boolean that_present_service_id = true;
      if (this_present_service_id || that_present_service_id) {
        if (!(this_present_service_id && that_present_service_id))
          return false;
        if (this.service_id != that.service_id)
          return false;
      }

      boolean this_present_session_id = true;
      boolean that_present_session_id = true;
      if (this_present_session_id || that_present_session_id) {
        if (!(this_present_session_id && that_present_session_id))
          return false;
        if (this.session_id != that.session_id)
          return false;
      }

      boolean this_present_msisdn = true && this.isSetMsisdn();
      boolean that_present_msisdn = true && that.isSetMsisdn();
      if (this_present_msisdn || that_present_msisdn) {
        if (!(this_present_msisdn && that_present_msisdn))
          return false;
        if (!this.msisdn.equals(that.msisdn))
          return false;
      }

      boolean this_present_extension = true && this.isSetExtension();
      boolean that_present_extension = true && that.isSetExtension();
      if (this_present_extension || that_present_extension) {
        if (!(this_present_extension && that_present_extension))
          return false;
        if (!this.extension.equals(that.extension))
          return false;
      }

      return true;
    }

    @Override
    public int hashCode() {
      return 0;
    }

    public int compareTo(reqMissedCallDetails_args other) {
      if (!getClass().equals(other.getClass())) {
        return getClass().getName().compareTo(other.getClass().getName());
      }

      int lastComparison = 0;
      reqMissedCallDetails_args typedOther = (reqMissedCallDetails_args)other;

      lastComparison = Boolean.valueOf(isSetService_id()).compareTo(typedOther.isSetService_id());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetService_id()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.service_id, typedOther.service_id);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetSession_id()).compareTo(typedOther.isSetSession_id());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetSession_id()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.session_id, typedOther.session_id);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetMsisdn()).compareTo(typedOther.isSetMsisdn());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetMsisdn()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.msisdn, typedOther.msisdn);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetExtension()).compareTo(typedOther.isSetExtension());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetExtension()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.extension, typedOther.extension);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      return 0;
    }

    public _Fields fieldForId(int fieldId) {
      return _Fields.findByThriftId(fieldId);
    }

    public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField field;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (field.id) {
          case 1: // SERVICE_ID
            if (field.type == org.apache.thrift.protocol.TType.I32) {
              this.service_id = iprot.readI32();
              setService_idIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 2: // SESSION_ID
            if (field.type == org.apache.thrift.protocol.TType.I64) {
              this.session_id = iprot.readI64();
              setSession_idIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 3: // MSISDN
            if (field.type == org.apache.thrift.protocol.TType.STRING) {
              this.msisdn = iprot.readString();
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 4: // EXTENSION
            if (field.type == org.apache.thrift.protocol.TType.MAP) {
              {
                org.apache.thrift.protocol.TMap _map0 = iprot.readMapBegin();
                this.extension = new HashMap<String,String>(2*_map0.size);
                for (int _i1 = 0; _i1 < _map0.size; ++_i1)
                {
                  String _key2;
                  String _val3;
                  _key2 = iprot.readString();
                  _val3 = iprot.readString();
                  this.extension.put(_key2, _val3);
                }
                iprot.readMapEnd();
              }
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(SERVICE_ID_FIELD_DESC);
      oprot.writeI32(this.service_id);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(SESSION_ID_FIELD_DESC);
      oprot.writeI64(this.session_id);
      oprot.writeFieldEnd();
      if (this.msisdn != null) {
        oprot.writeFieldBegin(MSISDN_FIELD_DESC);
        oprot.writeString(this.msisdn);
        oprot.writeFieldEnd();
      }
      if (this.extension != null) {
        oprot.writeFieldBegin(EXTENSION_FIELD_DESC);
        {
          oprot.writeMapBegin(new org.apache.thrift.protocol.TMap(org.apache.thrift.protocol.TType.STRING, org.apache.thrift.protocol.TType.STRING, this.extension.size()));
          for (Map.Entry<String, String> _iter4 : this.extension.entrySet())
          {
            oprot.writeString(_iter4.getKey());
            oprot.writeString(_iter4.getValue());
          }
          oprot.writeMapEnd();
        }
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder("reqMissedCallDetails_args(");
      boolean first = true;

      sb.append("service_id:");
      sb.append(this.service_id);
      first = false;
      if (!first) sb.append(", ");
      sb.append("session_id:");
      sb.append(this.session_id);
      first = false;
      if (!first) sb.append(", ");
      sb.append("msisdn:");
      if (this.msisdn == null) {
        sb.append("null");
      } else {
        sb.append(this.msisdn);
      }
      first = false;
      if (!first) sb.append(", ");
      sb.append("extension:");
      if (this.extension == null) {
        sb.append("null");
      } else {
        sb.append(this.extension);
      }
      first = false;
      sb.append(")");
      return sb.toString();
    }

    public void validate() throws org.apache.thrift.TException {
      // check for required fields
    }

    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
      try {
        write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
      try {
        // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
        __isset_bit_vector = new BitSet(1);
        read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

  }

  public static class respUpdateVLRDetails_args implements org.apache.thrift.TBase<respUpdateVLRDetails_args, respUpdateVLRDetails_args._Fields>, java.io.Serializable, Cloneable   {
    private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("respUpdateVLRDetails_args");

    private static final org.apache.thrift.protocol.TField SERVICE_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("service_id", org.apache.thrift.protocol.TType.I32, (short)1);
    private static final org.apache.thrift.protocol.TField SESSION_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("session_id", org.apache.thrift.protocol.TType.I64, (short)2);
    private static final org.apache.thrift.protocol.TField HLR_NUMBER_FIELD_DESC = new org.apache.thrift.protocol.TField("hlr_number", org.apache.thrift.protocol.TType.STRING, (short)3);
    private static final org.apache.thrift.protocol.TField ERROR_FIELD_DESC = new org.apache.thrift.protocol.TField("error", org.apache.thrift.protocol.TType.I16, (short)4);
    private static final org.apache.thrift.protocol.TField EXTENSION_FIELD_DESC = new org.apache.thrift.protocol.TField("extension", org.apache.thrift.protocol.TType.MAP, (short)5);

    public int service_id;
    public long session_id;
    public String hlr_number;
    public short error;
    public Map<String,String> extension;

    /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
    public enum _Fields implements org.apache.thrift.TFieldIdEnum {
      SERVICE_ID((short)1, "service_id"),
      SESSION_ID((short)2, "session_id"),
      HLR_NUMBER((short)3, "hlr_number"),
      ERROR((short)4, "error"),
      EXTENSION((short)5, "extension");

      private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

      static {
        for (_Fields field : EnumSet.allOf(_Fields.class)) {
          byName.put(field.getFieldName(), field);
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, or null if its not found.
       */
      public static _Fields findByThriftId(int fieldId) {
        switch(fieldId) {
          case 1: // SERVICE_ID
            return SERVICE_ID;
          case 2: // SESSION_ID
            return SESSION_ID;
          case 3: // HLR_NUMBER
            return HLR_NUMBER;
          case 4: // ERROR
            return ERROR;
          case 5: // EXTENSION
            return EXTENSION;
          default:
            return null;
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, throwing an exception
       * if it is not found.
       */
      public static _Fields findByThriftIdOrThrow(int fieldId) {
        _Fields fields = findByThriftId(fieldId);
        if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
        return fields;
      }

      /**
       * Find the _Fields constant that matches name, or null if its not found.
       */
      public static _Fields findByName(String name) {
        return byName.get(name);
      }

      private final short _thriftId;
      private final String _fieldName;

      _Fields(short thriftId, String fieldName) {
        _thriftId = thriftId;
        _fieldName = fieldName;
      }

      public short getThriftFieldId() {
        return _thriftId;
      }

      public String getFieldName() {
        return _fieldName;
      }
    }

    // isset id assignments
    private static final int __SERVICE_ID_ISSET_ID = 0;
    private static final int __SESSION_ID_ISSET_ID = 1;
    private static final int __ERROR_ISSET_ID = 2;
    private BitSet __isset_bit_vector = new BitSet(3);

    public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
    static {
      Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
      tmpMap.put(_Fields.SERVICE_ID, new org.apache.thrift.meta_data.FieldMetaData("service_id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
      tmpMap.put(_Fields.SESSION_ID, new org.apache.thrift.meta_data.FieldMetaData("session_id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
      tmpMap.put(_Fields.HLR_NUMBER, new org.apache.thrift.meta_data.FieldMetaData("hlr_number", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
      tmpMap.put(_Fields.ERROR, new org.apache.thrift.meta_data.FieldMetaData("error", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I16)));
      tmpMap.put(_Fields.EXTENSION, new org.apache.thrift.meta_data.FieldMetaData("extension", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.MapMetaData(org.apache.thrift.protocol.TType.MAP, 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING), 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING))));
      metaDataMap = Collections.unmodifiableMap(tmpMap);
      org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(respUpdateVLRDetails_args.class, metaDataMap);
    }

    public respUpdateVLRDetails_args() {
    }

    public respUpdateVLRDetails_args(
      int service_id,
      long session_id,
      String hlr_number,
      short error,
      Map<String,String> extension)
    {
      this();
      this.service_id = service_id;
      setService_idIsSet(true);
      this.session_id = session_id;
      setSession_idIsSet(true);
      this.hlr_number = hlr_number;
      this.error = error;
      setErrorIsSet(true);
      this.extension = extension;
    }

    /**
     * Performs a deep copy on <i>other</i>.
     */
    public respUpdateVLRDetails_args(respUpdateVLRDetails_args other) {
      __isset_bit_vector.clear();
      __isset_bit_vector.or(other.__isset_bit_vector);
      this.service_id = other.service_id;
      this.session_id = other.session_id;
      if (other.isSetHlr_number()) {
        this.hlr_number = other.hlr_number;
      }
      this.error = other.error;
      if (other.isSetExtension()) {
        Map<String,String> __this__extension = new HashMap<String,String>();
        for (Map.Entry<String, String> other_element : other.extension.entrySet()) {

          String other_element_key = other_element.getKey();
          String other_element_value = other_element.getValue();

          String __this__extension_copy_key = other_element_key;

          String __this__extension_copy_value = other_element_value;

          __this__extension.put(__this__extension_copy_key, __this__extension_copy_value);
        }
        this.extension = __this__extension;
      }
    }

    public respUpdateVLRDetails_args deepCopy() {
      return new respUpdateVLRDetails_args(this);
    }

    @Override
    public void clear() {
      setService_idIsSet(false);
      this.service_id = 0;
      setSession_idIsSet(false);
      this.session_id = 0;
      this.hlr_number = null;
      setErrorIsSet(false);
      this.error = 0;
      this.extension = null;
    }

    public int getService_id() {
      return this.service_id;
    }

    public respUpdateVLRDetails_args setService_id(int service_id) {
      this.service_id = service_id;
      setService_idIsSet(true);
      return this;
    }

    public void unsetService_id() {
      __isset_bit_vector.clear(__SERVICE_ID_ISSET_ID);
    }

    /** Returns true if field service_id is set (has been assigned a value) and false otherwise */
    public boolean isSetService_id() {
      return __isset_bit_vector.get(__SERVICE_ID_ISSET_ID);
    }

    public void setService_idIsSet(boolean value) {
      __isset_bit_vector.set(__SERVICE_ID_ISSET_ID, value);
    }

    public long getSession_id() {
      return this.session_id;
    }

    public respUpdateVLRDetails_args setSession_id(long session_id) {
      this.session_id = session_id;
      setSession_idIsSet(true);
      return this;
    }

    public void unsetSession_id() {
      __isset_bit_vector.clear(__SESSION_ID_ISSET_ID);
    }

    /** Returns true if field session_id is set (has been assigned a value) and false otherwise */
    public boolean isSetSession_id() {
      return __isset_bit_vector.get(__SESSION_ID_ISSET_ID);
    }

    public void setSession_idIsSet(boolean value) {
      __isset_bit_vector.set(__SESSION_ID_ISSET_ID, value);
    }

    public String getHlr_number() {
      return this.hlr_number;
    }

    public respUpdateVLRDetails_args setHlr_number(String hlr_number) {
      this.hlr_number = hlr_number;
      return this;
    }

    public void unsetHlr_number() {
      this.hlr_number = null;
    }

    /** Returns true if field hlr_number is set (has been assigned a value) and false otherwise */
    public boolean isSetHlr_number() {
      return this.hlr_number != null;
    }

    public void setHlr_numberIsSet(boolean value) {
      if (!value) {
        this.hlr_number = null;
      }
    }

    public short getError() {
      return this.error;
    }

    public respUpdateVLRDetails_args setError(short error) {
      this.error = error;
      setErrorIsSet(true);
      return this;
    }

    public void unsetError() {
      __isset_bit_vector.clear(__ERROR_ISSET_ID);
    }

    /** Returns true if field error is set (has been assigned a value) and false otherwise */
    public boolean isSetError() {
      return __isset_bit_vector.get(__ERROR_ISSET_ID);
    }

    public void setErrorIsSet(boolean value) {
      __isset_bit_vector.set(__ERROR_ISSET_ID, value);
    }

    public int getExtensionSize() {
      return (this.extension == null) ? 0 : this.extension.size();
    }

    public void putToExtension(String key, String val) {
      if (this.extension == null) {
        this.extension = new HashMap<String,String>();
      }
      this.extension.put(key, val);
    }

    public Map<String,String> getExtension() {
      return this.extension;
    }

    public respUpdateVLRDetails_args setExtension(Map<String,String> extension) {
      this.extension = extension;
      return this;
    }

    public void unsetExtension() {
      this.extension = null;
    }

    /** Returns true if field extension is set (has been assigned a value) and false otherwise */
    public boolean isSetExtension() {
      return this.extension != null;
    }

    public void setExtensionIsSet(boolean value) {
      if (!value) {
        this.extension = null;
      }
    }

    public void setFieldValue(_Fields field, Object value) {
      switch (field) {
      case SERVICE_ID:
        if (value == null) {
          unsetService_id();
        } else {
          setService_id((Integer)value);
        }
        break;

      case SESSION_ID:
        if (value == null) {
          unsetSession_id();
        } else {
          setSession_id((Long)value);
        }
        break;

      case HLR_NUMBER:
        if (value == null) {
          unsetHlr_number();
        } else {
          setHlr_number((String)value);
        }
        break;

      case ERROR:
        if (value == null) {
          unsetError();
        } else {
          setError((Short)value);
        }
        break;

      case EXTENSION:
        if (value == null) {
          unsetExtension();
        } else {
          setExtension((Map<String,String>)value);
        }
        break;

      }
    }

    public Object getFieldValue(_Fields field) {
      switch (field) {
      case SERVICE_ID:
        return new Integer(getService_id());

      case SESSION_ID:
        return new Long(getSession_id());

      case HLR_NUMBER:
        return getHlr_number();

      case ERROR:
        return new Short(getError());

      case EXTENSION:
        return getExtension();

      }
      throw new IllegalStateException();
    }

    /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
    public boolean isSet(_Fields field) {
      if (field == null) {
        throw new IllegalArgumentException();
      }

      switch (field) {
      case SERVICE_ID:
        return isSetService_id();
      case SESSION_ID:
        return isSetSession_id();
      case HLR_NUMBER:
        return isSetHlr_number();
      case ERROR:
        return isSetError();
      case EXTENSION:
        return isSetExtension();
      }
      throw new IllegalStateException();
    }

    @Override
    public boolean equals(Object that) {
      if (that == null)
        return false;
      if (that instanceof respUpdateVLRDetails_args)
        return this.equals((respUpdateVLRDetails_args)that);
      return false;
    }

    public boolean equals(respUpdateVLRDetails_args that) {
      if (that == null)
        return false;

      boolean this_present_service_id = true;
      boolean that_present_service_id = true;
      if (this_present_service_id || that_present_service_id) {
        if (!(this_present_service_id && that_present_service_id))
          return false;
        if (this.service_id != that.service_id)
          return false;
      }

      boolean this_present_session_id = true;
      boolean that_present_session_id = true;
      if (this_present_session_id || that_present_session_id) {
        if (!(this_present_session_id && that_present_session_id))
          return false;
        if (this.session_id != that.session_id)
          return false;
      }

      boolean this_present_hlr_number = true && this.isSetHlr_number();
      boolean that_present_hlr_number = true && that.isSetHlr_number();
      if (this_present_hlr_number || that_present_hlr_number) {
        if (!(this_present_hlr_number && that_present_hlr_number))
          return false;
        if (!this.hlr_number.equals(that.hlr_number))
          return false;
      }

      boolean this_present_error = true;
      boolean that_present_error = true;
      if (this_present_error || that_present_error) {
        if (!(this_present_error && that_present_error))
          return false;
        if (this.error != that.error)
          return false;
      }

      boolean this_present_extension = true && this.isSetExtension();
      boolean that_present_extension = true && that.isSetExtension();
      if (this_present_extension || that_present_extension) {
        if (!(this_present_extension && that_present_extension))
          return false;
        if (!this.extension.equals(that.extension))
          return false;
      }

      return true;
    }

    @Override
    public int hashCode() {
      return 0;
    }

    public int compareTo(respUpdateVLRDetails_args other) {
      if (!getClass().equals(other.getClass())) {
        return getClass().getName().compareTo(other.getClass().getName());
      }

      int lastComparison = 0;
      respUpdateVLRDetails_args typedOther = (respUpdateVLRDetails_args)other;

      lastComparison = Boolean.valueOf(isSetService_id()).compareTo(typedOther.isSetService_id());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetService_id()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.service_id, typedOther.service_id);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetSession_id()).compareTo(typedOther.isSetSession_id());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetSession_id()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.session_id, typedOther.session_id);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetHlr_number()).compareTo(typedOther.isSetHlr_number());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetHlr_number()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.hlr_number, typedOther.hlr_number);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetError()).compareTo(typedOther.isSetError());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetError()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.error, typedOther.error);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetExtension()).compareTo(typedOther.isSetExtension());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetExtension()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.extension, typedOther.extension);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      return 0;
    }

    public _Fields fieldForId(int fieldId) {
      return _Fields.findByThriftId(fieldId);
    }

    public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField field;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (field.id) {
          case 1: // SERVICE_ID
            if (field.type == org.apache.thrift.protocol.TType.I32) {
              this.service_id = iprot.readI32();
              setService_idIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 2: // SESSION_ID
            if (field.type == org.apache.thrift.protocol.TType.I64) {
              this.session_id = iprot.readI64();
              setSession_idIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 3: // HLR_NUMBER
            if (field.type == org.apache.thrift.protocol.TType.STRING) {
              this.hlr_number = iprot.readString();
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 4: // ERROR
            if (field.type == org.apache.thrift.protocol.TType.I16) {
              this.error = iprot.readI16();
              setErrorIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 5: // EXTENSION
            if (field.type == org.apache.thrift.protocol.TType.MAP) {
              {
                org.apache.thrift.protocol.TMap _map5 = iprot.readMapBegin();
                this.extension = new HashMap<String,String>(2*_map5.size);
                for (int _i6 = 0; _i6 < _map5.size; ++_i6)
                {
                  String _key7;
                  String _val8;
                  _key7 = iprot.readString();
                  _val8 = iprot.readString();
                  this.extension.put(_key7, _val8);
                }
                iprot.readMapEnd();
              }
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(SERVICE_ID_FIELD_DESC);
      oprot.writeI32(this.service_id);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(SESSION_ID_FIELD_DESC);
      oprot.writeI64(this.session_id);
      oprot.writeFieldEnd();
      if (this.hlr_number != null) {
        oprot.writeFieldBegin(HLR_NUMBER_FIELD_DESC);
        oprot.writeString(this.hlr_number);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldBegin(ERROR_FIELD_DESC);
      oprot.writeI16(this.error);
      oprot.writeFieldEnd();
      if (this.extension != null) {
        oprot.writeFieldBegin(EXTENSION_FIELD_DESC);
        {
          oprot.writeMapBegin(new org.apache.thrift.protocol.TMap(org.apache.thrift.protocol.TType.STRING, org.apache.thrift.protocol.TType.STRING, this.extension.size()));
          for (Map.Entry<String, String> _iter9 : this.extension.entrySet())
          {
            oprot.writeString(_iter9.getKey());
            oprot.writeString(_iter9.getValue());
          }
          oprot.writeMapEnd();
        }
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder("respUpdateVLRDetails_args(");
      boolean first = true;

      sb.append("service_id:");
      sb.append(this.service_id);
      first = false;
      if (!first) sb.append(", ");
      sb.append("session_id:");
      sb.append(this.session_id);
      first = false;
      if (!first) sb.append(", ");
      sb.append("hlr_number:");
      if (this.hlr_number == null) {
        sb.append("null");
      } else {
        sb.append(this.hlr_number);
      }
      first = false;
      if (!first) sb.append(", ");
      sb.append("error:");
      sb.append(this.error);
      first = false;
      if (!first) sb.append(", ");
      sb.append("extension:");
      if (this.extension == null) {
        sb.append("null");
      } else {
        sb.append(this.extension);
      }
      first = false;
      sb.append(")");
      return sb.toString();
    }

    public void validate() throws org.apache.thrift.TException {
      // check for required fields
    }

    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
      try {
        write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
      try {
        // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
        __isset_bit_vector = new BitSet(1);
        read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

  }

  public static class reqvMSRN_args implements org.apache.thrift.TBase<reqvMSRN_args, reqvMSRN_args._Fields>, java.io.Serializable, Cloneable   {
    private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("reqvMSRN_args");

    private static final org.apache.thrift.protocol.TField SERVICE_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("service_id", org.apache.thrift.protocol.TType.I32, (short)1);
    private static final org.apache.thrift.protocol.TField SESSION_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("session_id", org.apache.thrift.protocol.TType.I64, (short)2);
    private static final org.apache.thrift.protocol.TField IMSI_FIELD_DESC = new org.apache.thrift.protocol.TField("imsi", org.apache.thrift.protocol.TType.STRING, (short)3);
    private static final org.apache.thrift.protocol.TField EXTENSION_FIELD_DESC = new org.apache.thrift.protocol.TField("extension", org.apache.thrift.protocol.TType.MAP, (short)4);

    public int service_id;
    public long session_id;
    public String imsi;
    public Map<String,String> extension;

    /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
    public enum _Fields implements org.apache.thrift.TFieldIdEnum {
      SERVICE_ID((short)1, "service_id"),
      SESSION_ID((short)2, "session_id"),
      IMSI((short)3, "imsi"),
      EXTENSION((short)4, "extension");

      private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

      static {
        for (_Fields field : EnumSet.allOf(_Fields.class)) {
          byName.put(field.getFieldName(), field);
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, or null if its not found.
       */
      public static _Fields findByThriftId(int fieldId) {
        switch(fieldId) {
          case 1: // SERVICE_ID
            return SERVICE_ID;
          case 2: // SESSION_ID
            return SESSION_ID;
          case 3: // IMSI
            return IMSI;
          case 4: // EXTENSION
            return EXTENSION;
          default:
            return null;
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, throwing an exception
       * if it is not found.
       */
      public static _Fields findByThriftIdOrThrow(int fieldId) {
        _Fields fields = findByThriftId(fieldId);
        if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
        return fields;
      }

      /**
       * Find the _Fields constant that matches name, or null if its not found.
       */
      public static _Fields findByName(String name) {
        return byName.get(name);
      }

      private final short _thriftId;
      private final String _fieldName;

      _Fields(short thriftId, String fieldName) {
        _thriftId = thriftId;
        _fieldName = fieldName;
      }

      public short getThriftFieldId() {
        return _thriftId;
      }

      public String getFieldName() {
        return _fieldName;
      }
    }

    // isset id assignments
    private static final int __SERVICE_ID_ISSET_ID = 0;
    private static final int __SESSION_ID_ISSET_ID = 1;
    private BitSet __isset_bit_vector = new BitSet(2);

    public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
    static {
      Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
      tmpMap.put(_Fields.SERVICE_ID, new org.apache.thrift.meta_data.FieldMetaData("service_id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
      tmpMap.put(_Fields.SESSION_ID, new org.apache.thrift.meta_data.FieldMetaData("session_id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
      tmpMap.put(_Fields.IMSI, new org.apache.thrift.meta_data.FieldMetaData("imsi", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
      tmpMap.put(_Fields.EXTENSION, new org.apache.thrift.meta_data.FieldMetaData("extension", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.MapMetaData(org.apache.thrift.protocol.TType.MAP, 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING), 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING))));
      metaDataMap = Collections.unmodifiableMap(tmpMap);
      org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(reqvMSRN_args.class, metaDataMap);
    }

    public reqvMSRN_args() {
    }

    public reqvMSRN_args(
      int service_id,
      long session_id,
      String imsi,
      Map<String,String> extension)
    {
      this();
      this.service_id = service_id;
      setService_idIsSet(true);
      this.session_id = session_id;
      setSession_idIsSet(true);
      this.imsi = imsi;
      this.extension = extension;
    }

    /**
     * Performs a deep copy on <i>other</i>.
     */
    public reqvMSRN_args(reqvMSRN_args other) {
      __isset_bit_vector.clear();
      __isset_bit_vector.or(other.__isset_bit_vector);
      this.service_id = other.service_id;
      this.session_id = other.session_id;
      if (other.isSetImsi()) {
        this.imsi = other.imsi;
      }
      if (other.isSetExtension()) {
        Map<String,String> __this__extension = new HashMap<String,String>();
        for (Map.Entry<String, String> other_element : other.extension.entrySet()) {

          String other_element_key = other_element.getKey();
          String other_element_value = other_element.getValue();

          String __this__extension_copy_key = other_element_key;

          String __this__extension_copy_value = other_element_value;

          __this__extension.put(__this__extension_copy_key, __this__extension_copy_value);
        }
        this.extension = __this__extension;
      }
    }

    public reqvMSRN_args deepCopy() {
      return new reqvMSRN_args(this);
    }

    @Override
    public void clear() {
      setService_idIsSet(false);
      this.service_id = 0;
      setSession_idIsSet(false);
      this.session_id = 0;
      this.imsi = null;
      this.extension = null;
    }

    public int getService_id() {
      return this.service_id;
    }

    public reqvMSRN_args setService_id(int service_id) {
      this.service_id = service_id;
      setService_idIsSet(true);
      return this;
    }

    public void unsetService_id() {
      __isset_bit_vector.clear(__SERVICE_ID_ISSET_ID);
    }

    /** Returns true if field service_id is set (has been assigned a value) and false otherwise */
    public boolean isSetService_id() {
      return __isset_bit_vector.get(__SERVICE_ID_ISSET_ID);
    }

    public void setService_idIsSet(boolean value) {
      __isset_bit_vector.set(__SERVICE_ID_ISSET_ID, value);
    }

    public long getSession_id() {
      return this.session_id;
    }

    public reqvMSRN_args setSession_id(long session_id) {
      this.session_id = session_id;
      setSession_idIsSet(true);
      return this;
    }

    public void unsetSession_id() {
      __isset_bit_vector.clear(__SESSION_ID_ISSET_ID);
    }

    /** Returns true if field session_id is set (has been assigned a value) and false otherwise */
    public boolean isSetSession_id() {
      return __isset_bit_vector.get(__SESSION_ID_ISSET_ID);
    }

    public void setSession_idIsSet(boolean value) {
      __isset_bit_vector.set(__SESSION_ID_ISSET_ID, value);
    }

    public String getImsi() {
      return this.imsi;
    }

    public reqvMSRN_args setImsi(String imsi) {
      this.imsi = imsi;
      return this;
    }

    public void unsetImsi() {
      this.imsi = null;
    }

    /** Returns true if field imsi is set (has been assigned a value) and false otherwise */
    public boolean isSetImsi() {
      return this.imsi != null;
    }

    public void setImsiIsSet(boolean value) {
      if (!value) {
        this.imsi = null;
      }
    }

    public int getExtensionSize() {
      return (this.extension == null) ? 0 : this.extension.size();
    }

    public void putToExtension(String key, String val) {
      if (this.extension == null) {
        this.extension = new HashMap<String,String>();
      }
      this.extension.put(key, val);
    }

    public Map<String,String> getExtension() {
      return this.extension;
    }

    public reqvMSRN_args setExtension(Map<String,String> extension) {
      this.extension = extension;
      return this;
    }

    public void unsetExtension() {
      this.extension = null;
    }

    /** Returns true if field extension is set (has been assigned a value) and false otherwise */
    public boolean isSetExtension() {
      return this.extension != null;
    }

    public void setExtensionIsSet(boolean value) {
      if (!value) {
        this.extension = null;
      }
    }

    public void setFieldValue(_Fields field, Object value) {
      switch (field) {
      case SERVICE_ID:
        if (value == null) {
          unsetService_id();
        } else {
          setService_id((Integer)value);
        }
        break;

      case SESSION_ID:
        if (value == null) {
          unsetSession_id();
        } else {
          setSession_id((Long)value);
        }
        break;

      case IMSI:
        if (value == null) {
          unsetImsi();
        } else {
          setImsi((String)value);
        }
        break;

      case EXTENSION:
        if (value == null) {
          unsetExtension();
        } else {
          setExtension((Map<String,String>)value);
        }
        break;

      }
    }

    public Object getFieldValue(_Fields field) {
      switch (field) {
      case SERVICE_ID:
        return new Integer(getService_id());

      case SESSION_ID:
        return new Long(getSession_id());

      case IMSI:
        return getImsi();

      case EXTENSION:
        return getExtension();

      }
      throw new IllegalStateException();
    }

    /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
    public boolean isSet(_Fields field) {
      if (field == null) {
        throw new IllegalArgumentException();
      }

      switch (field) {
      case SERVICE_ID:
        return isSetService_id();
      case SESSION_ID:
        return isSetSession_id();
      case IMSI:
        return isSetImsi();
      case EXTENSION:
        return isSetExtension();
      }
      throw new IllegalStateException();
    }

    @Override
    public boolean equals(Object that) {
      if (that == null)
        return false;
      if (that instanceof reqvMSRN_args)
        return this.equals((reqvMSRN_args)that);
      return false;
    }

    public boolean equals(reqvMSRN_args that) {
      if (that == null)
        return false;

      boolean this_present_service_id = true;
      boolean that_present_service_id = true;
      if (this_present_service_id || that_present_service_id) {
        if (!(this_present_service_id && that_present_service_id))
          return false;
        if (this.service_id != that.service_id)
          return false;
      }

      boolean this_present_session_id = true;
      boolean that_present_session_id = true;
      if (this_present_session_id || that_present_session_id) {
        if (!(this_present_session_id && that_present_session_id))
          return false;
        if (this.session_id != that.session_id)
          return false;
      }

      boolean this_present_imsi = true && this.isSetImsi();
      boolean that_present_imsi = true && that.isSetImsi();
      if (this_present_imsi || that_present_imsi) {
        if (!(this_present_imsi && that_present_imsi))
          return false;
        if (!this.imsi.equals(that.imsi))
          return false;
      }

      boolean this_present_extension = true && this.isSetExtension();
      boolean that_present_extension = true && that.isSetExtension();
      if (this_present_extension || that_present_extension) {
        if (!(this_present_extension && that_present_extension))
          return false;
        if (!this.extension.equals(that.extension))
          return false;
      }

      return true;
    }

    @Override
    public int hashCode() {
      return 0;
    }

    public int compareTo(reqvMSRN_args other) {
      if (!getClass().equals(other.getClass())) {
        return getClass().getName().compareTo(other.getClass().getName());
      }

      int lastComparison = 0;
      reqvMSRN_args typedOther = (reqvMSRN_args)other;

      lastComparison = Boolean.valueOf(isSetService_id()).compareTo(typedOther.isSetService_id());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetService_id()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.service_id, typedOther.service_id);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetSession_id()).compareTo(typedOther.isSetSession_id());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetSession_id()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.session_id, typedOther.session_id);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetImsi()).compareTo(typedOther.isSetImsi());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetImsi()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.imsi, typedOther.imsi);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetExtension()).compareTo(typedOther.isSetExtension());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetExtension()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.extension, typedOther.extension);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      return 0;
    }

    public _Fields fieldForId(int fieldId) {
      return _Fields.findByThriftId(fieldId);
    }

    public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField field;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (field.id) {
          case 1: // SERVICE_ID
            if (field.type == org.apache.thrift.protocol.TType.I32) {
              this.service_id = iprot.readI32();
              setService_idIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 2: // SESSION_ID
            if (field.type == org.apache.thrift.protocol.TType.I64) {
              this.session_id = iprot.readI64();
              setSession_idIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 3: // IMSI
            if (field.type == org.apache.thrift.protocol.TType.STRING) {
              this.imsi = iprot.readString();
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 4: // EXTENSION
            if (field.type == org.apache.thrift.protocol.TType.MAP) {
              {
                org.apache.thrift.protocol.TMap _map10 = iprot.readMapBegin();
                this.extension = new HashMap<String,String>(2*_map10.size);
                for (int _i11 = 0; _i11 < _map10.size; ++_i11)
                {
                  String _key12;
                  String _val13;
                  _key12 = iprot.readString();
                  _val13 = iprot.readString();
                  this.extension.put(_key12, _val13);
                }
                iprot.readMapEnd();
              }
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(SERVICE_ID_FIELD_DESC);
      oprot.writeI32(this.service_id);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(SESSION_ID_FIELD_DESC);
      oprot.writeI64(this.session_id);
      oprot.writeFieldEnd();
      if (this.imsi != null) {
        oprot.writeFieldBegin(IMSI_FIELD_DESC);
        oprot.writeString(this.imsi);
        oprot.writeFieldEnd();
      }
      if (this.extension != null) {
        oprot.writeFieldBegin(EXTENSION_FIELD_DESC);
        {
          oprot.writeMapBegin(new org.apache.thrift.protocol.TMap(org.apache.thrift.protocol.TType.STRING, org.apache.thrift.protocol.TType.STRING, this.extension.size()));
          for (Map.Entry<String, String> _iter14 : this.extension.entrySet())
          {
            oprot.writeString(_iter14.getKey());
            oprot.writeString(_iter14.getValue());
          }
          oprot.writeMapEnd();
        }
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder("reqvMSRN_args(");
      boolean first = true;

      sb.append("service_id:");
      sb.append(this.service_id);
      first = false;
      if (!first) sb.append(", ");
      sb.append("session_id:");
      sb.append(this.session_id);
      first = false;
      if (!first) sb.append(", ");
      sb.append("imsi:");
      if (this.imsi == null) {
        sb.append("null");
      } else {
        sb.append(this.imsi);
      }
      first = false;
      if (!first) sb.append(", ");
      sb.append("extension:");
      if (this.extension == null) {
        sb.append("null");
      } else {
        sb.append(this.extension);
      }
      first = false;
      sb.append(")");
      return sb.toString();
    }

    public void validate() throws org.apache.thrift.TException {
      // check for required fields
    }

    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
      try {
        write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
      try {
        // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
        __isset_bit_vector = new BitSet(1);
        read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

  }

  public static class reqInitiateUSSDSession_args implements org.apache.thrift.TBase<reqInitiateUSSDSession_args, reqInitiateUSSDSession_args._Fields>, java.io.Serializable, Cloneable   {
    private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("reqInitiateUSSDSession_args");

    private static final org.apache.thrift.protocol.TField SERVICE_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("service_id", org.apache.thrift.protocol.TType.I32, (short)1);
    private static final org.apache.thrift.protocol.TField SESSION_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("session_id", org.apache.thrift.protocol.TType.I64, (short)2);
    private static final org.apache.thrift.protocol.TField CODING_SCHEME_FIELD_DESC = new org.apache.thrift.protocol.TField("coding_scheme", org.apache.thrift.protocol.TType.STRING, (short)3);
    private static final org.apache.thrift.protocol.TField DATA_STRING_FIELD_DESC = new org.apache.thrift.protocol.TField("data_string", org.apache.thrift.protocol.TType.STRING, (short)4);
    private static final org.apache.thrift.protocol.TField MSISDN_FIELD_DESC = new org.apache.thrift.protocol.TField("msisdn", org.apache.thrift.protocol.TType.STRING, (short)5);
    private static final org.apache.thrift.protocol.TField EXTENSION_FIELD_DESC = new org.apache.thrift.protocol.TField("extension", org.apache.thrift.protocol.TType.MAP, (short)6);

    public int service_id;
    public long session_id;
    public String coding_scheme;
    public String data_string;
    public String msisdn;
    public Map<String,String> extension;

    /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
    public enum _Fields implements org.apache.thrift.TFieldIdEnum {
      SERVICE_ID((short)1, "service_id"),
      SESSION_ID((short)2, "session_id"),
      CODING_SCHEME((short)3, "coding_scheme"),
      DATA_STRING((short)4, "data_string"),
      MSISDN((short)5, "msisdn"),
      EXTENSION((short)6, "extension");

      private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

      static {
        for (_Fields field : EnumSet.allOf(_Fields.class)) {
          byName.put(field.getFieldName(), field);
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, or null if its not found.
       */
      public static _Fields findByThriftId(int fieldId) {
        switch(fieldId) {
          case 1: // SERVICE_ID
            return SERVICE_ID;
          case 2: // SESSION_ID
            return SESSION_ID;
          case 3: // CODING_SCHEME
            return CODING_SCHEME;
          case 4: // DATA_STRING
            return DATA_STRING;
          case 5: // MSISDN
            return MSISDN;
          case 6: // EXTENSION
            return EXTENSION;
          default:
            return null;
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, throwing an exception
       * if it is not found.
       */
      public static _Fields findByThriftIdOrThrow(int fieldId) {
        _Fields fields = findByThriftId(fieldId);
        if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
        return fields;
      }

      /**
       * Find the _Fields constant that matches name, or null if its not found.
       */
      public static _Fields findByName(String name) {
        return byName.get(name);
      }

      private final short _thriftId;
      private final String _fieldName;

      _Fields(short thriftId, String fieldName) {
        _thriftId = thriftId;
        _fieldName = fieldName;
      }

      public short getThriftFieldId() {
        return _thriftId;
      }

      public String getFieldName() {
        return _fieldName;
      }
    }

    // isset id assignments
    private static final int __SERVICE_ID_ISSET_ID = 0;
    private static final int __SESSION_ID_ISSET_ID = 1;
    private BitSet __isset_bit_vector = new BitSet(2);

    public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
    static {
      Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
      tmpMap.put(_Fields.SERVICE_ID, new org.apache.thrift.meta_data.FieldMetaData("service_id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
      tmpMap.put(_Fields.SESSION_ID, new org.apache.thrift.meta_data.FieldMetaData("session_id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
      tmpMap.put(_Fields.CODING_SCHEME, new org.apache.thrift.meta_data.FieldMetaData("coding_scheme", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
      tmpMap.put(_Fields.DATA_STRING, new org.apache.thrift.meta_data.FieldMetaData("data_string", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
      tmpMap.put(_Fields.MSISDN, new org.apache.thrift.meta_data.FieldMetaData("msisdn", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
      tmpMap.put(_Fields.EXTENSION, new org.apache.thrift.meta_data.FieldMetaData("extension", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.MapMetaData(org.apache.thrift.protocol.TType.MAP, 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING), 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING))));
      metaDataMap = Collections.unmodifiableMap(tmpMap);
      org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(reqInitiateUSSDSession_args.class, metaDataMap);
    }

    public reqInitiateUSSDSession_args() {
    }

    public reqInitiateUSSDSession_args(
      int service_id,
      long session_id,
      String coding_scheme,
      String data_string,
      String msisdn,
      Map<String,String> extension)
    {
      this();
      this.service_id = service_id;
      setService_idIsSet(true);
      this.session_id = session_id;
      setSession_idIsSet(true);
      this.coding_scheme = coding_scheme;
      this.data_string = data_string;
      this.msisdn = msisdn;
      this.extension = extension;
    }

    /**
     * Performs a deep copy on <i>other</i>.
     */
    public reqInitiateUSSDSession_args(reqInitiateUSSDSession_args other) {
      __isset_bit_vector.clear();
      __isset_bit_vector.or(other.__isset_bit_vector);
      this.service_id = other.service_id;
      this.session_id = other.session_id;
      if (other.isSetCoding_scheme()) {
        this.coding_scheme = other.coding_scheme;
      }
      if (other.isSetData_string()) {
        this.data_string = other.data_string;
      }
      if (other.isSetMsisdn()) {
        this.msisdn = other.msisdn;
      }
      if (other.isSetExtension()) {
        Map<String,String> __this__extension = new HashMap<String,String>();
        for (Map.Entry<String, String> other_element : other.extension.entrySet()) {

          String other_element_key = other_element.getKey();
          String other_element_value = other_element.getValue();

          String __this__extension_copy_key = other_element_key;

          String __this__extension_copy_value = other_element_value;

          __this__extension.put(__this__extension_copy_key, __this__extension_copy_value);
        }
        this.extension = __this__extension;
      }
    }

    public reqInitiateUSSDSession_args deepCopy() {
      return new reqInitiateUSSDSession_args(this);
    }

    @Override
    public void clear() {
      setService_idIsSet(false);
      this.service_id = 0;
      setSession_idIsSet(false);
      this.session_id = 0;
      this.coding_scheme = null;
      this.data_string = null;
      this.msisdn = null;
      this.extension = null;
    }

    public int getService_id() {
      return this.service_id;
    }

    public reqInitiateUSSDSession_args setService_id(int service_id) {
      this.service_id = service_id;
      setService_idIsSet(true);
      return this;
    }

    public void unsetService_id() {
      __isset_bit_vector.clear(__SERVICE_ID_ISSET_ID);
    }

    /** Returns true if field service_id is set (has been assigned a value) and false otherwise */
    public boolean isSetService_id() {
      return __isset_bit_vector.get(__SERVICE_ID_ISSET_ID);
    }

    public void setService_idIsSet(boolean value) {
      __isset_bit_vector.set(__SERVICE_ID_ISSET_ID, value);
    }

    public long getSession_id() {
      return this.session_id;
    }

    public reqInitiateUSSDSession_args setSession_id(long session_id) {
      this.session_id = session_id;
      setSession_idIsSet(true);
      return this;
    }

    public void unsetSession_id() {
      __isset_bit_vector.clear(__SESSION_ID_ISSET_ID);
    }

    /** Returns true if field session_id is set (has been assigned a value) and false otherwise */
    public boolean isSetSession_id() {
      return __isset_bit_vector.get(__SESSION_ID_ISSET_ID);
    }

    public void setSession_idIsSet(boolean value) {
      __isset_bit_vector.set(__SESSION_ID_ISSET_ID, value);
    }

    public String getCoding_scheme() {
      return this.coding_scheme;
    }

    public reqInitiateUSSDSession_args setCoding_scheme(String coding_scheme) {
      this.coding_scheme = coding_scheme;
      return this;
    }

    public void unsetCoding_scheme() {
      this.coding_scheme = null;
    }

    /** Returns true if field coding_scheme is set (has been assigned a value) and false otherwise */
    public boolean isSetCoding_scheme() {
      return this.coding_scheme != null;
    }

    public void setCoding_schemeIsSet(boolean value) {
      if (!value) {
        this.coding_scheme = null;
      }
    }

    public String getData_string() {
      return this.data_string;
    }

    public reqInitiateUSSDSession_args setData_string(String data_string) {
      this.data_string = data_string;
      return this;
    }

    public void unsetData_string() {
      this.data_string = null;
    }

    /** Returns true if field data_string is set (has been assigned a value) and false otherwise */
    public boolean isSetData_string() {
      return this.data_string != null;
    }

    public void setData_stringIsSet(boolean value) {
      if (!value) {
        this.data_string = null;
      }
    }

    public String getMsisdn() {
      return this.msisdn;
    }

    public reqInitiateUSSDSession_args setMsisdn(String msisdn) {
      this.msisdn = msisdn;
      return this;
    }

    public void unsetMsisdn() {
      this.msisdn = null;
    }

    /** Returns true if field msisdn is set (has been assigned a value) and false otherwise */
    public boolean isSetMsisdn() {
      return this.msisdn != null;
    }

    public void setMsisdnIsSet(boolean value) {
      if (!value) {
        this.msisdn = null;
      }
    }

    public int getExtensionSize() {
      return (this.extension == null) ? 0 : this.extension.size();
    }

    public void putToExtension(String key, String val) {
      if (this.extension == null) {
        this.extension = new HashMap<String,String>();
      }
      this.extension.put(key, val);
    }

    public Map<String,String> getExtension() {
      return this.extension;
    }

    public reqInitiateUSSDSession_args setExtension(Map<String,String> extension) {
      this.extension = extension;
      return this;
    }

    public void unsetExtension() {
      this.extension = null;
    }

    /** Returns true if field extension is set (has been assigned a value) and false otherwise */
    public boolean isSetExtension() {
      return this.extension != null;
    }

    public void setExtensionIsSet(boolean value) {
      if (!value) {
        this.extension = null;
      }
    }

    public void setFieldValue(_Fields field, Object value) {
      switch (field) {
      case SERVICE_ID:
        if (value == null) {
          unsetService_id();
        } else {
          setService_id((Integer)value);
        }
        break;

      case SESSION_ID:
        if (value == null) {
          unsetSession_id();
        } else {
          setSession_id((Long)value);
        }
        break;

      case CODING_SCHEME:
        if (value == null) {
          unsetCoding_scheme();
        } else {
          setCoding_scheme((String)value);
        }
        break;

      case DATA_STRING:
        if (value == null) {
          unsetData_string();
        } else {
          setData_string((String)value);
        }
        break;

      case MSISDN:
        if (value == null) {
          unsetMsisdn();
        } else {
          setMsisdn((String)value);
        }
        break;

      case EXTENSION:
        if (value == null) {
          unsetExtension();
        } else {
          setExtension((Map<String,String>)value);
        }
        break;

      }
    }

    public Object getFieldValue(_Fields field) {
      switch (field) {
      case SERVICE_ID:
        return new Integer(getService_id());

      case SESSION_ID:
        return new Long(getSession_id());

      case CODING_SCHEME:
        return getCoding_scheme();

      case DATA_STRING:
        return getData_string();

      case MSISDN:
        return getMsisdn();

      case EXTENSION:
        return getExtension();

      }
      throw new IllegalStateException();
    }

    /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
    public boolean isSet(_Fields field) {
      if (field == null) {
        throw new IllegalArgumentException();
      }

      switch (field) {
      case SERVICE_ID:
        return isSetService_id();
      case SESSION_ID:
        return isSetSession_id();
      case CODING_SCHEME:
        return isSetCoding_scheme();
      case DATA_STRING:
        return isSetData_string();
      case MSISDN:
        return isSetMsisdn();
      case EXTENSION:
        return isSetExtension();
      }
      throw new IllegalStateException();
    }

    @Override
    public boolean equals(Object that) {
      if (that == null)
        return false;
      if (that instanceof reqInitiateUSSDSession_args)
        return this.equals((reqInitiateUSSDSession_args)that);
      return false;
    }

    public boolean equals(reqInitiateUSSDSession_args that) {
      if (that == null)
        return false;

      boolean this_present_service_id = true;
      boolean that_present_service_id = true;
      if (this_present_service_id || that_present_service_id) {
        if (!(this_present_service_id && that_present_service_id))
          return false;
        if (this.service_id != that.service_id)
          return false;
      }

      boolean this_present_session_id = true;
      boolean that_present_session_id = true;
      if (this_present_session_id || that_present_session_id) {
        if (!(this_present_session_id && that_present_session_id))
          return false;
        if (this.session_id != that.session_id)
          return false;
      }

      boolean this_present_coding_scheme = true && this.isSetCoding_scheme();
      boolean that_present_coding_scheme = true && that.isSetCoding_scheme();
      if (this_present_coding_scheme || that_present_coding_scheme) {
        if (!(this_present_coding_scheme && that_present_coding_scheme))
          return false;
        if (!this.coding_scheme.equals(that.coding_scheme))
          return false;
      }

      boolean this_present_data_string = true && this.isSetData_string();
      boolean that_present_data_string = true && that.isSetData_string();
      if (this_present_data_string || that_present_data_string) {
        if (!(this_present_data_string && that_present_data_string))
          return false;
        if (!this.data_string.equals(that.data_string))
          return false;
      }

      boolean this_present_msisdn = true && this.isSetMsisdn();
      boolean that_present_msisdn = true && that.isSetMsisdn();
      if (this_present_msisdn || that_present_msisdn) {
        if (!(this_present_msisdn && that_present_msisdn))
          return false;
        if (!this.msisdn.equals(that.msisdn))
          return false;
      }

      boolean this_present_extension = true && this.isSetExtension();
      boolean that_present_extension = true && that.isSetExtension();
      if (this_present_extension || that_present_extension) {
        if (!(this_present_extension && that_present_extension))
          return false;
        if (!this.extension.equals(that.extension))
          return false;
      }

      return true;
    }

    @Override
    public int hashCode() {
      return 0;
    }

    public int compareTo(reqInitiateUSSDSession_args other) {
      if (!getClass().equals(other.getClass())) {
        return getClass().getName().compareTo(other.getClass().getName());
      }

      int lastComparison = 0;
      reqInitiateUSSDSession_args typedOther = (reqInitiateUSSDSession_args)other;

      lastComparison = Boolean.valueOf(isSetService_id()).compareTo(typedOther.isSetService_id());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetService_id()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.service_id, typedOther.service_id);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetSession_id()).compareTo(typedOther.isSetSession_id());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetSession_id()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.session_id, typedOther.session_id);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetCoding_scheme()).compareTo(typedOther.isSetCoding_scheme());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetCoding_scheme()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.coding_scheme, typedOther.coding_scheme);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetData_string()).compareTo(typedOther.isSetData_string());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetData_string()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.data_string, typedOther.data_string);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetMsisdn()).compareTo(typedOther.isSetMsisdn());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetMsisdn()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.msisdn, typedOther.msisdn);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetExtension()).compareTo(typedOther.isSetExtension());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetExtension()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.extension, typedOther.extension);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      return 0;
    }

    public _Fields fieldForId(int fieldId) {
      return _Fields.findByThriftId(fieldId);
    }

    public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField field;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (field.id) {
          case 1: // SERVICE_ID
            if (field.type == org.apache.thrift.protocol.TType.I32) {
              this.service_id = iprot.readI32();
              setService_idIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 2: // SESSION_ID
            if (field.type == org.apache.thrift.protocol.TType.I64) {
              this.session_id = iprot.readI64();
              setSession_idIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 3: // CODING_SCHEME
            if (field.type == org.apache.thrift.protocol.TType.STRING) {
              this.coding_scheme = iprot.readString();
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 4: // DATA_STRING
            if (field.type == org.apache.thrift.protocol.TType.STRING) {
              this.data_string = iprot.readString();
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 5: // MSISDN
            if (field.type == org.apache.thrift.protocol.TType.STRING) {
              this.msisdn = iprot.readString();
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 6: // EXTENSION
            if (field.type == org.apache.thrift.protocol.TType.MAP) {
              {
                org.apache.thrift.protocol.TMap _map15 = iprot.readMapBegin();
                this.extension = new HashMap<String,String>(2*_map15.size);
                for (int _i16 = 0; _i16 < _map15.size; ++_i16)
                {
                  String _key17;
                  String _val18;
                  _key17 = iprot.readString();
                  _val18 = iprot.readString();
                  this.extension.put(_key17, _val18);
                }
                iprot.readMapEnd();
              }
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(SERVICE_ID_FIELD_DESC);
      oprot.writeI32(this.service_id);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(SESSION_ID_FIELD_DESC);
      oprot.writeI64(this.session_id);
      oprot.writeFieldEnd();
      if (this.coding_scheme != null) {
        oprot.writeFieldBegin(CODING_SCHEME_FIELD_DESC);
        oprot.writeString(this.coding_scheme);
        oprot.writeFieldEnd();
      }
      if (this.data_string != null) {
        oprot.writeFieldBegin(DATA_STRING_FIELD_DESC);
        oprot.writeString(this.data_string);
        oprot.writeFieldEnd();
      }
      if (this.msisdn != null) {
        oprot.writeFieldBegin(MSISDN_FIELD_DESC);
        oprot.writeString(this.msisdn);
        oprot.writeFieldEnd();
      }
      if (this.extension != null) {
        oprot.writeFieldBegin(EXTENSION_FIELD_DESC);
        {
          oprot.writeMapBegin(new org.apache.thrift.protocol.TMap(org.apache.thrift.protocol.TType.STRING, org.apache.thrift.protocol.TType.STRING, this.extension.size()));
          for (Map.Entry<String, String> _iter19 : this.extension.entrySet())
          {
            oprot.writeString(_iter19.getKey());
            oprot.writeString(_iter19.getValue());
          }
          oprot.writeMapEnd();
        }
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder("reqInitiateUSSDSession_args(");
      boolean first = true;

      sb.append("service_id:");
      sb.append(this.service_id);
      first = false;
      if (!first) sb.append(", ");
      sb.append("session_id:");
      sb.append(this.session_id);
      first = false;
      if (!first) sb.append(", ");
      sb.append("coding_scheme:");
      if (this.coding_scheme == null) {
        sb.append("null");
      } else {
        sb.append(this.coding_scheme);
      }
      first = false;
      if (!first) sb.append(", ");
      sb.append("data_string:");
      if (this.data_string == null) {
        sb.append("null");
      } else {
        sb.append(this.data_string);
      }
      first = false;
      if (!first) sb.append(", ");
      sb.append("msisdn:");
      if (this.msisdn == null) {
        sb.append("null");
      } else {
        sb.append(this.msisdn);
      }
      first = false;
      if (!first) sb.append(", ");
      sb.append("extension:");
      if (this.extension == null) {
        sb.append("null");
      } else {
        sb.append(this.extension);
      }
      first = false;
      sb.append(")");
      return sb.toString();
    }

    public void validate() throws org.apache.thrift.TException {
      // check for required fields
    }

    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
      try {
        write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
      try {
        // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
        __isset_bit_vector = new BitSet(1);
        read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

  }

  public static class respUSSD_args implements org.apache.thrift.TBase<respUSSD_args, respUSSD_args._Fields>, java.io.Serializable, Cloneable   {
    private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("respUSSD_args");

    private static final org.apache.thrift.protocol.TField SERVICE_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("service_id", org.apache.thrift.protocol.TType.I32, (short)1);
    private static final org.apache.thrift.protocol.TField SESSION_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("session_id", org.apache.thrift.protocol.TType.I64, (short)2);
    private static final org.apache.thrift.protocol.TField CODING_SCHEME_FIELD_DESC = new org.apache.thrift.protocol.TField("coding_scheme", org.apache.thrift.protocol.TType.STRING, (short)3);
    private static final org.apache.thrift.protocol.TField DATA_STRING_FIELD_DESC = new org.apache.thrift.protocol.TField("data_string", org.apache.thrift.protocol.TType.STRING, (short)4);
    private static final org.apache.thrift.protocol.TField MSISDN_FIELD_DESC = new org.apache.thrift.protocol.TField("msisdn", org.apache.thrift.protocol.TType.STRING, (short)5);
    private static final org.apache.thrift.protocol.TField EXTENSION_FIELD_DESC = new org.apache.thrift.protocol.TField("extension", org.apache.thrift.protocol.TType.MAP, (short)6);

    public int service_id;
    public long session_id;
    public String coding_scheme;
    public String data_string;
    public String msisdn;
    public Map<String,String> extension;

    /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
    public enum _Fields implements org.apache.thrift.TFieldIdEnum {
      SERVICE_ID((short)1, "service_id"),
      SESSION_ID((short)2, "session_id"),
      CODING_SCHEME((short)3, "coding_scheme"),
      DATA_STRING((short)4, "data_string"),
      MSISDN((short)5, "msisdn"),
      EXTENSION((short)6, "extension");

      private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

      static {
        for (_Fields field : EnumSet.allOf(_Fields.class)) {
          byName.put(field.getFieldName(), field);
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, or null if its not found.
       */
      public static _Fields findByThriftId(int fieldId) {
        switch(fieldId) {
          case 1: // SERVICE_ID
            return SERVICE_ID;
          case 2: // SESSION_ID
            return SESSION_ID;
          case 3: // CODING_SCHEME
            return CODING_SCHEME;
          case 4: // DATA_STRING
            return DATA_STRING;
          case 5: // MSISDN
            return MSISDN;
          case 6: // EXTENSION
            return EXTENSION;
          default:
            return null;
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, throwing an exception
       * if it is not found.
       */
      public static _Fields findByThriftIdOrThrow(int fieldId) {
        _Fields fields = findByThriftId(fieldId);
        if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
        return fields;
      }

      /**
       * Find the _Fields constant that matches name, or null if its not found.
       */
      public static _Fields findByName(String name) {
        return byName.get(name);
      }

      private final short _thriftId;
      private final String _fieldName;

      _Fields(short thriftId, String fieldName) {
        _thriftId = thriftId;
        _fieldName = fieldName;
      }

      public short getThriftFieldId() {
        return _thriftId;
      }

      public String getFieldName() {
        return _fieldName;
      }
    }

    // isset id assignments
    private static final int __SERVICE_ID_ISSET_ID = 0;
    private static final int __SESSION_ID_ISSET_ID = 1;
    private BitSet __isset_bit_vector = new BitSet(2);

    public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
    static {
      Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
      tmpMap.put(_Fields.SERVICE_ID, new org.apache.thrift.meta_data.FieldMetaData("service_id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
      tmpMap.put(_Fields.SESSION_ID, new org.apache.thrift.meta_data.FieldMetaData("session_id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
      tmpMap.put(_Fields.CODING_SCHEME, new org.apache.thrift.meta_data.FieldMetaData("coding_scheme", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
      tmpMap.put(_Fields.DATA_STRING, new org.apache.thrift.meta_data.FieldMetaData("data_string", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
      tmpMap.put(_Fields.MSISDN, new org.apache.thrift.meta_data.FieldMetaData("msisdn", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
      tmpMap.put(_Fields.EXTENSION, new org.apache.thrift.meta_data.FieldMetaData("extension", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.MapMetaData(org.apache.thrift.protocol.TType.MAP, 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING), 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING))));
      metaDataMap = Collections.unmodifiableMap(tmpMap);
      org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(respUSSD_args.class, metaDataMap);
    }

    public respUSSD_args() {
    }

    public respUSSD_args(
      int service_id,
      long session_id,
      String coding_scheme,
      String data_string,
      String msisdn,
      Map<String,String> extension)
    {
      this();
      this.service_id = service_id;
      setService_idIsSet(true);
      this.session_id = session_id;
      setSession_idIsSet(true);
      this.coding_scheme = coding_scheme;
      this.data_string = data_string;
      this.msisdn = msisdn;
      this.extension = extension;
    }

    /**
     * Performs a deep copy on <i>other</i>.
     */
    public respUSSD_args(respUSSD_args other) {
      __isset_bit_vector.clear();
      __isset_bit_vector.or(other.__isset_bit_vector);
      this.service_id = other.service_id;
      this.session_id = other.session_id;
      if (other.isSetCoding_scheme()) {
        this.coding_scheme = other.coding_scheme;
      }
      if (other.isSetData_string()) {
        this.data_string = other.data_string;
      }
      if (other.isSetMsisdn()) {
        this.msisdn = other.msisdn;
      }
      if (other.isSetExtension()) {
        Map<String,String> __this__extension = new HashMap<String,String>();
        for (Map.Entry<String, String> other_element : other.extension.entrySet()) {

          String other_element_key = other_element.getKey();
          String other_element_value = other_element.getValue();

          String __this__extension_copy_key = other_element_key;

          String __this__extension_copy_value = other_element_value;

          __this__extension.put(__this__extension_copy_key, __this__extension_copy_value);
        }
        this.extension = __this__extension;
      }
    }

    public respUSSD_args deepCopy() {
      return new respUSSD_args(this);
    }

    @Override
    public void clear() {
      setService_idIsSet(false);
      this.service_id = 0;
      setSession_idIsSet(false);
      this.session_id = 0;
      this.coding_scheme = null;
      this.data_string = null;
      this.msisdn = null;
      this.extension = null;
    }

    public int getService_id() {
      return this.service_id;
    }

    public respUSSD_args setService_id(int service_id) {
      this.service_id = service_id;
      setService_idIsSet(true);
      return this;
    }

    public void unsetService_id() {
      __isset_bit_vector.clear(__SERVICE_ID_ISSET_ID);
    }

    /** Returns true if field service_id is set (has been assigned a value) and false otherwise */
    public boolean isSetService_id() {
      return __isset_bit_vector.get(__SERVICE_ID_ISSET_ID);
    }

    public void setService_idIsSet(boolean value) {
      __isset_bit_vector.set(__SERVICE_ID_ISSET_ID, value);
    }

    public long getSession_id() {
      return this.session_id;
    }

    public respUSSD_args setSession_id(long session_id) {
      this.session_id = session_id;
      setSession_idIsSet(true);
      return this;
    }

    public void unsetSession_id() {
      __isset_bit_vector.clear(__SESSION_ID_ISSET_ID);
    }

    /** Returns true if field session_id is set (has been assigned a value) and false otherwise */
    public boolean isSetSession_id() {
      return __isset_bit_vector.get(__SESSION_ID_ISSET_ID);
    }

    public void setSession_idIsSet(boolean value) {
      __isset_bit_vector.set(__SESSION_ID_ISSET_ID, value);
    }

    public String getCoding_scheme() {
      return this.coding_scheme;
    }

    public respUSSD_args setCoding_scheme(String coding_scheme) {
      this.coding_scheme = coding_scheme;
      return this;
    }

    public void unsetCoding_scheme() {
      this.coding_scheme = null;
    }

    /** Returns true if field coding_scheme is set (has been assigned a value) and false otherwise */
    public boolean isSetCoding_scheme() {
      return this.coding_scheme != null;
    }

    public void setCoding_schemeIsSet(boolean value) {
      if (!value) {
        this.coding_scheme = null;
      }
    }

    public String getData_string() {
      return this.data_string;
    }

    public respUSSD_args setData_string(String data_string) {
      this.data_string = data_string;
      return this;
    }

    public void unsetData_string() {
      this.data_string = null;
    }

    /** Returns true if field data_string is set (has been assigned a value) and false otherwise */
    public boolean isSetData_string() {
      return this.data_string != null;
    }

    public void setData_stringIsSet(boolean value) {
      if (!value) {
        this.data_string = null;
      }
    }

    public String getMsisdn() {
      return this.msisdn;
    }

    public respUSSD_args setMsisdn(String msisdn) {
      this.msisdn = msisdn;
      return this;
    }

    public void unsetMsisdn() {
      this.msisdn = null;
    }

    /** Returns true if field msisdn is set (has been assigned a value) and false otherwise */
    public boolean isSetMsisdn() {
      return this.msisdn != null;
    }

    public void setMsisdnIsSet(boolean value) {
      if (!value) {
        this.msisdn = null;
      }
    }

    public int getExtensionSize() {
      return (this.extension == null) ? 0 : this.extension.size();
    }

    public void putToExtension(String key, String val) {
      if (this.extension == null) {
        this.extension = new HashMap<String,String>();
      }
      this.extension.put(key, val);
    }

    public Map<String,String> getExtension() {
      return this.extension;
    }

    public respUSSD_args setExtension(Map<String,String> extension) {
      this.extension = extension;
      return this;
    }

    public void unsetExtension() {
      this.extension = null;
    }

    /** Returns true if field extension is set (has been assigned a value) and false otherwise */
    public boolean isSetExtension() {
      return this.extension != null;
    }

    public void setExtensionIsSet(boolean value) {
      if (!value) {
        this.extension = null;
      }
    }

    public void setFieldValue(_Fields field, Object value) {
      switch (field) {
      case SERVICE_ID:
        if (value == null) {
          unsetService_id();
        } else {
          setService_id((Integer)value);
        }
        break;

      case SESSION_ID:
        if (value == null) {
          unsetSession_id();
        } else {
          setSession_id((Long)value);
        }
        break;

      case CODING_SCHEME:
        if (value == null) {
          unsetCoding_scheme();
        } else {
          setCoding_scheme((String)value);
        }
        break;

      case DATA_STRING:
        if (value == null) {
          unsetData_string();
        } else {
          setData_string((String)value);
        }
        break;

      case MSISDN:
        if (value == null) {
          unsetMsisdn();
        } else {
          setMsisdn((String)value);
        }
        break;

      case EXTENSION:
        if (value == null) {
          unsetExtension();
        } else {
          setExtension((Map<String,String>)value);
        }
        break;

      }
    }

    public Object getFieldValue(_Fields field) {
      switch (field) {
      case SERVICE_ID:
        return new Integer(getService_id());

      case SESSION_ID:
        return new Long(getSession_id());

      case CODING_SCHEME:
        return getCoding_scheme();

      case DATA_STRING:
        return getData_string();

      case MSISDN:
        return getMsisdn();

      case EXTENSION:
        return getExtension();

      }
      throw new IllegalStateException();
    }

    /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
    public boolean isSet(_Fields field) {
      if (field == null) {
        throw new IllegalArgumentException();
      }

      switch (field) {
      case SERVICE_ID:
        return isSetService_id();
      case SESSION_ID:
        return isSetSession_id();
      case CODING_SCHEME:
        return isSetCoding_scheme();
      case DATA_STRING:
        return isSetData_string();
      case MSISDN:
        return isSetMsisdn();
      case EXTENSION:
        return isSetExtension();
      }
      throw new IllegalStateException();
    }

    @Override
    public boolean equals(Object that) {
      if (that == null)
        return false;
      if (that instanceof respUSSD_args)
        return this.equals((respUSSD_args)that);
      return false;
    }

    public boolean equals(respUSSD_args that) {
      if (that == null)
        return false;

      boolean this_present_service_id = true;
      boolean that_present_service_id = true;
      if (this_present_service_id || that_present_service_id) {
        if (!(this_present_service_id && that_present_service_id))
          return false;
        if (this.service_id != that.service_id)
          return false;
      }

      boolean this_present_session_id = true;
      boolean that_present_session_id = true;
      if (this_present_session_id || that_present_session_id) {
        if (!(this_present_session_id && that_present_session_id))
          return false;
        if (this.session_id != that.session_id)
          return false;
      }

      boolean this_present_coding_scheme = true && this.isSetCoding_scheme();
      boolean that_present_coding_scheme = true && that.isSetCoding_scheme();
      if (this_present_coding_scheme || that_present_coding_scheme) {
        if (!(this_present_coding_scheme && that_present_coding_scheme))
          return false;
        if (!this.coding_scheme.equals(that.coding_scheme))
          return false;
      }

      boolean this_present_data_string = true && this.isSetData_string();
      boolean that_present_data_string = true && that.isSetData_string();
      if (this_present_data_string || that_present_data_string) {
        if (!(this_present_data_string && that_present_data_string))
          return false;
        if (!this.data_string.equals(that.data_string))
          return false;
      }

      boolean this_present_msisdn = true && this.isSetMsisdn();
      boolean that_present_msisdn = true && that.isSetMsisdn();
      if (this_present_msisdn || that_present_msisdn) {
        if (!(this_present_msisdn && that_present_msisdn))
          return false;
        if (!this.msisdn.equals(that.msisdn))
          return false;
      }

      boolean this_present_extension = true && this.isSetExtension();
      boolean that_present_extension = true && that.isSetExtension();
      if (this_present_extension || that_present_extension) {
        if (!(this_present_extension && that_present_extension))
          return false;
        if (!this.extension.equals(that.extension))
          return false;
      }

      return true;
    }

    @Override
    public int hashCode() {
      return 0;
    }

    public int compareTo(respUSSD_args other) {
      if (!getClass().equals(other.getClass())) {
        return getClass().getName().compareTo(other.getClass().getName());
      }

      int lastComparison = 0;
      respUSSD_args typedOther = (respUSSD_args)other;

      lastComparison = Boolean.valueOf(isSetService_id()).compareTo(typedOther.isSetService_id());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetService_id()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.service_id, typedOther.service_id);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetSession_id()).compareTo(typedOther.isSetSession_id());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetSession_id()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.session_id, typedOther.session_id);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetCoding_scheme()).compareTo(typedOther.isSetCoding_scheme());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetCoding_scheme()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.coding_scheme, typedOther.coding_scheme);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetData_string()).compareTo(typedOther.isSetData_string());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetData_string()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.data_string, typedOther.data_string);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetMsisdn()).compareTo(typedOther.isSetMsisdn());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetMsisdn()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.msisdn, typedOther.msisdn);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetExtension()).compareTo(typedOther.isSetExtension());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetExtension()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.extension, typedOther.extension);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      return 0;
    }

    public _Fields fieldForId(int fieldId) {
      return _Fields.findByThriftId(fieldId);
    }

    public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField field;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (field.id) {
          case 1: // SERVICE_ID
            if (field.type == org.apache.thrift.protocol.TType.I32) {
              this.service_id = iprot.readI32();
              setService_idIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 2: // SESSION_ID
            if (field.type == org.apache.thrift.protocol.TType.I64) {
              this.session_id = iprot.readI64();
              setSession_idIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 3: // CODING_SCHEME
            if (field.type == org.apache.thrift.protocol.TType.STRING) {
              this.coding_scheme = iprot.readString();
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 4: // DATA_STRING
            if (field.type == org.apache.thrift.protocol.TType.STRING) {
              this.data_string = iprot.readString();
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 5: // MSISDN
            if (field.type == org.apache.thrift.protocol.TType.STRING) {
              this.msisdn = iprot.readString();
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 6: // EXTENSION
            if (field.type == org.apache.thrift.protocol.TType.MAP) {
              {
                org.apache.thrift.protocol.TMap _map20 = iprot.readMapBegin();
                this.extension = new HashMap<String,String>(2*_map20.size);
                for (int _i21 = 0; _i21 < _map20.size; ++_i21)
                {
                  String _key22;
                  String _val23;
                  _key22 = iprot.readString();
                  _val23 = iprot.readString();
                  this.extension.put(_key22, _val23);
                }
                iprot.readMapEnd();
              }
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(SERVICE_ID_FIELD_DESC);
      oprot.writeI32(this.service_id);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(SESSION_ID_FIELD_DESC);
      oprot.writeI64(this.session_id);
      oprot.writeFieldEnd();
      if (this.coding_scheme != null) {
        oprot.writeFieldBegin(CODING_SCHEME_FIELD_DESC);
        oprot.writeString(this.coding_scheme);
        oprot.writeFieldEnd();
      }
      if (this.data_string != null) {
        oprot.writeFieldBegin(DATA_STRING_FIELD_DESC);
        oprot.writeString(this.data_string);
        oprot.writeFieldEnd();
      }
      if (this.msisdn != null) {
        oprot.writeFieldBegin(MSISDN_FIELD_DESC);
        oprot.writeString(this.msisdn);
        oprot.writeFieldEnd();
      }
      if (this.extension != null) {
        oprot.writeFieldBegin(EXTENSION_FIELD_DESC);
        {
          oprot.writeMapBegin(new org.apache.thrift.protocol.TMap(org.apache.thrift.protocol.TType.STRING, org.apache.thrift.protocol.TType.STRING, this.extension.size()));
          for (Map.Entry<String, String> _iter24 : this.extension.entrySet())
          {
            oprot.writeString(_iter24.getKey());
            oprot.writeString(_iter24.getValue());
          }
          oprot.writeMapEnd();
        }
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder("respUSSD_args(");
      boolean first = true;

      sb.append("service_id:");
      sb.append(this.service_id);
      first = false;
      if (!first) sb.append(", ");
      sb.append("session_id:");
      sb.append(this.session_id);
      first = false;
      if (!first) sb.append(", ");
      sb.append("coding_scheme:");
      if (this.coding_scheme == null) {
        sb.append("null");
      } else {
        sb.append(this.coding_scheme);
      }
      first = false;
      if (!first) sb.append(", ");
      sb.append("data_string:");
      if (this.data_string == null) {
        sb.append("null");
      } else {
        sb.append(this.data_string);
      }
      first = false;
      if (!first) sb.append(", ");
      sb.append("msisdn:");
      if (this.msisdn == null) {
        sb.append("null");
      } else {
        sb.append(this.msisdn);
      }
      first = false;
      if (!first) sb.append(", ");
      sb.append("extension:");
      if (this.extension == null) {
        sb.append("null");
      } else {
        sb.append(this.extension);
      }
      first = false;
      sb.append(")");
      return sb.toString();
    }

    public void validate() throws org.apache.thrift.TException {
      // check for required fields
    }

    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
      try {
        write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
      try {
        // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
        __isset_bit_vector = new BitSet(1);
        read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

  }

  public static class reqIncommingSmsData_args implements org.apache.thrift.TBase<reqIncommingSmsData_args, reqIncommingSmsData_args._Fields>, java.io.Serializable, Cloneable   {
    private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("reqIncommingSmsData_args");

    private static final org.apache.thrift.protocol.TField SERVICE_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("service_id", org.apache.thrift.protocol.TType.I32, (short)1);
    private static final org.apache.thrift.protocol.TField SESSION_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("session_id", org.apache.thrift.protocol.TType.I64, (short)2);
    private static final org.apache.thrift.protocol.TField IMSI_FIELD_DESC = new org.apache.thrift.protocol.TField("imsi", org.apache.thrift.protocol.TType.STRING, (short)3);
    private static final org.apache.thrift.protocol.TField CODING_SCHEME_FIELD_DESC = new org.apache.thrift.protocol.TField("coding_scheme", org.apache.thrift.protocol.TType.STRING, (short)4);
    private static final org.apache.thrift.protocol.TField DATA_STRING_FIELD_DESC = new org.apache.thrift.protocol.TField("data_string", org.apache.thrift.protocol.TType.STRING, (short)5);
    private static final org.apache.thrift.protocol.TField ORIG_PARTY_ADDR_FIELD_DESC = new org.apache.thrift.protocol.TField("orig_party_addr", org.apache.thrift.protocol.TType.STRING, (short)6);
    private static final org.apache.thrift.protocol.TField TIMESTAMP_FIELD_DESC = new org.apache.thrift.protocol.TField("timestamp", org.apache.thrift.protocol.TType.STRING, (short)7);
    private static final org.apache.thrift.protocol.TField EXTENSION_FIELD_DESC = new org.apache.thrift.protocol.TField("extension", org.apache.thrift.protocol.TType.MAP, (short)8);

    public int service_id;
    public long session_id;
    public String imsi;
    public String coding_scheme;
    public String data_string;
    public String orig_party_addr;
    public String timestamp;
    public Map<String,String> extension;

    /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
    public enum _Fields implements org.apache.thrift.TFieldIdEnum {
      SERVICE_ID((short)1, "service_id"),
      SESSION_ID((short)2, "session_id"),
      IMSI((short)3, "imsi"),
      CODING_SCHEME((short)4, "coding_scheme"),
      DATA_STRING((short)5, "data_string"),
      ORIG_PARTY_ADDR((short)6, "orig_party_addr"),
      TIMESTAMP((short)7, "timestamp"),
      EXTENSION((short)8, "extension");

      private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

      static {
        for (_Fields field : EnumSet.allOf(_Fields.class)) {
          byName.put(field.getFieldName(), field);
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, or null if its not found.
       */
      public static _Fields findByThriftId(int fieldId) {
        switch(fieldId) {
          case 1: // SERVICE_ID
            return SERVICE_ID;
          case 2: // SESSION_ID
            return SESSION_ID;
          case 3: // IMSI
            return IMSI;
          case 4: // CODING_SCHEME
            return CODING_SCHEME;
          case 5: // DATA_STRING
            return DATA_STRING;
          case 6: // ORIG_PARTY_ADDR
            return ORIG_PARTY_ADDR;
          case 7: // TIMESTAMP
            return TIMESTAMP;
          case 8: // EXTENSION
            return EXTENSION;
          default:
            return null;
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, throwing an exception
       * if it is not found.
       */
      public static _Fields findByThriftIdOrThrow(int fieldId) {
        _Fields fields = findByThriftId(fieldId);
        if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
        return fields;
      }

      /**
       * Find the _Fields constant that matches name, or null if its not found.
       */
      public static _Fields findByName(String name) {
        return byName.get(name);
      }

      private final short _thriftId;
      private final String _fieldName;

      _Fields(short thriftId, String fieldName) {
        _thriftId = thriftId;
        _fieldName = fieldName;
      }

      public short getThriftFieldId() {
        return _thriftId;
      }

      public String getFieldName() {
        return _fieldName;
      }
    }

    // isset id assignments
    private static final int __SERVICE_ID_ISSET_ID = 0;
    private static final int __SESSION_ID_ISSET_ID = 1;
    private BitSet __isset_bit_vector = new BitSet(2);

    public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
    static {
      Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
      tmpMap.put(_Fields.SERVICE_ID, new org.apache.thrift.meta_data.FieldMetaData("service_id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
      tmpMap.put(_Fields.SESSION_ID, new org.apache.thrift.meta_data.FieldMetaData("session_id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
      tmpMap.put(_Fields.IMSI, new org.apache.thrift.meta_data.FieldMetaData("imsi", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
      tmpMap.put(_Fields.CODING_SCHEME, new org.apache.thrift.meta_data.FieldMetaData("coding_scheme", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
      tmpMap.put(_Fields.DATA_STRING, new org.apache.thrift.meta_data.FieldMetaData("data_string", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
      tmpMap.put(_Fields.ORIG_PARTY_ADDR, new org.apache.thrift.meta_data.FieldMetaData("orig_party_addr", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
      tmpMap.put(_Fields.TIMESTAMP, new org.apache.thrift.meta_data.FieldMetaData("timestamp", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
      tmpMap.put(_Fields.EXTENSION, new org.apache.thrift.meta_data.FieldMetaData("extension", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.MapMetaData(org.apache.thrift.protocol.TType.MAP, 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING), 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING))));
      metaDataMap = Collections.unmodifiableMap(tmpMap);
      org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(reqIncommingSmsData_args.class, metaDataMap);
    }

    public reqIncommingSmsData_args() {
    }

    public reqIncommingSmsData_args(
      int service_id,
      long session_id,
      String imsi,
      String coding_scheme,
      String data_string,
      String orig_party_addr,
      String timestamp,
      Map<String,String> extension)
    {
      this();
      this.service_id = service_id;
      setService_idIsSet(true);
      this.session_id = session_id;
      setSession_idIsSet(true);
      this.imsi = imsi;
      this.coding_scheme = coding_scheme;
      this.data_string = data_string;
      this.orig_party_addr = orig_party_addr;
      this.timestamp = timestamp;
      this.extension = extension;
    }

    /**
     * Performs a deep copy on <i>other</i>.
     */
    public reqIncommingSmsData_args(reqIncommingSmsData_args other) {
      __isset_bit_vector.clear();
      __isset_bit_vector.or(other.__isset_bit_vector);
      this.service_id = other.service_id;
      this.session_id = other.session_id;
      if (other.isSetImsi()) {
        this.imsi = other.imsi;
      }
      if (other.isSetCoding_scheme()) {
        this.coding_scheme = other.coding_scheme;
      }
      if (other.isSetData_string()) {
        this.data_string = other.data_string;
      }
      if (other.isSetOrig_party_addr()) {
        this.orig_party_addr = other.orig_party_addr;
      }
      if (other.isSetTimestamp()) {
        this.timestamp = other.timestamp;
      }
      if (other.isSetExtension()) {
        Map<String,String> __this__extension = new HashMap<String,String>();
        for (Map.Entry<String, String> other_element : other.extension.entrySet()) {

          String other_element_key = other_element.getKey();
          String other_element_value = other_element.getValue();

          String __this__extension_copy_key = other_element_key;

          String __this__extension_copy_value = other_element_value;

          __this__extension.put(__this__extension_copy_key, __this__extension_copy_value);
        }
        this.extension = __this__extension;
      }
    }

    public reqIncommingSmsData_args deepCopy() {
      return new reqIncommingSmsData_args(this);
    }

    @Override
    public void clear() {
      setService_idIsSet(false);
      this.service_id = 0;
      setSession_idIsSet(false);
      this.session_id = 0;
      this.imsi = null;
      this.coding_scheme = null;
      this.data_string = null;
      this.orig_party_addr = null;
      this.timestamp = null;
      this.extension = null;
    }

    public int getService_id() {
      return this.service_id;
    }

    public reqIncommingSmsData_args setService_id(int service_id) {
      this.service_id = service_id;
      setService_idIsSet(true);
      return this;
    }

    public void unsetService_id() {
      __isset_bit_vector.clear(__SERVICE_ID_ISSET_ID);
    }

    /** Returns true if field service_id is set (has been assigned a value) and false otherwise */
    public boolean isSetService_id() {
      return __isset_bit_vector.get(__SERVICE_ID_ISSET_ID);
    }

    public void setService_idIsSet(boolean value) {
      __isset_bit_vector.set(__SERVICE_ID_ISSET_ID, value);
    }

    public long getSession_id() {
      return this.session_id;
    }

    public reqIncommingSmsData_args setSession_id(long session_id) {
      this.session_id = session_id;
      setSession_idIsSet(true);
      return this;
    }

    public void unsetSession_id() {
      __isset_bit_vector.clear(__SESSION_ID_ISSET_ID);
    }

    /** Returns true if field session_id is set (has been assigned a value) and false otherwise */
    public boolean isSetSession_id() {
      return __isset_bit_vector.get(__SESSION_ID_ISSET_ID);
    }

    public void setSession_idIsSet(boolean value) {
      __isset_bit_vector.set(__SESSION_ID_ISSET_ID, value);
    }

    public String getImsi() {
      return this.imsi;
    }

    public reqIncommingSmsData_args setImsi(String imsi) {
      this.imsi = imsi;
      return this;
    }

    public void unsetImsi() {
      this.imsi = null;
    }

    /** Returns true if field imsi is set (has been assigned a value) and false otherwise */
    public boolean isSetImsi() {
      return this.imsi != null;
    }

    public void setImsiIsSet(boolean value) {
      if (!value) {
        this.imsi = null;
      }
    }

    public String getCoding_scheme() {
      return this.coding_scheme;
    }

    public reqIncommingSmsData_args setCoding_scheme(String coding_scheme) {
      this.coding_scheme = coding_scheme;
      return this;
    }

    public void unsetCoding_scheme() {
      this.coding_scheme = null;
    }

    /** Returns true if field coding_scheme is set (has been assigned a value) and false otherwise */
    public boolean isSetCoding_scheme() {
      return this.coding_scheme != null;
    }

    public void setCoding_schemeIsSet(boolean value) {
      if (!value) {
        this.coding_scheme = null;
      }
    }

    public String getData_string() {
      return this.data_string;
    }

    public reqIncommingSmsData_args setData_string(String data_string) {
      this.data_string = data_string;
      return this;
    }

    public void unsetData_string() {
      this.data_string = null;
    }

    /** Returns true if field data_string is set (has been assigned a value) and false otherwise */
    public boolean isSetData_string() {
      return this.data_string != null;
    }

    public void setData_stringIsSet(boolean value) {
      if (!value) {
        this.data_string = null;
      }
    }

    public String getOrig_party_addr() {
      return this.orig_party_addr;
    }

    public reqIncommingSmsData_args setOrig_party_addr(String orig_party_addr) {
      this.orig_party_addr = orig_party_addr;
      return this;
    }

    public void unsetOrig_party_addr() {
      this.orig_party_addr = null;
    }

    /** Returns true if field orig_party_addr is set (has been assigned a value) and false otherwise */
    public boolean isSetOrig_party_addr() {
      return this.orig_party_addr != null;
    }

    public void setOrig_party_addrIsSet(boolean value) {
      if (!value) {
        this.orig_party_addr = null;
      }
    }

    public String getTimestamp() {
      return this.timestamp;
    }

    public reqIncommingSmsData_args setTimestamp(String timestamp) {
      this.timestamp = timestamp;
      return this;
    }

    public void unsetTimestamp() {
      this.timestamp = null;
    }

    /** Returns true if field timestamp is set (has been assigned a value) and false otherwise */
    public boolean isSetTimestamp() {
      return this.timestamp != null;
    }

    public void setTimestampIsSet(boolean value) {
      if (!value) {
        this.timestamp = null;
      }
    }

    public int getExtensionSize() {
      return (this.extension == null) ? 0 : this.extension.size();
    }

    public void putToExtension(String key, String val) {
      if (this.extension == null) {
        this.extension = new HashMap<String,String>();
      }
      this.extension.put(key, val);
    }

    public Map<String,String> getExtension() {
      return this.extension;
    }

    public reqIncommingSmsData_args setExtension(Map<String,String> extension) {
      this.extension = extension;
      return this;
    }

    public void unsetExtension() {
      this.extension = null;
    }

    /** Returns true if field extension is set (has been assigned a value) and false otherwise */
    public boolean isSetExtension() {
      return this.extension != null;
    }

    public void setExtensionIsSet(boolean value) {
      if (!value) {
        this.extension = null;
      }
    }

    public void setFieldValue(_Fields field, Object value) {
      switch (field) {
      case SERVICE_ID:
        if (value == null) {
          unsetService_id();
        } else {
          setService_id((Integer)value);
        }
        break;

      case SESSION_ID:
        if (value == null) {
          unsetSession_id();
        } else {
          setSession_id((Long)value);
        }
        break;

      case IMSI:
        if (value == null) {
          unsetImsi();
        } else {
          setImsi((String)value);
        }
        break;

      case CODING_SCHEME:
        if (value == null) {
          unsetCoding_scheme();
        } else {
          setCoding_scheme((String)value);
        }
        break;

      case DATA_STRING:
        if (value == null) {
          unsetData_string();
        } else {
          setData_string((String)value);
        }
        break;

      case ORIG_PARTY_ADDR:
        if (value == null) {
          unsetOrig_party_addr();
        } else {
          setOrig_party_addr((String)value);
        }
        break;

      case TIMESTAMP:
        if (value == null) {
          unsetTimestamp();
        } else {
          setTimestamp((String)value);
        }
        break;

      case EXTENSION:
        if (value == null) {
          unsetExtension();
        } else {
          setExtension((Map<String,String>)value);
        }
        break;

      }
    }

    public Object getFieldValue(_Fields field) {
      switch (field) {
      case SERVICE_ID:
        return new Integer(getService_id());

      case SESSION_ID:
        return new Long(getSession_id());

      case IMSI:
        return getImsi();

      case CODING_SCHEME:
        return getCoding_scheme();

      case DATA_STRING:
        return getData_string();

      case ORIG_PARTY_ADDR:
        return getOrig_party_addr();

      case TIMESTAMP:
        return getTimestamp();

      case EXTENSION:
        return getExtension();

      }
      throw new IllegalStateException();
    }

    /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
    public boolean isSet(_Fields field) {
      if (field == null) {
        throw new IllegalArgumentException();
      }

      switch (field) {
      case SERVICE_ID:
        return isSetService_id();
      case SESSION_ID:
        return isSetSession_id();
      case IMSI:
        return isSetImsi();
      case CODING_SCHEME:
        return isSetCoding_scheme();
      case DATA_STRING:
        return isSetData_string();
      case ORIG_PARTY_ADDR:
        return isSetOrig_party_addr();
      case TIMESTAMP:
        return isSetTimestamp();
      case EXTENSION:
        return isSetExtension();
      }
      throw new IllegalStateException();
    }

    @Override
    public boolean equals(Object that) {
      if (that == null)
        return false;
      if (that instanceof reqIncommingSmsData_args)
        return this.equals((reqIncommingSmsData_args)that);
      return false;
    }

    public boolean equals(reqIncommingSmsData_args that) {
      if (that == null)
        return false;

      boolean this_present_service_id = true;
      boolean that_present_service_id = true;
      if (this_present_service_id || that_present_service_id) {
        if (!(this_present_service_id && that_present_service_id))
          return false;
        if (this.service_id != that.service_id)
          return false;
      }

      boolean this_present_session_id = true;
      boolean that_present_session_id = true;
      if (this_present_session_id || that_present_session_id) {
        if (!(this_present_session_id && that_present_session_id))
          return false;
        if (this.session_id != that.session_id)
          return false;
      }

      boolean this_present_imsi = true && this.isSetImsi();
      boolean that_present_imsi = true && that.isSetImsi();
      if (this_present_imsi || that_present_imsi) {
        if (!(this_present_imsi && that_present_imsi))
          return false;
        if (!this.imsi.equals(that.imsi))
          return false;
      }

      boolean this_present_coding_scheme = true && this.isSetCoding_scheme();
      boolean that_present_coding_scheme = true && that.isSetCoding_scheme();
      if (this_present_coding_scheme || that_present_coding_scheme) {
        if (!(this_present_coding_scheme && that_present_coding_scheme))
          return false;
        if (!this.coding_scheme.equals(that.coding_scheme))
          return false;
      }

      boolean this_present_data_string = true && this.isSetData_string();
      boolean that_present_data_string = true && that.isSetData_string();
      if (this_present_data_string || that_present_data_string) {
        if (!(this_present_data_string && that_present_data_string))
          return false;
        if (!this.data_string.equals(that.data_string))
          return false;
      }

      boolean this_present_orig_party_addr = true && this.isSetOrig_party_addr();
      boolean that_present_orig_party_addr = true && that.isSetOrig_party_addr();
      if (this_present_orig_party_addr || that_present_orig_party_addr) {
        if (!(this_present_orig_party_addr && that_present_orig_party_addr))
          return false;
        if (!this.orig_party_addr.equals(that.orig_party_addr))
          return false;
      }

      boolean this_present_timestamp = true && this.isSetTimestamp();
      boolean that_present_timestamp = true && that.isSetTimestamp();
      if (this_present_timestamp || that_present_timestamp) {
        if (!(this_present_timestamp && that_present_timestamp))
          return false;
        if (!this.timestamp.equals(that.timestamp))
          return false;
      }

      boolean this_present_extension = true && this.isSetExtension();
      boolean that_present_extension = true && that.isSetExtension();
      if (this_present_extension || that_present_extension) {
        if (!(this_present_extension && that_present_extension))
          return false;
        if (!this.extension.equals(that.extension))
          return false;
      }

      return true;
    }

    @Override
    public int hashCode() {
      return 0;
    }

    public int compareTo(reqIncommingSmsData_args other) {
      if (!getClass().equals(other.getClass())) {
        return getClass().getName().compareTo(other.getClass().getName());
      }

      int lastComparison = 0;
      reqIncommingSmsData_args typedOther = (reqIncommingSmsData_args)other;

      lastComparison = Boolean.valueOf(isSetService_id()).compareTo(typedOther.isSetService_id());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetService_id()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.service_id, typedOther.service_id);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetSession_id()).compareTo(typedOther.isSetSession_id());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetSession_id()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.session_id, typedOther.session_id);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetImsi()).compareTo(typedOther.isSetImsi());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetImsi()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.imsi, typedOther.imsi);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetCoding_scheme()).compareTo(typedOther.isSetCoding_scheme());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetCoding_scheme()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.coding_scheme, typedOther.coding_scheme);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetData_string()).compareTo(typedOther.isSetData_string());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetData_string()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.data_string, typedOther.data_string);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetOrig_party_addr()).compareTo(typedOther.isSetOrig_party_addr());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetOrig_party_addr()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.orig_party_addr, typedOther.orig_party_addr);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetTimestamp()).compareTo(typedOther.isSetTimestamp());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetTimestamp()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.timestamp, typedOther.timestamp);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetExtension()).compareTo(typedOther.isSetExtension());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetExtension()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.extension, typedOther.extension);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      return 0;
    }

    public _Fields fieldForId(int fieldId) {
      return _Fields.findByThriftId(fieldId);
    }

    public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField field;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (field.id) {
          case 1: // SERVICE_ID
            if (field.type == org.apache.thrift.protocol.TType.I32) {
              this.service_id = iprot.readI32();
              setService_idIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 2: // SESSION_ID
            if (field.type == org.apache.thrift.protocol.TType.I64) {
              this.session_id = iprot.readI64();
              setSession_idIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 3: // IMSI
            if (field.type == org.apache.thrift.protocol.TType.STRING) {
              this.imsi = iprot.readString();
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 4: // CODING_SCHEME
            if (field.type == org.apache.thrift.protocol.TType.STRING) {
              this.coding_scheme = iprot.readString();
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 5: // DATA_STRING
            if (field.type == org.apache.thrift.protocol.TType.STRING) {
              this.data_string = iprot.readString();
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 6: // ORIG_PARTY_ADDR
            if (field.type == org.apache.thrift.protocol.TType.STRING) {
              this.orig_party_addr = iprot.readString();
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 7: // TIMESTAMP
            if (field.type == org.apache.thrift.protocol.TType.STRING) {
              this.timestamp = iprot.readString();
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 8: // EXTENSION
            if (field.type == org.apache.thrift.protocol.TType.MAP) {
              {
                org.apache.thrift.protocol.TMap _map25 = iprot.readMapBegin();
                this.extension = new HashMap<String,String>(2*_map25.size);
                for (int _i26 = 0; _i26 < _map25.size; ++_i26)
                {
                  String _key27;
                  String _val28;
                  _key27 = iprot.readString();
                  _val28 = iprot.readString();
                  this.extension.put(_key27, _val28);
                }
                iprot.readMapEnd();
              }
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(SERVICE_ID_FIELD_DESC);
      oprot.writeI32(this.service_id);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(SESSION_ID_FIELD_DESC);
      oprot.writeI64(this.session_id);
      oprot.writeFieldEnd();
      if (this.imsi != null) {
        oprot.writeFieldBegin(IMSI_FIELD_DESC);
        oprot.writeString(this.imsi);
        oprot.writeFieldEnd();
      }
      if (this.coding_scheme != null) {
        oprot.writeFieldBegin(CODING_SCHEME_FIELD_DESC);
        oprot.writeString(this.coding_scheme);
        oprot.writeFieldEnd();
      }
      if (this.data_string != null) {
        oprot.writeFieldBegin(DATA_STRING_FIELD_DESC);
        oprot.writeString(this.data_string);
        oprot.writeFieldEnd();
      }
      if (this.orig_party_addr != null) {
        oprot.writeFieldBegin(ORIG_PARTY_ADDR_FIELD_DESC);
        oprot.writeString(this.orig_party_addr);
        oprot.writeFieldEnd();
      }
      if (this.timestamp != null) {
        oprot.writeFieldBegin(TIMESTAMP_FIELD_DESC);
        oprot.writeString(this.timestamp);
        oprot.writeFieldEnd();
      }
      if (this.extension != null) {
        oprot.writeFieldBegin(EXTENSION_FIELD_DESC);
        {
          oprot.writeMapBegin(new org.apache.thrift.protocol.TMap(org.apache.thrift.protocol.TType.STRING, org.apache.thrift.protocol.TType.STRING, this.extension.size()));
          for (Map.Entry<String, String> _iter29 : this.extension.entrySet())
          {
            oprot.writeString(_iter29.getKey());
            oprot.writeString(_iter29.getValue());
          }
          oprot.writeMapEnd();
        }
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder("reqIncommingSmsData_args(");
      boolean first = true;

      sb.append("service_id:");
      sb.append(this.service_id);
      first = false;
      if (!first) sb.append(", ");
      sb.append("session_id:");
      sb.append(this.session_id);
      first = false;
      if (!first) sb.append(", ");
      sb.append("imsi:");
      if (this.imsi == null) {
        sb.append("null");
      } else {
        sb.append(this.imsi);
      }
      first = false;
      if (!first) sb.append(", ");
      sb.append("coding_scheme:");
      if (this.coding_scheme == null) {
        sb.append("null");
      } else {
        sb.append(this.coding_scheme);
      }
      first = false;
      if (!first) sb.append(", ");
      sb.append("data_string:");
      if (this.data_string == null) {
        sb.append("null");
      } else {
        sb.append(this.data_string);
      }
      first = false;
      if (!first) sb.append(", ");
      sb.append("orig_party_addr:");
      if (this.orig_party_addr == null) {
        sb.append("null");
      } else {
        sb.append(this.orig_party_addr);
      }
      first = false;
      if (!first) sb.append(", ");
      sb.append("timestamp:");
      if (this.timestamp == null) {
        sb.append("null");
      } else {
        sb.append(this.timestamp);
      }
      first = false;
      if (!first) sb.append(", ");
      sb.append("extension:");
      if (this.extension == null) {
        sb.append("null");
      } else {
        sb.append(this.extension);
      }
      first = false;
      sb.append(")");
      return sb.toString();
    }

    public void validate() throws org.apache.thrift.TException {
      // check for required fields
    }

    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
      try {
        write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
      try {
        // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
        __isset_bit_vector = new BitSet(1);
        read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

  }

  public static class respOutgoingSmsData_args implements org.apache.thrift.TBase<respOutgoingSmsData_args, respOutgoingSmsData_args._Fields>, java.io.Serializable, Cloneable   {
    private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("respOutgoingSmsData_args");

    private static final org.apache.thrift.protocol.TField SERVICE_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("service_id", org.apache.thrift.protocol.TType.I32, (short)1);
    private static final org.apache.thrift.protocol.TField SESSION_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("session_id", org.apache.thrift.protocol.TType.I64, (short)2);
    private static final org.apache.thrift.protocol.TField ERROR_FIELD_DESC = new org.apache.thrift.protocol.TField("error", org.apache.thrift.protocol.TType.I16, (short)3);
    private static final org.apache.thrift.protocol.TField EXTENSION_FIELD_DESC = new org.apache.thrift.protocol.TField("extension", org.apache.thrift.protocol.TType.MAP, (short)4);

    public int service_id;
    public long session_id;
    public short error;
    public Map<String,String> extension;

    /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
    public enum _Fields implements org.apache.thrift.TFieldIdEnum {
      SERVICE_ID((short)1, "service_id"),
      SESSION_ID((short)2, "session_id"),
      ERROR((short)3, "error"),
      EXTENSION((short)4, "extension");

      private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

      static {
        for (_Fields field : EnumSet.allOf(_Fields.class)) {
          byName.put(field.getFieldName(), field);
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, or null if its not found.
       */
      public static _Fields findByThriftId(int fieldId) {
        switch(fieldId) {
          case 1: // SERVICE_ID
            return SERVICE_ID;
          case 2: // SESSION_ID
            return SESSION_ID;
          case 3: // ERROR
            return ERROR;
          case 4: // EXTENSION
            return EXTENSION;
          default:
            return null;
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, throwing an exception
       * if it is not found.
       */
      public static _Fields findByThriftIdOrThrow(int fieldId) {
        _Fields fields = findByThriftId(fieldId);
        if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
        return fields;
      }

      /**
       * Find the _Fields constant that matches name, or null if its not found.
       */
      public static _Fields findByName(String name) {
        return byName.get(name);
      }

      private final short _thriftId;
      private final String _fieldName;

      _Fields(short thriftId, String fieldName) {
        _thriftId = thriftId;
        _fieldName = fieldName;
      }

      public short getThriftFieldId() {
        return _thriftId;
      }

      public String getFieldName() {
        return _fieldName;
      }
    }

    // isset id assignments
    private static final int __SERVICE_ID_ISSET_ID = 0;
    private static final int __SESSION_ID_ISSET_ID = 1;
    private static final int __ERROR_ISSET_ID = 2;
    private BitSet __isset_bit_vector = new BitSet(3);

    public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
    static {
      Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
      tmpMap.put(_Fields.SERVICE_ID, new org.apache.thrift.meta_data.FieldMetaData("service_id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
      tmpMap.put(_Fields.SESSION_ID, new org.apache.thrift.meta_data.FieldMetaData("session_id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
      tmpMap.put(_Fields.ERROR, new org.apache.thrift.meta_data.FieldMetaData("error", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I16)));
      tmpMap.put(_Fields.EXTENSION, new org.apache.thrift.meta_data.FieldMetaData("extension", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.MapMetaData(org.apache.thrift.protocol.TType.MAP, 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING), 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING))));
      metaDataMap = Collections.unmodifiableMap(tmpMap);
      org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(respOutgoingSmsData_args.class, metaDataMap);
    }

    public respOutgoingSmsData_args() {
    }

    public respOutgoingSmsData_args(
      int service_id,
      long session_id,
      short error,
      Map<String,String> extension)
    {
      this();
      this.service_id = service_id;
      setService_idIsSet(true);
      this.session_id = session_id;
      setSession_idIsSet(true);
      this.error = error;
      setErrorIsSet(true);
      this.extension = extension;
    }

    /**
     * Performs a deep copy on <i>other</i>.
     */
    public respOutgoingSmsData_args(respOutgoingSmsData_args other) {
      __isset_bit_vector.clear();
      __isset_bit_vector.or(other.__isset_bit_vector);
      this.service_id = other.service_id;
      this.session_id = other.session_id;
      this.error = other.error;
      if (other.isSetExtension()) {
        Map<String,String> __this__extension = new HashMap<String,String>();
        for (Map.Entry<String, String> other_element : other.extension.entrySet()) {

          String other_element_key = other_element.getKey();
          String other_element_value = other_element.getValue();

          String __this__extension_copy_key = other_element_key;

          String __this__extension_copy_value = other_element_value;

          __this__extension.put(__this__extension_copy_key, __this__extension_copy_value);
        }
        this.extension = __this__extension;
      }
    }

    public respOutgoingSmsData_args deepCopy() {
      return new respOutgoingSmsData_args(this);
    }

    @Override
    public void clear() {
      setService_idIsSet(false);
      this.service_id = 0;
      setSession_idIsSet(false);
      this.session_id = 0;
      setErrorIsSet(false);
      this.error = 0;
      this.extension = null;
    }

    public int getService_id() {
      return this.service_id;
    }

    public respOutgoingSmsData_args setService_id(int service_id) {
      this.service_id = service_id;
      setService_idIsSet(true);
      return this;
    }

    public void unsetService_id() {
      __isset_bit_vector.clear(__SERVICE_ID_ISSET_ID);
    }

    /** Returns true if field service_id is set (has been assigned a value) and false otherwise */
    public boolean isSetService_id() {
      return __isset_bit_vector.get(__SERVICE_ID_ISSET_ID);
    }

    public void setService_idIsSet(boolean value) {
      __isset_bit_vector.set(__SERVICE_ID_ISSET_ID, value);
    }

    public long getSession_id() {
      return this.session_id;
    }

    public respOutgoingSmsData_args setSession_id(long session_id) {
      this.session_id = session_id;
      setSession_idIsSet(true);
      return this;
    }

    public void unsetSession_id() {
      __isset_bit_vector.clear(__SESSION_ID_ISSET_ID);
    }

    /** Returns true if field session_id is set (has been assigned a value) and false otherwise */
    public boolean isSetSession_id() {
      return __isset_bit_vector.get(__SESSION_ID_ISSET_ID);
    }

    public void setSession_idIsSet(boolean value) {
      __isset_bit_vector.set(__SESSION_ID_ISSET_ID, value);
    }

    public short getError() {
      return this.error;
    }

    public respOutgoingSmsData_args setError(short error) {
      this.error = error;
      setErrorIsSet(true);
      return this;
    }

    public void unsetError() {
      __isset_bit_vector.clear(__ERROR_ISSET_ID);
    }

    /** Returns true if field error is set (has been assigned a value) and false otherwise */
    public boolean isSetError() {
      return __isset_bit_vector.get(__ERROR_ISSET_ID);
    }

    public void setErrorIsSet(boolean value) {
      __isset_bit_vector.set(__ERROR_ISSET_ID, value);
    }

    public int getExtensionSize() {
      return (this.extension == null) ? 0 : this.extension.size();
    }

    public void putToExtension(String key, String val) {
      if (this.extension == null) {
        this.extension = new HashMap<String,String>();
      }
      this.extension.put(key, val);
    }

    public Map<String,String> getExtension() {
      return this.extension;
    }

    public respOutgoingSmsData_args setExtension(Map<String,String> extension) {
      this.extension = extension;
      return this;
    }

    public void unsetExtension() {
      this.extension = null;
    }

    /** Returns true if field extension is set (has been assigned a value) and false otherwise */
    public boolean isSetExtension() {
      return this.extension != null;
    }

    public void setExtensionIsSet(boolean value) {
      if (!value) {
        this.extension = null;
      }
    }

    public void setFieldValue(_Fields field, Object value) {
      switch (field) {
      case SERVICE_ID:
        if (value == null) {
          unsetService_id();
        } else {
          setService_id((Integer)value);
        }
        break;

      case SESSION_ID:
        if (value == null) {
          unsetSession_id();
        } else {
          setSession_id((Long)value);
        }
        break;

      case ERROR:
        if (value == null) {
          unsetError();
        } else {
          setError((Short)value);
        }
        break;

      case EXTENSION:
        if (value == null) {
          unsetExtension();
        } else {
          setExtension((Map<String,String>)value);
        }
        break;

      }
    }

    public Object getFieldValue(_Fields field) {
      switch (field) {
      case SERVICE_ID:
        return new Integer(getService_id());

      case SESSION_ID:
        return new Long(getSession_id());

      case ERROR:
        return new Short(getError());

      case EXTENSION:
        return getExtension();

      }
      throw new IllegalStateException();
    }

    /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
    public boolean isSet(_Fields field) {
      if (field == null) {
        throw new IllegalArgumentException();
      }

      switch (field) {
      case SERVICE_ID:
        return isSetService_id();
      case SESSION_ID:
        return isSetSession_id();
      case ERROR:
        return isSetError();
      case EXTENSION:
        return isSetExtension();
      }
      throw new IllegalStateException();
    }

    @Override
    public boolean equals(Object that) {
      if (that == null)
        return false;
      if (that instanceof respOutgoingSmsData_args)
        return this.equals((respOutgoingSmsData_args)that);
      return false;
    }

    public boolean equals(respOutgoingSmsData_args that) {
      if (that == null)
        return false;

      boolean this_present_service_id = true;
      boolean that_present_service_id = true;
      if (this_present_service_id || that_present_service_id) {
        if (!(this_present_service_id && that_present_service_id))
          return false;
        if (this.service_id != that.service_id)
          return false;
      }

      boolean this_present_session_id = true;
      boolean that_present_session_id = true;
      if (this_present_session_id || that_present_session_id) {
        if (!(this_present_session_id && that_present_session_id))
          return false;
        if (this.session_id != that.session_id)
          return false;
      }

      boolean this_present_error = true;
      boolean that_present_error = true;
      if (this_present_error || that_present_error) {
        if (!(this_present_error && that_present_error))
          return false;
        if (this.error != that.error)
          return false;
      }

      boolean this_present_extension = true && this.isSetExtension();
      boolean that_present_extension = true && that.isSetExtension();
      if (this_present_extension || that_present_extension) {
        if (!(this_present_extension && that_present_extension))
          return false;
        if (!this.extension.equals(that.extension))
          return false;
      }

      return true;
    }

    @Override
    public int hashCode() {
      return 0;
    }

    public int compareTo(respOutgoingSmsData_args other) {
      if (!getClass().equals(other.getClass())) {
        return getClass().getName().compareTo(other.getClass().getName());
      }

      int lastComparison = 0;
      respOutgoingSmsData_args typedOther = (respOutgoingSmsData_args)other;

      lastComparison = Boolean.valueOf(isSetService_id()).compareTo(typedOther.isSetService_id());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetService_id()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.service_id, typedOther.service_id);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetSession_id()).compareTo(typedOther.isSetSession_id());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetSession_id()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.session_id, typedOther.session_id);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetError()).compareTo(typedOther.isSetError());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetError()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.error, typedOther.error);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetExtension()).compareTo(typedOther.isSetExtension());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetExtension()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.extension, typedOther.extension);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      return 0;
    }

    public _Fields fieldForId(int fieldId) {
      return _Fields.findByThriftId(fieldId);
    }

    public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField field;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (field.id) {
          case 1: // SERVICE_ID
            if (field.type == org.apache.thrift.protocol.TType.I32) {
              this.service_id = iprot.readI32();
              setService_idIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 2: // SESSION_ID
            if (field.type == org.apache.thrift.protocol.TType.I64) {
              this.session_id = iprot.readI64();
              setSession_idIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 3: // ERROR
            if (field.type == org.apache.thrift.protocol.TType.I16) {
              this.error = iprot.readI16();
              setErrorIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 4: // EXTENSION
            if (field.type == org.apache.thrift.protocol.TType.MAP) {
              {
                org.apache.thrift.protocol.TMap _map30 = iprot.readMapBegin();
                this.extension = new HashMap<String,String>(2*_map30.size);
                for (int _i31 = 0; _i31 < _map30.size; ++_i31)
                {
                  String _key32;
                  String _val33;
                  _key32 = iprot.readString();
                  _val33 = iprot.readString();
                  this.extension.put(_key32, _val33);
                }
                iprot.readMapEnd();
              }
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(SERVICE_ID_FIELD_DESC);
      oprot.writeI32(this.service_id);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(SESSION_ID_FIELD_DESC);
      oprot.writeI64(this.session_id);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(ERROR_FIELD_DESC);
      oprot.writeI16(this.error);
      oprot.writeFieldEnd();
      if (this.extension != null) {
        oprot.writeFieldBegin(EXTENSION_FIELD_DESC);
        {
          oprot.writeMapBegin(new org.apache.thrift.protocol.TMap(org.apache.thrift.protocol.TType.STRING, org.apache.thrift.protocol.TType.STRING, this.extension.size()));
          for (Map.Entry<String, String> _iter34 : this.extension.entrySet())
          {
            oprot.writeString(_iter34.getKey());
            oprot.writeString(_iter34.getValue());
          }
          oprot.writeMapEnd();
        }
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder("respOutgoingSmsData_args(");
      boolean first = true;

      sb.append("service_id:");
      sb.append(this.service_id);
      first = false;
      if (!first) sb.append(", ");
      sb.append("session_id:");
      sb.append(this.session_id);
      first = false;
      if (!first) sb.append(", ");
      sb.append("error:");
      sb.append(this.error);
      first = false;
      if (!first) sb.append(", ");
      sb.append("extension:");
      if (this.extension == null) {
        sb.append("null");
      } else {
        sb.append(this.extension);
      }
      first = false;
      sb.append(")");
      return sb.toString();
    }

    public void validate() throws org.apache.thrift.TException {
      // check for required fields
    }

    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
      try {
        write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
      try {
        // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
        __isset_bit_vector = new BitSet(1);
        read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

  }

  public static class registerNodeRequest_args implements org.apache.thrift.TBase<registerNodeRequest_args, registerNodeRequest_args._Fields>, java.io.Serializable, Cloneable   {
    private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("registerNodeRequest_args");

    private static final org.apache.thrift.protocol.TField EXTENSION_FIELD_DESC = new org.apache.thrift.protocol.TField("extension", org.apache.thrift.protocol.TType.MAP, (short)1);

    public Map<String,String> extension;

    /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
    public enum _Fields implements org.apache.thrift.TFieldIdEnum {
      EXTENSION((short)1, "extension");

      private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

      static {
        for (_Fields field : EnumSet.allOf(_Fields.class)) {
          byName.put(field.getFieldName(), field);
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, or null if its not found.
       */
      public static _Fields findByThriftId(int fieldId) {
        switch(fieldId) {
          case 1: // EXTENSION
            return EXTENSION;
          default:
            return null;
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, throwing an exception
       * if it is not found.
       */
      public static _Fields findByThriftIdOrThrow(int fieldId) {
        _Fields fields = findByThriftId(fieldId);
        if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
        return fields;
      }

      /**
       * Find the _Fields constant that matches name, or null if its not found.
       */
      public static _Fields findByName(String name) {
        return byName.get(name);
      }

      private final short _thriftId;
      private final String _fieldName;

      _Fields(short thriftId, String fieldName) {
        _thriftId = thriftId;
        _fieldName = fieldName;
      }

      public short getThriftFieldId() {
        return _thriftId;
      }

      public String getFieldName() {
        return _fieldName;
      }
    }

    // isset id assignments

    public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
    static {
      Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
      tmpMap.put(_Fields.EXTENSION, new org.apache.thrift.meta_data.FieldMetaData("extension", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.MapMetaData(org.apache.thrift.protocol.TType.MAP, 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING), 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING))));
      metaDataMap = Collections.unmodifiableMap(tmpMap);
      org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(registerNodeRequest_args.class, metaDataMap);
    }

    public registerNodeRequest_args() {
    }

    public registerNodeRequest_args(
      Map<String,String> extension)
    {
      this();
      this.extension = extension;
    }

    /**
     * Performs a deep copy on <i>other</i>.
     */
    public registerNodeRequest_args(registerNodeRequest_args other) {
      if (other.isSetExtension()) {
        Map<String,String> __this__extension = new HashMap<String,String>();
        for (Map.Entry<String, String> other_element : other.extension.entrySet()) {

          String other_element_key = other_element.getKey();
          String other_element_value = other_element.getValue();

          String __this__extension_copy_key = other_element_key;

          String __this__extension_copy_value = other_element_value;

          __this__extension.put(__this__extension_copy_key, __this__extension_copy_value);
        }
        this.extension = __this__extension;
      }
    }

    public registerNodeRequest_args deepCopy() {
      return new registerNodeRequest_args(this);
    }

    @Override
    public void clear() {
      this.extension = null;
    }

    public int getExtensionSize() {
      return (this.extension == null) ? 0 : this.extension.size();
    }

    public void putToExtension(String key, String val) {
      if (this.extension == null) {
        this.extension = new HashMap<String,String>();
      }
      this.extension.put(key, val);
    }

    public Map<String,String> getExtension() {
      return this.extension;
    }

    public registerNodeRequest_args setExtension(Map<String,String> extension) {
      this.extension = extension;
      return this;
    }

    public void unsetExtension() {
      this.extension = null;
    }

    /** Returns true if field extension is set (has been assigned a value) and false otherwise */
    public boolean isSetExtension() {
      return this.extension != null;
    }

    public void setExtensionIsSet(boolean value) {
      if (!value) {
        this.extension = null;
      }
    }

    public void setFieldValue(_Fields field, Object value) {
      switch (field) {
      case EXTENSION:
        if (value == null) {
          unsetExtension();
        } else {
          setExtension((Map<String,String>)value);
        }
        break;

      }
    }

    public Object getFieldValue(_Fields field) {
      switch (field) {
      case EXTENSION:
        return getExtension();

      }
      throw new IllegalStateException();
    }

    /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
    public boolean isSet(_Fields field) {
      if (field == null) {
        throw new IllegalArgumentException();
      }

      switch (field) {
      case EXTENSION:
        return isSetExtension();
      }
      throw new IllegalStateException();
    }

    @Override
    public boolean equals(Object that) {
      if (that == null)
        return false;
      if (that instanceof registerNodeRequest_args)
        return this.equals((registerNodeRequest_args)that);
      return false;
    }

    public boolean equals(registerNodeRequest_args that) {
      if (that == null)
        return false;

      boolean this_present_extension = true && this.isSetExtension();
      boolean that_present_extension = true && that.isSetExtension();
      if (this_present_extension || that_present_extension) {
        if (!(this_present_extension && that_present_extension))
          return false;
        if (!this.extension.equals(that.extension))
          return false;
      }

      return true;
    }

    @Override
    public int hashCode() {
      return 0;
    }

    public int compareTo(registerNodeRequest_args other) {
      if (!getClass().equals(other.getClass())) {
        return getClass().getName().compareTo(other.getClass().getName());
      }

      int lastComparison = 0;
      registerNodeRequest_args typedOther = (registerNodeRequest_args)other;

      lastComparison = Boolean.valueOf(isSetExtension()).compareTo(typedOther.isSetExtension());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetExtension()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.extension, typedOther.extension);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      return 0;
    }

    public _Fields fieldForId(int fieldId) {
      return _Fields.findByThriftId(fieldId);
    }

    public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField field;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (field.id) {
          case 1: // EXTENSION
            if (field.type == org.apache.thrift.protocol.TType.MAP) {
              {
                org.apache.thrift.protocol.TMap _map35 = iprot.readMapBegin();
                this.extension = new HashMap<String,String>(2*_map35.size);
                for (int _i36 = 0; _i36 < _map35.size; ++_i36)
                {
                  String _key37;
                  String _val38;
                  _key37 = iprot.readString();
                  _val38 = iprot.readString();
                  this.extension.put(_key37, _val38);
                }
                iprot.readMapEnd();
              }
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (this.extension != null) {
        oprot.writeFieldBegin(EXTENSION_FIELD_DESC);
        {
          oprot.writeMapBegin(new org.apache.thrift.protocol.TMap(org.apache.thrift.protocol.TType.STRING, org.apache.thrift.protocol.TType.STRING, this.extension.size()));
          for (Map.Entry<String, String> _iter39 : this.extension.entrySet())
          {
            oprot.writeString(_iter39.getKey());
            oprot.writeString(_iter39.getValue());
          }
          oprot.writeMapEnd();
        }
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder("registerNodeRequest_args(");
      boolean first = true;

      sb.append("extension:");
      if (this.extension == null) {
        sb.append("null");
      } else {
        sb.append(this.extension);
      }
      first = false;
      sb.append(")");
      return sb.toString();
    }

    public void validate() throws org.apache.thrift.TException {
      // check for required fields
    }

    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
      try {
        write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
      try {
        read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

  }

  public static class registerNodeResponse_args implements org.apache.thrift.TBase<registerNodeResponse_args, registerNodeResponse_args._Fields>, java.io.Serializable, Cloneable   {
    private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("registerNodeResponse_args");

    private static final org.apache.thrift.protocol.TField EXTENSION_FIELD_DESC = new org.apache.thrift.protocol.TField("extension", org.apache.thrift.protocol.TType.MAP, (short)1);

    public Map<String,String> extension;

    /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
    public enum _Fields implements org.apache.thrift.TFieldIdEnum {
      EXTENSION((short)1, "extension");

      private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

      static {
        for (_Fields field : EnumSet.allOf(_Fields.class)) {
          byName.put(field.getFieldName(), field);
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, or null if its not found.
       */
      public static _Fields findByThriftId(int fieldId) {
        switch(fieldId) {
          case 1: // EXTENSION
            return EXTENSION;
          default:
            return null;
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, throwing an exception
       * if it is not found.
       */
      public static _Fields findByThriftIdOrThrow(int fieldId) {
        _Fields fields = findByThriftId(fieldId);
        if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
        return fields;
      }

      /**
       * Find the _Fields constant that matches name, or null if its not found.
       */
      public static _Fields findByName(String name) {
        return byName.get(name);
      }

      private final short _thriftId;
      private final String _fieldName;

      _Fields(short thriftId, String fieldName) {
        _thriftId = thriftId;
        _fieldName = fieldName;
      }

      public short getThriftFieldId() {
        return _thriftId;
      }

      public String getFieldName() {
        return _fieldName;
      }
    }

    // isset id assignments

    public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
    static {
      Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
      tmpMap.put(_Fields.EXTENSION, new org.apache.thrift.meta_data.FieldMetaData("extension", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.MapMetaData(org.apache.thrift.protocol.TType.MAP, 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING), 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING))));
      metaDataMap = Collections.unmodifiableMap(tmpMap);
      org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(registerNodeResponse_args.class, metaDataMap);
    }

    public registerNodeResponse_args() {
    }

    public registerNodeResponse_args(
      Map<String,String> extension)
    {
      this();
      this.extension = extension;
    }

    /**
     * Performs a deep copy on <i>other</i>.
     */
    public registerNodeResponse_args(registerNodeResponse_args other) {
      if (other.isSetExtension()) {
        Map<String,String> __this__extension = new HashMap<String,String>();
        for (Map.Entry<String, String> other_element : other.extension.entrySet()) {

          String other_element_key = other_element.getKey();
          String other_element_value = other_element.getValue();

          String __this__extension_copy_key = other_element_key;

          String __this__extension_copy_value = other_element_value;

          __this__extension.put(__this__extension_copy_key, __this__extension_copy_value);
        }
        this.extension = __this__extension;
      }
    }

    public registerNodeResponse_args deepCopy() {
      return new registerNodeResponse_args(this);
    }

    @Override
    public void clear() {
      this.extension = null;
    }

    public int getExtensionSize() {
      return (this.extension == null) ? 0 : this.extension.size();
    }

    public void putToExtension(String key, String val) {
      if (this.extension == null) {
        this.extension = new HashMap<String,String>();
      }
      this.extension.put(key, val);
    }

    public Map<String,String> getExtension() {
      return this.extension;
    }

    public registerNodeResponse_args setExtension(Map<String,String> extension) {
      this.extension = extension;
      return this;
    }

    public void unsetExtension() {
      this.extension = null;
    }

    /** Returns true if field extension is set (has been assigned a value) and false otherwise */
    public boolean isSetExtension() {
      return this.extension != null;
    }

    public void setExtensionIsSet(boolean value) {
      if (!value) {
        this.extension = null;
      }
    }

    public void setFieldValue(_Fields field, Object value) {
      switch (field) {
      case EXTENSION:
        if (value == null) {
          unsetExtension();
        } else {
          setExtension((Map<String,String>)value);
        }
        break;

      }
    }

    public Object getFieldValue(_Fields field) {
      switch (field) {
      case EXTENSION:
        return getExtension();

      }
      throw new IllegalStateException();
    }

    /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
    public boolean isSet(_Fields field) {
      if (field == null) {
        throw new IllegalArgumentException();
      }

      switch (field) {
      case EXTENSION:
        return isSetExtension();
      }
      throw new IllegalStateException();
    }

    @Override
    public boolean equals(Object that) {
      if (that == null)
        return false;
      if (that instanceof registerNodeResponse_args)
        return this.equals((registerNodeResponse_args)that);
      return false;
    }

    public boolean equals(registerNodeResponse_args that) {
      if (that == null)
        return false;

      boolean this_present_extension = true && this.isSetExtension();
      boolean that_present_extension = true && that.isSetExtension();
      if (this_present_extension || that_present_extension) {
        if (!(this_present_extension && that_present_extension))
          return false;
        if (!this.extension.equals(that.extension))
          return false;
      }

      return true;
    }

    @Override
    public int hashCode() {
      return 0;
    }

    public int compareTo(registerNodeResponse_args other) {
      if (!getClass().equals(other.getClass())) {
        return getClass().getName().compareTo(other.getClass().getName());
      }

      int lastComparison = 0;
      registerNodeResponse_args typedOther = (registerNodeResponse_args)other;

      lastComparison = Boolean.valueOf(isSetExtension()).compareTo(typedOther.isSetExtension());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetExtension()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.extension, typedOther.extension);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      return 0;
    }

    public _Fields fieldForId(int fieldId) {
      return _Fields.findByThriftId(fieldId);
    }

    public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField field;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (field.id) {
          case 1: // EXTENSION
            if (field.type == org.apache.thrift.protocol.TType.MAP) {
              {
                org.apache.thrift.protocol.TMap _map40 = iprot.readMapBegin();
                this.extension = new HashMap<String,String>(2*_map40.size);
                for (int _i41 = 0; _i41 < _map40.size; ++_i41)
                {
                  String _key42;
                  String _val43;
                  _key42 = iprot.readString();
                  _val43 = iprot.readString();
                  this.extension.put(_key42, _val43);
                }
                iprot.readMapEnd();
              }
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (this.extension != null) {
        oprot.writeFieldBegin(EXTENSION_FIELD_DESC);
        {
          oprot.writeMapBegin(new org.apache.thrift.protocol.TMap(org.apache.thrift.protocol.TType.STRING, org.apache.thrift.protocol.TType.STRING, this.extension.size()));
          for (Map.Entry<String, String> _iter44 : this.extension.entrySet())
          {
            oprot.writeString(_iter44.getKey());
            oprot.writeString(_iter44.getValue());
          }
          oprot.writeMapEnd();
        }
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder("registerNodeResponse_args(");
      boolean first = true;

      sb.append("extension:");
      if (this.extension == null) {
        sb.append("null");
      } else {
        sb.append(this.extension);
      }
      first = false;
      sb.append(")");
      return sb.toString();
    }

    public void validate() throws org.apache.thrift.TException {
      // check for required fields
    }

    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
      try {
        write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
      try {
        read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

  }

  public static class serviceTypeRequest_args implements org.apache.thrift.TBase<serviceTypeRequest_args, serviceTypeRequest_args._Fields>, java.io.Serializable, Cloneable   {
    private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("serviceTypeRequest_args");

    private static final org.apache.thrift.protocol.TField SERVICE_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("service_id", org.apache.thrift.protocol.TType.I32, (short)1);
    private static final org.apache.thrift.protocol.TField SESSION_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("session_id", org.apache.thrift.protocol.TType.I64, (short)2);
    private static final org.apache.thrift.protocol.TField MSISDN_FIELD_DESC = new org.apache.thrift.protocol.TField("msisdn", org.apache.thrift.protocol.TType.STRING, (short)3);
    private static final org.apache.thrift.protocol.TField IMSI_FIELD_DESC = new org.apache.thrift.protocol.TField("imsi", org.apache.thrift.protocol.TType.STRING, (short)4);
    private static final org.apache.thrift.protocol.TField EXTENSION_FIELD_DESC = new org.apache.thrift.protocol.TField("extension", org.apache.thrift.protocol.TType.MAP, (short)5);

    public int service_id;
    public long session_id;
    public String msisdn;
    public String imsi;
    public Map<String,String> extension;

    /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
    public enum _Fields implements org.apache.thrift.TFieldIdEnum {
      SERVICE_ID((short)1, "service_id"),
      SESSION_ID((short)2, "session_id"),
      MSISDN((short)3, "msisdn"),
      IMSI((short)4, "imsi"),
      EXTENSION((short)5, "extension");

      private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

      static {
        for (_Fields field : EnumSet.allOf(_Fields.class)) {
          byName.put(field.getFieldName(), field);
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, or null if its not found.
       */
      public static _Fields findByThriftId(int fieldId) {
        switch(fieldId) {
          case 1: // SERVICE_ID
            return SERVICE_ID;
          case 2: // SESSION_ID
            return SESSION_ID;
          case 3: // MSISDN
            return MSISDN;
          case 4: // IMSI
            return IMSI;
          case 5: // EXTENSION
            return EXTENSION;
          default:
            return null;
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, throwing an exception
       * if it is not found.
       */
      public static _Fields findByThriftIdOrThrow(int fieldId) {
        _Fields fields = findByThriftId(fieldId);
        if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
        return fields;
      }

      /**
       * Find the _Fields constant that matches name, or null if its not found.
       */
      public static _Fields findByName(String name) {
        return byName.get(name);
      }

      private final short _thriftId;
      private final String _fieldName;

      _Fields(short thriftId, String fieldName) {
        _thriftId = thriftId;
        _fieldName = fieldName;
      }

      public short getThriftFieldId() {
        return _thriftId;
      }

      public String getFieldName() {
        return _fieldName;
      }
    }

    // isset id assignments
    private static final int __SERVICE_ID_ISSET_ID = 0;
    private static final int __SESSION_ID_ISSET_ID = 1;
    private BitSet __isset_bit_vector = new BitSet(2);

    public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
    static {
      Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
      tmpMap.put(_Fields.SERVICE_ID, new org.apache.thrift.meta_data.FieldMetaData("service_id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
      tmpMap.put(_Fields.SESSION_ID, new org.apache.thrift.meta_data.FieldMetaData("session_id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
      tmpMap.put(_Fields.MSISDN, new org.apache.thrift.meta_data.FieldMetaData("msisdn", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
      tmpMap.put(_Fields.IMSI, new org.apache.thrift.meta_data.FieldMetaData("imsi", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
      tmpMap.put(_Fields.EXTENSION, new org.apache.thrift.meta_data.FieldMetaData("extension", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.MapMetaData(org.apache.thrift.protocol.TType.MAP, 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING), 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING))));
      metaDataMap = Collections.unmodifiableMap(tmpMap);
      org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(serviceTypeRequest_args.class, metaDataMap);
    }

    public serviceTypeRequest_args() {
    }

    public serviceTypeRequest_args(
      int service_id,
      long session_id,
      String msisdn,
      String imsi,
      Map<String,String> extension)
    {
      this();
      this.service_id = service_id;
      setService_idIsSet(true);
      this.session_id = session_id;
      setSession_idIsSet(true);
      this.msisdn = msisdn;
      this.imsi = imsi;
      this.extension = extension;
    }

    /**
     * Performs a deep copy on <i>other</i>.
     */
    public serviceTypeRequest_args(serviceTypeRequest_args other) {
      __isset_bit_vector.clear();
      __isset_bit_vector.or(other.__isset_bit_vector);
      this.service_id = other.service_id;
      this.session_id = other.session_id;
      if (other.isSetMsisdn()) {
        this.msisdn = other.msisdn;
      }
      if (other.isSetImsi()) {
        this.imsi = other.imsi;
      }
      if (other.isSetExtension()) {
        Map<String,String> __this__extension = new HashMap<String,String>();
        for (Map.Entry<String, String> other_element : other.extension.entrySet()) {

          String other_element_key = other_element.getKey();
          String other_element_value = other_element.getValue();

          String __this__extension_copy_key = other_element_key;

          String __this__extension_copy_value = other_element_value;

          __this__extension.put(__this__extension_copy_key, __this__extension_copy_value);
        }
        this.extension = __this__extension;
      }
    }

    public serviceTypeRequest_args deepCopy() {
      return new serviceTypeRequest_args(this);
    }

    @Override
    public void clear() {
      setService_idIsSet(false);
      this.service_id = 0;
      setSession_idIsSet(false);
      this.session_id = 0;
      this.msisdn = null;
      this.imsi = null;
      this.extension = null;
    }

    public int getService_id() {
      return this.service_id;
    }

    public serviceTypeRequest_args setService_id(int service_id) {
      this.service_id = service_id;
      setService_idIsSet(true);
      return this;
    }

    public void unsetService_id() {
      __isset_bit_vector.clear(__SERVICE_ID_ISSET_ID);
    }

    /** Returns true if field service_id is set (has been assigned a value) and false otherwise */
    public boolean isSetService_id() {
      return __isset_bit_vector.get(__SERVICE_ID_ISSET_ID);
    }

    public void setService_idIsSet(boolean value) {
      __isset_bit_vector.set(__SERVICE_ID_ISSET_ID, value);
    }

    public long getSession_id() {
      return this.session_id;
    }

    public serviceTypeRequest_args setSession_id(long session_id) {
      this.session_id = session_id;
      setSession_idIsSet(true);
      return this;
    }

    public void unsetSession_id() {
      __isset_bit_vector.clear(__SESSION_ID_ISSET_ID);
    }

    /** Returns true if field session_id is set (has been assigned a value) and false otherwise */
    public boolean isSetSession_id() {
      return __isset_bit_vector.get(__SESSION_ID_ISSET_ID);
    }

    public void setSession_idIsSet(boolean value) {
      __isset_bit_vector.set(__SESSION_ID_ISSET_ID, value);
    }

    public String getMsisdn() {
      return this.msisdn;
    }

    public serviceTypeRequest_args setMsisdn(String msisdn) {
      this.msisdn = msisdn;
      return this;
    }

    public void unsetMsisdn() {
      this.msisdn = null;
    }

    /** Returns true if field msisdn is set (has been assigned a value) and false otherwise */
    public boolean isSetMsisdn() {
      return this.msisdn != null;
    }

    public void setMsisdnIsSet(boolean value) {
      if (!value) {
        this.msisdn = null;
      }
    }

    public String getImsi() {
      return this.imsi;
    }

    public serviceTypeRequest_args setImsi(String imsi) {
      this.imsi = imsi;
      return this;
    }

    public void unsetImsi() {
      this.imsi = null;
    }

    /** Returns true if field imsi is set (has been assigned a value) and false otherwise */
    public boolean isSetImsi() {
      return this.imsi != null;
    }

    public void setImsiIsSet(boolean value) {
      if (!value) {
        this.imsi = null;
      }
    }

    public int getExtensionSize() {
      return (this.extension == null) ? 0 : this.extension.size();
    }

    public void putToExtension(String key, String val) {
      if (this.extension == null) {
        this.extension = new HashMap<String,String>();
      }
      this.extension.put(key, val);
    }

    public Map<String,String> getExtension() {
      return this.extension;
    }

    public serviceTypeRequest_args setExtension(Map<String,String> extension) {
      this.extension = extension;
      return this;
    }

    public void unsetExtension() {
      this.extension = null;
    }

    /** Returns true if field extension is set (has been assigned a value) and false otherwise */
    public boolean isSetExtension() {
      return this.extension != null;
    }

    public void setExtensionIsSet(boolean value) {
      if (!value) {
        this.extension = null;
      }
    }

    public void setFieldValue(_Fields field, Object value) {
      switch (field) {
      case SERVICE_ID:
        if (value == null) {
          unsetService_id();
        } else {
          setService_id((Integer)value);
        }
        break;

      case SESSION_ID:
        if (value == null) {
          unsetSession_id();
        } else {
          setSession_id((Long)value);
        }
        break;

      case MSISDN:
        if (value == null) {
          unsetMsisdn();
        } else {
          setMsisdn((String)value);
        }
        break;

      case IMSI:
        if (value == null) {
          unsetImsi();
        } else {
          setImsi((String)value);
        }
        break;

      case EXTENSION:
        if (value == null) {
          unsetExtension();
        } else {
          setExtension((Map<String,String>)value);
        }
        break;

      }
    }

    public Object getFieldValue(_Fields field) {
      switch (field) {
      case SERVICE_ID:
        return new Integer(getService_id());

      case SESSION_ID:
        return new Long(getSession_id());

      case MSISDN:
        return getMsisdn();

      case IMSI:
        return getImsi();

      case EXTENSION:
        return getExtension();

      }
      throw new IllegalStateException();
    }

    /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
    public boolean isSet(_Fields field) {
      if (field == null) {
        throw new IllegalArgumentException();
      }

      switch (field) {
      case SERVICE_ID:
        return isSetService_id();
      case SESSION_ID:
        return isSetSession_id();
      case MSISDN:
        return isSetMsisdn();
      case IMSI:
        return isSetImsi();
      case EXTENSION:
        return isSetExtension();
      }
      throw new IllegalStateException();
    }

    @Override
    public boolean equals(Object that) {
      if (that == null)
        return false;
      if (that instanceof serviceTypeRequest_args)
        return this.equals((serviceTypeRequest_args)that);
      return false;
    }

    public boolean equals(serviceTypeRequest_args that) {
      if (that == null)
        return false;

      boolean this_present_service_id = true;
      boolean that_present_service_id = true;
      if (this_present_service_id || that_present_service_id) {
        if (!(this_present_service_id && that_present_service_id))
          return false;
        if (this.service_id != that.service_id)
          return false;
      }

      boolean this_present_session_id = true;
      boolean that_present_session_id = true;
      if (this_present_session_id || that_present_session_id) {
        if (!(this_present_session_id && that_present_session_id))
          return false;
        if (this.session_id != that.session_id)
          return false;
      }

      boolean this_present_msisdn = true && this.isSetMsisdn();
      boolean that_present_msisdn = true && that.isSetMsisdn();
      if (this_present_msisdn || that_present_msisdn) {
        if (!(this_present_msisdn && that_present_msisdn))
          return false;
        if (!this.msisdn.equals(that.msisdn))
          return false;
      }

      boolean this_present_imsi = true && this.isSetImsi();
      boolean that_present_imsi = true && that.isSetImsi();
      if (this_present_imsi || that_present_imsi) {
        if (!(this_present_imsi && that_present_imsi))
          return false;
        if (!this.imsi.equals(that.imsi))
          return false;
      }

      boolean this_present_extension = true && this.isSetExtension();
      boolean that_present_extension = true && that.isSetExtension();
      if (this_present_extension || that_present_extension) {
        if (!(this_present_extension && that_present_extension))
          return false;
        if (!this.extension.equals(that.extension))
          return false;
      }

      return true;
    }

    @Override
    public int hashCode() {
      return 0;
    }

    public int compareTo(serviceTypeRequest_args other) {
      if (!getClass().equals(other.getClass())) {
        return getClass().getName().compareTo(other.getClass().getName());
      }

      int lastComparison = 0;
      serviceTypeRequest_args typedOther = (serviceTypeRequest_args)other;

      lastComparison = Boolean.valueOf(isSetService_id()).compareTo(typedOther.isSetService_id());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetService_id()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.service_id, typedOther.service_id);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetSession_id()).compareTo(typedOther.isSetSession_id());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetSession_id()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.session_id, typedOther.session_id);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetMsisdn()).compareTo(typedOther.isSetMsisdn());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetMsisdn()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.msisdn, typedOther.msisdn);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetImsi()).compareTo(typedOther.isSetImsi());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetImsi()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.imsi, typedOther.imsi);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetExtension()).compareTo(typedOther.isSetExtension());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetExtension()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.extension, typedOther.extension);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      return 0;
    }

    public _Fields fieldForId(int fieldId) {
      return _Fields.findByThriftId(fieldId);
    }

    public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField field;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (field.id) {
          case 1: // SERVICE_ID
            if (field.type == org.apache.thrift.protocol.TType.I32) {
              this.service_id = iprot.readI32();
              setService_idIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 2: // SESSION_ID
            if (field.type == org.apache.thrift.protocol.TType.I64) {
              this.session_id = iprot.readI64();
              setSession_idIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 3: // MSISDN
            if (field.type == org.apache.thrift.protocol.TType.STRING) {
              this.msisdn = iprot.readString();
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 4: // IMSI
            if (field.type == org.apache.thrift.protocol.TType.STRING) {
              this.imsi = iprot.readString();
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 5: // EXTENSION
            if (field.type == org.apache.thrift.protocol.TType.MAP) {
              {
                org.apache.thrift.protocol.TMap _map45 = iprot.readMapBegin();
                this.extension = new HashMap<String,String>(2*_map45.size);
                for (int _i46 = 0; _i46 < _map45.size; ++_i46)
                {
                  String _key47;
                  String _val48;
                  _key47 = iprot.readString();
                  _val48 = iprot.readString();
                  this.extension.put(_key47, _val48);
                }
                iprot.readMapEnd();
              }
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(SERVICE_ID_FIELD_DESC);
      oprot.writeI32(this.service_id);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(SESSION_ID_FIELD_DESC);
      oprot.writeI64(this.session_id);
      oprot.writeFieldEnd();
      if (this.msisdn != null) {
        oprot.writeFieldBegin(MSISDN_FIELD_DESC);
        oprot.writeString(this.msisdn);
        oprot.writeFieldEnd();
      }
      if (this.imsi != null) {
        oprot.writeFieldBegin(IMSI_FIELD_DESC);
        oprot.writeString(this.imsi);
        oprot.writeFieldEnd();
      }
      if (this.extension != null) {
        oprot.writeFieldBegin(EXTENSION_FIELD_DESC);
        {
          oprot.writeMapBegin(new org.apache.thrift.protocol.TMap(org.apache.thrift.protocol.TType.STRING, org.apache.thrift.protocol.TType.STRING, this.extension.size()));
          for (Map.Entry<String, String> _iter49 : this.extension.entrySet())
          {
            oprot.writeString(_iter49.getKey());
            oprot.writeString(_iter49.getValue());
          }
          oprot.writeMapEnd();
        }
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder("serviceTypeRequest_args(");
      boolean first = true;

      sb.append("service_id:");
      sb.append(this.service_id);
      first = false;
      if (!first) sb.append(", ");
      sb.append("session_id:");
      sb.append(this.session_id);
      first = false;
      if (!first) sb.append(", ");
      sb.append("msisdn:");
      if (this.msisdn == null) {
        sb.append("null");
      } else {
        sb.append(this.msisdn);
      }
      first = false;
      if (!first) sb.append(", ");
      sb.append("imsi:");
      if (this.imsi == null) {
        sb.append("null");
      } else {
        sb.append(this.imsi);
      }
      first = false;
      if (!first) sb.append(", ");
      sb.append("extension:");
      if (this.extension == null) {
        sb.append("null");
      } else {
        sb.append(this.extension);
      }
      first = false;
      sb.append(")");
      return sb.toString();
    }

    public void validate() throws org.apache.thrift.TException {
      // check for required fields
    }

    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
      try {
        write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
      try {
        // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
        __isset_bit_vector = new BitSet(1);
        read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

  }

  public static class subscriberInfoResp_args implements org.apache.thrift.TBase<subscriberInfoResp_args, subscriberInfoResp_args._Fields>, java.io.Serializable, Cloneable   {
    private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("subscriberInfoResp_args");

    private static final org.apache.thrift.protocol.TField SERVICE_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("service_id", org.apache.thrift.protocol.TType.I32, (short)1);
    private static final org.apache.thrift.protocol.TField SESSION_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("session_id", org.apache.thrift.protocol.TType.I64, (short)2);
    private static final org.apache.thrift.protocol.TField MSISDN_FIELD_DESC = new org.apache.thrift.protocol.TField("msisdn", org.apache.thrift.protocol.TType.STRING, (short)3);
    private static final org.apache.thrift.protocol.TField EXTENSION_FIELD_DESC = new org.apache.thrift.protocol.TField("extension", org.apache.thrift.protocol.TType.MAP, (short)4);

    public int service_id;
    public long session_id;
    public String msisdn;
    public Map<String,String> extension;

    /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
    public enum _Fields implements org.apache.thrift.TFieldIdEnum {
      SERVICE_ID((short)1, "service_id"),
      SESSION_ID((short)2, "session_id"),
      MSISDN((short)3, "msisdn"),
      EXTENSION((short)4, "extension");

      private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

      static {
        for (_Fields field : EnumSet.allOf(_Fields.class)) {
          byName.put(field.getFieldName(), field);
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, or null if its not found.
       */
      public static _Fields findByThriftId(int fieldId) {
        switch(fieldId) {
          case 1: // SERVICE_ID
            return SERVICE_ID;
          case 2: // SESSION_ID
            return SESSION_ID;
          case 3: // MSISDN
            return MSISDN;
          case 4: // EXTENSION
            return EXTENSION;
          default:
            return null;
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, throwing an exception
       * if it is not found.
       */
      public static _Fields findByThriftIdOrThrow(int fieldId) {
        _Fields fields = findByThriftId(fieldId);
        if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
        return fields;
      }

      /**
       * Find the _Fields constant that matches name, or null if its not found.
       */
      public static _Fields findByName(String name) {
        return byName.get(name);
      }

      private final short _thriftId;
      private final String _fieldName;

      _Fields(short thriftId, String fieldName) {
        _thriftId = thriftId;
        _fieldName = fieldName;
      }

      public short getThriftFieldId() {
        return _thriftId;
      }

      public String getFieldName() {
        return _fieldName;
      }
    }

    // isset id assignments
    private static final int __SERVICE_ID_ISSET_ID = 0;
    private static final int __SESSION_ID_ISSET_ID = 1;
    private BitSet __isset_bit_vector = new BitSet(2);

    public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
    static {
      Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
      tmpMap.put(_Fields.SERVICE_ID, new org.apache.thrift.meta_data.FieldMetaData("service_id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
      tmpMap.put(_Fields.SESSION_ID, new org.apache.thrift.meta_data.FieldMetaData("session_id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
      tmpMap.put(_Fields.MSISDN, new org.apache.thrift.meta_data.FieldMetaData("msisdn", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
      tmpMap.put(_Fields.EXTENSION, new org.apache.thrift.meta_data.FieldMetaData("extension", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.MapMetaData(org.apache.thrift.protocol.TType.MAP, 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING), 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING))));
      metaDataMap = Collections.unmodifiableMap(tmpMap);
      org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(subscriberInfoResp_args.class, metaDataMap);
    }

    public subscriberInfoResp_args() {
    }

    public subscriberInfoResp_args(
      int service_id,
      long session_id,
      String msisdn,
      Map<String,String> extension)
    {
      this();
      this.service_id = service_id;
      setService_idIsSet(true);
      this.session_id = session_id;
      setSession_idIsSet(true);
      this.msisdn = msisdn;
      this.extension = extension;
    }

    /**
     * Performs a deep copy on <i>other</i>.
     */
    public subscriberInfoResp_args(subscriberInfoResp_args other) {
      __isset_bit_vector.clear();
      __isset_bit_vector.or(other.__isset_bit_vector);
      this.service_id = other.service_id;
      this.session_id = other.session_id;
      if (other.isSetMsisdn()) {
        this.msisdn = other.msisdn;
      }
      if (other.isSetExtension()) {
        Map<String,String> __this__extension = new HashMap<String,String>();
        for (Map.Entry<String, String> other_element : other.extension.entrySet()) {

          String other_element_key = other_element.getKey();
          String other_element_value = other_element.getValue();

          String __this__extension_copy_key = other_element_key;

          String __this__extension_copy_value = other_element_value;

          __this__extension.put(__this__extension_copy_key, __this__extension_copy_value);
        }
        this.extension = __this__extension;
      }
    }

    public subscriberInfoResp_args deepCopy() {
      return new subscriberInfoResp_args(this);
    }

    @Override
    public void clear() {
      setService_idIsSet(false);
      this.service_id = 0;
      setSession_idIsSet(false);
      this.session_id = 0;
      this.msisdn = null;
      this.extension = null;
    }

    public int getService_id() {
      return this.service_id;
    }

    public subscriberInfoResp_args setService_id(int service_id) {
      this.service_id = service_id;
      setService_idIsSet(true);
      return this;
    }

    public void unsetService_id() {
      __isset_bit_vector.clear(__SERVICE_ID_ISSET_ID);
    }

    /** Returns true if field service_id is set (has been assigned a value) and false otherwise */
    public boolean isSetService_id() {
      return __isset_bit_vector.get(__SERVICE_ID_ISSET_ID);
    }

    public void setService_idIsSet(boolean value) {
      __isset_bit_vector.set(__SERVICE_ID_ISSET_ID, value);
    }

    public long getSession_id() {
      return this.session_id;
    }

    public subscriberInfoResp_args setSession_id(long session_id) {
      this.session_id = session_id;
      setSession_idIsSet(true);
      return this;
    }

    public void unsetSession_id() {
      __isset_bit_vector.clear(__SESSION_ID_ISSET_ID);
    }

    /** Returns true if field session_id is set (has been assigned a value) and false otherwise */
    public boolean isSetSession_id() {
      return __isset_bit_vector.get(__SESSION_ID_ISSET_ID);
    }

    public void setSession_idIsSet(boolean value) {
      __isset_bit_vector.set(__SESSION_ID_ISSET_ID, value);
    }

    public String getMsisdn() {
      return this.msisdn;
    }

    public subscriberInfoResp_args setMsisdn(String msisdn) {
      this.msisdn = msisdn;
      return this;
    }

    public void unsetMsisdn() {
      this.msisdn = null;
    }

    /** Returns true if field msisdn is set (has been assigned a value) and false otherwise */
    public boolean isSetMsisdn() {
      return this.msisdn != null;
    }

    public void setMsisdnIsSet(boolean value) {
      if (!value) {
        this.msisdn = null;
      }
    }

    public int getExtensionSize() {
      return (this.extension == null) ? 0 : this.extension.size();
    }

    public void putToExtension(String key, String val) {
      if (this.extension == null) {
        this.extension = new HashMap<String,String>();
      }
      this.extension.put(key, val);
    }

    public Map<String,String> getExtension() {
      return this.extension;
    }

    public subscriberInfoResp_args setExtension(Map<String,String> extension) {
      this.extension = extension;
      return this;
    }

    public void unsetExtension() {
      this.extension = null;
    }

    /** Returns true if field extension is set (has been assigned a value) and false otherwise */
    public boolean isSetExtension() {
      return this.extension != null;
    }

    public void setExtensionIsSet(boolean value) {
      if (!value) {
        this.extension = null;
      }
    }

    public void setFieldValue(_Fields field, Object value) {
      switch (field) {
      case SERVICE_ID:
        if (value == null) {
          unsetService_id();
        } else {
          setService_id((Integer)value);
        }
        break;

      case SESSION_ID:
        if (value == null) {
          unsetSession_id();
        } else {
          setSession_id((Long)value);
        }
        break;

      case MSISDN:
        if (value == null) {
          unsetMsisdn();
        } else {
          setMsisdn((String)value);
        }
        break;

      case EXTENSION:
        if (value == null) {
          unsetExtension();
        } else {
          setExtension((Map<String,String>)value);
        }
        break;

      }
    }

    public Object getFieldValue(_Fields field) {
      switch (field) {
      case SERVICE_ID:
        return new Integer(getService_id());

      case SESSION_ID:
        return new Long(getSession_id());

      case MSISDN:
        return getMsisdn();

      case EXTENSION:
        return getExtension();

      }
      throw new IllegalStateException();
    }

    /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
    public boolean isSet(_Fields field) {
      if (field == null) {
        throw new IllegalArgumentException();
      }

      switch (field) {
      case SERVICE_ID:
        return isSetService_id();
      case SESSION_ID:
        return isSetSession_id();
      case MSISDN:
        return isSetMsisdn();
      case EXTENSION:
        return isSetExtension();
      }
      throw new IllegalStateException();
    }

    @Override
    public boolean equals(Object that) {
      if (that == null)
        return false;
      if (that instanceof subscriberInfoResp_args)
        return this.equals((subscriberInfoResp_args)that);
      return false;
    }

    public boolean equals(subscriberInfoResp_args that) {
      if (that == null)
        return false;

      boolean this_present_service_id = true;
      boolean that_present_service_id = true;
      if (this_present_service_id || that_present_service_id) {
        if (!(this_present_service_id && that_present_service_id))
          return false;
        if (this.service_id != that.service_id)
          return false;
      }

      boolean this_present_session_id = true;
      boolean that_present_session_id = true;
      if (this_present_session_id || that_present_session_id) {
        if (!(this_present_session_id && that_present_session_id))
          return false;
        if (this.session_id != that.session_id)
          return false;
      }

      boolean this_present_msisdn = true && this.isSetMsisdn();
      boolean that_present_msisdn = true && that.isSetMsisdn();
      if (this_present_msisdn || that_present_msisdn) {
        if (!(this_present_msisdn && that_present_msisdn))
          return false;
        if (!this.msisdn.equals(that.msisdn))
          return false;
      }

      boolean this_present_extension = true && this.isSetExtension();
      boolean that_present_extension = true && that.isSetExtension();
      if (this_present_extension || that_present_extension) {
        if (!(this_present_extension && that_present_extension))
          return false;
        if (!this.extension.equals(that.extension))
          return false;
      }

      return true;
    }

    @Override
    public int hashCode() {
      return 0;
    }

    public int compareTo(subscriberInfoResp_args other) {
      if (!getClass().equals(other.getClass())) {
        return getClass().getName().compareTo(other.getClass().getName());
      }

      int lastComparison = 0;
      subscriberInfoResp_args typedOther = (subscriberInfoResp_args)other;

      lastComparison = Boolean.valueOf(isSetService_id()).compareTo(typedOther.isSetService_id());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetService_id()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.service_id, typedOther.service_id);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetSession_id()).compareTo(typedOther.isSetSession_id());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetSession_id()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.session_id, typedOther.session_id);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetMsisdn()).compareTo(typedOther.isSetMsisdn());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetMsisdn()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.msisdn, typedOther.msisdn);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetExtension()).compareTo(typedOther.isSetExtension());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetExtension()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.extension, typedOther.extension);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      return 0;
    }

    public _Fields fieldForId(int fieldId) {
      return _Fields.findByThriftId(fieldId);
    }

    public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField field;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (field.id) {
          case 1: // SERVICE_ID
            if (field.type == org.apache.thrift.protocol.TType.I32) {
              this.service_id = iprot.readI32();
              setService_idIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 2: // SESSION_ID
            if (field.type == org.apache.thrift.protocol.TType.I64) {
              this.session_id = iprot.readI64();
              setSession_idIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 3: // MSISDN
            if (field.type == org.apache.thrift.protocol.TType.STRING) {
              this.msisdn = iprot.readString();
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 4: // EXTENSION
            if (field.type == org.apache.thrift.protocol.TType.MAP) {
              {
                org.apache.thrift.protocol.TMap _map50 = iprot.readMapBegin();
                this.extension = new HashMap<String,String>(2*_map50.size);
                for (int _i51 = 0; _i51 < _map50.size; ++_i51)
                {
                  String _key52;
                  String _val53;
                  _key52 = iprot.readString();
                  _val53 = iprot.readString();
                  this.extension.put(_key52, _val53);
                }
                iprot.readMapEnd();
              }
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(SERVICE_ID_FIELD_DESC);
      oprot.writeI32(this.service_id);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(SESSION_ID_FIELD_DESC);
      oprot.writeI64(this.session_id);
      oprot.writeFieldEnd();
      if (this.msisdn != null) {
        oprot.writeFieldBegin(MSISDN_FIELD_DESC);
        oprot.writeString(this.msisdn);
        oprot.writeFieldEnd();
      }
      if (this.extension != null) {
        oprot.writeFieldBegin(EXTENSION_FIELD_DESC);
        {
          oprot.writeMapBegin(new org.apache.thrift.protocol.TMap(org.apache.thrift.protocol.TType.STRING, org.apache.thrift.protocol.TType.STRING, this.extension.size()));
          for (Map.Entry<String, String> _iter54 : this.extension.entrySet())
          {
            oprot.writeString(_iter54.getKey());
            oprot.writeString(_iter54.getValue());
          }
          oprot.writeMapEnd();
        }
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder("subscriberInfoResp_args(");
      boolean first = true;

      sb.append("service_id:");
      sb.append(this.service_id);
      first = false;
      if (!first) sb.append(", ");
      sb.append("session_id:");
      sb.append(this.session_id);
      first = false;
      if (!first) sb.append(", ");
      sb.append("msisdn:");
      if (this.msisdn == null) {
        sb.append("null");
      } else {
        sb.append(this.msisdn);
      }
      first = false;
      if (!first) sb.append(", ");
      sb.append("extension:");
      if (this.extension == null) {
        sb.append("null");
      } else {
        sb.append(this.extension);
      }
      first = false;
      sb.append(")");
      return sb.toString();
    }

    public void validate() throws org.apache.thrift.TException {
      // check for required fields
    }

    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
      try {
        write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
      try {
        // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
        __isset_bit_vector = new BitSet(1);
        read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

  }

  public static class respSriSm_args implements org.apache.thrift.TBase<respSriSm_args, respSriSm_args._Fields>, java.io.Serializable, Cloneable   {
    private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("respSriSm_args");

    private static final org.apache.thrift.protocol.TField SERVICE_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("service_id", org.apache.thrift.protocol.TType.I32, (short)1);
    private static final org.apache.thrift.protocol.TField SESSION_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("session_id", org.apache.thrift.protocol.TType.I64, (short)2);
    private static final org.apache.thrift.protocol.TField IMSI_FIELD_DESC = new org.apache.thrift.protocol.TField("imsi", org.apache.thrift.protocol.TType.STRING, (short)3);
    private static final org.apache.thrift.protocol.TField EXTENSION_FIELD_DESC = new org.apache.thrift.protocol.TField("extension", org.apache.thrift.protocol.TType.MAP, (short)4);

    public int service_id;
    public long session_id;
    public String imsi;
    public Map<String,String> extension;

    /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
    public enum _Fields implements org.apache.thrift.TFieldIdEnum {
      SERVICE_ID((short)1, "service_id"),
      SESSION_ID((short)2, "session_id"),
      IMSI((short)3, "imsi"),
      EXTENSION((short)4, "extension");

      private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

      static {
        for (_Fields field : EnumSet.allOf(_Fields.class)) {
          byName.put(field.getFieldName(), field);
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, or null if its not found.
       */
      public static _Fields findByThriftId(int fieldId) {
        switch(fieldId) {
          case 1: // SERVICE_ID
            return SERVICE_ID;
          case 2: // SESSION_ID
            return SESSION_ID;
          case 3: // IMSI
            return IMSI;
          case 4: // EXTENSION
            return EXTENSION;
          default:
            return null;
        }
      }

      /**
       * Find the _Fields constant that matches fieldId, throwing an exception
       * if it is not found.
       */
      public static _Fields findByThriftIdOrThrow(int fieldId) {
        _Fields fields = findByThriftId(fieldId);
        if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
        return fields;
      }

      /**
       * Find the _Fields constant that matches name, or null if its not found.
       */
      public static _Fields findByName(String name) {
        return byName.get(name);
      }

      private final short _thriftId;
      private final String _fieldName;

      _Fields(short thriftId, String fieldName) {
        _thriftId = thriftId;
        _fieldName = fieldName;
      }

      public short getThriftFieldId() {
        return _thriftId;
      }

      public String getFieldName() {
        return _fieldName;
      }
    }

    // isset id assignments
    private static final int __SERVICE_ID_ISSET_ID = 0;
    private static final int __SESSION_ID_ISSET_ID = 1;
    private BitSet __isset_bit_vector = new BitSet(2);

    public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
    static {
      Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
      tmpMap.put(_Fields.SERVICE_ID, new org.apache.thrift.meta_data.FieldMetaData("service_id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
      tmpMap.put(_Fields.SESSION_ID, new org.apache.thrift.meta_data.FieldMetaData("session_id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
      tmpMap.put(_Fields.IMSI, new org.apache.thrift.meta_data.FieldMetaData("imsi", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
      tmpMap.put(_Fields.EXTENSION, new org.apache.thrift.meta_data.FieldMetaData("extension", org.apache.thrift.TFieldRequirementType.DEFAULT, 
          new org.apache.thrift.meta_data.MapMetaData(org.apache.thrift.protocol.TType.MAP, 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING), 
              new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING))));
      metaDataMap = Collections.unmodifiableMap(tmpMap);
      org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(respSriSm_args.class, metaDataMap);
    }

    public respSriSm_args() {
    }

    public respSriSm_args(
      int service_id,
      long session_id,
      String imsi,
      Map<String,String> extension)
    {
      this();
      this.service_id = service_id;
      setService_idIsSet(true);
      this.session_id = session_id;
      setSession_idIsSet(true);
      this.imsi = imsi;
      this.extension = extension;
    }

    /**
     * Performs a deep copy on <i>other</i>.
     */
    public respSriSm_args(respSriSm_args other) {
      __isset_bit_vector.clear();
      __isset_bit_vector.or(other.__isset_bit_vector);
      this.service_id = other.service_id;
      this.session_id = other.session_id;
      if (other.isSetImsi()) {
        this.imsi = other.imsi;
      }
      if (other.isSetExtension()) {
        Map<String,String> __this__extension = new HashMap<String,String>();
        for (Map.Entry<String, String> other_element : other.extension.entrySet()) {

          String other_element_key = other_element.getKey();
          String other_element_value = other_element.getValue();

          String __this__extension_copy_key = other_element_key;

          String __this__extension_copy_value = other_element_value;

          __this__extension.put(__this__extension_copy_key, __this__extension_copy_value);
        }
        this.extension = __this__extension;
      }
    }

    public respSriSm_args deepCopy() {
      return new respSriSm_args(this);
    }

    @Override
    public void clear() {
      setService_idIsSet(false);
      this.service_id = 0;
      setSession_idIsSet(false);
      this.session_id = 0;
      this.imsi = null;
      this.extension = null;
    }

    public int getService_id() {
      return this.service_id;
    }

    public respSriSm_args setService_id(int service_id) {
      this.service_id = service_id;
      setService_idIsSet(true);
      return this;
    }

    public void unsetService_id() {
      __isset_bit_vector.clear(__SERVICE_ID_ISSET_ID);
    }

    /** Returns true if field service_id is set (has been assigned a value) and false otherwise */
    public boolean isSetService_id() {
      return __isset_bit_vector.get(__SERVICE_ID_ISSET_ID);
    }

    public void setService_idIsSet(boolean value) {
      __isset_bit_vector.set(__SERVICE_ID_ISSET_ID, value);
    }

    public long getSession_id() {
      return this.session_id;
    }

    public respSriSm_args setSession_id(long session_id) {
      this.session_id = session_id;
      setSession_idIsSet(true);
      return this;
    }

    public void unsetSession_id() {
      __isset_bit_vector.clear(__SESSION_ID_ISSET_ID);
    }

    /** Returns true if field session_id is set (has been assigned a value) and false otherwise */
    public boolean isSetSession_id() {
      return __isset_bit_vector.get(__SESSION_ID_ISSET_ID);
    }

    public void setSession_idIsSet(boolean value) {
      __isset_bit_vector.set(__SESSION_ID_ISSET_ID, value);
    }

    public String getImsi() {
      return this.imsi;
    }

    public respSriSm_args setImsi(String imsi) {
      this.imsi = imsi;
      return this;
    }

    public void unsetImsi() {
      this.imsi = null;
    }

    /** Returns true if field imsi is set (has been assigned a value) and false otherwise */
    public boolean isSetImsi() {
      return this.imsi != null;
    }

    public void setImsiIsSet(boolean value) {
      if (!value) {
        this.imsi = null;
      }
    }

    public int getExtensionSize() {
      return (this.extension == null) ? 0 : this.extension.size();
    }

    public void putToExtension(String key, String val) {
      if (this.extension == null) {
        this.extension = new HashMap<String,String>();
      }
      this.extension.put(key, val);
    }

    public Map<String,String> getExtension() {
      return this.extension;
    }

    public respSriSm_args setExtension(Map<String,String> extension) {
      this.extension = extension;
      return this;
    }

    public void unsetExtension() {
      this.extension = null;
    }

    /** Returns true if field extension is set (has been assigned a value) and false otherwise */
    public boolean isSetExtension() {
      return this.extension != null;
    }

    public void setExtensionIsSet(boolean value) {
      if (!value) {
        this.extension = null;
      }
    }

    public void setFieldValue(_Fields field, Object value) {
      switch (field) {
      case SERVICE_ID:
        if (value == null) {
          unsetService_id();
        } else {
          setService_id((Integer)value);
        }
        break;

      case SESSION_ID:
        if (value == null) {
          unsetSession_id();
        } else {
          setSession_id((Long)value);
        }
        break;

      case IMSI:
        if (value == null) {
          unsetImsi();
        } else {
          setImsi((String)value);
        }
        break;

      case EXTENSION:
        if (value == null) {
          unsetExtension();
        } else {
          setExtension((Map<String,String>)value);
        }
        break;

      }
    }

    public Object getFieldValue(_Fields field) {
      switch (field) {
      case SERVICE_ID:
        return new Integer(getService_id());

      case SESSION_ID:
        return new Long(getSession_id());

      case IMSI:
        return getImsi();

      case EXTENSION:
        return getExtension();

      }
      throw new IllegalStateException();
    }

    /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
    public boolean isSet(_Fields field) {
      if (field == null) {
        throw new IllegalArgumentException();
      }

      switch (field) {
      case SERVICE_ID:
        return isSetService_id();
      case SESSION_ID:
        return isSetSession_id();
      case IMSI:
        return isSetImsi();
      case EXTENSION:
        return isSetExtension();
      }
      throw new IllegalStateException();
    }

    @Override
    public boolean equals(Object that) {
      if (that == null)
        return false;
      if (that instanceof respSriSm_args)
        return this.equals((respSriSm_args)that);
      return false;
    }

    public boolean equals(respSriSm_args that) {
      if (that == null)
        return false;

      boolean this_present_service_id = true;
      boolean that_present_service_id = true;
      if (this_present_service_id || that_present_service_id) {
        if (!(this_present_service_id && that_present_service_id))
          return false;
        if (this.service_id != that.service_id)
          return false;
      }

      boolean this_present_session_id = true;
      boolean that_present_session_id = true;
      if (this_present_session_id || that_present_session_id) {
        if (!(this_present_session_id && that_present_session_id))
          return false;
        if (this.session_id != that.session_id)
          return false;
      }

      boolean this_present_imsi = true && this.isSetImsi();
      boolean that_present_imsi = true && that.isSetImsi();
      if (this_present_imsi || that_present_imsi) {
        if (!(this_present_imsi && that_present_imsi))
          return false;
        if (!this.imsi.equals(that.imsi))
          return false;
      }

      boolean this_present_extension = true && this.isSetExtension();
      boolean that_present_extension = true && that.isSetExtension();
      if (this_present_extension || that_present_extension) {
        if (!(this_present_extension && that_present_extension))
          return false;
        if (!this.extension.equals(that.extension))
          return false;
      }

      return true;
    }

    @Override
    public int hashCode() {
      return 0;
    }

    public int compareTo(respSriSm_args other) {
      if (!getClass().equals(other.getClass())) {
        return getClass().getName().compareTo(other.getClass().getName());
      }

      int lastComparison = 0;
      respSriSm_args typedOther = (respSriSm_args)other;

      lastComparison = Boolean.valueOf(isSetService_id()).compareTo(typedOther.isSetService_id());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetService_id()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.service_id, typedOther.service_id);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetSession_id()).compareTo(typedOther.isSetSession_id());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetSession_id()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.session_id, typedOther.session_id);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetImsi()).compareTo(typedOther.isSetImsi());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetImsi()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.imsi, typedOther.imsi);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      lastComparison = Boolean.valueOf(isSetExtension()).compareTo(typedOther.isSetExtension());
      if (lastComparison != 0) {
        return lastComparison;
      }
      if (isSetExtension()) {
        lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.extension, typedOther.extension);
        if (lastComparison != 0) {
          return lastComparison;
        }
      }
      return 0;
    }

    public _Fields fieldForId(int fieldId) {
      return _Fields.findByThriftId(fieldId);
    }

    public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField field;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (field.id) {
          case 1: // SERVICE_ID
            if (field.type == org.apache.thrift.protocol.TType.I32) {
              this.service_id = iprot.readI32();
              setService_idIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 2: // SESSION_ID
            if (field.type == org.apache.thrift.protocol.TType.I64) {
              this.session_id = iprot.readI64();
              setSession_idIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 3: // IMSI
            if (field.type == org.apache.thrift.protocol.TType.STRING) {
              this.imsi = iprot.readString();
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case 4: // EXTENSION
            if (field.type == org.apache.thrift.protocol.TType.MAP) {
              {
                org.apache.thrift.protocol.TMap _map55 = iprot.readMapBegin();
                this.extension = new HashMap<String,String>(2*_map55.size);
                for (int _i56 = 0; _i56 < _map55.size; ++_i56)
                {
                  String _key57;
                  String _val58;
                  _key57 = iprot.readString();
                  _val58 = iprot.readString();
                  this.extension.put(_key57, _val58);
                }
                iprot.readMapEnd();
              }
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, field.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(SERVICE_ID_FIELD_DESC);
      oprot.writeI32(this.service_id);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(SESSION_ID_FIELD_DESC);
      oprot.writeI64(this.session_id);
      oprot.writeFieldEnd();
      if (this.imsi != null) {
        oprot.writeFieldBegin(IMSI_FIELD_DESC);
        oprot.writeString(this.imsi);
        oprot.writeFieldEnd();
      }
      if (this.extension != null) {
        oprot.writeFieldBegin(EXTENSION_FIELD_DESC);
        {
          oprot.writeMapBegin(new org.apache.thrift.protocol.TMap(org.apache.thrift.protocol.TType.STRING, org.apache.thrift.protocol.TType.STRING, this.extension.size()));
          for (Map.Entry<String, String> _iter59 : this.extension.entrySet())
          {
            oprot.writeString(_iter59.getKey());
            oprot.writeString(_iter59.getValue());
          }
          oprot.writeMapEnd();
        }
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder("respSriSm_args(");
      boolean first = true;

      sb.append("service_id:");
      sb.append(this.service_id);
      first = false;
      if (!first) sb.append(", ");
      sb.append("session_id:");
      sb.append(this.session_id);
      first = false;
      if (!first) sb.append(", ");
      sb.append("imsi:");
      if (this.imsi == null) {
        sb.append("null");
      } else {
        sb.append(this.imsi);
      }
      first = false;
      if (!first) sb.append(", ");
      sb.append("extension:");
      if (this.extension == null) {
        sb.append("null");
      } else {
        sb.append(this.extension);
      }
      first = false;
      sb.append(")");
      return sb.toString();
    }

    public void validate() throws org.apache.thrift.TException {
      // check for required fields
    }

    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
      try {
        write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
      try {
        // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
        __isset_bit_vector = new BitSet(1);
        read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
      } catch (org.apache.thrift.TException te) {
        throw new java.io.IOException(te);
      }
    }

  }

}

package com.movirtu.thrift.connection;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.map.MapJavaThriftHandler;
import com.movirtu.map.message.LUResponse;
import com.movirtu.map.message.MsrnRequest;
import com.movirtu.map.message.SRISMResponse;
import com.movirtu.thrift.generated.MapJavagw;
import com.movirtu.thrift.generated.ServiceAppType;
import com.movirtu.thrift.generated.TypeOfService;
import com.movirtu.ussd.thrift.UssdHandler;
import com.movirtu.ussd.thrift.VspUssdHandlerImpl;
import com.movirtu.vsp.persistence.dao.hb.PrimaryMsisdnDao;
import com.movirtu.vsp.persistence.dao.hb.SettingsDao;
import com.movirtu.vsp.persistence.dao.hb.impl.PrimaryMsisdnDaoImpl;
import com.movirtu.vsp.persistence.dao.hb.impl.SettingsDaoImpl;
import com.movirtu.vsp.persistence.domain.PrimaryMsisdn;
import com.movirtu.vsp.persistence.domain.SubscriberInfo;
import com.movirtu.vsp.persistence.exceptions.RecordNotFoundException;

public class MapJavagwProcessor extends Observable implements MapJavagw.Iface
{
	private static Logger logger = LoggerFactory.getLogger(MapJavagwProcessor.class);
	private UssdHandler ussd;
	private static SettingsDao settingsDao = new SettingsDaoImpl();
	private static PrimaryMsisdnDao primaryMsisdnDao= new PrimaryMsisdnDaoImpl();

	public MapJavagwProcessor()
	{
		String baseUrl = settingsDao.getValue("app_base_url");
		this.ussd = new VspUssdHandlerImpl(baseUrl);
	}

	public void respUpdateVLRDetails(int serviceId, long sessionId, String hlrNumber, short error, Map<String, String> extension)
			throws TException
	{
		logger.info("MapJavagwProcessor:: Entering respUpdateVLRDetails with serviceId = {}, sessionId = {}, hlrNumber = {}, error={},  extension = {}", new Object[] { Integer.valueOf(serviceId), Long.valueOf(sessionId), hlrNumber, Short.valueOf(error), extension });

		String msisdn = (String)extension.get("msisdn");
		String imsi = (String)extension.get("imsi");

		if ((((imsi == null) || (imsi.isEmpty()))) && (((msisdn == null) || (msisdn.isEmpty())))) {
			logger.error("No imsi or msisdn present in extension");
			return;
		}

		LUResponse luResponse = new LUResponse(imsi, serviceId, sessionId, extension);
		setChanged();
		notifyObservers(luResponse);
	}

	public void reqvMSRN(int serviceId, long sessionId, String imsi, Map<String, String> extension) throws TException
	{
		logger.info("MapJavagwProcessor:: Entering reqvMSRN with serviceId = {}, sessionId = {}, imsi = {}, extension = {}", new Object[] { Integer.valueOf(serviceId), Long.valueOf(sessionId), imsi, extension });
		try
		{
			MsrnRequest request = new MsrnRequest(imsi, extension);
			request.setServiceId(serviceId);
			request.setSessionId(sessionId);
			setChanged();
			notifyObservers(request);
		}
		catch (Exception ex) {
			logger.error("Exception in handling reqvMSRN ", ex);
		}
	}

	public void reqMissedCallDetails(int serviceId, long sessionId, String msisdn, Map<String, String> extension) throws TException
	{
		logger.info("MapJavagwProcessor:: Entering reqMissedCallDetails with serviceId = {}, sessionId = {}, msisdn = {}, extension = {}", new Object[] { Integer.valueOf(serviceId), Long.valueOf(sessionId), msisdn, extension });

		logger.info("Implementation not provided yet");
	}

	public void reqInitiateUSSDSession(int serviceId, long sessionId, String codingScheme, String dataString, String msisdn, Map<String, String> extension) throws TException
	{
		logger.info("MapJavagwProcessor:: Entering reqInitiateUSSDSession with serviceId = {}, sessionId = {}, codingScheme = {}, dataString = {}, msisdn = {}, extension = {}", new Object[] { Integer.valueOf(serviceId), Long.valueOf(sessionId), codingScheme, dataString, msisdn, extension });

		dataString = reformatDataString(dataString);
		try
		{
			MapJavaInterfaceWrapper client = MapgwJavaConnectionManager.getInstance().getClientConnection();
			dataString = this.ussd.respondUssd(serviceId, sessionId, codingScheme, dataString, msisdn, null);

			if (dataString.contains("<end>")) {
				dataString = dataString.replace("<end>", "");
				logger.debug("MapJavagwProcessor:: End Session Response is: " + dataString);
				client.respInitiateUSSDSession(serviceId, sessionId, codingScheme, dataString, msisdn,(short) 0, extension);
			}
			else {
				logger.debug("MapJavagwProcessor:: reqUSSD SEND : " + dataString);
				client.reqUSSD(serviceId, sessionId, codingScheme, dataString, msisdn, extension);
			}

			logger.info("MapJavagwProcessor:: Gracefully leaving reqInitiateUSSDSession with serviceId = {}, sessionId = {}, codingScheme = {}, dataString = {}, msisdn = {}, extension = {}", new Object[] { Integer.valueOf(serviceId), Long.valueOf(sessionId), codingScheme, dataString, msisdn, extension });
		}
		catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
			throw new RuntimeException(ex);
		}
	}

	public void respUSSD(int serviceId, long sessionId, String codingScheme, String dataString, String msisdn, Map<String, String> extension)
			throws TException
	{
		logger.info("MapJavagwProcessor:: Entering respUSSD with serviceId = {}, sessionId = {}, codingScheme = {}, dataString = {}, msisdn = {}, extension = {}", new Object[] { Integer.valueOf(serviceId), Long.valueOf(sessionId), codingScheme, dataString, msisdn, extension });

		if (extension == null) {
			extension = new HashMap();
		}

		extension.put("IMSI", msisdn);
		dataString = reformatDataString(dataString);
		try
		{
			MapJavaInterfaceWrapper client = MapgwJavaConnectionManager.getInstance().getClientConnection();

			dataString = this.ussd.respondUssd(serviceId, sessionId, codingScheme, dataString, msisdn, extension);

			if (dataString.contains("<end>")) {
				dataString = dataString.replace("<end>", "");
				logger.debug("MapJavagwProcessor:: Ending USSD Session");
				client.respInitiateUSSDSession(serviceId, sessionId, codingScheme, dataString, msisdn, (short)0, extension);
			} else {
				logger.debug("MapJavagwProcessor:: Sending reqUSSD");
				client.reqUSSD(serviceId, sessionId, codingScheme, dataString, msisdn, extension);
			}

			logger.info("MapJavagwProcessor:: Gracefully leaving respUSSD with serviceId = {}, sessionId = {}, codingScheme = {}, dataString = {}, msisdn = {}, extension = {}", new Object[] { Integer.valueOf(serviceId), Long.valueOf(sessionId), codingScheme, dataString, msisdn, extension });
		}
		catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
			throw new RuntimeException(ex);
		}
	}

	public void reqIncommingSmsData(int serviceId, long sessionId, String imsi, String codingScheme, String dataString, String origPartyAddr, String timestamp, Map<String, String> extension)
			throws TException
	{
		logger.info("MapJavagwProcessor:: Entering reqIncommingSmsData with serviceId = {}, sessionId = {}, imsi={}, codingScheme = {}, dataString = {}, origPartyAddr = {}, timestamp={}, extension = {}", new Object[] { Integer.valueOf(serviceId), Long.valueOf(sessionId), imsi, codingScheme, dataString, origPartyAddr, timestamp, extension });

		logger.info("Implementation not provided yet");
	}

	public void respOutgoingSmsData(int serviceId, long sessionId, short error, Map<String, String> extension) throws TException
	{
		logger.info("MapJavagwProcessor:: Entering respOutgoingSmsData with serviceId = {}, sessionId = {}, error = {}, extension = {}", new Object[] { Integer.valueOf(serviceId), Long.valueOf(sessionId), Short.valueOf(error), extension });

		logger.info("Implementation not provided yet");
	}

	public void serviceTypeRequest(int serviceId, long sessionId, String msisdn, String imsi, Map<String, String> extension) throws TException {
		logger.info("MapJavagwProcessor:: Entering serviceTypeRequest with: serviceId = {}, sessionId = {}, msisdn = {}, imsi = {}, extension = {}",
				new Object[] { Integer.valueOf(serviceId), Long.valueOf(sessionId), msisdn, imsi, extension });

		ServiceAppType appType = new ServiceAppType();
		appType.setType(TypeOfService.ManyMeService);

		if (extension == null) {
			extension = new HashMap<String, String>();
		}	

		try {
			PrimaryMsisdn primaryMsisdn = null;

			if(imsi != null) {
				primaryMsisdn = primaryMsisdnDao.findByImsi(imsi);

				if(primaryMsisdn != null) {
					msisdn = primaryMsisdn.getVirtual_msisdn();
				}

			} else if(msisdn !=null) {
				primaryMsisdn = primaryMsisdnDao.findByVirtualMsisdn(msisdn);

				if(primaryMsisdn != null) {
					imsi = primaryMsisdn.getVimsi();
				}

			}
			extension.put("msisdn", msisdn);
			extension.put("imsi", imsi);

			MapJavaInterfaceWrapper client = MapgwJavaConnectionManager.getInstance().getClientConnection();
			client.serviceTypeResponse(serviceId, sessionId, appType, extension);

		} catch (RecordNotFoundException e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException(e);

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			throw new RuntimeException(ex);
		}
		logger.info("Leaving serviceTypeRequest with: service_id = {}, session_id = {}, appType = {}, extension = {}",
				serviceId, sessionId, appType, extension);	
	}

	public void subscriberInfoResp(int serviceId, long sessionId, String msisdn, Map<String, String> extension) throws TException {
		logger.info("MapJavagwProcessor:: Entering subscriberInfoResp with serviceId = {}, sessionId = {}, msisdn = {}, extension = {}", new Object[] { Integer.valueOf(serviceId), Long.valueOf(sessionId), msisdn, extension });

		logger.info("Implementation not provided yet");
	}

	public void registerNodeRequest(Map<String, String> extension) throws TException
	{
		logger.info("MapJavagwProcessor:: Registering Node Request received");
		MapgwJavaConnectionManager.getInstance().reinitializeClientConnection();
		MapJavaInterfaceWrapper client = MapgwJavaConnectionManager.getInstance().getClientConnection();
		logger.info("MapJavagwProcessor:: Send NodeResponse");
		client.registerNodeResponse(extension);
	}

	public void registerNodeResponse(Map<String, String> extension) throws TException {
		logger.debug("MapJavagwProcessor:: Registering Node Response recieved");
	}

	public void respSriSm(int serviceId, long sessionId, String imsi, Map<String, String> extension)
			throws TException
	{
		
		logger.info("MapJavagwProcessor:: Entering respSriSm with serviceId = {}, sessionId = {}, imsi = {}, extension = {}", new Object[] { Integer.valueOf(serviceId), Long.valueOf(sessionId), imsi, extension });
		try
		{
			logger.info("MapJavagwProcessor:: imsi string length="+imsi.length());
			SRISMResponse sriSmResponse = (SRISMResponse)MapJavaThriftHandler.getRequest(serviceId, sessionId).createResponse();
			sriSmResponse.setImsi(imsi);
			sriSmResponse.setExtension(extension);

			setChanged();
			notifyObservers(sriSmResponse);
		}
		catch (Exception ex) {
			logger.error("Exception in handling respSriSm", ex);
		}
	}

	private String reformatDataString(String dataString) {
		if ((dataString != null) && (dataString.startsWith("4:")))
			dataString = dataString.substring(dataString.indexOf("4:") + 1);

		logger.debug("MapJavagwProcessor:: dataString = {}", dataString);
		return dataString;
	}
}
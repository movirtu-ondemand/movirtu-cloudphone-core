package com.movirtu.thrift.connection;

import java.util.Map;

import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.thrift.generated.MapJavaInterface;
import com.movirtu.thrift.generated.ServiceAppType;


public class MapJavaInterfaceWrapper implements MapJavaInterface.Iface
{
  private static Logger logger = LoggerFactory.getLogger(MapJavaInterfaceWrapper.class);
  private MapJavaInterface.Client client;

  public MapJavaInterfaceWrapper(MapJavaInterface.Client client)
  {
    this.client = client;
  }

  public MapJavaInterface.Client getClient() {
    return this.client;
  }

  public void setClient(MapJavaInterface.Client client) {
    this.client = client;
  }

  public void respMissedCallDetails(int service_id, long session_id, String coding_scheme, String missed_call_data, String diverted_msisdn, Map<String, String> extension)
  {
  }

  public void reqUpdateVLRDetails(int service_id, long session_id, String imsi, Map<String, String> extension) {
    logger.debug("MapJavaInterfaceWrapper::sending reqUpdateVLRDetails");
    try
    {
      this.client.reqUpdateVLRDetails(service_id, session_id, imsi, extension);
      logger.debug("MapJavaInterfaceWrapper::done sending reqUpdateVLRDetails");
    }
    catch (TException ex) {
      logger.error("Unable to send reqUpdateVLRDetails. Trying again");
      try
      {
        MapgwJavaConnectionManager.getInstance().reinitializeClientConnection();
        this.client.reqUpdateVLRDetails(service_id, session_id, imsi, extension);
        logger.debug("MapJavaInterfaceWrapper::done sending reqUpdateVLRDetails");
      } catch (TException ex1) {
        logger.error(ex1.getMessage(), ex1);
      }
    }
  }

  public void respvMSRN(int service_id, long session_id, String vmsrn, short error, ServiceAppType type, Map<String, String> extension)
  {
    logger.debug("MapJavaInterfaceWrapper::Sending respvMSRN");
    try {
      this.client.respvMSRN(service_id, session_id, vmsrn, error, type, extension);
      logger.debug("MapJavaInterfaceWrapper::done sending respvMSRN");
    } catch (TException ex) {
      logger.error("Unable to send respvMSRN. Trying again");
      try {
        MapgwJavaConnectionManager.getInstance().reinitializeClientConnection();
        this.client.respvMSRN(service_id, session_id, vmsrn, error, type, extension);
        logger.debug("MapJavaInterfaceWrapper::done sending respvMSRN");
      } catch (TException ex1) {
        logger.error(ex1.getMessage(), ex1);
      }
    }
  }

  public void reqUSSD(int service_id, long session_id, String coding_scheme, String data_string, String msisdn, Map<String, String> extension) {
    logger.debug("MapJavaInterfaceWrapper::sending reqUSSD");
    try {
      this.client.reqUSSD(service_id, session_id, coding_scheme, data_string, msisdn, extension);
      logger.debug("MapJavaInterfaceWrapper::done sending reqUSSD");
    } catch (Exception ex) {
      logger.error("Unable to send reqUSSD. Trying again");
      try {
        MapgwJavaConnectionManager.getInstance().reinitializeClientConnection();
        this.client.reqUSSD(service_id, session_id, coding_scheme, data_string, msisdn, extension);
        logger.debug("MapJavaInterfaceWrapper::done sending reqUSSD");
      } catch (TException ex1) {
        logger.error(ex1.getMessage(), ex1);
      }
    }
  }

  public void respInitiateUSSDSession(int service_id, long session_id, String coding_scheme, String data_string, String msisdn, short error, Map<String, String> extension) {
    logger.debug("MapJavaInterfaceWrapper::sending respInitiateUSSDSession");
    try {
      this.client.respInitiateUSSDSession(service_id, session_id, coding_scheme, data_string, msisdn, error, extension);
      logger.debug("MapJavaInterfaceWrapper::done sending respInitiateUSSDSession");
    } catch (TException ex) {
      logger.error("Unable to send respInitiateUSSDSession. Trying again");
      try {
        MapgwJavaConnectionManager.getInstance().reinitializeClientConnection();
        this.client.respInitiateUSSDSession(service_id, session_id, coding_scheme, data_string, msisdn, error, extension);
        logger.debug("MapJavaInterfaceWrapper::done sending respInitiateUSSDSession");
      } catch (TException ex1) {
        logger.error(ex1.getMessage(), ex1);
      }
    }
  }

  public void respIncommingSmsData(int service_id, long session_id, short error, Map<String, String> extension)
  {
  }

  public void reqOutgoingSmsData(int service_id, long session_id, String calling_party, String called_party, String coding_scheme, String data_string, Map<String, String> extension) {
    logger.debug("MapJavaInterfaceWrapper::sending reqOutgoingSmsData");
    try {
      this.client.reqOutgoingSmsData(service_id, session_id, calling_party, called_party, coding_scheme, data_string, extension);
      logger.debug("MapJavaInterfaceWrapper::done sending reqOutgoingSmsData");
    } catch (TException ex) {
      logger.error("Unable to send reqOutgoingSmsData. Trying again");
      try {
        MapgwJavaConnectionManager.getInstance().reinitializeClientConnection();
        this.client.reqOutgoingSmsData(service_id, session_id, calling_party, called_party, coding_scheme, data_string, extension);
        logger.debug("MapJavaInterfaceWrapper::done sending respIncommingSmsData");
      } catch (TException ex1) {
        logger.error(ex1.getMessage(), ex1);
      }
    }
  }

  public void registerNodeRequest(Map<String, String> extension) {
    logger.debug("MapJavaInterfaceWrapper::sending registerNodeRequest");
    try {
      this.client.registerNodeRequest(extension);
      logger.debug("MapJavaInterfaceWrapper::done sending registerNodeRequest");
    } catch (TException ex) {
      logger.error("Unable to send registerNodeRequest. Trying again");
      try {
        MapgwJavaConnectionManager.getInstance().reinitializeClientConnection();
        this.client.registerNodeRequest(extension);
        logger.debug("MapJavaInterfaceWrapper::done sending registerNodeRequest");
      } catch (TException ex1) {
        logger.error(ex1.getMessage(), ex1);
      }
    }
  }

  public void registerNodeResponse(Map<String, String> extension) {
    logger.debug("MapJavaInterfaceWrapper::sending registerNodeResponse");
    try {
      this.client.registerNodeResponse(extension);
      logger.debug("MapJavaInterfaceWrapper::done sending registerNodeResponse");
    } catch (TException ex) {
      logger.error("Unable to send registerNodeResponse. Trying again");
      try {
        MapgwJavaConnectionManager.getInstance().reinitializeClientConnection();
        this.client.registerNodeResponse(extension);
        logger.debug("MapJavaInterfaceWrapper::done sending registerNodeResponse");
      } catch (TException ex1) {
        logger.error(ex1.getMessage(), ex1);
      }
    }
  }

  public void serviceTypeResponse(int service_id, long session_id, ServiceAppType toss, Map<String, String> extension) {
    logger.debug("MapJavaInterfaceWrapper::sending serviceTypeResponse");
    try {
      this.client.serviceTypeResponse(service_id, session_id, toss, extension);
      logger.debug("MapJavaInterfaceWrapper::done sending serviceTypeResponse");
    } catch (TException ex) {
      logger.error("Unable to send serviceTypeResponse. Trying again");
      try {
        MapgwJavaConnectionManager.getInstance().reinitializeClientConnection();
        this.client.registerNodeResponse(extension);
        logger.debug("MapJavaInterfaceWrapper::done sending serviceTypeResponse");
      } catch (TException ex1) {
        logger.error(ex1.getMessage(), ex1);
      }
    }
  }

  public void subscriberInfoReq(int service_id, long session_id, String msisdn, Map<String, String> extension)
  {
  }

  public void reqSriSm(int service_id, long session_id, String msisdn, Map<String, String> extension)
    throws TException
  {
    logger.debug("MapJavaInterfaceWrapper::sending reqSriSm");
    try {
      this.client.reqSriSm(service_id, session_id, msisdn, extension);
      logger.debug("MapJavaInterfaceWrapper::done sending reqSriSm");
    } catch (TException ex) {
      logger.error("Unable to send reqSriSm. Trying again");
      try {
        MapgwJavaConnectionManager.getInstance().reinitializeClientConnection();
        this.client.reqSriSm(service_id, session_id, msisdn, extension);
        logger.debug("MapJavaInterfaceWrapper::done sending reqSriSm");
      } catch (TException ex1) {
        logger.error(ex1.getMessage(), ex1);
      }
    }
  }
}
package com.movirtu.thrift.connection;

import java.util.Observer;

import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TServer.Args;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.apache.thrift.transport.TTransportFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.thrift.generated.MapJavaInterface;
import com.movirtu.thrift.generated.MapJavagw;
import com.movirtu.thrift.generated.MapJavagw.Processor;
import com.movirtu.vsp.persistence.dao.hb.SettingsDao;
import com.movirtu.vsp.persistence.dao.hb.impl.SettingsDaoImpl;

public class MapgwJavaConnectionManager
{
	private static Logger logger = LoggerFactory.getLogger(MapgwJavaConnectionManager.class);
	private MapJavaInterfaceWrapper clientConnection;
	private static volatile MapgwJavaConnectionManager instance = new MapgwJavaConnectionManager();
	private static SettingsDao settingsDao = new SettingsDaoImpl();
	protected TServerTransport serverTransport;
	protected TServer server;
	protected TTransport clientTransport;
	protected TProtocol clientProtocol;
	protected String clientHost;
	protected int clientPort;
	protected int serverPort;
	private MapJavagwProcessor serverProcessor;

	private MapgwJavaConnectionManager()
	{
		settingsDao = new SettingsDaoImpl();
		this.serverPort = Integer.parseInt(settingsDao.getValue("common_javaapp_port"));
		logger.debug("11111111=="+serverPort);
		this.clientHost = settingsDao.getValue("mapGWHost");
		logger.debug("22222222="+clientHost);
		this.clientPort = Integer.parseInt(settingsDao.getValue("mapGWPort"));
		logger.debug("33333333="+clientPort);
	}

	public static MapgwJavaConnectionManager getInstance()
	{
//		if (instance == null) {
//			instance = new MapgwJavaConnectionManager();
//		}

		return instance;
	}

	public MapJavaInterfaceWrapper getClientConnection()
	{
		logger.info("Returning mapgw thrift client" +this.clientConnection);
		return this.clientConnection;
	}

	public void reinitializeServerConnection() throws Exception {
		this.serverTransport.close();
		createServer();
	}

	public MapJavaInterfaceWrapper reinitializeClientConnection() throws TTransportException {
		logger.info("Reinitializing client");
		this.clientTransport.close();
		return createClient();
	}

	public MapJavaInterfaceWrapper createClient()
			throws TTransportException
	{
		logger.info("Creating client");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.clientTransport = new TFramedTransport(new TSocket(this.clientHost, this.clientPort));
		this.clientTransport.open();
		this.clientProtocol = new TBinaryProtocol(this.clientTransport);
		MapJavaInterface.Client client = new MapJavaInterface.Client(this.clientProtocol);
		logger.info("Creating mapgw thrift client" +this);
		this.clientConnection = new MapJavaInterfaceWrapper(client);
		logger.info("Created mapgw thrift client" +clientConnection);
		return this.clientConnection;
	}

	public void createServer() throws Exception
	{		
		TProtocolFactory protocolFactory = new TBinaryProtocol.Factory();
		TTransportFactory transportFactory = new TFramedTransport.Factory();
		this.serverTransport = new TServerSocket(this.serverPort);
		this.serverProcessor = new MapJavagwProcessor();
		Processor processor = new MapJavagw.Processor(this.serverProcessor);
		this.server = new TSimpleServer(new Args(serverTransport).processor(processor).transportFactory(transportFactory).protocolFactory(protocolFactory));
	}

	public void addServerObserver(Observer observer)
	{
		this.serverProcessor.addObserver(observer);
	}

	public void startServer()
	{
		logger.debug("calling serve");
		server.serve();
	}
}
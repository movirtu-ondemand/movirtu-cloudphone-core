package com.movirtu.map;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.map.message.BaseMessage;
import com.movirtu.map.message.LURequest;
import com.movirtu.map.message.MsrnResponse;
import com.movirtu.map.message.SRISMRequest;
import com.movirtu.map.message.SmsRequest;
import com.movirtu.thrift.connection.MapJavaInterfaceWrapper;
import com.movirtu.thrift.connection.MapgwJavaConnectionManager;

public class MapJavaThriftHandler extends Observable implements Observer
{
	private static Logger logger = LoggerFactory.getLogger(MapJavaThriftHandler.class);
	private static volatile MapJavaThriftHandler mapJavaHandler;
	protected static ExecutorService Process = Executors.newFixedThreadPool(2);
	public static final int SERVICE_ID = 1;
	public static final String SERVICE_ID_KEY = "ServiceId";
	private static int thriftMessageTimeoutInSec = 60;
	private static Map<String, TimerTask> thriftMessageMap;
	ExecutorService executor = Executors.newFixedThreadPool(1);
	protected boolean mIsRunning = false;
	private Timer timer;

	private MapJavaThriftHandler()
	{
		this.timer = new Timer();
		thriftMessageMap = new ConcurrentHashMap();
	}

	public static MapJavaThriftHandler getInstance() {
		if (mapJavaHandler == null) {
			mapJavaHandler = new MapJavaThriftHandler();
		}
		
		return mapJavaHandler;
	}

	public void initializeThriftInterface() {
		logger.debug("initialize Thrift Interface ");
		this.executor.execute(new ThriftServerInitializationtask());
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		startClient();
		logger.debug("Thrift Interface initialized");
	}

	public void startServer()
	{
		logger.debug("Inside MapJavaHandler startServer");
		try
		{
			logger.debug("Creating server");
			MapgwJavaConnectionManager.getInstance().createServer();
			MapgwJavaConnectionManager.getInstance().addServerObserver(this);
			logger.debug("Starting server");
			MapgwJavaConnectionManager.getInstance().startServer();
		}
		catch (Exception e) {
			logger.error("Exception in starting thrift server connection", e);
		}
	}

	public void startClient()
	{
		logger.debug("Inside MapJavaHandler startClient");
		try
		{
			MapgwJavaConnectionManager.getInstance().createClient();
			sendInitRegisterNodeRequest();
		}
		catch (Exception e) {
			logger.error("Exception in starting thrift server connection", e);
		}
	}

	public void stop(int listeningTo, String remoteHost, int remotePort, int tBinaryProtocolMaxLength, String url)
	{
	}

	public void submit(Object object)
	{
		if (object instanceof BaseMessage) {
			createAndAddTimerTaskToMap((BaseMessage)object);
		}

		if (object instanceof SmsRequest)
			sendSmsRequest((SmsRequest)object);
		else if (object instanceof SRISMRequest)
			sendSriSmRequest((SRISMRequest)object);
		else if (object instanceof LURequest)
			sendLocationUpdateRequest((LURequest)object);
		else if (object instanceof MsrnResponse)
			sendMsrnResponse((MsrnResponse)object);
	}

	public void update(Observable observer, Object message)
	{
		logger.debug("Inside update");

		if (message instanceof BaseMessage) {
			cancelTimerTask((BaseMessage)message);
		}

		setChanged();
		notifyObservers(message);
	} 

	public void sendInitRegisterNodeRequest()
	{
		Map extension;
		try {
			extension = new HashMap();
			logger.debug("Sending Node request");
			extension.put("ServiceId", "1");
			MapJavaInterfaceWrapper client = MapgwJavaConnectionManager.getInstance().getClientConnection();
			client.registerNodeRequest(extension);
			logger.debug("##Sent initial Node Request");
		}
		catch (Exception ex) {
			logger.error("Initial Node Request failed", ex);
		}
	}

	private void sendMsrnResponse(MsrnResponse response) {
		logger.debug("Sending Msrn response " + response);
		try
		{
			logger.debug("Sending Msrn response");
			MapJavaInterfaceWrapper client = MapgwJavaConnectionManager.getInstance().getClientConnection();
			client.respvMSRN(response.getServiceId(), response.getSessionId(), response.getMsrn(), response.getError(), response.getServiceAppType(), response.getExtension());
			logger.debug("Sent Msrn Response");
		}
		catch (Exception ex) {
			logger.error("Msrn Response failed", ex);
		}
	}

	public void sendSmsRequest(SmsRequest req)
	{
		logger.debug("Sending SMS request " + req);
		try
		{
			logger.debug("Sending Sms request");
			MapJavaInterfaceWrapper client = MapgwJavaConnectionManager.getInstance().getClientConnection();
			client.reqOutgoingSmsData(111, 222L, req.getCalling_party(), req.getCalled_party(), req.getCoding_scheme(), req.getData_string(), req.getExtension());
			logger.debug("Sent Sms Request");
		}
		catch (Exception ex) {
			logger.error("Sms Request failed", ex);
		}
	}

	public void sendSriSmRequest(SRISMRequest req)
	{
		logger.debug("Sending SRISM request " + req);
		try
		{
			MapJavaInterfaceWrapper client = MapgwJavaConnectionManager.getInstance().getClientConnection();
			client.reqSriSm(req.getServiceId(), req.getSessionId(), req.getMsisdn(), req.getExtension());
			logger.debug("Sent SRISM Request");
		}
		catch (Exception ex) {
			logger.error("Sms Request failed", ex);
		}
	}

	public void sendLocationUpdateRequest(LURequest req)
	{
		logger.debug("Sending location update request " + req);
		try
		{
			MapJavaInterfaceWrapper client = MapgwJavaConnectionManager.getInstance().getClientConnection();
			client.reqUpdateVLRDetails(req.getServiceId(), req.getSessionId(), req.getImsi(), req.getExtension());
			logger.debug("Sent location update");
		}
		catch (Exception ex) {
			logger.error("Location Update Request failed", ex);
		}
	}

	private void createAndAddTimerTaskToMap(BaseMessage message)
	{
		logger.debug("Inside createAndAddTimerTaskToMap");
		String mapId = createMapId(message);
		TimerTask timerTask = new ThriftMessageTimerTask(message);
		thriftMessageMap.put(mapId, timerTask);
		this.timer.schedule(timerTask, thriftMessageTimeoutInSec * 1000);
		logger.debug("Leaving createAndAddTimerTaskToMap");
	}

	public static BaseMessage getRequest(int serviceId, long sessionId) {
		logger.debug("Inside getRequest");
		String mapId = createMapId(serviceId, sessionId);
		logger.debug("Leaving getRequest with " + thriftMessageMap.get(mapId));
		return ((ThriftMessageTimerTask)thriftMessageMap.get(mapId)).getMessage();
	}

	private void cancelTimerTask(BaseMessage message) {
		logger.debug("Inside cancelTimerTask");
		String mapId = createMapId(message);
		TimerTask timerTask = (TimerTask)thriftMessageMap.get(mapId);
		if (timerTask != null)
			timerTask.cancel();

		logger.debug("Leaving cancelTimerTask");
	}

	private String createMapId(BaseMessage message) {
		return createMapId(message.getServiceId(), message.getSessionId());
	}

	private static String createMapId(int serviceId, long sessionId) {
		String mapId = "" + serviceId + "" + sessionId;
		return mapId;
	}

	class ThriftMessageTimerTask extends TimerTask
	{
		int serviceId;
		long sessionId;
		BaseMessage message;

		ThriftMessageTimerTask(BaseMessage message) {
			this.serviceId = message.getServiceId();
			this.sessionId = message.getSessionId();
			this.message = message;
		}

		public void run()
		{
			logger.error("Did not receive/send response for serviceId=[" + this.serviceId + "] and sessionId=[" + this.sessionId + "]");
		}

		public int getServiceId() {
			return this.serviceId;
		}

		public void setServiceId() {
			this.serviceId = serviceId;
		}

		public long getSessionId() {
			return this.sessionId;
		}

		public void setSessionId() {
			this.sessionId = sessionId;
		}

		public BaseMessage getMessage() {
			return this.message;
		}

		public void setMessage() {
			this.message = message;
		}
	}

	class ThriftServerInitializationtask implements Runnable
	{
		public void run()
		{
			try
			{
				logger.debug("Starting thriftHandler");
				MapJavaThriftHandler.getInstance().startServer();
			} catch (Exception ex) {
				logger.error("Unable to start Map handler", ex);
			}
		}
	}
}
package com.movirtu.map.message;

import java.util.HashMap;
import java.util.Map;

import com.movirtu.thrift.generated.ServiceAppType;

public class MsrnResponse extends BaseMessage
{
  String msisdn;
  String imsi;
  String msrn;
  Map<String, String> extension;
  short error;
  ServiceAppType serviceAppType;

  public MsrnResponse()
  {
    this.error = 0;
  }

  public MsrnResponse(String imsi, int serviceId, long sessionId)
  {
    this(imsi, serviceId, sessionId, new HashMap());
  }

  public MsrnResponse(String imsi, int serviceId, long sessionId, Map<String, String> extension)
  {
    this.error = 0;

    this.sessionId = sessionId;

    if (imsi != null)
      this.imsi = imsi;

    if (extension != null)
      this.extension = extension;
  }

  public String getMsisdn()
  {
    return this.msisdn;
  }

  public void setMsisdn(String msisdn) {
    this.msisdn = msisdn;
  }

  public String getImsi() {
    return this.imsi;
  }

  public void setImsi(String imsi) {
    this.imsi = imsi;
  }

  public String getMsrn() {
    return this.msrn;
  }

  public void setMsrn(String msrn) {
    this.msrn = msrn;
  }

  public short getError() {
    return this.error;
  }

  public void setError(short error) {
    this.error = error;
  }

  public ServiceAppType getServiceAppType() {
    return this.serviceAppType;
  }

  public void setServiceAppType(ServiceAppType serviceAppType) {
    this.serviceAppType = serviceAppType;
  }

  public void setServiceId(int serviceId)
  {
    this.serviceId = serviceId;
  }

  public Map<String, String> getExtension() {
    return this.extension;
  }

  public void setExtension(Map<String, String> extension) {
    this.extension = extension;
  }

  public BaseMessage createResponse() throws Exception
  {
    throw new Exception("Not applicable");
  }

  public String toString()
  {
    StringBuffer buffer = new StringBuffer();
    buffer.append(super.toString());
    return new StringBuilder().append("msisdn=[").append(this.msisdn).append("]").append("imsi=[").append(this.imsi).append("]").append("msrn=[").append(this.msrn).append("]").append("extension=[").append(this.extension).append("]").toString();
  }
}
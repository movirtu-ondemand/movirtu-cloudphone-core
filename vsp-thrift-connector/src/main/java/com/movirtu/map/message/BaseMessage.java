package com.movirtu.map.message;

import java.util.concurrent.atomic.AtomicLong;

public abstract class BaseMessage
{
	protected int serviceId = 1;
	long sessionId;
	private static AtomicLong counter = new AtomicLong();

	public BaseMessage()
	{
		this.sessionId = generateSessionId();
	}

	public BaseMessage(int serviceId) {
		serviceId = serviceId;
		this.sessionId = generateSessionId();
	}

	public BaseMessage(int serviceId, long sessionId) {
		this.serviceId = serviceId;
		this.sessionId = sessionId;
	}

	public int getServiceId() {
		return this.serviceId;
	}

	protected void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public long getSessionId() {
		return this.sessionId;
	}

	protected void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}

	protected static long generateSessionId() {
		return counter.getAndIncrement();
	}

	public String toString()
	{
		StringBuffer buffer = new StringBuffer();
		return new StringBuilder().append("serviceId=[").append(this.serviceId).append("]").append("sessionId=[").append(this.sessionId).append("]").toString();
	}

	public abstract BaseMessage createResponse()
			throws Exception;
}
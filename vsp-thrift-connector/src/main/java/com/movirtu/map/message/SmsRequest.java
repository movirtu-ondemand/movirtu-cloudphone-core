package com.movirtu.map.message;

import java.util.HashMap;
import java.util.Map;

public class SmsRequest extends BaseMessage
{
  String calling_party;
  String called_party;
  String coding_scheme;
  String data_string;
  public static final String MT_KEY = "MT";
  public static final String MT_KEY_VALUE = "";
  Map<String, String> extension;

  public SmsRequest(String called_party, String data_string)
  {
    this(null, called_party, null, data_string, new HashMap());
  }

  public SmsRequest(String called_party, String data_string, Map<String, String> extension) {
    this(null, called_party, null, data_string, extension);
  }

  public SmsRequest(String called_party, String coding_scheme, String data_string, Map<String, String> extension) {
    this(null, called_party, coding_scheme, data_string, extension);
  }

  public SmsRequest(String calling_party, String called_party, String coding_scheme, String data_string, Map<String, String> extension)
  {
    this.calling_party = "123456";

    this.coding_scheme = "15";

    if (calling_party != null)
      this.calling_party = calling_party;

    if (called_party != null)
      this.called_party = called_party;

    if (coding_scheme != null)
      this.coding_scheme = coding_scheme;

    if (data_string != null)
      this.data_string = data_string;

    if (extension != null)
      this.extension = extension;
  }

  public String getCalling_party()
  {
    return this.calling_party; }

  public void setCalling_party(String calling_party) {
    this.calling_party = calling_party; }

  public String getCalled_party() {
    return this.called_party; }

  public void setCalled_party(String called_party) {
    this.called_party = called_party; }

  public String getCoding_scheme() {
    return this.coding_scheme; }

  public void setCoding_scheme(String coding_scheme) {
    this.coding_scheme = coding_scheme; }

  public String getData_string() {
    return this.data_string; }

  public void setData_string(String data_string) {
    this.data_string = data_string; }

  public Map<String, String> getExtension() {
    return this.extension; }

  public void setExtension(Map<String, String> extension) {
    this.extension = extension;
  }

  public BaseMessage createResponse()
    throws Exception
  {
    throw new Exception("Not applicable");
  }

  public String toString()
  {
    StringBuffer buffer = new StringBuffer();
    buffer.append(super.toString());
    return new StringBuilder().append("calling_party=[").append(this.calling_party).append("]").append("called_party=[").append(this.called_party).append("]").append("coding_scheme=[").append(this.coding_scheme).append("]").append("data_string=[").append(this.data_string).append("]").append("extension=[").append(this.extension).append("]").toString();
  }
}
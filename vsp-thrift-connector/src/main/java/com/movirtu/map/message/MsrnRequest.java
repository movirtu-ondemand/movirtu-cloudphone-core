package com.movirtu.map.message;

import java.util.HashMap;
import java.util.Map;

import com.movirtu.thrift.generated.ServiceAppType;
import com.movirtu.thrift.generated.TypeOfService;

public class MsrnRequest extends BaseMessage
{
  String imsi;
  String msisdn;
  long sessionId;
  public static final String MSISDN = "MSISDN";
  Map<String, String> extension;

  public MsrnRequest(String imsi)
  {
    this(imsi, new HashMap());
  }

  public MsrnRequest(String imsi, Map<String, String> extension)
  {
    if (imsi != null) {
      this.imsi = imsi;
    }

    if (extension != null)
      this.extension = extension;
  }

  public String getImsi()
  {
    return this.imsi;
  }

  public void setImsi(String imsi) {
    this.imsi = imsi;
  }

  public String getMsisdn() {
    return this.msisdn;
  }

  public void setMsisdn(String msisdn) {
    this.extension.put("MSISDN", msisdn);
    this.msisdn = msisdn;
  }

  public Map<String, String> getExtension() {
    return this.extension;
  }

  public void setExtension(Map<String, String> extension) {
    this.extension = extension;
  }

  public long getSessionId() {
    return this.sessionId;
  }

  public void setSessionId(long sessionId) {
    this.sessionId = sessionId;
  }

  public void setServiceId(int serviceId)
  {
    this.serviceId = serviceId;
  }

  public BaseMessage createResponse() throws Exception
  {
    MsrnResponse response = new MsrnResponse();
    response.setMsisdn(this.msisdn);
    response.setImsi(this.imsi);
    response.setServiceId(this.serviceId);
    response.setSessionId(this.sessionId);
    response.setServiceAppType(new ServiceAppType(TypeOfService.ManyMeService));
    return response;
  }

  public String toString()
  {
    StringBuffer buffer = new StringBuffer();
    buffer.append(super.toString());
    return new StringBuilder().append("msisdn=[").append(this.msisdn).append("]").append("imsi=[").append(this.imsi).append("]").append("extension=[").append(this.extension).append("]").toString();
  }
}
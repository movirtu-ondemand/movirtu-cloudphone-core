package com.movirtu.map.message;

public enum ServiceId
{
  SMS, SRISM, LU;

  private int serviceId;

  public int getServiceId()
  {
    return this.serviceId;
  }
}
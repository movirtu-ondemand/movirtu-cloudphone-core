package com.movirtu.map.message;

import java.util.HashMap;
import java.util.Map;

public class SRISMRequest extends BaseMessage
{
  String msisdn;
  long sessionId;
  public static final String MCC = "MCC";
  public static final String MNC = "MNC";
  Map<String, String> extension;

  public SRISMRequest(String msisdn)
  {
    this(msisdn, new HashMap());
  }

  public SRISMRequest(String msisdn, Map<String, String> extension)
  {
    if (msisdn != null) {
      this.msisdn = msisdn;
    }

    if (extension != null)
      this.extension = extension;
  }

  public String getMsisdn()
  {
    return this.msisdn;
  }

  public void setMsisdn(String msisdn) {
    this.msisdn = msisdn;
  }

  public Map<String, String> getExtension() {
    return this.extension;
  }

  public void setExtension(Map<String, String> extension) {
    this.extension = extension;
  }

  public BaseMessage createResponse() throws Exception
  {
    SRISMResponse response = new SRISMResponse();
    response.setMsisdn(this.msisdn);
    response.setServiceId(this.serviceId);
    response.setSessionId(this.sessionId);
    return response;
  }

  public String toString()
  {
    StringBuffer buffer = new StringBuffer();
    buffer.append(super.toString());
    return new StringBuilder().append("msisdn=[").append(this.msisdn).append("]").append("extension=[").append(this.extension).append("]").toString();
  }
}
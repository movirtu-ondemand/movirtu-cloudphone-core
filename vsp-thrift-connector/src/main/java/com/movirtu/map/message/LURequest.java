package com.movirtu.map.message;

import java.util.HashMap;
import java.util.Map;

public class LURequest extends BaseMessage
{
	String imsi;
	String msisdn;
	long sessionId;
	public static final String MSISDN = "vMSISDN";
	Map<String, String> extension;

	public LURequest(String imsi)
	{
		this(imsi, new HashMap());
	}

	public LURequest(String imsi, Map<String, String> extension)
	{
		if (imsi != null) {
			this.imsi = imsi;
		}

		if (extension != null)
			this.extension = extension;
	}

	public String getImsi()
	{
		return this.imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getMsisdn() {
		return this.msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.extension.put("vMSISDN", msisdn);
		this.msisdn = msisdn;
	}

	public Map<String, String> getExtension() {
		return this.extension;
	}

	public void setExtension(Map<String, String> extension) {
		this.extension = extension;
	}

	public String toString()
	{
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		return new StringBuilder().append("msisdn=[").append(this.msisdn).append("]").append("imsi=[").append(this.imsi).append("]").append("extension=[").append(this.extension).append("]").toString();
	}

	public BaseMessage createResponse()
			throws Exception
	{
		LUResponse response = new LUResponse();
		response.setMsisdn(this.msisdn);
		response.setImsi(this.imsi);
		response.setServiceId(this.serviceId);
		response.setSessionId(this.sessionId);
		return response;
	}
}
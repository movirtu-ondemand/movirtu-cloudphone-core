package com.movirtu.map.message;

import java.util.HashMap;
import java.util.Map;

public class SRISMResponse extends BaseMessage
{
  String msisdn;
  String imsi;
  int mnc;
  int mcc;
  Map<String, String> extension;

  public SRISMResponse()
  {
    this.mnc = -1;
    this.mcc = -1;
  }

  public SRISMResponse(String imsi, int serviceId, long sessionId)
  {
    this(imsi, serviceId, sessionId, new HashMap());
  }

  public SRISMResponse(String imsi, int serviceId, long sessionId, Map<String, String> extension)
  {
    this.mnc = -1;
    this.mcc = -1;

    this.sessionId = sessionId;

    if (imsi != null)
      this.imsi = imsi;

    if (extension != null)
      this.extension = extension;
  }

  public String getMsisdn()
  {
    return this.msisdn;
  }

  public void setMsisdn(String msisdn) {
    this.msisdn = msisdn;
  }

  public String getImsi() {
    return this.imsi;
  }

  public void setImsi(String imsi) {
    this.imsi = imsi;
  }

  public int getMnc() {
    if (this.mnc == -1)
      this.mnc = Integer.parseInt((String)this.extension.get("MNC"));

    return this.mnc;
  }

  public void setMnc(int mnc) {
    this.mnc = mnc;
  }

  public int getMcc() {
    if (this.mcc == -1)
      this.mcc = Integer.parseInt((String)this.extension.get("MCC"));

    return this.mcc;
  }

  public void setMcc(int mcc) {
    this.mcc = mcc;
  }

  public Map<String, String> getExtension() {
    return this.extension;
  }

  public void setExtension(Map<String, String> extension) {
    this.extension = extension;
  }

  public BaseMessage createResponse() throws Exception
  {
    throw new Exception("Not applicable");
  }

  public String toString()
  {
    StringBuffer buffer = new StringBuffer();
    buffer.append(super.toString());
    return new StringBuilder().append("msisdn=[").append(this.msisdn).append("]").append("imsi=[").append(this.imsi).append("]").append("extension=[").append(this.extension).append("]").toString();
  }
}
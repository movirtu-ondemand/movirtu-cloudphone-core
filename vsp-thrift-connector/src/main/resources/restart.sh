#!/bin/sh
PID=`ps -ef | grep mx-thrift-mapgw-java-console | awk '{ print $2 }'`
PATH=$PWD
echo "$PATH"
if ps -p $PID > /dev/null
then
    echo "$PID is already running on $PWD"
    kill -9 $PID
else
    echo "Starting mx-thrift mxshare on $PWD"
    java -jar mx-thrift-mapgw-java-console.jar &
    if [ "$2" == "test" ]; then
        echo "Running on test mode"
        java -cp mx-thrift-mapgw-java-console.jar com.movirtu.thrift.server.CPPServerThread &
    else
        echo "Running on production mode"
    fi
    
fi
#kill -9 `ps -aef | grep mx-thrift-mxshare | grep -v grep | awk '{print $2}'`

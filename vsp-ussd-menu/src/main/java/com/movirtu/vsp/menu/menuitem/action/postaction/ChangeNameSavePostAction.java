package com.movirtu.vsp.menu.menuitem.action.postaction;

import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menuitem.action.MenuItemAction;
import com.movirtu.vsp.menu.menuitem.action.postresponse.ChangeNameSavePostResponse;

public class ChangeNameSavePostAction implements MenuItemAction {

	public Object performAction(MenuSession menuSession) {

		ChangeNameSavePostResponse response = new ChangeNameSavePostResponse();
		// TODO save language to DB
		response.setOperationSuccess(true);
		return response;
	}


}

package com.movirtu.vsp.menu.menu.node;

import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menu.MenuTreeOperation;
import com.movirtu.vsp.menu.menuitem.action.postaction.ChangeNameSavePostAction;
import com.movirtu.vsp.menu.menuitem.action.preaction.ChangeNameSavePreAction;
import com.movirtu.vsp.menu.menuitem.action.preresponse.ChangeNameSavePreResponse;

public class ChangeNameSaveMenuNode extends BaseMenuNode {

	private static final String DISPLAY_NAME = "Change Name";
	private ChangeNameSavePreResponse preResponse;

	public ChangeNameSaveMenuNode(int id) {
		super(id, 
				DISPLAY_NAME, 
				new ChangeNameSavePreAction(), 
				new ChangeNameSavePostAction());
	}

	@Override
	public void performPreUssdAction(MenuSession menuSession) {
		super.performPreUssdAction(menuSession);
		preResponse = (ChangeNameSavePreResponse) getResponse();
	}

	@Override
	public MenuTreeOperation executeUssd(MenuSession menuSession, String ussdInput) {
		int input = -1;
		try {
			input = Integer.parseInt(ussdInput);
		}catch(Exception ex) {

		}

		MenuTreeOperation operation = processDefaultMenu(menuSession, input);

		if(operation !=null) {
			return operation;
		}

		menuSession.setNewName(ussdInput);
		super.performPostUssdAction(menuSession);
		nextSelectedNode = this.childNodes.get(0);

		return MenuTreeOperation.MOVE_TO_NEXT_MENU;
	}

	public String getDisplayString() {
		StringBuffer sb = new StringBuffer();		
		sb.append(this.displayName);
		sb.append("\n");
		// TODO We should save this value in menuSession object
		sb.append(preResponse.getString());
		sb.append(getUssdDefaultMenuDisplayString());
		return sb.toString();
	}

}

package com.movirtu.vsp.menu.model;

public class VspDefaultMenuResponse {
	
	private String userName;
	private String sipUser;
	private String phoneNumber;
	private String imsi;
	private String msrn;
	private int id;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getSipUser() {
		return sipUser;
	}
	public void setSipUser(String sipUser) {
		this.sipUser = sipUser;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getImsi() {
		return imsi;
	}
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}
	public String getMsrn() {
		return msrn;
	}
	public void setMsrn(String msrn) {
		this.msrn = msrn;
	}
	
	
}

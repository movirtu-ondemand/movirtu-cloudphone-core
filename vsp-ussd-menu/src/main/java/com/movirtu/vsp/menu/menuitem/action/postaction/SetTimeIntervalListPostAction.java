package com.movirtu.vsp.menu.menuitem.action.postaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menuitem.action.MenuItemAction;
import com.movirtu.vsp.menu.menuitem.action.postresponse.SetTimeIntervalListPostResponse;

public class SetTimeIntervalListPostAction implements MenuItemAction {

	Logger logger = LoggerFactory.getLogger(SetTimeIntervalListPostAction.class);
	
	public Object performAction(MenuSession menuSession) {

		SetTimeIntervalListPostResponse response = new SetTimeIntervalListPostResponse();
		saveTimeInterval(menuSession.getTimeInterval());
		response.setOperationSuccess(true);
		return response;
	}

	private void saveTimeInterval(String timeInterval) {
		logger.debug("saveTimeInterval :: TODO ");
	}

}

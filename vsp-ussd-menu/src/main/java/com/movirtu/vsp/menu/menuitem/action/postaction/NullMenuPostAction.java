package com.movirtu.vsp.menu.menuitem.action.postaction;

import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menuitem.action.MenuItemAction;

public class NullMenuPostAction implements MenuItemAction {

	public Object performAction(MenuSession menuSession) {

		return null;
	}

}

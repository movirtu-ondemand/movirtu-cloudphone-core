package com.movirtu.vsp.menu.menuitem.action.preresponse;

import java.util.ArrayList;
import java.util.List;

public class PurchaseNumberListPreResponse {

	List<String> purchaseNumberList = new ArrayList<String>();

	public List<String> getPurchaseNumberList() {
		return purchaseNumberList;
	}

	public void addPurchaseNumber(String number) {
		this.purchaseNumberList.add(number);
	}

}

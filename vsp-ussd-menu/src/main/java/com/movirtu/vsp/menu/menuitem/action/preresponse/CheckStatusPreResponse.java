package com.movirtu.vsp.menu.menuitem.action.preresponse;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class CheckStatusPreResponse {

	Map<String, String> statusMap = new ConcurrentHashMap<String, String>();

	public Map<String, String> getStatusMap() {
		return statusMap;
	}

	public void addStatus(String msisdn, String status) {
		this.statusMap.put(msisdn, status);
	}

	public String getStatus(String msisdn) {
		return this.statusMap.get(msisdn);
	}
}

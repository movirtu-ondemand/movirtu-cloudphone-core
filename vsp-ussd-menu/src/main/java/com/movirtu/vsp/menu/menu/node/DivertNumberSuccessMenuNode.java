package com.movirtu.vsp.menu.menu.node;



public class DivertNumberSuccessMenuNode extends BaseMenuNode {

	private static final String DISPLAY_NAME = "Divert successfully";
	
	public DivertNumberSuccessMenuNode(int id) {
		super(id, DISPLAY_NAME);
	}

	public String getDisplayString() {
		StringBuffer sb = new StringBuffer();		
		sb.append(this.displayName);
		sb.append("\n");
		sb.append(getUssdDefaultMenuDisplayString());
		return sb.toString();
	}
	
}

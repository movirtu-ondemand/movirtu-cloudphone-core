package com.movirtu.vsp.menu.menuitem.action.preaction;

import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menuitem.action.MenuItemAction;
import com.movirtu.vsp.menu.menuitem.action.preresponse.ChangeLanguageSavePreResponse;

public class ChangeLanguageSavePreAction implements MenuItemAction {

	public Object performAction(MenuSession menuSession) {

		ChangeLanguageSavePreResponse response = new ChangeLanguageSavePreResponse();
		response.addLanguage("English");
		response.addLanguage("Zulu");
		response.addLanguage("SeSotho");
		response.addLanguage("Malawai");

		return response;
	}


}

package com.movirtu.vsp.menu;


import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Application {

	private static Logger logger = Logger.getLogger(Application.class);
	private static ApplicationContext context;

	public static ApplicationContext getContext() {
		return context;
	}

	private static String xmlPath = "";
	private static String xmlConfigFile = "vsp.xml";

	public static void main( String[] args )
	{
		logger.info("Application started");

		if(args.length>0 && args[0]!=null) {
			logger.info("Load XML path = "+args[0]);
			setXmlPath(args[0]);
		}

		context = new ClassPathXmlApplicationContext(getXmlPath()+"/"+getXmlConfigFile());

	}

	public static String getXmlPath() {
		return xmlPath;
	}

	public static void setXmlPath(String xmlPath) {
		Application.xmlPath = xmlPath;
	}

	public static String getXmlConfigFile() {
		return xmlConfigFile;
	}

	public static void setXmlConfigFile(String xmlConfigFile) {
		Application.xmlConfigFile = xmlConfigFile;
	}

}

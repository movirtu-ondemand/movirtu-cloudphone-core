package com.movirtu.vsp.menu.menu;

import com.movirtu.vsp.menu.menu.node.BackMenuNode;
import com.movirtu.vsp.menu.menu.node.BalanceQueryMenuNode;
import com.movirtu.vsp.menu.menu.node.BaseMenuNode;
import com.movirtu.vsp.menu.menu.node.ChangeLanguageSaveMenuNode;
import com.movirtu.vsp.menu.menu.node.ChangeLanguageSuccessMenuNode;
import com.movirtu.vsp.menu.menu.node.ChangeNameSaveMenuNode;
import com.movirtu.vsp.menu.menu.node.ChangeNameSuccessMenuNode;
import com.movirtu.vsp.menu.menu.node.CheckStatusMenuNode;
import com.movirtu.vsp.menu.menu.node.DefaultNumberSaveNode;
import com.movirtu.vsp.menu.menu.node.DefaultNumberSuccessNode;
import com.movirtu.vsp.menu.menu.node.DivertNumberListNumberMenuNode;
import com.movirtu.vsp.menu.menu.node.DivertNumberListOptionMenuNode;
import com.movirtu.vsp.menu.menu.node.DivertNumberSuccessMenuNode;
import com.movirtu.vsp.menu.menu.node.ExitMenuNode;
import com.movirtu.vsp.menu.menu.node.HomeMenuNode;
import com.movirtu.vsp.menu.menu.node.PurchaseNumberConfirmNode;
import com.movirtu.vsp.menu.menu.node.PurchaseNumberListNode;
import com.movirtu.vsp.menu.menu.node.PurchaseNumberSuccessNode;
import com.movirtu.vsp.menu.menu.node.SetTimeIntervalListNode;
import com.movirtu.vsp.menu.menu.node.SetTimeIntervalSuccessNode;

public class UssdMenuHandlerService {

	private static volatile UssdMenuHandlerService ussdService;

	MenuTree menuTree;
	private UssdMenuHandlerService() {
		initialize();
	}

	public static UssdMenuHandlerService getInstance() {
		if(ussdService==null) {
			ussdService = new UssdMenuHandlerService();
		}
		return ussdService;
	}

	public void initialize() {
		// 
		BaseMenuNode backMenu = new BackMenuNode(); 
		BaseMenuNode homeMenu = new HomeMenuNode();
		BaseMenuNode exitMenu = new ExitMenuNode();

		// Purchase number nodes
		BaseMenuNode purchaseNumberNode = new PurchaseNumberListNode(1);
		purchaseNumberNode.addDefaultNode(exitMenu);

		BaseMenuNode purchaseNumberConfirmNode = new PurchaseNumberConfirmNode(2);
		purchaseNumberConfirmNode.addDefaultNode(backMenu);
		purchaseNumberConfirmNode.addDefaultNode(exitMenu);

		BaseMenuNode purchaseNumberSuccessNode = new PurchaseNumberSuccessNode(3);
		purchaseNumberSuccessNode.addDefaultNode(exitMenu);

		// Manage number nodes
		BaseMenuNode setTimeIntervalNode = new SetTimeIntervalListNode(5);
		BaseMenuNode setTimeIntervalSuccessNode = new SetTimeIntervalSuccessNode(6);
		setTimeIntervalSuccessNode.addDefaultNode(homeMenu);
		setTimeIntervalSuccessNode.addDefaultNode(exitMenu);

		// Divert number nodes
		BaseMenuNode divertNumberListNode = new DivertNumberListNumberMenuNode(7);
		divertNumberListNode.addDefaultNode(backMenu);
		divertNumberListNode.addDefaultNode(exitMenu);

		BaseMenuNode divertNumberSaveNode = new DivertNumberListOptionMenuNode(8);
		divertNumberSaveNode.addDefaultNode(backMenu);
		divertNumberSaveNode.addDefaultNode(exitMenu);

		//		MenuNode divertToVoiceMail = new DivertToVoiceMail(21);
		//		MenuNode divertToNumber = new DivertToNumber(22);

		BaseMenuNode divertNumberSuccessNode = new DivertNumberSuccessMenuNode(9);
		divertNumberSuccessNode.addDefaultNode(homeMenu);
		divertNumberSuccessNode.addDefaultNode(exitMenu);


		// check status code
		BaseMenuNode checkStatusNode = new CheckStatusMenuNode(10);
		checkStatusNode.addDefaultNode(homeMenu);

		// set default number
		BaseMenuNode defaultNumberSetNode = new DefaultNumberSaveNode(11);
		defaultNumberSetNode.addDefaultNode(backMenu);

		BaseMenuNode defaultNumberSuccessNode = new DefaultNumberSuccessNode(12);
		defaultNumberSuccessNode.addDefaultNode(homeMenu);
		defaultNumberSuccessNode.addDefaultNode(exitMenu);

		// Balance Query node
		BaseMenuNode balanceQueryNode = new BalanceQueryMenuNode(13);
		balanceQueryNode.addDefaultNode(homeMenu);
		balanceQueryNode.addDefaultNode(exitMenu);

		// Change Name node
		BaseMenuNode changeNameSaveNode = new ChangeNameSaveMenuNode(15);

		BaseMenuNode changeNameSuccessNode = new ChangeNameSuccessMenuNode(16);
		changeNameSuccessNode.addDefaultNode(homeMenu);
		changeNameSuccessNode.addDefaultNode(exitMenu);

		// change language node
		BaseMenuNode changeLanguageSaveNode = new ChangeLanguageSaveMenuNode(17);

		BaseMenuNode changeLanguageSuccessNode = new ChangeLanguageSuccessMenuNode(18);
		changeLanguageSuccessNode.addDefaultNode(homeMenu);
		changeLanguageSuccessNode.addDefaultNode(exitMenu);


		// Welcome menu
		BaseMenuNode welcomeNode = MenuManager.getInstance().createMenuNode(0, "Welcome test user");
		BaseMenuNode manageNumberNode = MenuManager.getInstance().createMenuNode(4, "Manage Numbers");
		BaseMenuNode optionsNumberNode = MenuManager.getInstance().createMenuNode(14, "Options");

		// Level 1
		welcomeNode.addChildNode(1, purchaseNumberNode);
		welcomeNode.addChildNode(2, manageNumberNode);
		welcomeNode.addChildNode(3, balanceQueryNode);
		welcomeNode.addChildNode(4, optionsNumberNode);
		welcomeNode.addDefaultNode(backMenu);
		welcomeNode.addDefaultNode(homeMenu);
		welcomeNode.addDefaultNode(exitMenu);

		// Level 2
		purchaseNumberNode.addChildNode(purchaseNumberConfirmNode);
		purchaseNumberNode.addDefaultNode(backMenu);
		purchaseNumberNode.addDefaultNode(homeMenu);
		purchaseNumberNode.addDefaultNode(exitMenu);


		manageNumberNode.addChildNode(1, setTimeIntervalNode);
		manageNumberNode.addChildNode(2, divertNumberListNode);
		manageNumberNode.addChildNode(3, checkStatusNode);
		manageNumberNode.addChildNode(4, defaultNumberSetNode);
		manageNumberNode.addDefaultNode(backMenu);
		manageNumberNode.addDefaultNode(homeMenu);
		manageNumberNode.addDefaultNode(exitMenu);

		optionsNumberNode.addChildNode(1, changeNameSaveNode);
		optionsNumberNode.addChildNode(3, changeLanguageSaveNode);
		optionsNumberNode.addDefaultNode(backMenu);
		optionsNumberNode.addDefaultNode(homeMenu);
		optionsNumberNode.addDefaultNode(exitMenu);

		// Level 3
		purchaseNumberConfirmNode.addChildNode(1, purchaseNumberSuccessNode);
		setTimeIntervalNode.addChildNode(setTimeIntervalSuccessNode);
		divertNumberListNode.addChildNode(divertNumberSaveNode);
		defaultNumberSetNode.addChildNode(defaultNumberSuccessNode);
		changeNameSaveNode.addChildNode(changeNameSuccessNode);
		changeLanguageSaveNode.addChildNode(changeLanguageSuccessNode);

		// Level 4
		//		divertNumberSaveNode.addChildNode(divertToVoiceMail);
		//		divertNumberSaveNode.addChildNode(divertToNumber);
		divertNumberSaveNode.addChildNode(divertNumberSuccessNode);

		// Level 5
		//		divertToVoiceMail.addChildNode(divertNumberSuccessNode);
		//		divertToNumber.addChildNode(divertNumberSuccessNode);

		menuTree = MenuManager.getInstance().createMenuTree(welcomeNode);
	}

	public String process(String msisdn, String userInput) {

		if(userInput!=null) {
			if(MenuManager.getInstance().isNewUssdSession(userInput)) {
				MenuManager.getInstance().deleteMenuSession(MenuManager.getInstance().getMenuSession(msisdn));
			}
		}		

		MenuSession session = MenuManager.getInstance().getOrCreateMenuSession(msisdn, menuTree);
		return menuTree.processUssdInput(session, userInput);
	}

}

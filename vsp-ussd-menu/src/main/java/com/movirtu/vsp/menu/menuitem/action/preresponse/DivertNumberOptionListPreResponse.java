package com.movirtu.vsp.menu.menuitem.action.preresponse;

import java.util.ArrayList;
import java.util.List;

public class DivertNumberOptionListPreResponse {

	List<String> divertOptionList = new ArrayList<String>();

	public List<String> getDivertOptionList() {
		return divertOptionList;
	}

	public void addDivertOption(String divertOption) {
		this.divertOptionList.add(divertOption);
	}

}

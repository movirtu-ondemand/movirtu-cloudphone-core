package com.movirtu.vsp.menu.menuitem.action.preaction;

import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menuitem.action.MenuItemAction;
import com.movirtu.vsp.menu.menuitem.action.preresponse.DivertNumberListNumberPreResponse;

public class DivertNumberListNumberPreAction implements MenuItemAction {

	public Object performAction(MenuSession menuSession) {

		DivertNumberListNumberPreResponse response = new DivertNumberListNumberPreResponse();
		response.addNumber("1111111");
		response.addNumber("2222222");
		return response;
	}

}

package com.movirtu.vsp.menu.menu;

import java.util.List;

import com.movirtu.vsp.menu.menu.node.BaseMenuNode;

public interface MenuNode {

	public void addChildNode(BaseMenuNode childNode);

	public void addDefaultNode(BaseMenuNode childNode);

	public void addChildNode(int ussd, BaseMenuNode childNode);

	public MenuTreeOperation executeUssd(MenuSession menuSession, String ussdInput);

	public void performPreUssdAction(MenuSession menuSession);

	public BaseMenuNode getParentNode();

	public List<BaseMenuNode> getChildNodes();

	public boolean isRoot();

	public boolean isLeafNode();
	
	public BaseMenuNode getNextSelectedNode();
	
	public String getDisplayString();

}

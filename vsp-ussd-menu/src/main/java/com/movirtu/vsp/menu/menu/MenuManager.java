package com.movirtu.vsp.menu.menu;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.vsp.menu.menu.node.BaseMenuNode;
import com.movirtu.vsp.persistence.dao.hb.SettingsDao;
import com.movirtu.vsp.persistence.dao.hb.impl.SettingsDaoImpl;

public class MenuManager {

	private static Logger logger = LoggerFactory.getLogger(MenuManager.class);

	private static SettingsDao settingsDao = new SettingsDaoImpl();
	private static volatile MenuManager instance = new MenuManager();
	private Map<String, MenuSession> menuSessionMap = new ConcurrentHashMap<String, MenuSession>();
	private String newUssdSessionString = "*226#"; 
	public static final String USSD_SESSSION_END_STRING = "<end>";
	public static final String USSD_SHORT_CODE = "ussd_short_code";

	private MenuManager() {
		newUssdSessionString = settingsDao.getValue(USSD_SHORT_CODE);
	}
	public static MenuManager getInstance() {
		return instance;
	}

	public BaseMenuNode createMenuNode(int id, String displayName) {
		return new BaseMenuNode(id,displayName);
	}

	public MenuTree createMenuTree(BaseMenuNode rootNode) {
		return new MenuTree(rootNode);
	}

	public MenuSession getOrCreateMenuSession(String msisdn, MenuTree menuTree) {

		if(menuSessionExists(msisdn)) {
			return getMenuSession(msisdn);
		}

		return createMenuSession(msisdn, menuTree);
	}

	public MenuSession createMenuSession(String msisdn, MenuTree menuTree) {
		logger.debug("create new session");
		MenuSession session = new MenuSession(msisdn, menuTree.getRootNode());
		this.menuSessionMap.put(msisdn,session);
		return session;
	}

	public MenuSession getMenuSession(String msisdn) {
		return menuSessionMap.get(msisdn);
	}

	public boolean menuSessionExists(String msisdn) {
		return menuSessionMap.containsKey(msisdn);
	}

	public void deleteMenuSession(MenuSession session) {
		logger.debug("Deleting session");
		if(session != null) {
			menuSessionMap.remove(session.getMsisdn());
		}
	}

	public boolean isNewUssdSession(String input) {
		String trimInput = input.trim();

		if(newUssdSessionString.equalsIgnoreCase(trimInput)) {
			return true;
		}
		return false;
	}

}

package com.movirtu.vsp.menu.menu.node;

import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menu.MenuTreeOperation;
import com.movirtu.vsp.menu.menuitem.action.postaction.DefaultNumberSavePostAction;
import com.movirtu.vsp.menu.menuitem.action.preaction.DefaultNumberSavePreAction;
import com.movirtu.vsp.menu.menuitem.action.preresponse.DefaultNumberSavePreResponse;

public class DefaultNumberSaveNode extends BaseMenuNode {

	private static final String DISPLAY_NAME = "Set Default Number";
	public SortedMap<Integer, String> userNumbersMap;

	public DefaultNumberSaveNode(int id) {
		super(id, 
				DISPLAY_NAME, 
				new DefaultNumberSavePreAction(), 
				new DefaultNumberSavePostAction());		
	}

	public void performPreUssdAction(MenuSession menuSession) {
		super.performPreUssdAction(menuSession);
		userNumbersMap = new TreeMap<Integer, String>();
		
		DefaultNumberSavePreResponse response = (DefaultNumberSavePreResponse) getResponse();

		int index =1;

		for(String number:response.getDefaultNumberList()) {

			userNumbersMap.put(index, number);
			index++;
		}

		reshuffleDefaultMenuMap(userNumbersMap.size());
	}

	@Override
	public MenuTreeOperation executeUssd(MenuSession menuSession, String ussdInput) {
		int input = -1;
		try {
			input = Integer.parseInt(ussdInput);
		}catch(Exception ex) {

		}

		MenuTreeOperation operation = processDefaultMenu(menuSession, input);

		if(operation !=null) {
			return operation;
		}

		if(userNumbersMap.get(input) !=null) {
			menuSession.setDefaultNumber(userNumbersMap.get(input));
			super.performPostUssdAction(menuSession);
			nextSelectedNode = this.childNodes.get(0);
			return MenuTreeOperation.MOVE_TO_NEXT_MENU;

		}

		return MenuTreeOperation.TERMINATE_SESSION;
	}

	public String getDisplayString() {
		StringBuffer sb = new StringBuffer();		
		sb.append(this.displayName);
		sb.append("\n");

		for (Entry<Integer, String> entry : userNumbersMap.entrySet())
		{
			sb.append(entry.getKey() + "." + entry.getValue());
			sb.append("\n");
		}

		sb.append(getUssdDefaultMenuDisplayString());
		return sb.toString();
	}
}

package com.movirtu.vsp.menu.menu.node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.vsp.menu.menu.MenuManager;
import com.movirtu.vsp.menu.menu.MenuNode;
import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menu.MenuState;
import com.movirtu.vsp.menu.menu.MenuTreeOperation;
import com.movirtu.vsp.menu.menuitem.action.MenuItemAction;
import com.movirtu.vsp.menu.menuitem.action.postaction.NullMenuPostAction;
import com.movirtu.vsp.menu.menuitem.action.preaction.NullMenuPreAction;

public class BaseMenuNode implements MenuNode {

	private static Logger logger = LoggerFactory.getLogger(BaseMenuNode.class);

	private static final String END_OF_SESSION = "Thank you for using our service" + MenuManager.USSD_SESSSION_END_STRING;
	boolean isRoot = true;
	private int id;
	protected String displayName;
	private BaseMenuNode parentNode;
	protected List<BaseMenuNode> childNodes;
	public SortedMap<Integer, BaseMenuNode> ussdAssociatedMap;
	public SortedMap<Integer, BaseMenuNode> defaultMenuMap;
	protected MenuState currentState;
	protected MenuItemAction preAction;
	protected MenuItemAction postAction;
	protected BaseMenuNode nextSelectedNode;
	protected String inputToNextNode;
	protected boolean isLeafNode=true;
	Integer chidlMenuIndexCounter = 0;
	Integer defaultMenuIndexCounter = 0;
	protected Object response;

	public Object getResponse() {
		return response;
	}

	public BaseMenuNode(int id, String displayName) {
		this(id,displayName, new NullMenuPreAction(), new NullMenuPostAction());
	}

	public BaseMenuNode(int id, String displayName, MenuItemAction preAction) {
		this(id,displayName, preAction, new NullMenuPostAction());
	}

	public BaseMenuNode(int id, String displayName, MenuItemAction preAction, MenuItemAction postAction) {
		currentState = MenuState.PROCESS_PRE_USSD;
		this.setId(id);
		this.setDisplayName(displayName);
		this.preAction = preAction;
		this.postAction = postAction;
		childNodes = new ArrayList<BaseMenuNode>();
		ussdAssociatedMap = new TreeMap<Integer, BaseMenuNode>();
		defaultMenuMap = new TreeMap<Integer, BaseMenuNode>();
	}

	public BaseMenuNode(String displayName) {
		this.displayName = displayName;
	}

	/* (non-Javadoc)
	 * @see com.movirtu.vsp.menu.menu.node.MenuNodeInterface#addChildNode(com.movirtu.vsp.menu.menu.node.MenuNode)
	 */
	public void addChildNode(BaseMenuNode childNode) {
		this.addChildNode(++chidlMenuIndexCounter, childNode);
	}	

	/* (non-Javadoc)
	 * @see com.movirtu.vsp.menu.menu.node.MenuNodeInterface#addChildNode(java.lang.String, com.movirtu.vsp.menu.menu.node.MenuNode)
	 */
	public void addChildNode(int ussd, BaseMenuNode childNode) {
		logger.debug("adding child node ussd=" +ussd +" "+childNode.getDisplayName());
		isLeafNode = false;
		childNode.setParentNode(this);
		this.ussdAssociatedMap.put(ussd, childNode);
		this.childNodes.add(childNode);
		reshuffleDefaultMenuMap();
	}

	public void addDefaultNode(BaseMenuNode childNode) {
		logger.debug("adding default node = "+childNode.getDisplayName());
		logger.debug("defaultMenuIndexCounter = "+defaultMenuIndexCounter);
		this.defaultMenuMap.put(++defaultMenuIndexCounter, childNode);
		reshuffleDefaultMenuMap();

	}

	private void reshuffleDefaultMenuMap() {

		if(this.defaultMenuMap.size()==0 || ussdAssociatedMap.size()==0) {
			return;
		}

		reshuffleDefaultMenuMap(ussdAssociatedMap.lastKey());

	}

	protected void reshuffleDefaultMenuMap(int offset) {

		Map<Integer, BaseMenuNode> tmpMap = new HashMap<Integer, BaseMenuNode>(); 
		Iterator<Entry<Integer, BaseMenuNode>> iter = this.defaultMenuMap.entrySet().iterator();

		while (iter.hasNext()) {
			Map.Entry<Integer, BaseMenuNode> entry = iter.next();
			int key = entry.getKey();
			BaseMenuNode value = entry.getValue();
			iter.remove();
			tmpMap.put(++offset, value);
		}

		defaultMenuMap.putAll(tmpMap);
	}

	/* (non-Javadoc)
	 * @see com.movirtu.vsp.menu.menu.node.MenuNodeInterface#getDisplayString()
	 */
	public String getDisplayString() {
		StringBuffer sb = new StringBuffer();		
		sb.append(this.displayName);
		sb.append("\n");
		sb.append(getUssdMenuDisplayString());
		sb.append(getUssdDefaultMenuDisplayString());
		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see com.movirtu.vsp.menu.menu.node.MenuNodeInterface#getUssdMenuDisplayString()
	 */
	public String getUssdMenuDisplayString() {
		StringBuffer sb = new StringBuffer();

		for (Map.Entry<Integer, BaseMenuNode> entry : ussdAssociatedMap.entrySet()) {
			sb.append(entry.getKey() + "." + entry.getValue().getDisplayName());
			sb.append("\n");
		}

		return sb.toString();
	}

	public String getUssdDefaultMenuDisplayString() {
		StringBuffer sb = new StringBuffer();

		for (Map.Entry<Integer, BaseMenuNode> entry : defaultMenuMap.entrySet()) {
			sb.append(entry.getKey() + "." + entry.getValue().getDisplayName());
			sb.append("\n");
		}

		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see com.movirtu.vsp.menu.menu.node.MenuNodeInterface#executeUssd(java.lang.String)
	 */
	public MenuTreeOperation executeUssd(MenuSession menuSession, String ussdInput) {

		currentState=MenuState.PROCESS_PRE_USSD;
		int input = -1;
		try {
			input = Integer.parseInt(ussdInput);
		}catch(Exception ex) {

		}


		MenuTreeOperation operation = processDefaultMenu(menuSession, input);

		if(operation !=null) {
			return operation;
		}

		if(ussdAssociatedMap.get(input)!=null) {

			nextSelectedNode = ussdAssociatedMap.get(input);
			return MenuTreeOperation.MOVE_TO_NEXT_MENU;		
		}


		if(isLeafNode()) {
			return MenuTreeOperation.TERMINATE_SESSION;
		}

		return MenuTreeOperation.STAY_ON_CURRENT_MENU;
	}

	protected MenuTreeOperation processDefaultMenu(MenuSession menuSession, int ussdInput) {

		if(defaultMenuMap.get(ussdInput)!=null) {

			BaseMenuNode defaultSelectedNode = defaultMenuMap.get(ussdInput);

			if(defaultSelectedNode instanceof BackMenuNode) {
				return MenuTreeOperation.MOVE_TO_PRE_MENU;

			} else if(defaultSelectedNode instanceof HomeMenuNode) {
				return MenuTreeOperation.MOVE_TO_HOME;

				//			} else if(defaultSelectedNode instanceof ExitMenuNode) {
				//				menuSession.setTerminateSessionString(END_OF_SESSION);
				//				return MenuTreeOperation.TERMINATE_SESSION;

			} else {
				menuSession.setTerminateSessionString(END_OF_SESSION);
				return MenuTreeOperation.TERMINATE_SESSION;
			}
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.movirtu.vsp.menu.menu.node.MenuNodeInterface#performPreUssdAction()
	 */
	public void performPreUssdAction(MenuSession menuSession) {
		currentState=MenuState.PROCESS_USSD;
		response = preAction.performAction(menuSession);

	}

	public void performPostUssdAction(MenuSession menuSession) {
		response = postAction.performAction(menuSession);

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see com.movirtu.vsp.menu.menu.node.MenuNodeInterface#getDisplayName()
	 */
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/* (non-Javadoc)
	 * @see com.movirtu.vsp.menu.menu.node.MenuNodeInterface#getParentNode()
	 */
	public BaseMenuNode getParentNode() {
		return parentNode;
	}

	public void setParentNode(BaseMenuNode parentNode) {
		this.isRoot = false;
		this.parentNode = parentNode;
	}

	/* (non-Javadoc)
	 * @see com.movirtu.vsp.menu.menu.node.MenuNodeInterface#getChildNodes()
	 */
	public List<BaseMenuNode> getChildNodes() {
		return childNodes;
	}

	/* (non-Javadoc)
	 * @see com.movirtu.vsp.menu.menu.node.MenuNodeInterface#getNextSelectedNode()
	 */
	public BaseMenuNode getNextSelectedNode() {

		return nextSelectedNode;
	}

	/* (non-Javadoc)
	 * @see com.movirtu.vsp.menu.menu.node.MenuNodeInterface#getInputToNextNode()
	 */
	public String getInputToNextNode() {
		return inputToNextNode;
	}

	public void setInputToNextNode(String inputToNextNode) {
		this.inputToNextNode = inputToNextNode;
	}

	/* (non-Javadoc)
	 * @see com.movirtu.vsp.menu.menu.node.MenuNodeInterface#isRoot()
	 */
	public boolean isRoot() {
		return isRoot;
	}

	public void setRoot(boolean isRoot) {
		this.isRoot = isRoot;
	}

	/* (non-Javadoc)
	 * @see com.movirtu.vsp.menu.menu.node.MenuNodeInterface#isLeafNode()
	 */
	public boolean isLeafNode() {
		return isLeafNode;
	}

	public void setLeafNode(boolean isLeafNode) {
		this.isLeafNode = isLeafNode;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(displayName);
		return sb.toString();
	}

}

package com.movirtu.vsp.menu.menu;


public class MenuTree {

	MenuNode currentNode;
	MenuNode rootNode;

	public MenuNode getRootNode() {
		return rootNode;
	}

	public MenuTree(MenuNode rootNode) {
		this.rootNode = rootNode;
	}

	public String processUssdInput(MenuSession menuSession, String userInput) {

		currentNode = menuSession.getCurrentNode();

		if(currentNode.isRoot()) {
			currentNode.performPreUssdAction(menuSession);
		}

		MenuTreeOperation state = currentNode.executeUssd(menuSession, userInput);

		switch(state) {

		case MOVE_TO_HOME:

			currentNode = menuSession.getRootNode();
			break;

		case MOVE_TO_PRE_MENU:

			if(currentNode.getParentNode() != null) {
				currentNode = currentNode.getParentNode();
			}

			break;

		case MOVE_TO_NEXT_MENU:

			if(currentNode.getNextSelectedNode()!=null) {
				currentNode = currentNode.getNextSelectedNode();
			}

			break;

		case STAY_ON_CURRENT_MENU:

			break;

		case TERMINATE_SESSION:
			
			MenuManager.getInstance().deleteMenuSession(menuSession);
			return menuSession.getTerminateSessionString();
		}

		menuSession.setCurrentNode(currentNode);
		currentNode.performPreUssdAction(menuSession);

		return currentNode.getDisplayString();
	}

}

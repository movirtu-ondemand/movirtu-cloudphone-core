package com.movirtu.vsp.menu.menu.node;

public class ExitMenuNode extends BaseMenuNode {

	private static final String DISPLAY_NAME = "Exit";
	
	public ExitMenuNode() {
		super(DISPLAY_NAME);
	}

}

package com.movirtu.vsp.menu.menuitem.action.preresponse;

import java.util.ArrayList;
import java.util.List;

public class SetTimeIntervalListPreResponse {

	List<String> timeIntervalList = new ArrayList<String>();

	public List<String> getTimeIntervalList() {
		return timeIntervalList;
	}

	public void addTimeInterval(String number) {
		this.timeIntervalList.add(number);
	}

}

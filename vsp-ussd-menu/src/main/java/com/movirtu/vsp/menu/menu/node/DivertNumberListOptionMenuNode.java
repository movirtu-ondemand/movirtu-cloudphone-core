package com.movirtu.vsp.menu.menu.node;

import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menu.MenuTreeOperation;
import com.movirtu.vsp.menu.menuitem.action.postaction.DivertNumberOptionListPostAction;
import com.movirtu.vsp.menu.menuitem.action.preaction.DivertNumberOptionListPreAction;
import com.movirtu.vsp.menu.menuitem.action.preresponse.DivertNumberOptionListPreResponse;


public class DivertNumberListOptionMenuNode extends BaseMenuNode {

	private static final String DISPLAY_NAME = "Set Divert";
	public SortedMap<Integer, String> divertOptionMap = new TreeMap<Integer, String>();

	public DivertNumberListOptionMenuNode(int id) {
		super(id, 
				DISPLAY_NAME, 
				new DivertNumberOptionListPreAction(), 
				new DivertNumberOptionListPostAction());		
	}

	public void performPreUssdAction(MenuSession menuSession) {
		super.performPreUssdAction(menuSession);
		divertOptionMap = new TreeMap<Integer, String>();
		
		DivertNumberOptionListPreResponse response = (DivertNumberOptionListPreResponse) getResponse();

		int index =1;

		for(String number:response.getDivertOptionList()) {

			divertOptionMap.put(index, number);
			index++;
		}
		
		reshuffleDefaultMenuMap(divertOptionMap.size());
	}

	@Override
	public MenuTreeOperation executeUssd(MenuSession menuSession, String ussdInput) {
		int input = -1;
		try {
			input = Integer.parseInt(ussdInput);
		}catch(Exception ex) {

		}

		MenuTreeOperation operation = processDefaultMenu(menuSession, input);

		if(operation !=null) {
			return operation;
		}

		if(divertOptionMap.get(input) !=null) {

			menuSession.setDivertOption(divertOptionMap.get(input));
			super.performPostUssdAction(menuSession);
			nextSelectedNode = this.childNodes.get(0);
			return MenuTreeOperation.MOVE_TO_NEXT_MENU;

		}

		return MenuTreeOperation.TERMINATE_SESSION;
	}

	public String getDisplayString() {
		StringBuffer sb = new StringBuffer();		
		sb.append(this.displayName);
		sb.append("\n");
		for (Entry<Integer, String> entry : divertOptionMap.entrySet())
		{
			sb.append(entry.getKey() + "." + entry.getValue());
			sb.append("\n");
		}

		sb.append(getUssdDefaultMenuDisplayString());
		return sb.toString();
	}

}

package com.movirtu.vsp.menu.menu;

public enum MenuTreeOperation {

	MOVE_TO_HOME,	
	MOVE_TO_PRE_MENU,
	MOVE_TO_NEXT_MENU,
	STAY_ON_CURRENT_MENU,
	TERMINATE_SESSION;
}
package com.movirtu.vsp.menu.menuitem.action.postaction;

import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menuitem.action.MenuItemAction;
import com.movirtu.vsp.menu.menuitem.action.postresponse.ChangeLanguageSavePostResponse;

public class ChangeLanguageSavePostAction implements MenuItemAction {

	public Object performAction(MenuSession menuSession) {

		ChangeLanguageSavePostResponse response = new ChangeLanguageSavePostResponse();
		// TODO save language to DB
		response.setOperationSuccess(true);
		return response;
	}


}

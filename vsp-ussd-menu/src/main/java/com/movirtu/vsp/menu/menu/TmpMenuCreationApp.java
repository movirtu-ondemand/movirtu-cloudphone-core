package com.movirtu.vsp.menu.menu;

import java.util.Scanner;

import com.movirtu.vsp.menu.menu.node.BackMenuNode;
import com.movirtu.vsp.menu.menu.node.BalanceQueryMenuNode;
import com.movirtu.vsp.menu.menu.node.BaseMenuNode;
import com.movirtu.vsp.menu.menu.node.ChangeLanguageSaveMenuNode;
import com.movirtu.vsp.menu.menu.node.ChangeLanguageSuccessMenuNode;
import com.movirtu.vsp.menu.menu.node.ChangeNameSaveMenuNode;
import com.movirtu.vsp.menu.menu.node.ChangeNameSuccessMenuNode;
import com.movirtu.vsp.menu.menu.node.CheckStatusMenuNode;
import com.movirtu.vsp.menu.menu.node.DefaultNumberSaveNode;
import com.movirtu.vsp.menu.menu.node.DefaultNumberSuccessNode;
import com.movirtu.vsp.menu.menu.node.DivertNumberListNumberMenuNode;
import com.movirtu.vsp.menu.menu.node.DivertNumberListOptionMenuNode;
import com.movirtu.vsp.menu.menu.node.DivertNumberSuccessMenuNode;
import com.movirtu.vsp.menu.menu.node.ExitMenuNode;
import com.movirtu.vsp.menu.menu.node.HomeMenuNode;
import com.movirtu.vsp.menu.menu.node.PurchaseNumberConfirmNode;
import com.movirtu.vsp.menu.menu.node.PurchaseNumberListNode;
import com.movirtu.vsp.menu.menu.node.PurchaseNumberSuccessNode;
import com.movirtu.vsp.menu.menu.node.SetTimeIntervalListNode;
import com.movirtu.vsp.menu.menu.node.SetTimeIntervalSuccessNode;

public class TmpMenuCreationApp {

	MenuTree menuTree;

	public static void main(String[] args) {

		// 
		BaseMenuNode backMenu = new BackMenuNode(); 
		BaseMenuNode homeMenu = new HomeMenuNode();
		BaseMenuNode exitMenu = new ExitMenuNode();

		// Purchase number nodes
		BaseMenuNode purchaseNumberNode = new PurchaseNumberListNode(1);
		purchaseNumberNode.addDefaultNode(exitMenu);

		BaseMenuNode purchaseNumberConfirmNode = new PurchaseNumberConfirmNode(2);
		purchaseNumberConfirmNode.addDefaultNode(backMenu);
		purchaseNumberConfirmNode.addDefaultNode(exitMenu);

		BaseMenuNode purchaseNumberSuccessNode = new PurchaseNumberSuccessNode(3);
		purchaseNumberSuccessNode.addDefaultNode(exitMenu);

		// Manage number nodes
		BaseMenuNode setTimeIntervalNode = new SetTimeIntervalListNode(5);
		BaseMenuNode setTimeIntervalSuccessNode = new SetTimeIntervalSuccessNode(6);
		setTimeIntervalSuccessNode.addDefaultNode(homeMenu);
		setTimeIntervalSuccessNode.addDefaultNode(exitMenu);

		// Divert number nodes
		BaseMenuNode divertNumberListNode = new DivertNumberListNumberMenuNode(7);
		divertNumberListNode.addDefaultNode(backMenu);
		divertNumberListNode.addDefaultNode(exitMenu);

		BaseMenuNode divertNumberSaveNode = new DivertNumberListOptionMenuNode(8);
		divertNumberSaveNode.addDefaultNode(backMenu);
		divertNumberSaveNode.addDefaultNode(exitMenu);

		//		MenuNode divertToVoiceMail = new DivertToVoiceMail(21);
		//		MenuNode divertToNumber = new DivertToNumber(22);

		BaseMenuNode divertNumberSuccessNode = new DivertNumberSuccessMenuNode(9);
		divertNumberSuccessNode.addDefaultNode(homeMenu);
		divertNumberSuccessNode.addDefaultNode(exitMenu);


		// check status code
		BaseMenuNode checkStatusNode = new CheckStatusMenuNode(10);
		checkStatusNode.addDefaultNode(homeMenu);

		// set default number
		BaseMenuNode defaultNumberSetNode = new DefaultNumberSaveNode(11);
		defaultNumberSetNode.addDefaultNode(backMenu);

		BaseMenuNode defaultNumberSuccessNode = new DefaultNumberSuccessNode(12);
		defaultNumberSuccessNode.addDefaultNode(homeMenu);
		defaultNumberSuccessNode.addDefaultNode(exitMenu);

		// Balance Query node
		BaseMenuNode balanceQueryNode = new BalanceQueryMenuNode(13);
		balanceQueryNode.addDefaultNode(homeMenu);
		balanceQueryNode.addDefaultNode(exitMenu);

		// Change Name node
		BaseMenuNode changeNameSaveNode = new ChangeNameSaveMenuNode(15);

		BaseMenuNode changeNameSuccessNode = new ChangeNameSuccessMenuNode(16);
		changeNameSuccessNode.addDefaultNode(homeMenu);
		changeNameSuccessNode.addDefaultNode(exitMenu);

		// change language node
		BaseMenuNode changeLanguageSaveNode = new ChangeLanguageSaveMenuNode(17);

		BaseMenuNode changeLanguageSuccessNode = new ChangeLanguageSuccessMenuNode(18);
		changeLanguageSuccessNode.addDefaultNode(homeMenu);
		changeLanguageSuccessNode.addDefaultNode(exitMenu);


		// Welcome menu
		BaseMenuNode welcomeNode = MenuManager.getInstance().createMenuNode(0, "Welcome test user");
		BaseMenuNode manageNumberNode = MenuManager.getInstance().createMenuNode(4, "Manage Numbers");
		BaseMenuNode optionsNumberNode = MenuManager.getInstance().createMenuNode(14, "Options");

		// Level 1
		welcomeNode.addChildNode(purchaseNumberNode);
		welcomeNode.addChildNode(manageNumberNode);
		welcomeNode.addChildNode(balanceQueryNode);
		welcomeNode.addChildNode(optionsNumberNode);
		welcomeNode.addDefaultNode(backMenu);
		welcomeNode.addDefaultNode(homeMenu);
		welcomeNode.addDefaultNode(exitMenu);

		// Level 2
		purchaseNumberNode.addChildNode(purchaseNumberConfirmNode);
		purchaseNumberNode.addDefaultNode(backMenu);
		purchaseNumberNode.addDefaultNode(homeMenu);
		purchaseNumberNode.addDefaultNode(exitMenu);


		manageNumberNode.addChildNode(setTimeIntervalNode);
		manageNumberNode.addChildNode(divertNumberListNode);
		manageNumberNode.addChildNode(checkStatusNode);
		manageNumberNode.addChildNode(defaultNumberSetNode);
		manageNumberNode.addDefaultNode(backMenu);
		manageNumberNode.addDefaultNode(homeMenu);
		manageNumberNode.addDefaultNode(exitMenu);

		optionsNumberNode.addChildNode(changeNameSaveNode);
		optionsNumberNode.addChildNode(changeLanguageSaveNode);
		optionsNumberNode.addDefaultNode(backMenu);
		optionsNumberNode.addDefaultNode(homeMenu);
		optionsNumberNode.addDefaultNode(exitMenu);

		// Level 3
		purchaseNumberConfirmNode.addChildNode(purchaseNumberSuccessNode);
		setTimeIntervalNode.addChildNode(setTimeIntervalSuccessNode);
		divertNumberListNode.addChildNode(divertNumberSaveNode);
		defaultNumberSetNode.addChildNode(defaultNumberSuccessNode);
		changeNameSaveNode.addChildNode(changeNameSuccessNode);
		changeLanguageSaveNode.addChildNode(changeLanguageSuccessNode);

		// Level 4
		//		divertNumberSaveNode.addChildNode(divertToVoiceMail);
		//		divertNumberSaveNode.addChildNode(divertToNumber);
		divertNumberSaveNode.addChildNode(divertNumberSuccessNode);

		// Level 5
		//		divertToVoiceMail.addChildNode(divertNumberSuccessNode);
		//		divertToNumber.addChildNode(divertNumberSuccessNode);

		TmpMenuCreationApp app = new TmpMenuCreationApp();
		app.menuTree = MenuManager.getInstance().createMenuTree(welcomeNode);
		app.showMenu();
	}

	private void showMenu() {

		while(true) {						
			Scanner in = new Scanner(System.in);
			System.out.println("Enter Msisdn");
			String msisdn = in.nextLine();
			System.out.println("Enter USSD");
			processUssdInput(msisdn, in.nextLine());
		}

	}

	public void processUssdInput(String msisdn, String userInput) {
		MenuSession session = MenuManager.getInstance().getOrCreateMenuSession(msisdn, menuTree);
		
//		MenuSession session = MenuManager.getInstance().getMenuSession(msisdn);
//		
//		if(session==null) {
//			session = MenuManager.getInstance().createMenuSession(msisdn, menuTree);
//			System.out.println("Creating new session="+session.getCurrentNode());
//		} else {
//			System.out.println("Previous session node="+session.getCurrentNode());
//		}
		
		System.out.println(menuTree.processUssdInput(session, userInput));
	}
	
}

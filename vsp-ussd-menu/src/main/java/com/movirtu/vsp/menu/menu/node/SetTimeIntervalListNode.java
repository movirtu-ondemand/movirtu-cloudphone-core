package com.movirtu.vsp.menu.menu.node;

import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menu.MenuTreeOperation;
import com.movirtu.vsp.menu.menuitem.action.postaction.SetTimeIntervalListPostAction;
import com.movirtu.vsp.menu.menuitem.action.preaction.SetTimeIntervalListPreAction;
import com.movirtu.vsp.menu.menuitem.action.preresponse.SetTimeIntervalListPreResponse;

public class SetTimeIntervalListNode extends BaseMenuNode {

	private static final String DISPLAY_NAME = "Set Time Interval";
	public SortedMap<Integer, String> timeIntervalMap;

	public SetTimeIntervalListNode(int id) {
		super(id, 
				DISPLAY_NAME, 
				new SetTimeIntervalListPreAction(), 
				new SetTimeIntervalListPostAction());		
	}

	public void performPreUssdAction(MenuSession menuSession) {
		super.performPreUssdAction(menuSession);
		timeIntervalMap = new TreeMap<Integer, String>();
		
		SetTimeIntervalListPreResponse response = (SetTimeIntervalListPreResponse) getResponse();

		int index =1;
		
		for(String timeinterval:response.getTimeIntervalList()) {
			timeIntervalMap.put(index, timeinterval);
			index++;
		}

		reshuffleDefaultMenuMap(timeIntervalMap.size());
	}	

	@Override
	public MenuTreeOperation executeUssd(MenuSession menuSession, String ussdInput) {
		int input = -1;
		try {
			input = Integer.parseInt(ussdInput);
		}catch(Exception ex) {

		}

		MenuTreeOperation operation = processDefaultMenu(menuSession, input);
		if(operation !=null) {
			return operation;
		}

		if(timeIntervalMap.get(input) !=null) {
			menuSession.setTimeInterval(timeIntervalMap.get(input));
			super.performPostUssdAction(menuSession);
			nextSelectedNode = this.childNodes.get(0);
			return MenuTreeOperation.MOVE_TO_NEXT_MENU;

		}

		return MenuTreeOperation.TERMINATE_SESSION;
	}

	public String getDisplayString() {
		StringBuffer sb = new StringBuffer();		
		sb.append(this.displayName);
		sb.append("\n");
		for (Entry<Integer, String> entry : timeIntervalMap.entrySet())
		{
			sb.append(entry.getKey() + "." + entry.getValue());
			sb.append("\n");
		}

		sb.append(getUssdDefaultMenuDisplayString());
		return sb.toString();
	}
}

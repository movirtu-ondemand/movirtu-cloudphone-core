package com.movirtu.vsp.menu.menuitem.action.preaction;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.movirtu.vsp.core.service.iface.VspCoreService;
import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menuitem.action.MenuItemAction;
import com.movirtu.vsp.menu.menuitem.action.preresponse.PurchaseNumberListPreResponse;

@Component
public class PurchaseNumberListPreAction implements MenuItemAction {
	@Autowired
	VspCoreService vspService;

	public Object performAction(MenuSession menuSession) {
		List<String> numberList = vspService.getPurchaseNumbers();
		PurchaseNumberListPreResponse response = new PurchaseNumberListPreResponse();
		
		for(String number: numberList) {
			response.addPurchaseNumber(number);
		}
		
		return response;
	}


}
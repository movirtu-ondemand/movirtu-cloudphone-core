package com.movirtu.vsp.menu.menu.node;

import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menu.MenuTreeOperation;
import com.movirtu.vsp.menu.menuitem.action.postaction.PurchaseNumberConfirmPostAction;
import com.movirtu.vsp.menu.menuitem.action.preaction.NullMenuPreAction;

public class PurchaseNumberConfirmNode extends BaseMenuNode {

	private static final String DISPLAY_NAME = "You selected";
	private String purchaseNumber;

	public PurchaseNumberConfirmNode(int id) {
		super(id, DISPLAY_NAME, new NullMenuPreAction(), new PurchaseNumberConfirmPostAction());
	}

	public void performPreUssdAction(MenuSession menuSession) {
		super.performPreUssdAction(menuSession);
		purchaseNumber = menuSession.getPurchaseNumber();
	}

	@Override
	public MenuTreeOperation executeUssd(MenuSession menuSession, String ussdInput) {
		int input = -1;
		try {
			input = Integer.parseInt(ussdInput);
		}catch(Exception ex) {

		}

		MenuTreeOperation operation = processDefaultMenu(menuSession, input);
		if(operation !=null) {
			return operation;
		}

		super.performPostUssdAction(menuSession);
		setInputToNextNode(purchaseNumber);
		nextSelectedNode = this.childNodes.get(0);
		return MenuTreeOperation.MOVE_TO_NEXT_MENU;
	}

	public String getDisplayString() {
		StringBuffer sb = new StringBuffer();		
		sb.append(this.displayName);
		sb.append(" ");
		sb.append(purchaseNumber);
		sb.append("\n");

		sb.append(getUssdMenuDisplayString());
		sb.append(getUssdDefaultMenuDisplayString());
		return sb.toString();
	}

}

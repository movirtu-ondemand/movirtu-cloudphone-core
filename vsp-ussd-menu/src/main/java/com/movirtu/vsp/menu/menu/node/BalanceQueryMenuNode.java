package com.movirtu.vsp.menu.menu.node;

import java.util.Map;

import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menuitem.action.preaction.BalanceQueryListPreAction;
import com.movirtu.vsp.menu.menuitem.action.preresponse.BalanceQueryPreResponse;

public class BalanceQueryMenuNode extends BaseMenuNode {

	private static final String DISPLAY_NAME = "Balance Query";

	public BalanceQueryMenuNode(int id) {
		super(id, 
				DISPLAY_NAME, 
				new BalanceQueryListPreAction());
	}

	public void performPreUssdAction(MenuSession menuSession) {
		super.performPreUssdAction(menuSession);

	}

	public String getDisplayString() {

		BalanceQueryPreResponse balanceResponse = (BalanceQueryPreResponse) getResponse();

		StringBuffer sb = new StringBuffer();		
		sb.append(this.displayName);
		sb.append("\n");

		Map<String, Double> balanceMap = balanceResponse.getBalanceMap();

		for (Map.Entry<String, Double> entry: balanceMap.entrySet()) {
			sb.append("Balance for " +entry.getKey() +" is "+balanceMap.get(entry.getKey()));
			sb.append("\n");
		}

		sb.append(getUssdDefaultMenuDisplayString());

		return sb.toString();
	}

}

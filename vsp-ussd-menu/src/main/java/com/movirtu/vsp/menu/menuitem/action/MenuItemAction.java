package com.movirtu.vsp.menu.menuitem.action;

import com.movirtu.vsp.menu.menu.MenuSession;

public interface MenuItemAction {

	public Object performAction(MenuSession menuSession);

}

package com.movirtu.vsp.menu.dao;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.movirtu.vsp.menu.Application;

public class VspDefaultMenuResponseDaoImpl {

	private static String config;
	private static SessionFactory sessionFactory;
	private static ServiceRegistry serviceRegistry;

	private Logger logger = Logger.getLogger(VspDefaultMenuResponseDaoImpl.class);

	private static volatile VspDefaultMenuResponseDaoImpl instance; //= new UserDetailsManager();

	private VspDefaultMenuResponseDaoImpl(String config) {

		try {
			// Create the SessionFactory from hibernate.cfg.xml
			Configuration configuration = new Configuration();
			configuration.configure(Application.getXmlPath()+"/"+config);
			serviceRegistry = new ServiceRegistryBuilder().applySettings(
					configuration.getProperties()).buildServiceRegistry();
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);

		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}

	}

	public static void setDatabaseConfigFile(String sConfig) {
		config = sConfig;
	}

	public static VspDefaultMenuResponseDaoImpl getInstance() {
		if(instance == null){
			instance = new VspDefaultMenuResponseDaoImpl(config);
		}
		return instance;
	}


	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void closeSessionFactory() {
		if (sessionFactory != null)
			sessionFactory.close();
	}

}

package com.movirtu.vsp.menu.menuitem.action.preresponse;

import java.util.ArrayList;
import java.util.List;

public class DivertNumberListNumberPreResponse {

	List<String> numberList = new ArrayList<String>();

	public List<String> getNumberList() {
		return numberList;
	}

	public void addNumber(String number) {
		this.numberList.add(number);
	}

}

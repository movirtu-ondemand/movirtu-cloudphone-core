package com.movirtu.vsp.menu.menu;


public class MenuSession {

	private MenuNode currentNode;
	private MenuNode rootNode;
	private String msisdn;
	private String language;
	private String newName;
	private String defaultNumber;
	private String divertNumber;
	private String divertOption;
	private String purchaseNumber;
	private String timeInterval;
	private String terminateSessionString = MenuManager.USSD_SESSSION_END_STRING;

	public MenuSession(MenuNode rootNode) {
		this.rootNode = rootNode;
		this.currentNode = rootNode;
	}

	public MenuSession(String msisdn, MenuNode rootNode) {
		this.rootNode = rootNode;
		this.currentNode = rootNode;
		this.msisdn = msisdn;
	}

	public MenuNode getCurrentNode() {
		return currentNode;
	}

	public void setCurrentNode(MenuNode currentNode) {
		this.currentNode = currentNode;
	}

	public MenuNode getRootNode() {
		return rootNode;
	}

	public void setRootNode(MenuNode rootNode) {
		this.rootNode = rootNode;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getNewName() {
		return newName;
	}

	public void setNewName(String newName) {
		this.newName = newName;
	}

	public String getDefaultNumber() {
		return defaultNumber;
	}

	public void setDefaultNumber(String defaultNumber) {
		this.defaultNumber = defaultNumber;
	}

	public String getDivertNumber() {
		return divertNumber;
	}

	public void setDivertNumber(String divertNumber) {
		this.divertNumber = divertNumber;
	}

	public String getDivertOption() {
		return divertOption;
	}

	public void setDivertOption(String divertOption) {
		this.divertOption = divertOption;
	}

	public String getPurchaseNumber() {
		return purchaseNumber;
	}

	public void setPurchaseNumber(String purchaseNumber) {
		this.purchaseNumber = purchaseNumber;
	}

	public String getTimeInterval() {
		return timeInterval;
	}

	public void setTimeInterval(String timeInterval) {
		this.timeInterval = timeInterval;
	}

	public String getTerminateSessionString() {
		return terminateSessionString;
	}

	public void setTerminateSessionString(String terminateSessionString) {
		this.terminateSessionString = terminateSessionString;
	}
	
	public void resetSessin() {
		
	}

}

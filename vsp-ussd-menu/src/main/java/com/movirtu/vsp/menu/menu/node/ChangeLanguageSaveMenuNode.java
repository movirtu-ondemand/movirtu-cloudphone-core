package com.movirtu.vsp.menu.menu.node;

import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menu.MenuTreeOperation;
import com.movirtu.vsp.menu.menuitem.action.postaction.ChangeLanguageSavePostAction;
import com.movirtu.vsp.menu.menuitem.action.preaction.ChangeLanguageSavePreAction;
import com.movirtu.vsp.menu.menuitem.action.preresponse.ChangeLanguageSavePreResponse;

public class ChangeLanguageSaveMenuNode extends BaseMenuNode {

	private static final String DISPLAY_NAME = "Select Language";
	public SortedMap<Integer, String> languageMap;

	public ChangeLanguageSaveMenuNode(int id) {
		super(id, 
				DISPLAY_NAME, 
				new ChangeLanguageSavePreAction(), 
				new ChangeLanguageSavePostAction());		
	}

	public void performPreUssdAction(MenuSession menuSession) {
		super.performPreUssdAction(menuSession);
		languageMap = new TreeMap<Integer, String>();
		
		ChangeLanguageSavePreResponse languageListResponse = (ChangeLanguageSavePreResponse) getResponse();

		int index =1;

		for(String language:languageListResponse.getLanguageList()) {

			languageMap.put(index, language);
			index++;
		}
		
		reshuffleDefaultMenuMap(languageMap.size());
	}

	@Override
	public MenuTreeOperation executeUssd(MenuSession menuSession, String ussdInput) {
		int input = Integer.parseInt(ussdInput);
		MenuTreeOperation operation = processDefaultMenu(menuSession, input);

		if(operation !=null) {
			return operation;
		}

		if(languageMap.get(input) !=null) {
			menuSession.setLanguage(languageMap.get(input));
			super.performPostUssdAction(menuSession);
			nextSelectedNode = this.childNodes.get(0);
			return MenuTreeOperation.MOVE_TO_NEXT_MENU;

		}

		return MenuTreeOperation.TERMINATE_SESSION;
	}

	public String getDisplayString() {

		StringBuffer sb = new StringBuffer();		

		sb.append(this.displayName);
		sb.append("\n");

		for (Entry<Integer, String> entry : languageMap.entrySet())
		{
			sb.append(entry.getKey() + "." + entry.getValue());
			sb.append("\n");
		}

		sb.append(getUssdDefaultMenuDisplayString());
		return sb.toString();
	}
}

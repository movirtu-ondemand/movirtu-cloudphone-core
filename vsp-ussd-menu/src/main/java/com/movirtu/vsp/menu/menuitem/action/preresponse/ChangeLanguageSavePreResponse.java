package com.movirtu.vsp.menu.menuitem.action.preresponse;

import java.util.ArrayList;
import java.util.List;

public class ChangeLanguageSavePreResponse {

	List<String> languageList = new ArrayList<String>();

	public List<String> getLanguageList() {
		return languageList;
	}

	public void addLanguage(String language) {
		this.languageList.add(language);
	}

}

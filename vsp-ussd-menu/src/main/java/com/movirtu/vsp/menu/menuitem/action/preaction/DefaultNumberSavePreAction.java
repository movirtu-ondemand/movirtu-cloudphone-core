package com.movirtu.vsp.menu.menuitem.action.preaction;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.movirtu.vsp.core.service.iface.VspCoreService;
import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menuitem.action.MenuItemAction;
import com.movirtu.vsp.menu.menuitem.action.preresponse.DefaultNumberSavePreResponse;
import com.movirtu.vsp.persistence.dao.hb.SubscriberInfoDao;
import com.movirtu.vsp.persistence.dao.hb.impl.SubscriberInfoDaoImpl;
@Component
public class DefaultNumberSavePreAction implements MenuItemAction {

	@Autowired
	VspCoreService vspService;
	
	private static Logger logger = LoggerFactory.getLogger(DefaultNumberSavePreAction.class);
	SubscriberInfoDao primaryMsisdnDao = new SubscriberInfoDaoImpl();
	
	public Object performAction(MenuSession menuSession) {
		logger.debug("Inside DefaultNumberSavePreAction:performAction for "+menuSession.getMsisdn());
		
		DefaultNumberSavePreResponse response = new DefaultNumberSavePreResponse();
		
		String primaryMsisdn = menuSession.getMsisdn();
		
		List<String> allNumberList = vspService.getAllAssociatedNumber(primaryMsisdn);
		
		for(String number: allNumberList) {
			response.addDefaultNumber(number);
		}
		
		return response;
	}

}

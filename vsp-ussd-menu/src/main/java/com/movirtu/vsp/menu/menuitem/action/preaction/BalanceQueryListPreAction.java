package com.movirtu.vsp.menu.menuitem.action.preaction;

import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.movirtu.vsp.core.service.iface.VspCoreService;
import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menuitem.action.MenuItemAction;
import com.movirtu.vsp.menu.menuitem.action.preresponse.BalanceQueryPreResponse;

@Component
public class BalanceQueryListPreAction implements MenuItemAction {

	@Autowired
	VspCoreService vspService;
	
	private static Logger logger = LoggerFactory.getLogger(BalanceQueryListPreAction.class);



	public Object performAction(MenuSession menuSession) {
		logger.debug("Inside BalanceQueryListPreAction:performAction for "+menuSession.getMsisdn());
		BalanceQueryPreResponse response = new BalanceQueryPreResponse();
		String primaryMsisdn = menuSession.getMsisdn();

		//		List<PrimaryMsisdn> primaryMsisdnList=null;
		//
		//		boolean success = true;
		//		try {
		//			primaryMsisdnList = primaryMsisdnDao.findByPrimaryMsisdn(primaryMsisdn);
		//
		//		} catch (RecordNotFoundException e) {
		//			logger.info("No primary number entries found");
		//			success=false;
		//		}
		//
		//		if(success || primaryMsisdnList.size()==0) {
		//			response.addBalance(primaryMsisdn, 0.0);
		//			return response;
		//		}
		//
		//		for(PrimaryMsisdn msisdnObject: primaryMsisdnList) {
		//			String msisdn = msisdnObject.getVirtual_msisdn();
		//			logger.debug("Performing balance query for "+msisdn);
		//			double balance = VspCoreServiceImpl.getInstance().getBalance(msisdn);
		//			response.addBalance(msisdn,balance);
		//		}

		Map<String,Double> map = vspService.performBalanceQuery(primaryMsisdn);

		for(Entry<String, Double> entry: map.entrySet()) {
			response.addBalance(entry.getKey(), entry.getValue());
		}

		return response;
	}

}

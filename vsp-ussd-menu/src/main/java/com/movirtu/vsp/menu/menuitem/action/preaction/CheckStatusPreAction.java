package com.movirtu.vsp.menu.menuitem.action.preaction;

import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.movirtu.vsp.core.service.iface.VspCoreService;
import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menuitem.action.MenuItemAction;
import com.movirtu.vsp.menu.menuitem.action.preresponse.CheckStatusPreResponse;
@Component
public class CheckStatusPreAction implements MenuItemAction {

	@Autowired
	VspCoreService vspService;
	
	public Object performAction(MenuSession menuSession) {

		CheckStatusPreResponse response = new CheckStatusPreResponse();
		Map<String,Boolean> statusMap = vspService.checkStatus(menuSession.getMsisdn());

		if(statusMap==null) {
			return response;
		}
		
		for(Entry<String, Boolean> entry:statusMap.entrySet()) {
			
			if(entry.getValue()) {
				response.addStatus(entry.getKey() , "active");
			} else {
				response.addStatus(entry.getKey() , "inactive");
			}
		}

		return response;
	}

}

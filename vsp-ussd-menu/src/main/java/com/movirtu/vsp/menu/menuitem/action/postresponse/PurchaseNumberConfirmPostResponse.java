package com.movirtu.vsp.menu.menuitem.action.postresponse;


public class PurchaseNumberConfirmPostResponse {

	private boolean operationSuccess;

	public boolean isOperationSuccess() {
		return operationSuccess;
	}

	public void setOperationSuccess(boolean operationSuccess) {
		this.operationSuccess = operationSuccess;
	}

}

package com.movirtu.vsp.menu.menuitem.action.postresponse;


public class SetTimeIntervalListPostResponse {

	private boolean operationSuccess;

	public boolean isOperationSuccess() {
		return operationSuccess;
	}

	public void setOperationSuccess(boolean operationSuccess) {
		this.operationSuccess = operationSuccess;
	}

}

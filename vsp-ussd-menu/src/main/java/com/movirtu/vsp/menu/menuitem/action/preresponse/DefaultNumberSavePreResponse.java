package com.movirtu.vsp.menu.menuitem.action.preresponse;

import java.util.ArrayList;
import java.util.List;

public class DefaultNumberSavePreResponse {

	List<String> defaultNumberList = new ArrayList<String>();

	public List<String> getDefaultNumberList() {
		return defaultNumberList;
	}

	public void addDefaultNumber(String defaultNumber) {
		this.defaultNumberList.add(defaultNumber);
	}

}

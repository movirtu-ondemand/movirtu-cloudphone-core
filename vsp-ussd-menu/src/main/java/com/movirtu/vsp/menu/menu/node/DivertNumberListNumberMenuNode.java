package com.movirtu.vsp.menu.menu.node;

import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menu.MenuTreeOperation;
import com.movirtu.vsp.menu.menuitem.action.preaction.DivertNumberListNumberPreAction;
import com.movirtu.vsp.menu.menuitem.action.preresponse.DivertNumberListNumberPreResponse;

public class DivertNumberListNumberMenuNode extends BaseMenuNode {

	private static final String DISPLAY_NAME = "Select number to divert";
	public SortedMap<Integer, String> numberMap;

	public DivertNumberListNumberMenuNode(int id) {
		super(id, 
				DISPLAY_NAME, 
				new DivertNumberListNumberPreAction());		
	}

	public void performPreUssdAction(MenuSession menuSession) {
		super.performPreUssdAction(menuSession);
		numberMap = new TreeMap<Integer, String>();
		
		DivertNumberListNumberPreResponse response = (DivertNumberListNumberPreResponse) getResponse();

		int index =1;

		for(String number:response.getNumberList()) {

			numberMap.put(index, number);
			index++;
		}

		reshuffleDefaultMenuMap(numberMap.size());
	}

	@Override
	public MenuTreeOperation executeUssd(MenuSession menuSession, String ussdInput) {
		int input = -1;
		try {
			input = Integer.parseInt(ussdInput);
		}catch(Exception ex) {

		}

		MenuTreeOperation operation = processDefaultMenu(menuSession, input);

		if(operation !=null) {
			return operation;
		}

		if(numberMap.get(input) !=null) {

			menuSession.setDivertNumber(numberMap.get(input));
			super.performPostUssdAction(menuSession);
			nextSelectedNode = this.childNodes.get(0);
			return MenuTreeOperation.MOVE_TO_NEXT_MENU;

		}

		return MenuTreeOperation.TERMINATE_SESSION;
	}

	public String getDisplayString() {
		StringBuffer sb = new StringBuffer();		
		sb.append(this.displayName);
		sb.append("\n");
		for (Entry<Integer, String> entry : numberMap.entrySet())
		{
			sb.append(entry.getKey() + "." + entry.getValue());
			sb.append("\n");
		}

		sb.append(getUssdDefaultMenuDisplayString());
		return sb.toString();
	}
}
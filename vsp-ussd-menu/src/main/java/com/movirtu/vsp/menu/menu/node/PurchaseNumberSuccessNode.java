package com.movirtu.vsp.menu.menu.node;



public class PurchaseNumberSuccessNode extends BaseMenuNode {

	private static final String DISPLAY_NAME = "Confirm";
	private static final String DISPLAY_STRING = "Thank you. You have sucessfully purchased ";

	public PurchaseNumberSuccessNode(int id) {
		super(id, DISPLAY_NAME);
	}

	public String getDisplayString() {
		StringBuffer sb = new StringBuffer();		
		sb.append(this.DISPLAY_STRING);
		sb.append("\n");

		sb.append(getParentNode().getInputToNextNode());
		sb.append("\n");

		sb.append(getUssdDefaultMenuDisplayString());

		return sb.toString();
	}
}

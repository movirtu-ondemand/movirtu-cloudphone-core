package com.movirtu.vsp.menu.menuitem.action.preresponse;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class BalanceQueryPreResponse {

	Map<String, Double> balanceMap = new ConcurrentHashMap<String, Double>();

	public Map<String, Double> getBalanceMap() {
		return balanceMap;
	}

	public void addBalance(String msisdn, double balance) {
		this.balanceMap.put(msisdn, balance);
	}

	public double getBalance(String msisdn) {
		return this.balanceMap.get(msisdn);
	}

}

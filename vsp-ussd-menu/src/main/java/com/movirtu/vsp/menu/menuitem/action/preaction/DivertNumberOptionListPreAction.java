package com.movirtu.vsp.menu.menuitem.action.preaction;

import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menuitem.action.MenuItemAction;
import com.movirtu.vsp.menu.menuitem.action.preresponse.DivertNumberOptionListPreResponse;

public class DivertNumberOptionListPreAction implements MenuItemAction {

	String[] numberList = {OPTION_VM,OPTION_NUMBER};

	public static final String OPTION_VM = "Divert to voice mail";
	public static final String OPTION_NUMBER = "Divert to number";

	public Object performAction(MenuSession menuSession) {

		DivertNumberOptionListPreResponse response = new DivertNumberOptionListPreResponse();
		response.addDivertOption(OPTION_VM);
		response.addDivertOption(OPTION_NUMBER);
		return response;
	}

}

package com.movirtu.vsp.menu.menuitem.action.preaction;

import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menuitem.action.MenuItemAction;

public class NullMenuPreAction implements MenuItemAction {

	public Object performAction(MenuSession menuSession) {

		return null;
	}
	
}

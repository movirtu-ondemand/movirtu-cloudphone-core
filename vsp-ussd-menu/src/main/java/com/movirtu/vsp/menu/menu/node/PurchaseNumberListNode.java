package com.movirtu.vsp.menu.menu.node;

import java.util.HashMap;
import java.util.Map;

import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menu.MenuTreeOperation;
import com.movirtu.vsp.menu.menuitem.action.preaction.PurchaseNumberListPreAction;
import com.movirtu.vsp.menu.menuitem.action.preresponse.PurchaseNumberListPreResponse;

public class PurchaseNumberListNode extends BaseMenuNode {

	private static final String DISPLAY_NAME = "Purchase new Number";
	public Map<Integer, String> purchaseNumberMap;

	public PurchaseNumberListNode(int id) {
		super(id, DISPLAY_NAME, new PurchaseNumberListPreAction());		
	}

	public void performPreUssdAction(MenuSession menuSession) {
		super.performPreUssdAction(menuSession);
		purchaseNumberMap = new HashMap<Integer, String>();
		
		PurchaseNumberListPreResponse response = (PurchaseNumberListPreResponse) getResponse();

		int index =1;

		for(String number:response.getPurchaseNumberList()) {

			purchaseNumberMap.put(index, number);
			index++;
		}

		reshuffleDefaultMenuMap(purchaseNumberMap.size());
	}

	@Override
	public MenuTreeOperation executeUssd(MenuSession menuSession, String ussdInput) {
		int input = -1;
		try {
			input = Integer.parseInt(ussdInput);
		}catch(Exception ex) {

		}

		MenuTreeOperation operation = processDefaultMenu(menuSession, input);

		if(operation !=null) {
			return operation;
		}

		if(purchaseNumberMap.get(input) !=null) {

			menuSession.setPurchaseNumber(purchaseNumberMap.get(input));
			super.performPostUssdAction(menuSession);
			nextSelectedNode = this.childNodes.get(0);
			return MenuTreeOperation.MOVE_TO_NEXT_MENU;

		}

		return MenuTreeOperation.TERMINATE_SESSION;
	}

	public String getDisplayString() {
		StringBuffer sb = new StringBuffer();		
		sb.append(this.displayName);
		sb.append("\n");
		for (Map.Entry<Integer, String> entry : purchaseNumberMap.entrySet())
		{
			sb.append(entry.getKey() + "." + entry.getValue());
			sb.append("\n");
		}

		sb.append(getUssdDefaultMenuDisplayString());
		return sb.toString();
	}
}

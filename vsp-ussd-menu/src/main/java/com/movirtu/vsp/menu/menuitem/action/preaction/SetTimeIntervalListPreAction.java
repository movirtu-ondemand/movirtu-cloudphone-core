package com.movirtu.vsp.menu.menuitem.action.preaction;

import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menuitem.action.MenuItemAction;
import com.movirtu.vsp.menu.menuitem.action.preresponse.SetTimeIntervalListPreResponse;

public class SetTimeIntervalListPreAction implements MenuItemAction {

	String[] timeIntervals = {"Off 8am-5pm", "On 8am-5pm ", "Off 8am-1pm", "On 8am-1pm "};
	
	public Object performAction(MenuSession menuSession) {
		SetTimeIntervalListPreResponse response = new SetTimeIntervalListPreResponse();
		response.addTimeInterval("Off 8am-5pm");
		response.addTimeInterval("On 8am-5pm");
		response.addTimeInterval("Off 8am-1pm");
		response.addTimeInterval("On 8am-1pm");
		return response;
	}
	
}

package com.movirtu.vsp.menu.menu.node;



public class SetTimeIntervalSuccessNode extends BaseMenuNode {

	private static final String DISPLAY_NAME = "Time Interval set successfully";
	
	public SetTimeIntervalSuccessNode(int id) {
		super(id, DISPLAY_NAME);
	}

	public String getDisplayString() {
		StringBuffer sb = new StringBuffer();		
		sb.append(this.displayName);
		sb.append("\n");

		sb.append(getUssdDefaultMenuDisplayString());
		
		return sb.toString();
	}
	
}

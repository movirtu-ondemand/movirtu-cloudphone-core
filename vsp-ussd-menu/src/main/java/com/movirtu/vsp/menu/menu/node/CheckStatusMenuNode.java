package com.movirtu.vsp.menu.menu.node;

import java.util.Map;

import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menuitem.action.preaction.CheckStatusPreAction;
import com.movirtu.vsp.menu.menuitem.action.preresponse.CheckStatusPreResponse;

public class CheckStatusMenuNode extends BaseMenuNode {

	private static final String DISPLAY_NAME = "Check status";

	public CheckStatusMenuNode(int id) {
		super(id, 
				DISPLAY_NAME, 
				new CheckStatusPreAction());

	}

	public void performPreUssdAction(MenuSession menuSession) {
		super.performPreUssdAction(menuSession);

	}

	public String getDisplayString() {
		CheckStatusPreResponse response = (CheckStatusPreResponse) getResponse();

		StringBuffer sb = new StringBuffer();
		sb.append(this.displayName);
		sb.append("\n");

		Map<String, String> statusMap = response.getStatusMap();

		for (Map.Entry<String, String> entry: statusMap.entrySet()) {
			sb.append(entry.getKey() +" is "+statusMap.get(entry.getKey()));
			sb.append("\n");
		}

		sb.append(getUssdDefaultMenuDisplayString());

		return sb.toString();
	}
}

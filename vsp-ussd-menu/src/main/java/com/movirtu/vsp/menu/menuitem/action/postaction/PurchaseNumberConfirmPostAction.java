package com.movirtu.vsp.menu.menuitem.action.postaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.movirtu.vsp.core.service.iface.VspCoreService;
import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menuitem.action.MenuItemAction;
import com.movirtu.vsp.menu.menuitem.action.postresponse.PurchaseNumberConfirmPostResponse;

@Component
public class PurchaseNumberConfirmPostAction implements MenuItemAction {


	@Autowired
	VspCoreService vspService;
	
	public Object performAction(MenuSession menuSession) {

		System.out.println("Purchasing number");
		PurchaseNumberConfirmPostResponse response = new PurchaseNumberConfirmPostResponse();
		// change status in DB
		vspService.purchaseNumber(menuSession.getPurchaseNumber());
		System.out.println("Sending LU request");
		// Send LU request
		vspService.performLocationUpdate(menuSession.getMsisdn(), menuSession.getPurchaseNumber());
		
		response.setOperationSuccess(true);
		return response;
	}


}

package com.movirtu.vsp.menu.menuitem.action.postaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menuitem.action.MenuItemAction;
import com.movirtu.vsp.menu.menuitem.action.postresponse.DivertNumberOptionListPostResponse;
import com.movirtu.vsp.menu.menuitem.action.preaction.DivertNumberOptionListPreAction;

public class DivertNumberOptionListPostAction implements MenuItemAction {

	Logger logger = LoggerFactory.getLogger(DivertNumberOptionListPostAction.class);

	public Object performAction(MenuSession menuSession) {

		DivertNumberOptionListPostResponse response = new DivertNumberOptionListPostResponse();
		divertNumber(menuSession.getDivertNumber(), menuSession.getDivertOption());
		// TODO save divert number to DB
		response.setOperationSuccess(true);
		return response;
	}

	private void divertNumber(String option, String number) {

		if(option.equalsIgnoreCase(DivertNumberOptionListPreAction.OPTION_NUMBER)) {
			divertToNumber(number);
		} else if(option.equalsIgnoreCase(DivertNumberOptionListPreAction.OPTION_VM)) {
			divertToVoiceMail(number);
		}
	}

	private void divertToVoiceMail(String number) {
		logger.debug("divertToVoiceMail: TODO");
	}


	private void divertToNumber(String number) {
		logger.debug("divertToNumber: TODO");

	}



}

package com.movirtu.vsp.menu.menuitem.action.preaction;

import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menuitem.action.MenuItemAction;
import com.movirtu.vsp.menu.menuitem.action.preresponse.ChangeNameSavePreResponse;

public class ChangeNameSavePreAction implements MenuItemAction {

	public Object performAction(MenuSession menuSession) {

		ChangeNameSavePreResponse response = new ChangeNameSavePreResponse();
		response.setString("Please enter name");
		return response;
	}

}

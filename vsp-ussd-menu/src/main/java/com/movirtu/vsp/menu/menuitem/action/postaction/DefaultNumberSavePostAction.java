package com.movirtu.vsp.menu.menuitem.action.postaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.movirtu.vsp.core.service.iface.VspCoreService;
import com.movirtu.vsp.menu.menu.MenuSession;
import com.movirtu.vsp.menu.menuitem.action.MenuItemAction;
import com.movirtu.vsp.menu.menuitem.action.postresponse.DefaultNumberSavePostResponse;
import com.movirtu.vsp.persistence.dao.hb.SubscriberInfoDao;
import com.movirtu.vsp.persistence.dao.hb.impl.SubscriberInfoDaoImpl;

@Component
public class DefaultNumberSavePostAction implements MenuItemAction {

	
	@Autowired
	VspCoreService vspService;
	private static Logger logger = LoggerFactory.getLogger(DefaultNumberSavePostAction.class);
	SubscriberInfoDao primaryMsisdnDao = new SubscriberInfoDaoImpl();

	public Object performAction(MenuSession menuSession) {
		logger.debug("Inside DefaultNumberSavePostAction:performAction for "+menuSession.getDefaultNumber());
		DefaultNumberSavePostResponse response = new DefaultNumberSavePostResponse();

		String defaultNumber = menuSession.getDefaultNumber();

		if(vspService.setDefaultNumber(defaultNumber)) {
			response.setOperationSuccess(true);
		} else {
			response.setOperationSuccess(false);
		}

		return response;
	}
}

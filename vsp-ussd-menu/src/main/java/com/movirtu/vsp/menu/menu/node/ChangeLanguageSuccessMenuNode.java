package com.movirtu.vsp.menu.menu.node;



public class ChangeLanguageSuccessMenuNode extends BaseMenuNode {

	private static final String DISPLAY_NAME = "Language successfully changed";

	public ChangeLanguageSuccessMenuNode(int id) {
		super(id, DISPLAY_NAME);
	}

	public String getDisplayString() {
		StringBuffer sb = new StringBuffer();		
		sb.append(this.displayName);
		sb.append("\n");
		sb.append(getUssdDefaultMenuDisplayString());
		return sb.toString();
	}

}

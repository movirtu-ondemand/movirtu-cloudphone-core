package org.tinyradius.test;

import org.tinyradius.util.TransactionInfo;

public interface processEAPEvents {
public void event_sim_identity(String status, int event, TransactionInfo obj);
public void event_sim_start_resp(String status, int event, TransactionInfo obj);
public void event_sim_challenge_resp(String status, int event, TransactionInfo obj);
public void event_client_error_code(String status, int event, TransactionInfo obj);
public void event_notification_resp(String status, int event, TransactionInfo obj);

//Events for AKA
public void event_identity_3g(String status, int event, TransactionInfo obj);
public void event_aka_identity_resp(String status, int event, TransactionInfo obj);
public void event_aka_challenge_resp(String status, int event, TransactionInfo obj);
public void event_aka_client_error(String status, int event, TransactionInfo obj);
public void event_aka_notification_resp(String status, int event, TransactionInfo obj);
public void event_aka_authentication_reject(String status, int event, TransactionInfo obj);
public void event_aka_synchronization_failure(String status, int event, TransactionInfo obj);
}

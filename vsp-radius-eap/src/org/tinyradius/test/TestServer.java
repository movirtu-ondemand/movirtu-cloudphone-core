/**
 * $Id: TestServer.java,v 1.6 2006/02/17 18:14:54 wuttke Exp $
 * Created on 08.04.2005
 * @author Matthias Wuttke
 * @version $Revision: 1.6 $
 */
package org.tinyradius.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.util.Properties;

import org.tinyradius.attribute.IntegerAttribute;
import org.tinyradius.attribute.RadiusAttribute;
import org.tinyradius.packet.AccessRequest;
import org.tinyradius.packet.RadiusPacket;
import org.tinyradius.util.RadiusException;
import org.tinyradius.util.RadiusServer;
import org.tinyradius.util.RadiusServerImpl;

/**
 * Test server which terminates after 30 s.
 * Knows only the client "localhost" with secret "testing123" and
 * the user "mw" with the password "test".
 */
public class TestServer {
	private static String versionId;
	private static String atIdentityType;
	public static void main(String[] args) 
	throws IOException, Exception {
		 processEAPEventsImpl eapEventInterface = new processEAPEventsImpl();
		RadiusServerImpl server = new RadiusServerImpl(eapEventInterface);
			
		if (args.length >= 1)
			server.setAuthPort(Integer.parseInt(args[0]));
		if (args.length >= 2)
			server.setAcctPort(Integer.parseInt(args[1]));
		
		server.start(true, true);
		
		System.out.println("Server started.");
		
		Thread.sleep(1000*60*30);
		System.out.println("Stop server");
		server.stop();
	}
	
	public static void loadProperties() {

		Properties prop = new Properties();
		InputStream input = null;

		try {
			input = new FileInputStream("server.config");//file non existent needs to be added 
			// load a properties file
			prop.load(input);
			// get the property value and print it out
			versionId = prop.getProperty("versionId");
			atIdentityType = prop.getProperty("atIdentityType");
			
			//heartBeatInterval = Integer.parseInt(prop.getProperty("heartBeatInterval"));
			

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
	


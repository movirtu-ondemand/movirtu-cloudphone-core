package org.tinyradius.test;

import org.tinyradius.packet.AccessRequest;
import org.tinyradius.packet.RadiusPacket;

public class SubscriberInfo {
	public String imsi;
	public String sessionId;
	public int event;
	public RadiusPacket packet;
	public AccessRequest ar;

}

package org.tinyradius.test;

import java.util.HashMap;

public class HashMapDS {

	public static HashMap<String, SubscriberInfo> hm = new HashMap<String, SubscriberInfo>();

	public HashMapDS() {
		System.out.println("called demoHash constructor");
	}

	public static final HashMapDS INSTANCE = new HashMapDS();

	public void insertInHash(String key, SubscriberInfo obj) {
		synchronized (this) {
			hm.put(key, obj);
		}
	}

	public SubscriberInfo searchInHash(String key) {
		synchronized (this) {
			SubscriberInfo obj = (SubscriberInfo) hm.get(key);
			return obj;
		}
	}

	public boolean deleteFromHash(String key) {
		synchronized (this) {
			if (hm.containsKey(key) == true) {
				hm.remove(key);
				return true;
			} else {
				return false;
			}
		}
	}

}

package org.tinyradius.test;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.tinyradius.util.RadiusServerImpl;
import org.tinyradius.util.TransactionInfo;

public class processEAPEventsImpl implements processEAPEvents {
	 protected Log logger = LogFactory.getLog(processEAPEventsImpl.class);
	public RadiusServerImpl triggerEapMsg ;
	public processEAPEventsImpl(RadiusServerImpl triggerEapMsg) {
		
			this.triggerEapMsg = triggerEapMsg;
	}
	public processEAPEventsImpl() {
		
	
	}
	public void event_sim_identity(String status, int event, TransactionInfo obj) {
		logger.debug("inside event_sim_identity");
		try {
			triggerEapMsg = obj.rServerImplObj;
		if (status == "success") {
			
			SubscriberInfo subs = new SubscriberInfo();
			subs.imsi = obj.eapImsi;
			subs.sessionId = obj.sessionId;
			subs.event = obj.eapType;
			subs.packet = obj.packet;
			subs.ar = obj.ar;
			HashMapDS ds = HashMapDS.INSTANCE;
			ds.insertInHash(obj.sessionId, subs);
			logger.debug(" TS6 about to invoke trigger_eap_aka_challenge");
			triggerEapMsg.trigger_eap_aka_challenge(obj);
			}
			
		} catch(Exception e) {
			logger.debug("exception occured");
			e.printStackTrace();
		}
	}

	public void event_sim_start_resp(String status, int event,
			TransactionInfo obj) {
		logger.debug("inside event_sim_start_resp");
		try {
			
			triggerEapMsg = obj.rServerImplObj;
		HashMapDS ds = HashMapDS.INSTANCE;
		SubscriberInfo subs = ds.searchInHash(obj.sessionId);
		subs.event = obj.eapType;
		subs.packet = obj.packet;
		subs.ar = obj.ar;
		obj.eapImsi = subs.imsi;
		
		logger.debug(" TS2 about to invoke event_sim_start_resp");
		triggerEapMsg.trigger_sim_challenge(obj);
		
		/*logger.debug(" TS2 about to invoke trigger_notification");
		triggerEapMsg.trigger_notification(obj);*/
		} catch(Exception e) {
		logger.debug("exception occured");
		e.printStackTrace();
	}
	}

	public void event_sim_challenge_resp(String status, int event, TransactionInfo obj) {
		logger.debug("inside event_sim_challenge_resp");
		try {
			triggerEapMsg = obj.rServerImplObj;
		HashMapDS ds = HashMapDS.INSTANCE;
		SubscriberInfo subs = ds.searchInHash(obj.sessionId);
		
		obj.eapImsi = subs.imsi;
		logger.debug(" TS4 about to invoke trigger_sim_success");
		triggerEapMsg.trigger_sim_success(obj);
		} catch(Exception e) {
		logger.debug("exception occured");
		e.printStackTrace();
	}
	}
	public void event_client_error_code(String status, int event, TransactionInfo obj) {
		logger.debug("inside event_client_error_code");
		try {
			triggerEapMsg = obj.rServerImplObj;
		HashMapDS ds = HashMapDS.INSTANCE;
		SubscriberInfo subs = ds.searchInHash(obj.sessionId);
		
		obj.eapImsi = subs.imsi;
		logger.debug(" TS4 about to invoke trigger_eap_failure_packet");
		triggerEapMsg.trigger_eap_failure_packet(obj);
		} catch(Exception e) {
		logger.debug("exception occured");
		e.printStackTrace();
	}
	}
	public void event_notification_resp(String status, int event, TransactionInfo obj) {
		logger.debug("inside event_notification_resp");
		try {
			triggerEapMsg = obj.rServerImplObj;
		HashMapDS ds = HashMapDS.INSTANCE;
		SubscriberInfo subs = ds.searchInHash(obj.sessionId);
		
		obj.eapImsi = subs.imsi;
		logger.debug(" TS4 about to invoke trigger_eap_failure_packet");
		triggerEapMsg.trigger_eap_failure_packet(obj);
		} catch(Exception e) {
		logger.debug("exception occured");
		e.printStackTrace();
	}
	}
	public void event_identity_3g(String status, int event, TransactionInfo obj) {
		logger.debug("inside event_identity");
		try {
			triggerEapMsg = obj.rServerImplObj;
		if (status == "success") {
			
			SubscriberInfo subs = new SubscriberInfo();
			subs.imsi = obj.eapImsi;
			subs.sessionId = obj.sessionId;
			subs.event = obj.eapType;
			subs.packet = obj.packet;
			subs.ar = obj.ar;
			HashMapDS ds = HashMapDS.INSTANCE;
			ds.insertInHash(obj.sessionId, subs);
			logger.debug(" TS6 about to invoke trigger_eap_aka_identity");
			triggerEapMsg.trigger_eap_aka_identity(obj);
			}
			
		} catch(Exception e) {
			logger.debug("exception occured");
			e.printStackTrace();
		}
	}
	public void event_aka_challenge_resp(String status, int event, TransactionInfo obj) {
		logger.debug("inside event_aka_challenge_resp");
		try {
			triggerEapMsg = obj.rServerImplObj;
		HashMapDS ds = HashMapDS.INSTANCE;
		SubscriberInfo subs = ds.searchInHash(obj.sessionId);
		if(subs!=null)
			obj.eapImsi = subs.imsi;
		logger.debug(" invoke trigger_eap_success_packet for sessionId = "+obj.sessionId);
		triggerEapMsg.trigger_eap_aka_success(obj);
		} catch(Exception e) {
		logger.debug("exception occured");
		e.printStackTrace();
		}
	}
	
	public void event_aka_identity_resp(String status, int event, TransactionInfo obj) {
		logger.debug("inside event_aka_identity_resp");
		try {
			triggerEapMsg = obj.rServerImplObj;
		HashMapDS ds = HashMapDS.INSTANCE;
		SubscriberInfo subs = ds.searchInHash(obj.sessionId);
		
		obj.eapImsi = subs.imsi;
		logger.debug(" TS4 about to invoke trigger_eap_failure_packet");
		triggerEapMsg.trigger_eap_aka_challenge(obj);
		} catch(Exception e) {
		logger.debug("exception occured");
		e.printStackTrace();
		}
	}
	public void event_aka_client_error(String status, int event, TransactionInfo obj) {
		
	}
	public void event_aka_notification_resp(String status, int event, TransactionInfo obj) {
		
	}
	public void event_aka_authentication_reject(String status, int event, TransactionInfo obj) {
		
	}
	public void event_aka_synchronization_failure(String status, int event, TransactionInfo obj) {
		
	}
}

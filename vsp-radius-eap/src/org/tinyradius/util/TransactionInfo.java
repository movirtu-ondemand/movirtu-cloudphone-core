package org.tinyradius.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.tinyradius.packet.AccessRequest;
import org.tinyradius.packet.RadiusPacket;

public class TransactionInfo {
	// public static int id = 0;
	
	public static final int ERROR_MANDATORY_PARAM_MISSING = 1;
	public static final int  ERROR_EVENT_UNEXPECTED = 2;
	public static final int  ERROR_UNEXPECTED_NON_SKIPPABLE_ATTR= 3;
	
	public static final int EAP_AKA_3G = 0;
	public static final int  EAP_SIM_GSM = 1;
	protected Log logger = LogFactory.getLog(TransactionInfo.class);
	public String callingStationId;
	public byte[] eapMessage;
	public String sessionId;//used to differentiate between sessions
	public String result;//if "success" than encoding successfull else need to take care of failure handling
	public int errorCode;//initialized to -1, set by service only in case of ERROR_EVENT_UNEXPECTED
	
	
	/* atNotificationPbitStatus set by service when AT_NOTIFICATION needs to be sent.
	 *  if event before challenge than atNotificationPbitStatus = true
	 *  else atNotificationPbitStatus = false
	 */
	public boolean atNotificationPbitStatus;
	public String radiusMsgType;
	public byte subType;//defines eap-message subtype 10=sim start, 11= challenge, 12 = notification, 14 = client error
	public byte[] atMac;
	public byte atMacPosRespChallenge;//stores position of AT_MAC value part in challenge resp message
	public String eapImsi;//subscriber identity extracted from at_identity
	public byte[] eapMtNonce;
	public byte[] at_identity;//complete subscriber identity as received in EAP message
	public byte[] eapSelectedVersion;//value
	public byte eapCode, eapId, eapType;
	public int eapLength;
	public RadiusPacket packet;
	public AccessRequest ar;//received radius request
	public RadiusServerImpl rServerImplObj;
	int eapClientErrorCode;//error code received 
	
	public List<String> atRand;//to be populated by service after taking avlues from MAPGw
	public List<String> kc;//to be populated by service after taking avlues from MAPGw
	public List<String> sres;//to be populated by service after taking avlues from MAPGw
	
	/*EAP-AKA specific authentication params */
	public List<String> ik;//to be populated by service after taking avlues from MAPGw
	public List<String> ck;//to be populated by service after taking avlues from MAPGw
	public List<String> autn;//to be populated by service after taking avlues from MAPGw
	public List<String> res;//to be populated by service after taking avlues from MAPGw
	
	public byte[] kAut;//to be stored by service and used in case of reauthentication
	public byte[] kEncr;//to be stored by service and used in case of reauthentication
	public byte[] msk;//to be stored by service and used in case of reauthentication
	public byte[] eMsk;//to be stored by service and used in case of reauthentication
	public byte radiusPacketId;
	public int eapProtocolSupported;//value 0 for EAP-AKA and 1 for EAP-SIM
	public TransactionInfo() {
		atRand = new ArrayList<String>();
		kc = new ArrayList<String>();
		sres = new ArrayList<String>();
		
		ik = new ArrayList<String>();
		ck = new ArrayList<String>();
		autn = new ArrayList<String>();
		res = new ArrayList<String>();
		eapProtocolSupported = TransactionInfo.EAP_SIM_GSM;
		/*GSM*/
		/*atRand.add("101112131415161718191a1b1c1d1e1f");
		atRand.add("202122232425262728292a2b2c2d2e2f");
		atRand.add("303132333435363738393a3b3c3d3e3f");
		*/
		/* 3G*/
		 atRand.add("c6f3ce8d02394218d89ff793cd125276");
		//atRand.add("df59680dbddab2209ea13f130e4386cc");
		
		/*GSM*/
		/*kc.add("a0a1a2a3a4a5a6a7");
		kc.add("b0b1b2b3b4b5b6b7");
		kc.add("c0c1c2c3c4c5c6c7");*/
		
		/* ck 3G*/
		ck.add("f91ef697156e4c33abb8607506e8c35e");
		//ck.add("849a2bdd898724b46cd2c5c2366a7fcf");
		
		/* ik 3G*/
		ik.add("5f0dec76a391cbc863daaac654eefb46");
		//ik.add("849a2bdd898724b46cd2c5c2366a7fcf");
		
		/*autn 3G*/
		autn.add("10e4d64201dd000090d43cee472bfaf3");
		//autn.add("849a2bdd898724b46cd2c5c2366a7fcf");
		
		/*res 3G*/
		res.add("bf84a0cd");
		//res.add("849a2bdd898724b46cd2c5c2366a7fcf");
		
		errorCode = -1;
		atNotificationPbitStatus = true;
	}
	public void display() {
		try {
			logger.debug("sessionId = " + sessionId);
			logger.debug("eapImsi = " + eapImsi);

			logger.debug("eapCode = " + eapCode);
			logger.debug("eapId = " + eapId);
			logger.debug("eapType = " + eapType);
			logger.debug("eapLength = " + eapLength);
			logger.debug("subType = " + subType);
			logger.debug("radiusPacketId = " + radiusPacketId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

package org.tinyradius.util;

public interface triggerEAPEvents {
public void trigger_sim_start(TransactionInfo obj);
public void trigger_sim_challenge(TransactionInfo obj);
public void trigger_sim_success(TransactionInfo obj);
public void trigger_notification(TransactionInfo obj);
public void trigger_eap_failure_packet(TransactionInfo obj);

public void trigger_eap_aka_identity(TransactionInfo obj);
public void trigger_eap_aka_challenge(TransactionInfo obj);
public void trigger_eap_aka_notification(TransactionInfo obj);
public void trigger_eap_aka_failure_packet(TransactionInfo obj);
public void trigger_eap_aka_success(TransactionInfo obj);
}

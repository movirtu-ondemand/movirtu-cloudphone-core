package org.tinyradius.util;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.Formatter;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.tinyradius.attribute.RadiusAttribute;
import org.tinyradius.dictionary.EAPAvpTypes;
import org.tinyradius.dictionary.EAPMsgTypes;
import org.tinyradius.packet.AccessRequest;
import org.tinyradius.packet.RadiusPacket;
import org.tinyradius.test.processEAPEventsImpl;

public class RadiusServerImpl extends RadiusServer implements triggerEAPEvents {
	protected processEAPEventsImpl eapEventInterface = null;
	protected Log logger = LogFactory.getLog(RadiusServerImpl.class);

	// private RadiusPacket packet;
	public RadiusServerImpl(processEAPEventsImpl eapEventInterface) {

		this.eapEventInterface = eapEventInterface;
	}

	// Authorize localhost/testing123
	public String getSharedSecret(InetSocketAddress client) {
		/*
		 * if (client.getAddress().getHostAddress().equals("127.0.0.1")) return
		 * "testing123"; else return null;
		 */
		return "123456secret";
	}

	public String getSharedSecret() {
		/*
		 * if (client.getAddress().getHostAddress().equals("127.0.0.1")) return
		 * "testing123"; else return null;
		 */
		return "123456secret";
	}

	// Authenticate mw
	public String getUserPassword(String userName) {
		/*
		 * if (userName.equals("mw")) return "test"; else return null;
		 */
		return "test";
	}

	// Adds an attribute to the Access-Accept packet
	public RadiusPacket accessRequestReceived(AccessRequest accessRequest,
			InetSocketAddress client) throws RadiusException {
		System.out.println("Received Access-Request:\n" + accessRequest);
		TransactionInfo obj = decodeRadiusParams(accessRequest);
		boolean decodingResult = decodeEAPPackets(obj.eapMessage, obj);
		obj.ar = accessRequest;
		obj.rServerImplObj = this;
		obj.display();
		/*
		 * RadiusPacket packet = super .accessRequestReceived(accessRequest,
		 * client);
		 */
		//String plaintext = getUserPassword(accessRequest.getUserName());
		int typeRadius = 0;
		
		/*if (obj.eapType == 1)
			typeRadius = RadiusPacket.ACCESS_CHALLENGE;
		else if (obj.eapType == 18 && obj.subType == 10)// start response
			typeRadius = RadiusPacket.ACCESS_CHALLENGE;
		else if (obj.eapType == 18 && obj.subType == 11)// challenge response
			typeRadius = RadiusPacket.ACCESS_ACCEPT;
		else if (obj.eapType == 14) // client error code
			typeRadius = RadiusPacket.ACCESS_REJECT;
		else if ( obj.subType == 12)
			// AT_NOTIFICATION for failure cases,before challenge round,P=1,S=0
			typeRadius = RadiusPacket.ACCESS_CHALLENGE;
		else
			typeRadius = RadiusPacket.ACCESS_ACCEPT;*/
		
		
		/* rohit end of temp code */
		/*
		 * RadiusPacket packet = new RadiusPacket(typeRadius,
		 * accessRequest.getPacketIdentifier()); copyProxyState(accessRequest,
		 * packet);
		 */
		RadiusPacket packet = new RadiusPacket();
		copyProxyState(accessRequest, packet);
		packet.setPacketIdentifier(obj.radiusPacketId);
		obj.packet = packet;

		/* rohit temp changes */
		if (obj.eapType == 1) {
			if (obj.packet != null) {
				if(obj.eapProtocolSupported == TransactionInfo.EAP_SIM_GSM)	{
				logger.debug("calling event_sim_identity for sessionId = "
						+ obj.sessionId);
				if (eapEventInterface != null) {
					logger.debug("eapEventInterface not null,sessionId "
							+ obj.sessionId);
					eapEventInterface.event_sim_identity(obj.result,
							obj.eapType, obj);
				}
				logger.debug("TS1R sending response ");
				return obj.packet;
				} else {
					logger.debug("Iphone supporting EAP-AKA");
					logger.debug("calling event_identity for sessionId = "
							+ obj.sessionId);
					if (eapEventInterface != null) {
						logger.debug("eapEventInterface not null,sessionId "
								+ obj.sessionId);
						eapEventInterface.event_identity_3g(obj.result,
								obj.eapType, obj);
					}
					logger.debug("TS6R sending response ");
					return obj.packet;
					
				}
			} else {
				logger.warn("Empty radius accept packet");
				// send status failure or perform some action
			}
		} else if (obj.eapType == 0x12 && (obj.subType == 0x0a)) {
			if (obj.packet != null) {
				logger.debug("calling event_sim_start_resp for sessionId = "
						+ obj.sessionId);
			}
			if (eapEventInterface != null) {
				logger.debug("eapEventInterface not null,sessionId "
						+ obj.sessionId);
				eapEventInterface.event_sim_start_resp(obj.result, obj.eapType,
						obj);
			}
			logger.debug("TS2R sending response ");
			return obj.packet;
		} else if (obj.eapType == 0x12 && (obj.subType == 0x0b)) {
			if (obj.packet != null) {
				logger.debug("calling event_sim_challenge_resp for sessionId = "
						+ obj.sessionId);
			}
			if (eapEventInterface != null) {
				logger.debug("eapEventInterface not null,sessionId "
						+ obj.sessionId);
				eapEventInterface.event_sim_challenge_resp(obj.result,
						obj.eapType, obj);
			}
			logger.debug("TS3R sending response ");
			return obj.packet;
		} else if (obj.eapType == 0x12 && (obj.subType == 0x0c)) {
			if (obj.packet != null) {
				logger.debug("calling event_notification_resp for sessionId = "
						+ obj.sessionId);
			}
			if (eapEventInterface != null) {
				logger.debug("eapEventInterface not null,sessionId "
						+ obj.sessionId);
				eapEventInterface.event_notification_resp(obj.result,
						obj.eapType, obj);
			}
			logger.debug("TS5R sending response ");
			return obj.packet;
		} else if (obj.eapType == 0x12 && (obj.subType == 0x0d)) {

			logger.debug("received reauthentication request for sessionId = "
					+ obj.sessionId);

		}

		else if (obj.eapType == 0x12 && (obj.subType == 0x0e)) {
			if (obj.packet != null) {
				logger.debug("calling event_client_error_code for sessionId = "
						+ obj.sessionId);
			}
			if (eapEventInterface != null) {
				logger.debug("eapEventInterface not null,sessionId "
						+ obj.sessionId);
				eapEventInterface.event_client_error_code(obj.result,
						obj.eapType, obj);
			}
			logger.debug("TS4R sending response ");
			return obj.packet;
		} else if(obj.eapType == EAPMsgTypes.EAP_AKA && obj.subType == EAPMsgTypes.AKA_IDENTITY) {
			if (obj.packet != null) {
				logger.debug("event_aka_identity_resp calling  for sessionId = "
						+ obj.sessionId);
			}
			if (eapEventInterface != null) {
				logger.debug("eapEventInterface not null,sessionId "
						+ obj.sessionId);
				eapEventInterface.event_aka_identity_resp(obj.result,
						obj.eapType, obj);
			}
			logger.debug("TS7R sending response ");
			return obj.packet;
		} else if(obj.eapType == EAPMsgTypes.EAP_AKA && obj.subType == EAPMsgTypes.AKA_CHALLENGE){
			if (obj.packet != null) {
				logger.debug("calling event_aka_challenge_resp for sessionId = "
						+ obj.sessionId);
			}
			if (eapEventInterface != null) {
				logger.debug("eapEventInterface not null,sessionId "
						+ obj.sessionId);
				eapEventInterface.event_aka_challenge_resp(obj.result,
						obj.eapType, obj);
			}
			logger.debug("TS6R sending response ");
			return obj.packet;
		} else if(obj.eapType == EAPMsgTypes.EAP_AKA && obj.subType == EAPMsgTypes.SIM_CLIENT_ERROR){
			if (obj.packet != null) {
				logger.debug("calling event_aka_client_error for sessionId = "
						+ obj.sessionId);
			}
			if (eapEventInterface != null) {
				logger.debug("eapEventInterface not null,sessionId "
						+ obj.sessionId);
				eapEventInterface.event_aka_client_error(obj.result,
						obj.eapType, obj);
			}
			logger.debug("TS6R sending response ");
			return obj.packet;
		} else if(obj.eapType == EAPMsgTypes.EAP_AKA && obj.subType == EAPMsgTypes.AKA_SYNCHRONIZATION_FAILURE){
			if (obj.packet != null) {
				logger.debug("calling event_aka_synchronization_failure for sessionId = "
						+ obj.sessionId);
			}
			if (eapEventInterface != null) {
				logger.debug("eapEventInterface not null,sessionId "
						+ obj.sessionId);
				eapEventInterface.event_aka_synchronization_failure(obj.result,
						obj.eapType, obj);
			}
			logger.debug("TS6R sending response ");
			return obj.packet;
		} else if(obj.eapType == EAPMsgTypes.EAP_AKA && obj.subType == EAPMsgTypes.AKA_AUTHENTICATION_REJECT){
			if (obj.packet != null) {
				logger.debug("calling event_aka_authentication_reject for sessionId = "
						+ obj.sessionId);
			}
			if (eapEventInterface != null) {
				logger.debug("eapEventInterface not null,sessionId "
						+ obj.sessionId);
				eapEventInterface.event_aka_authentication_reject(obj.result,
						obj.eapType, obj);
			}
			logger.debug("TS6R sending response ");
			return obj.packet;
		} else if(obj.eapType == EAPMsgTypes.EAP_AKA && obj.subType == EAPMsgTypes.SIM_NOTIFICATION){
			if (obj.packet != null) {
				logger.debug("calling event_aka_notification_resp for sessionId = "
						+ obj.sessionId);
			}
			if (eapEventInterface != null) {
				logger.debug("eapEventInterface not null,sessionId "
						+ obj.sessionId);
				eapEventInterface.event_aka_notification_resp(obj.result,
						obj.eapType, obj);
			}
			logger.debug("TS6R sending response ");
			return obj.packet;
		}

		// service has responded back with the trigger

		{

			packet = obj.packet;
			
		}
		if (packet == null)
			System.out.println("Ignore packet.");
		else
			System.out.println("Answer:\n" + packet);
		return packet;
	}

	public TransactionInfo decodeRadiusParams(AccessRequest accessRequest) {
		TransactionInfo obj = new TransactionInfo();
		RadiusAttribute sessionId = accessRequest.getAttribute(44);// sessionId
		obj.sessionId = sessionId.getAttributeValue();
		obj.radiusPacketId = (byte)accessRequest.getPacketIdentifier();
		RadiusAttribute eapPacket = accessRequest.getAttribute(79);// eapPacket
		obj.eapMessage = eapPacket.getAttributeData();
		String hexStream = ByteToHexStream(obj.eapMessage);
		logger.debug("EAP-Packet = " + hexStream);

		return obj;
	}

	public boolean decodeEAPPackets(byte[] eapPacket, TransactionInfo obj) {
		String hex = ByteToHexStream(eapPacket);
		logger.debug("eap packet = " + hex);
		try {
			boolean at_nonce_pres = false;
			boolean at_mac_pres = false;
			boolean non_skippable_attr_pres = false;
			obj.eapCode = eapPacket[0];
			obj.eapId = eapPacket[1];
			obj.eapLength = eapPacket[2];
			obj.eapLength = obj.eapLength << 8;
			obj.eapLength += eapPacket[3];
			obj.eapType = eapPacket[4];
			obj.result = "success";

			// obj.display();
			HelperClass helper = new HelperClass();
			byte handsetIdType;
			int msgIterator = 6;
			switch (obj.eapType) {
			case 1:// identity request

				handsetIdType = (byte) (eapPacket[5] - 0x30);
				if (handsetIdType == 1) {
					try {
						byte[] imsiByte = new byte[15];
						for (int i = 0; i < 15; i++) {
							imsiByte[i] = eapPacket[6 + i];
						}
						obj.eapImsi = ByteToDecStream(imsiByte);
						obj.eapProtocolSupported = TransactionInfo.EAP_SIM_GSM;
					} catch (Exception e) {
						e.printStackTrace();
						return false;
					}

				} else if(handsetIdType == 0) { 
					try {
						byte[] imsiByte = new byte[15];
						for (int i = 0; i < 15; i++) {
							imsiByte[i] = eapPacket[6 + i];
						}
						obj.eapImsi = ByteToDecStream(imsiByte);
						obj.eapProtocolSupported = TransactionInfo.EAP_AKA_3G;
					} catch (Exception e) {
						e.printStackTrace();
						return false;
					}
				} else {
					logger.warn("The first field of IMSI is not set to 1 as expected");
					// need to send Notification request
					obj.result = "failure";
					obj.errorCode = TransactionInfo.ERROR_MANDATORY_PARAM_MISSING;
					return false;
				}
				break;
			case EAPMsgTypes.EAP_SIM:// EAP-SIM
				logger.debug("received type=18 and subtype =10");
				obj.subType = eapPacket[5];
				
				if (obj.subType == EAPMsgTypes.SIM_START) {
					logger.debug("subtype of received EAP message = 10");
					// SIM start response received
					msgIterator += 2;// 2 bytes are reserved

					while (msgIterator < obj.eapLength) {
						logger.debug("inside while loop, eapPacket value = "
								+ eapPacket[msgIterator]);
						if (eapPacket[msgIterator] == 14) {// AT_IDENTITY
							logger.debug("parsing AT_IDENTITY, eapPacket value = "
									+ eapPacket[msgIterator]);
							msgIterator++;
							int len = eapPacket[msgIterator];
							msgIterator++;
							byte[] lengthByteArr = new byte[2];
							lengthByteArr[0] = eapPacket[msgIterator];
							msgIterator++;
							lengthByteArr[1] = eapPacket[msgIterator];
							// length of actual Handset identity excluding
							// Handset
							// Identity type(set to 1 for IMSI)
							int handsetIdLength = findLength2Bytes(lengthByteArr);
							logger.debug("length of identity");
							msgIterator++;
							byte[] at_identity = new byte[handsetIdLength];
							for (int m = 0; m < handsetIdLength; m++) {
								at_identity[m] = eapPacket[msgIterator + m];
							}
							obj.at_identity = at_identity;
							logger.debug("obj.at_identity = "
									+ helper.bytesToHex(obj.at_identity));
							handsetIdType = (byte) (eapPacket[msgIterator] - 0x30);
							msgIterator++;
							if (handsetIdType == 1) {
								try {
									byte[] imsiByte = new byte[15];
									for (int i = 0; i < 15; i++) {
										imsiByte[i] = eapPacket[msgIterator + i];
									}
									obj.eapImsi = ByteToDecStream(imsiByte);
								} catch (Exception e) {
									e.printStackTrace();
									return false;
								}

							} else {
								logger.warn("The first field of IMSI is not set to 1 as expected");
								obj.result = "failure";
								obj.errorCode = TransactionInfo.ERROR_MANDATORY_PARAM_MISSING;
								return false;
							}
							msgIterator += handsetIdLength;

						}
						else if (eapPacket[msgIterator] == 16) {// AT_SELECTED_VERSION
							logger.debug("parsing AT_SELECTED_VERSION");
							msgIterator++;
							byte atSelVerLength = eapPacket[msgIterator];
							msgIterator++;
							byte[] value = new byte[2];
							value[0] = eapPacket[msgIterator];
							msgIterator++;
							value[1] = eapPacket[msgIterator];
							msgIterator++;
							obj.eapSelectedVersion = value;
							logger.debug("obj.eapSelectedVersion = "
									+ helper.bytesToHex(obj.eapSelectedVersion));
						}
						else if (eapPacket[msgIterator] == 7) {// AT_NONCE_MT

							msgIterator += 4; // 1 for AT_NONCE_MT_Type, 1 for
							byte[] atNonceMt = new byte[16];
							for (int i = 0; i < 16; i++) {
								atNonceMt[i] = eapPacket[msgIterator + i];
							}
							// length, 2 for reserved fields
							logger.debug("AT_NONCE_MT, atNonceMt ="
									+ helper.bytesToHex(atNonceMt));
							obj.eapMtNonce = atNonceMt;
							msgIterator += 16;
							logger.debug("obj.eapMtNonce = "
									+ helper.bytesToHex(obj.eapMtNonce));
							at_nonce_pres = true;
							/*
							 * conditional parameter we assume always
							 * authentication will happen and no
							 * fast-reauthentication so this parameter would
							 * always be required
							 */

						}
						else if (((byte) eapPacket[msgIterator] < 127 && (eapPacket[msgIterator] != 7
								&& eapPacket[msgIterator] != 16 && eapPacket[msgIterator] != 14)) == true) {
							// received non-skippable unexpected attribute
							// msgIterator++;
							logger.debug("received unexpected non-skippable attribute of type = "
									+ eapPacket[msgIterator]);
							int len = eapPacket[msgIterator + 1] * 4;
							msgIterator += len;
							non_skippable_attr_pres = true;
							obj.errorCode = TransactionInfo.ERROR_UNEXPECTED_NON_SKIPPABLE_ATTR;
							

						}
						else {
							
						}
					}

				} else if (obj.subType == EAPMsgTypes.SIM_CHALLENGE) {
					// case when sim-challenge response is received
					logger.debug("subtype of received EAP message = 11,sessionId = "
							+ obj.sessionId);
					// SIM start response received
					msgIterator += 2;// 2 bytes are reserved

					while (msgIterator < obj.eapLength) {
						logger.debug("inside while loop, eapPacket value = "
								+ eapPacket[msgIterator]);
						if (eapPacket[msgIterator] == 135) {// AT_RESULT_IND
							msgIterator += 4;
						}
						else if (eapPacket[msgIterator] == 11) {// AT_MAC

							at_mac_pres = true;
							msgIterator += 4;
							obj.atMacPosRespChallenge = (byte) msgIterator;
							for (int i = 0; i < 16; i++) {
								obj.atMac[i] = eapPacket[msgIterator + i];
							}
							msgIterator += 16;
						}
						else if (((byte) eapPacket[msgIterator] < 127 && (eapPacket[msgIterator] != 7
								&& eapPacket[msgIterator] != 16 && eapPacket[msgIterator] != 14)) == true) {
							// received non-skippable unexpected attribute
							// msgIterator++;
							logger.debug("received unexpected non-skippable attribute of type = "
									+ eapPacket[msgIterator]);
							int len = eapPacket[msgIterator + 1] * 4;
							msgIterator += len;
							non_skippable_attr_pres = true;
							obj.errorCode = TransactionInfo.ERROR_UNEXPECTED_NON_SKIPPABLE_ATTR;

						}
						else {
							
						}
					}
				} else if (obj.subType == EAPMsgTypes.SIM_CLIENT_ERROR) {// AT_CLIENT_ERROR
					msgIterator += 2;// 2 bytes are reserved, initially set to 6
					while (msgIterator < obj.eapLength) {
						if (eapPacket[msgIterator] == 22) {// AT_CLIENT_ERROR_CODE

							int len = eapPacket[msgIterator + 1] * 4;
							byte[] valClientErrorCode = {
									(byte) eapPacket[msgIterator],
									(byte) eapPacket[msgIterator + 1] };
							obj.eapClientErrorCode = findLength2Bytes(valClientErrorCode);
							msgIterator += len;
							logger.debug("received AT_CLIENT_ERROR,msgIterator = "+ msgIterator);
							logger.debug("received AT_CLIENT_ERROR, obj.eapLength = "+obj.eapLength);
						}
						else if (((byte) eapPacket[msgIterator] < 127 && (eapPacket[msgIterator] != 7
								&& eapPacket[msgIterator] != 16 && eapPacket[msgIterator] != 14)) == true) {
							// received non-skippable unexpected attribute
							// msgIterator++;
							logger.debug("received unexpected non-skippable attribute of type = "
									+ eapPacket[msgIterator]);
							int len = eapPacket[msgIterator + 1] * 4;
							msgIterator += len;
							non_skippable_attr_pres = true;
							obj.errorCode = TransactionInfo.ERROR_UNEXPECTED_NON_SKIPPABLE_ATTR;

						}
						else {
							
						}
					}

				}
				logger.debug("comming out of parsing type=18 ");
				break;
			case EAPMsgTypes.EAP_AKA:
				logger.debug("getting inside aka message parsing");
				
				obj.subType = eapPacket[5];
				
				if(obj.subType == EAPMsgTypes.AKA_IDENTITY) {
					logger.debug("received msg EAPMsgTypes.AKA_IDENTITY for sessionId = "+obj.sessionId);
					
					logger.debug("subtype of received EAP message = 10");
					// SIM start response received
					msgIterator += 2;// 2 bytes are reserved

					while (msgIterator < obj.eapLength) {
						logger.debug("inside while loop, eapPacket value = "
								+ eapPacket[msgIterator]);
						if (eapPacket[msgIterator] == EAPAvpTypes.AT_IDENTITY_TYPE) {// AT_IDENTITY
							logger.debug("parsing AT_IDENTITY, eapPacket value = "
									+ eapPacket[msgIterator]);
							msgIterator++;
							int len = eapPacket[msgIterator];
							msgIterator++;
							byte[] lengthByteArr = new byte[2];
							lengthByteArr[0] = eapPacket[msgIterator];
							msgIterator++;
							lengthByteArr[1] = eapPacket[msgIterator];
							// length of actual Handset identity excluding
							// Handset
							// Identity type(set to 1 for IMSI)
							int handsetIdLength = findLength2Bytes(lengthByteArr);
							logger.debug("length of identity");
							msgIterator++;
							byte[] at_identity = new byte[handsetIdLength];
							for (int m = 0; m < handsetIdLength; m++) {
								at_identity[m] = eapPacket[msgIterator + m];
							}
							obj.at_identity = at_identity;
							logger.debug("obj.at_identity = "
									+ helper.bytesToHex(obj.at_identity));
							handsetIdType = (byte) (eapPacket[msgIterator] - 0x30);
							msgIterator++;
							if (handsetIdType == 0) {
								try {
									byte[] imsiByte = new byte[15];
									for (int i = 0; i < 15; i++) {
										imsiByte[i] = eapPacket[msgIterator + i];
									}
									obj.eapImsi = ByteToDecStream(imsiByte);
								} catch (Exception e) {
									e.printStackTrace();
									return false;
								}

							} else {
								logger.warn("The first field of IMSI is not set to 0 as expected");
								obj.result = "failure";
								obj.errorCode = TransactionInfo.ERROR_MANDATORY_PARAM_MISSING;
								return false;
							}
							msgIterator += handsetIdLength;

						}
						
						else if (((byte) eapPacket[msgIterator] < 127 && (eapPacket[msgIterator] != 7
								&& eapPacket[msgIterator] != 16 && eapPacket[msgIterator] != 14)) == true) {
							// received non-skippable unexpected attribute
							// msgIterator++;
							logger.debug("received unexpected non-skippable attribute of type = "
									+ eapPacket[msgIterator]);
							int len = eapPacket[msgIterator + 1] * 4;
							msgIterator += len;
							non_skippable_attr_pres = true;
							obj.errorCode = TransactionInfo.ERROR_UNEXPECTED_NON_SKIPPABLE_ATTR;
							

						}
						else {
							
						}
					}
					
				} else if(obj.subType == EAPMsgTypes.AKA_CHALLENGE) {
					logger.debug("received msg EAPMsgTypes.AKA_CHALLENGE for sessionId = "+obj.sessionId);
				} else if(obj.subType == EAPMsgTypes.SIM_NOTIFICATION) {
					logger.debug("received msg EAPMsgTypes.SIM_NOTIFICATION for sessionId = "+obj.sessionId);
				}else if(obj.subType == EAPMsgTypes.SIM_CLIENT_ERROR) {
					logger.debug("received msg EAPMsgTypes.SIM_CLIENT_ERROR for sessionId = "+obj.sessionId);
				} else {
					
				}
				break;
			}

			if (obj.subType == 10) {
				if (at_nonce_pres == false) {
					obj.errorCode = TransactionInfo.ERROR_MANDATORY_PARAM_MISSING;
					obj.result = "failure";
					return false;
				} else if (non_skippable_attr_pres == true) {
					obj.result = "failure";
					return false;
				}
			} else if (obj.subType == 11) {
				if (at_nonce_pres == false) {
					obj.errorCode = TransactionInfo.ERROR_MANDATORY_PARAM_MISSING;
					obj.result = "failure";
					return false;

				} else if (non_skippable_attr_pres == true) {
					obj.result = "failure";
					return false;
				}
			} else
				return true;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public String ByteToHexStream(byte[] input) {
		Formatter formatter = new Formatter();
		for (byte b : input) {
			formatter.format("%02x", b);
		}
		String hex = formatter.toString();

		return hex;
	}

	public String ByteToDecStream(byte[] input) {
		Formatter formatter = new Formatter();
		for (byte b : input) {
			b = (byte) (b - 0x30);
			formatter.format("%d", b);

		}
		String hex = formatter.toString();
		return hex;
	}

	public int findLength2Bytes(byte[] input) {
		int length = input[0];
		length <<= 8;
		length += input[1];
		return length;
	}

	public void trigger_sim_start(TransactionInfo obj) {

		logger.debug("inside trigger_sim_start");
		obj.packet.setPacketType(RadiusPacket.ACCESS_CHALLENGE);
		RadiusAttribute attribute = new RadiusAttribute();
		attribute.setAttributeType(79);
		ByteBuffer buffer = ByteBuffer.allocate(128);

		int pos = 0;

		buffer.put((byte) 0x01);// setting code as 1 for EAP request
		pos++;
		System.out.println("obj.eapId = " + obj.eapId);
		int id = obj.eapId;
		if(id<0)
			id = id+256;
		id = (byte)id+1;
		buffer.put((byte) id);// setting packetIdentifier
		// buffer.put((byte)0x20);//id
		pos++;
		int eapMsgLength = 20;// hard coded EAP SIM start message length
		int eapLengthPos = pos;
		pos += 2;
		/*
		 * buffer.put((byte)(eapMsgLength>>8)); buffer.put((byte)(eapMsgLength
		 * &0x00ff));
		 */
		buffer.put((byte) ((eapMsgLength >> 8) & 0xff));
		buffer.put((byte) ((eapMsgLength) & 0xff));

		// type
		buffer.put((byte) (0x12));
		// GSM subtype sim-start
		buffer.put((byte) (0x0a));
		byte[] reserved = { (byte) (0x00), (byte) (0x00) };
		buffer.put(reserved);
		// mandatory field AT_VERSION_LIST
		byte[] atVersionList = { (byte) 0x0f, (byte) 0x02, (byte) 0x00,
				(byte) 0x02, (byte) 0x00, (byte) 0x01, (byte) 0x00, (byte) 0x00 };
		buffer.put(atVersionList);
		// AT_FULLAUTH_ID conditional param
		// only one of AT_PERMANENT_ID_REQ.AT_ANY_ID_REQ,AT_FULLAUTH_ID will be
		// present
		byte[] atFullAuthIdReq = { (byte) 0x11, (byte) 0x01, (byte) 0x01,
				(byte) 0x00 };
		buffer.put(atFullAuthIdReq);
		byte[] dst = new byte[buffer.position()];
		for (int i = 0; i < dst.length; i++) {
			dst[i] = buffer.get(i);
		}
		attribute.setAttributeData(dst);
		logger.debug("EAP message content = " + ByteToHexStream(dst));
		obj.packet.addAttribute(attribute);

		// add Message Authenticator
		byte[] zeroVal = new byte[16];
		for (int i = 0; i < 16; i++)
			zeroVal[i] = 0;
		RadiusAttribute attr = new RadiusAttribute();
		attr.setAttributeType(80);
		attr.setAttributeData(zeroVal);
		obj.packet.addAttribute(attr);

		RadiusAttribute attr1 = new RadiusAttribute();
		attr1.setAttributeType(80);
		logger.debug("before creating message authenticator");
		try {
			System.out.println("getSharedSecret = " + getSharedSecret());
			System.out.println("obj.packet.getAttributeBytes().length = "
					+ obj.packet.getAttributeBytes().length);
			System.out.println("RadiusPacket.RADIUS_HEADER_LENGTH = "
					+ RadiusPacket.RADIUS_HEADER_LENGTH);
			System.out.println("obj.packet.getAttributeBytes() = "
					+ ByteToHexStream(obj.packet.getAttributeBytes()));
			System.out.println("obj.ar.getAuthenticator() = "
					+ ByteToHexStream(obj.ar.getAuthenticator()));

			attr1.setAttributeData(obj.packet.createMessageAuthenticator(
					getSharedSecret(), obj.packet.getAttributeBytes().length
							+ RadiusPacket.RADIUS_HEADER_LENGTH,
					obj.packet.getAttributeBytes(),
					/* accessRequest.getAttribute(80).getAttributeData() */
					obj.ar.getAuthenticator()));
			logger.debug("abc1");
			/*
			 * attr.setAttributeData(packet.createResponseAuthenticator(
			 * getSharedSecret(), packet.getAttributeBytes().length+RadiusPacket
			 * .RADIUS_HEADER_LENGTH, packet.getAttributeBytes(),
			 * accessRequest.getAuthenticator()));
			 */
			obj.packet.removeAttribute(attr);
			logger.debug("abc2");
			obj.packet.addAttribute(attr1);

			logger.debug("created MAC for access Challenge Message");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void trigger_sim_challenge(TransactionInfo obj) {
		logger.debug("inside trigger_sim_challenge");
		try {
			obj.packet.setPacketType(RadiusPacket.ACCESS_CHALLENGE);
			RadiusAttribute attribute = new RadiusAttribute();
			attribute.setAttributeType(79);
			ByteBuffer buffer = ByteBuffer.allocate(512);
			int pos = 0;

			buffer.put((byte) 0x01);// setting code as 1 for EAP request
			pos++;
			System.out.println("obj.eapId = " + obj.eapId);
			int id = (byte) (obj.eapId + 1);
			buffer.put((byte) id);// setting packetIdentifier
			// buffer.put((byte)0x20);//id
			pos++;
			int eapMsgLength = 20;// hard coded EAP SIM start message length
			int eapLengthPos = pos;
			pos += 2;
			buffer.put((byte) ((eapMsgLength >> 8) & 0xff));
			buffer.put((byte) ((eapMsgLength) & 0xff));

			// type
			buffer.put((byte) (0x12));
			// GSM subtype sim-challenge
			buffer.put((byte) (0x0b));
			byte[] reserved = { (byte) (0x00), (byte) (0x00) };
			buffer.put(reserved);
			// AT_RAND
			buffer.put((byte) 0x01);// AT_RAND_TYPE
			// length of RAND
			HelperClass helper = new HelperClass();
			Iterator itRand = obj.atRand.iterator();
			String atRandInString = "";
			while (itRand.hasNext())
				atRandInString += (String) itRand.next();
			int atRandLength = helper.hexStringToHexByte(atRandInString).length;

			byte[] rand = helper.hexStringToHexByte(atRandInString);
			logger.debug("rand = " + helper.bytesToHex(rand));
			int atRandLengthPut = atRandLength / 4 + 1;
			/* length of AT_RAND as multiples of 4 as expected by RFC 4186 */

			buffer.put((byte) atRandLengthPut);
			buffer.put(reserved);
			buffer.put(rand);
			// AT_MAC
			buffer.put((byte) 0x0b);
			buffer.put((byte) 0x05);// length of MAC
			buffer.put(reserved);
			int macPos = buffer.position();
			byte[] zerovals = new byte[16];

			for (int m = 0; m < 16; m++)
				zerovals[m] = 0;
			buffer.put(zerovals);// remember to replace MAC with actual value
									// later

			eapMsgLength = buffer.position();
			byte[] lengthBytes = new byte[2];

			lengthBytes[0] = (byte) ((eapMsgLength >> 8) & 0xff);
			lengthBytes[1] = (byte) ((eapMsgLength) & 0xff);
			for (int i = 0; i < 2; i++)
				buffer.put(eapLengthPos + i, lengthBytes[i]);

			/*
			 * byte[] dst = new byte[buffer.position()]; for (int m = 0; m <
			 * buffer.position(); m++) { dst[m] = buffer.get(); }
			 */

			byte[] dst1 = new byte[buffer.position()];
			for (int i = 0; i < dst1.length; i++) {
				if (i == eapLengthPos)
					dst1[i] = lengthBytes[0];
				else if (i == eapLengthPos + 1)
					dst1[i] = lengthBytes[1];
				else
					dst1[i] = buffer.get(i);
			}

			// getting Kc for calculating AT_MAC
			String kcString = "";
			Iterator itKc = obj.kc.iterator();
			while (itKc.hasNext())
				kcString += (String) itKc.next();

			logger.debug("kcString.length = " + kcString.length());

			logger.debug("kcString = " + kcString);
			byte[] kc = helper.hexStringToHexByte(kcString);// check
			logger.debug("kc =" + helper.bytesToHex(kc));
			byte[] k_aut = new byte[16];
			byte[] k_encr = new byte[16];
			byte[] msk = new byte[64];
			byte[] emsk = new byte[64];
			k_aut = helper.calculateKAut(obj.eapMtNonce, kc, obj.at_identity,
					obj.eapSelectedVersion, obj.eapSelectedVersion, k_aut,
					k_encr, msk, emsk);
			logger.debug("k_aut = " + helper.bytesToHex(k_aut));
			obj.kAut = k_aut;
			obj.kEncr = k_encr;
			obj.eMsk = emsk;
			obj.msk = msk;
			logger.debug("before calculating mac");
			byte[] mac = helper.calculateAtMac(dst1, obj.eapMtNonce, k_aut);
			logger.debug("obj.eapMtNonce =" + helper.bytesToHex(obj.eapMtNonce));
			/*temp code */
			/*byte[] mtNonce4184 = { (byte)0x01,(byte)0x23,(byte)0x45,(byte)0x67,
					(byte)0x89,(byte)0xab,(byte)0xcd,(byte)0xef,
					(byte)0xfe,(byte)0xdc,(byte)0xba,(byte)0x98,
					(byte)0x76,(byte)0x54,(byte)0x32,(byte)0x10 };
			logger.debug("before calculating tempMac");
			byte[]tempMac = helper.calculateAtMac(dst1, mtNonce4184, k_aut);
			logger.debug("tempMac =" + helper.bytesToHex(tempMac));*/
			/* end of temp code */
			
			logger.debug("mac =" + helper.bytesToHex(mac));
			for (int i = 0; i < 16; i++) {
				dst1[macPos + i] = mac[i];
			}
			
			attribute.setAttributeData(dst1);
			obj.packet.addAttribute(attribute);
			logger.debug("dst1 = " + helper.bytesToHex(dst1));
			// code for creating message authenticator
			byte[] zeroVal = new byte[16];
			for (int i = 0; i < 16; i++)
				zeroVal[i] = 0;
			RadiusAttribute attr = new RadiusAttribute();
			attr.setAttributeType(80);
			attr.setAttributeData(zeroVal);
			obj.packet.addAttribute(attr);

			RadiusAttribute attr1 = new RadiusAttribute();
			attr1.setAttributeType(80);
			logger.debug("before creating message authenticator");
			try {
				System.out.println("getSharedSecret = " + getSharedSecret());
				System.out.println("obj.packet.getAttributeBytes().length = "
						+ obj.packet.getAttributeBytes().length);
				System.out.println("RadiusPacket.RADIUS_HEADER_LENGTH = "
						+ RadiusPacket.RADIUS_HEADER_LENGTH);
				System.out.println("obj.packet.getAttributeBytes() = "
						+ ByteToHexStream(obj.packet.getAttributeBytes()));
				System.out.println("obj.ar.getAuthenticator() = "
						+ ByteToHexStream(obj.ar.getAuthenticator()));

				attr1.setAttributeData(obj.packet.createMessageAuthenticator(
						getSharedSecret(),
						obj.packet.getAttributeBytes().length
								+ RadiusPacket.RADIUS_HEADER_LENGTH,
						obj.packet.getAttributeBytes(),
						/* accessRequest.getAttribute(80).getAttributeData() */
						obj.ar.getAuthenticator()));
				logger.debug("abc1");
				/*
				 * attr.setAttributeData(packet.createResponseAuthenticator(
				 * getSharedSecret(),
				 * packet.getAttributeBytes().length+RadiusPacket
				 * .RADIUS_HEADER_LENGTH, packet.getAttributeBytes(),
				 * accessRequest.getAuthenticator()));
				 */
				obj.packet.removeAttribute(attr);
				logger.debug("abc2");
				obj.packet.addAttribute(attr1);

				logger.debug("created MAC for access Challenge Message");

				logger.debug("created MAC for access Challenge Message");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}

	}

	public void trigger_sim_success(TransactionInfo obj) {
		obj.packet.setPacketType(RadiusPacket.ACCESS_ACCEPT);
		RadiusAttribute attribute = new RadiusAttribute();
		attribute.setAttributeType(79);

		byte[] data = new byte[4];
		data[0] = 0x03;// code =3 for success
		int id = (byte) obj.eapId;
		if (id < 0)
			id = id + 255;
		id += 1;
		data[1] = (byte) id;
		data[2] = 0x00;
		data[3] = 0x04;
		attribute.setAttributeData(data);
		obj.packet.addAttribute(attribute);

		// add Message Authenticator
		byte[] zeroVal = new byte[16];
		for (int i = 0; i < 16; i++)
			zeroVal[i] = 0;
		RadiusAttribute attr = new RadiusAttribute();
		attr.setAttributeType(80);
		attr.setAttributeData(zeroVal);
		obj.packet.addAttribute(attr);

		RadiusAttribute attr1 = new RadiusAttribute();
		attr1.setAttributeType(80);

		try {

			attr1.setAttributeData(obj.packet.createMessageAuthenticator(
					getSharedSecret(), obj.packet.getAttributeBytes().length
							+ RadiusPacket.RADIUS_HEADER_LENGTH,
					obj.packet.getAttributeBytes(),
					/* accessRequest.getAttribute(80).getAttributeData() */
					obj.ar.getAuthenticator()));
			/*
			 * attr.setAttributeData(packet.createResponseAuthenticator(
			 * getSharedSecret(), packet.getAttributeBytes().length+RadiusPacket
			 * .RADIUS_HEADER_LENGTH, packet.getAttributeBytes(),
			 * accessRequest.getAuthenticator()));
			 */
			obj.packet.removeAttribute(attr);
			obj.packet.addAttribute(attr1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void trigger_notification(TransactionInfo obj) {
		try {
			logger.debug("inside trigger_notification, sessionId = "
					+ obj.sessionId);
			obj.packet.setPacketType(RadiusPacket.ACCESS_CHALLENGE);
			if (obj.atNotificationPbitStatus == true) {
				// dont send AT_MAC attribute
				RadiusAttribute attribute = new RadiusAttribute();
				attribute.setAttributeType(79);
				byte[] data = new byte[12];
				data[0] = 0x01;// code =1 for request
				int id = (byte) obj.eapId;
				if (id < 0)
					id = id + 255;
				id += 1;
				data[1] = (byte) id;
				// next 2 bytes denote length of eap packet
				data[2] = 0x00;
				data[3] = 0x0c;
				data[4] = 0x12;// GSM_SIM
				data[5] = 0x0c;// AT_NOTIFICATION message SUBTYPE
				// 2 reserved bytes
				data[6] = 0x00;
				data[7] = 0x00;
				data[8] = 0x0c;// AT_NOTIFICATION param type
				data[9] = 0x01;
				int atNotificationVal = 16384;
				data[10] = (byte) (atNotificationVal >>> 8 & 0xFF);
				data[11] = (byte) (atNotificationVal & 0xFF);
				attribute.setAttributeData(data);
				obj.packet.addAttribute(attribute);
				logger.debug("eap AT_NOTIFICATION packet ="+ByteToHexStream(data));
				// set message authenticator
				byte[] zeroVal = new byte[16];
				for (int i = 0; i < 16; i++)
					zeroVal[i] = 0;
				RadiusAttribute attr = new RadiusAttribute();
				attr.setAttributeType(80);
				attr.setAttributeData(zeroVal);
				obj.packet.addAttribute(attr);

				RadiusAttribute attr1 = new RadiusAttribute();
				attr1.setAttributeType(80);

				attr1.setAttributeData(obj.packet.createMessageAuthenticator(
						getSharedSecret(),
						obj.packet.getAttributeBytes().length
								+ RadiusPacket.RADIUS_HEADER_LENGTH,
						obj.packet.getAttributeBytes(),
						/* accessRequest.getAttribute(80).getAttributeData() */
						obj.ar.getAuthenticator()));

				obj.packet.removeAttribute(attr);
				obj.packet.addAttribute(attr1);

			} 
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void trigger_eap_failure_packet(TransactionInfo obj) {
		obj.packet.setPacketType(RadiusPacket.ACCESS_REJECT);
		RadiusAttribute attribute = new RadiusAttribute();
		attribute.setAttributeType(79);

		byte[] data = new byte[4];
		data[0] = 0x04;// code =4 for failure
		int id = (byte) obj.eapId;
		if (id < 0)
			id = id + 255;
		id += 1;
		data[1] = (byte) id;
		data[2] = 0x00;
		data[3] = 0x04;
		attribute.setAttributeData(data);
		obj.packet.addAttribute(attribute);

		// add Message Authenticator
		byte[] zeroVal = new byte[16];
		for (int i = 0; i < 16; i++)
			zeroVal[i] = 0;
		RadiusAttribute attr = new RadiusAttribute();
		attr.setAttributeType(80);
		attr.setAttributeData(zeroVal);
		obj.packet.addAttribute(attr);

		RadiusAttribute attr1 = new RadiusAttribute();
		attr1.setAttributeType(80);

		try {

			attr1.setAttributeData(obj.packet.createMessageAuthenticator(
					getSharedSecret(), obj.packet.getAttributeBytes().length
							+ RadiusPacket.RADIUS_HEADER_LENGTH,
					obj.packet.getAttributeBytes(),
					/* accessRequest.getAttribute(80).getAttributeData() */
					obj.ar.getAuthenticator()));
			/*
			 * attr.setAttributeData(packet.createResponseAuthenticator(
			 * getSharedSecret(), packet.getAttributeBytes().length+RadiusPacket
			 * .RADIUS_HEADER_LENGTH, packet.getAttributeBytes(),
			 * accessRequest.getAuthenticator()));
			 */
			obj.packet.removeAttribute(attr);
			obj.packet.addAttribute(attr1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void trigger_eap_aka_challenge(TransactionInfo obj) {
		logger.debug("inside trigger_eap_aka_challenge");
		try {
			obj.packet.setPacketType(RadiusPacket.ACCESS_CHALLENGE);
			RadiusAttribute attribute = new RadiusAttribute();
			attribute.setAttributeType(79);
			ByteBuffer buffer = ByteBuffer.allocate(512);
			int pos = 0;

			buffer.put((byte) 0x01);// setting code as 1 for EAP request
			pos++;
			System.out.println("obj.eapId = " + obj.eapId);
			int id = (byte) (obj.eapId + 1);
			buffer.put((byte) id);// setting packetIdentifier
			// buffer.put((byte)0x20);//id
			pos++;
			int eapMsgLength = 20;// hard coded EAP SIM start message length
			int eapLengthPos = pos;
			pos += 2;
			buffer.put((byte) ((eapMsgLength >> 8) & 0xff));
			buffer.put((byte) ((eapMsgLength) & 0xff));

			// type
			buffer.put((byte) EAPMsgTypes.EAP_AKA);
			//  subtype 
			buffer.put((byte) EAPMsgTypes.AKA_CHALLENGE);
			byte[] reserved = { (byte) (0x00), (byte) (0x00) };
			buffer.put(reserved);
			// AT_RAND
			buffer.put((byte) EAPAvpTypes.AT_RAND_TYPE);// AT_RAND_TYPE
			// length of RAND
			HelperClass helper = new HelperClass();
			Iterator itRand = obj.atRand.iterator();
			String atRandInString = "";
			while (itRand.hasNext())
				atRandInString += (String) itRand.next();
			int atRandLength = helper.hexStringToHexByte(atRandInString).length;

			byte[] rand = helper.hexStringToHexByte(atRandInString);
			logger.debug("rand = " + helper.bytesToHex(rand));
			int atRandLengthPut = atRandLength / 4 + 1;
			/* length of AT_RAND as multiples of 4 as expected by RFC 4186 */

			buffer.put((byte) atRandLengthPut);
			buffer.put(reserved);
			buffer.put(rand);
			
			//AT_AUTN
			buffer.put((byte) EAPAvpTypes.AT_AUTN_TYPE);// AT_AUTN_TYPE
			// length of RAND
			
			Iterator itAutn = obj.autn.iterator();
			String atAutnInString = "";
			while (itAutn.hasNext())
				atAutnInString += (String) itAutn.next();
			int atAutnLength = helper.hexStringToHexByte(atAutnInString).length;

			byte[] autn = helper.hexStringToHexByte(atAutnInString);
			logger.debug("autn = " + helper.bytesToHex(autn));
			int atAutnLengthPut = atAutnLength / 4 + 1;
			/* length of AT_RAND as multiples of 4 as expected by RFC 4186 */

			buffer.put((byte) atAutnLengthPut);
			buffer.put(reserved);
			buffer.put(autn);
			
			// AT_MAC
			buffer.put((byte) 0x0b);
			buffer.put((byte) 0x05);// length of MAC
			buffer.put(reserved);
			int macPos = buffer.position();
			byte[] zerovals = new byte[16];

			for (int m = 0; m < 16; m++)
				zerovals[m] = 0;
			buffer.put(zerovals);// remember to replace MAC with actual value
									// later

			eapMsgLength = buffer.position();
			byte[] lengthBytes = new byte[2];

			lengthBytes[0] = (byte) ((eapMsgLength >> 8) & 0xff);
			lengthBytes[1] = (byte) ((eapMsgLength) & 0xff);
			for (int i = 0; i < 2; i++)
				buffer.put(eapLengthPos + i, lengthBytes[i]);

			/*
			 * byte[] dst = new byte[buffer.position()]; for (int m = 0; m <
			 * buffer.position(); m++) { dst[m] = buffer.get(); }
			 */

			byte[] dst1 = new byte[buffer.position()];
			for (int i = 0; i < dst1.length; i++) {
				if (i == eapLengthPos)
					dst1[i] = lengthBytes[0];
				else if (i == eapLengthPos + 1)
					dst1[i] = lengthBytes[1];
				else
					dst1[i] = buffer.get(i);
			}

			// getting ck for calculating AT_MAC
			String ckString = "";
			Iterator itCk = obj.ck.iterator();
			while (itCk.hasNext())
				ckString += (String) itCk.next();

			logger.debug("ckString.length = " + ckString.length());

			logger.debug("ckString = " + ckString);
			byte[] ck = helper.hexStringToHexByte(ckString);// check
			logger.debug("ck =" + helper.bytesToHex(ck));
			
			//getting ik for calculating AT_MAC
			
			String ikString = "";
			Iterator itIk = obj.ik.iterator();
			while (itIk.hasNext())
				ikString += (String) itIk.next();

			logger.debug("ikString.length = " + ikString.length());

			logger.debug("ikString = " + ikString);
			byte[] ik = helper.hexStringToHexByte(ikString);// check
			logger.debug("ik =" + helper.bytesToHex(ik));
			
			byte[] k_aut = new byte[16];
			byte[] k_encr = new byte[16];
			byte[] msk = new byte[64];
			byte[] emsk = new byte[64];
			k_aut = helper.calculateAkaKAut(ik, ck, obj.at_identity, k_aut, k_encr, msk, emsk);
					
			logger.debug("k_aut = " + helper.bytesToHex(k_aut));
			obj.kAut = k_aut;
			obj.kEncr = k_encr;
			obj.eMsk = emsk;
			obj.msk = msk;
			logger.debug("before calculating mac");
			byte[] mac = helper.calculateAkaAtMac(dst1, k_aut);
			logger.debug("eap-aka mac =" + helper.bytesToHex(mac));
			/*temp code */
			/*byte[] mtNonce4184 = { (byte)0x01,(byte)0x23,(byte)0x45,(byte)0x67,
					(byte)0x89,(byte)0xab,(byte)0xcd,(byte)0xef,
					(byte)0xfe,(byte)0xdc,(byte)0xba,(byte)0x98,
					(byte)0x76,(byte)0x54,(byte)0x32,(byte)0x10 };
			logger.debug("before calculating tempMac");
			byte[]tempMac = helper.calculateAtMac(dst1, mtNonce4184, k_aut);
			logger.debug("tempMac =" + helper.bytesToHex(tempMac));*/
			/* end of temp code */
			
			
			for (int i = 0; i < 16; i++) {
				dst1[macPos + i] = mac[i];
			}
			
			attribute.setAttributeData(dst1);
			obj.packet.addAttribute(attribute);
			logger.debug("dst1 = " + helper.bytesToHex(dst1));
			// code for creating message authenticator
			byte[] zeroVal = new byte[16];
			for (int i = 0; i < 16; i++)
				zeroVal[i] = 0;
			RadiusAttribute attr = new RadiusAttribute();
			attr.setAttributeType(80);
			attr.setAttributeData(zeroVal);
			obj.packet.addAttribute(attr);

			RadiusAttribute attr1 = new RadiusAttribute();
			attr1.setAttributeType(80);
			logger.debug("before creating message authenticator");
			try {
				System.out.println("getSharedSecret = " + getSharedSecret());
				System.out.println("obj.packet.getAttributeBytes().length = "
						+ obj.packet.getAttributeBytes().length);
				System.out.println("RadiusPacket.RADIUS_HEADER_LENGTH = "
						+ RadiusPacket.RADIUS_HEADER_LENGTH);
				System.out.println("obj.packet.getAttributeBytes() = "
						+ ByteToHexStream(obj.packet.getAttributeBytes()));
				System.out.println("obj.ar.getAuthenticator() = "
						+ ByteToHexStream(obj.ar.getAuthenticator()));

				attr1.setAttributeData(obj.packet.createMessageAuthenticator(
						getSharedSecret(),
						obj.packet.getAttributeBytes().length
								+ RadiusPacket.RADIUS_HEADER_LENGTH,
						obj.packet.getAttributeBytes(),
						/* accessRequest.getAttribute(80).getAttributeData() */
						obj.ar.getAuthenticator()));
				logger.debug("abc1");
				/*
				 * attr.setAttributeData(packet.createResponseAuthenticator(
				 * getSharedSecret(),
				 * packet.getAttributeBytes().length+RadiusPacket
				 * .RADIUS_HEADER_LENGTH, packet.getAttributeBytes(),
				 * accessRequest.getAuthenticator()));
				 */
				obj.packet.removeAttribute(attr);
				logger.debug("abc2");
				obj.packet.addAttribute(attr1);

				logger.debug("created MAC for access Challenge Message");

				logger.debug("created MAC for access Challenge Message");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
	public void trigger_eap_aka_identity(TransactionInfo obj) {
		logger.debug("inside trigger_eap_aka_identity");
		obj.packet.setPacketType(RadiusPacket.ACCESS_CHALLENGE);
		RadiusAttribute attribute = new RadiusAttribute();
		attribute.setAttributeType(79);
		ByteBuffer buffer = ByteBuffer.allocate(128);

		int pos = 0;

		buffer.put((byte) 0x01);// setting code as 1 for EAP request
		pos++;
		System.out.println("obj.eapId = " + obj.eapId);
		int id = obj.eapId;
		if(id<0)
			id = id+256;
		id = (byte)id+1;
		buffer.put((byte) id);// setting packetIdentifier
		// buffer.put((byte)0x20);//id
		pos++;
		int eapMsgLength = 12;// hard coded EAP SIM start message length
		int eapLengthPos = pos;
		pos += 2;
		/*
		 * buffer.put((byte)(eapMsgLength>>8)); buffer.put((byte)(eapMsgLength
		 * &0x00ff));
		 */
		buffer.put((byte) ((eapMsgLength >> 8) & 0xff));
		buffer.put((byte) ((eapMsgLength) & 0xff));

		// type
		buffer.put((byte) EAPMsgTypes.EAP_AKA);//0x17
		//  subtype 
		buffer.put((byte) EAPMsgTypes.AKA_IDENTITY);
		byte[] reserved = { (byte) (0x00), (byte) (0x00) };
		buffer.put(reserved);
		
		// AT_FULLAUTH_ID conditional param
		// only one of AT_PERMANENT_ID_REQ.AT_ANY_ID_REQ,AT_FULLAUTH_ID will be
		// present
		/*byte[] atFullAuthIdReq = { (byte) 0x11, (byte) 0x01, (byte) 0x01,
				(byte) 0x00 };*/
		byte[] atFullAuthIdReq = { (byte) 0x11, (byte) 0x01, (byte) 0x00,
				(byte) 0x00 };
		
		buffer.put(atFullAuthIdReq);
		byte[] dst = new byte[buffer.position()];
		for (int i = 0; i < dst.length; i++) {
			dst[i] = buffer.get(i);
		}
		attribute.setAttributeData(dst);
		logger.debug("EAP message content = " + ByteToHexStream(dst));
		obj.packet.addAttribute(attribute);

		// add Message Authenticator
		byte[] zeroVal = new byte[16];
		for (int i = 0; i < 16; i++)
			zeroVal[i] = 0;
		RadiusAttribute attr = new RadiusAttribute();
		attr.setAttributeType(80);
		attr.setAttributeData(zeroVal);
		obj.packet.addAttribute(attr);

		RadiusAttribute attr1 = new RadiusAttribute();
		attr1.setAttributeType(80);
		logger.debug("before creating message authenticator");
		try {
			System.out.println("getSharedSecret = " + getSharedSecret());
			System.out.println("obj.packet.getAttributeBytes().length = "
					+ obj.packet.getAttributeBytes().length);
			System.out.println("RadiusPacket.RADIUS_HEADER_LENGTH = "
					+ RadiusPacket.RADIUS_HEADER_LENGTH);
			System.out.println("obj.packet.getAttributeBytes() = "
					+ ByteToHexStream(obj.packet.getAttributeBytes()));
			System.out.println("obj.ar.getAuthenticator() = "
					+ ByteToHexStream(obj.ar.getAuthenticator()));

			attr1.setAttributeData(obj.packet.createMessageAuthenticator(
					getSharedSecret(), obj.packet.getAttributeBytes().length
							+ RadiusPacket.RADIUS_HEADER_LENGTH,
					obj.packet.getAttributeBytes(),
					
					obj.ar.getAuthenticator()));
			logger.debug("abc11");
			
			obj.packet.removeAttribute(attr);
			logger.debug("abc12");
			obj.packet.addAttribute(attr1);

			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void trigger_eap_aka_notification(TransactionInfo obj) {
		
	}
	
	public void trigger_eap_aka_failure_packet(TransactionInfo obj) {
		trigger_eap_failure_packet(obj);
	}
	public void trigger_eap_aka_success(TransactionInfo obj) {
		trigger_sim_success(obj);
	}

}

package org.tinyradius.util;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
public class HelperClass {
	private static final char[] HEX_CHARS = "0123456789abcdef".toCharArray();
	protected Log logger = LogFactory.getLog(HelperClass.class);
	private static Mac hmacsha1;
	static {
		
	 try {
		 hmacsha1 = Mac.getInstance("HmacSHA1");
	} catch (NoSuchAlgorithmException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	
	public  byte[] fips186_2prf(byte[] input) {
		try {
			byte[] xkey = new byte[20];
			byte[] xval = new byte[20];
			byte[] one = new byte[20];
			byte[] zeros = new byte[64];
			byte[] w_0 = new byte[20];
			byte[] w_1 = new byte[20];
			byte[] sum = new byte[20];
			byte[] x_seed = new byte[20];
			byte[] f = new byte[160];
			for (int i = 0; i < 19; i++) {
				one[i] = sum[i] = 0;
			}
			one[19] = 1;
			int pos = 0;
			/*
			 * for(int i = 0;i<20;i++) { xkey[i] = xval[i] =
			 * dummyoutputMKKey[i]; }
			 */
			System.out.println("sum = " + bytesToHex(sum));
			for (int i = 0; i < 20; i++) {
				xkey[i] = input[i];
			}

			for (int j = 0; j < 4; j++) {
				/* a */
				for (int i = 0; i < 20; i++) {
					xval[i] = xkey[i];
				}
				/* b */
				for (int i = 0; i < 64; i++) {
					if (i <= 19)
						zeros[i] = xval[i];

					else
						zeros[i] = 0;

				}

				/*
				 * MessageDigest md1 = MessageDigest.getInstance("SHA1");
				 * md1.update(zeros); w_0 = md1.digest();
				 */
				// System.out.println("w_0 = " + bytesToHex(w_0));
				// System.out.println("zeros = "+zeros.toString());
				System.out.println("xval in hex before encoding = "
						+ bytesToHex(xval));
				String w0_base64 = encode(bytesToHex(xval));
				// System.out.println("w0_base64 = " + w0_base64);

				Base64 base64 = new Base64();
				try {
					w_0 = base64.decode(w0_base64);
				} catch (Exception e) {
					e.printStackTrace();
				}

				System.out.println("w_0 = " + bytesToHex(w_0));

				/* c */
				/*onesixty_add_mod(sum, xkey, w_0);
				System.out.println("sum in c= " + bytesToHex(sum));
				onesixty_add_mod(xkey, sum, one);*/
				
				
				//System.out.println("xkey in c= " + bytesToHex(xkey));
				BigInteger xkeyBig = new BigInteger(xkey);
				BigInteger w_0Big = new BigInteger(w_0);
				BigInteger result = bigIntAdd(xkeyBig,w_0Big);
				BigInteger oneBig = new BigInteger(one);
				
				xkeyBig = bigIntAdd(result,oneBig);
				xkey = xkeyBig.toByteArray();
				
				/*
				 * onesixty_add_mod_try(sum, xkey, w_0);
				 * onesixty_add_mod_try(xkey, sum, one);
				 */

				/* d */
				for (int i = 0; i < 20; i++) {
					xval[i] = xkey[i];
				}
				/* e */
				for (int i = 0; i < 64; i++) {
					if (i <= 19)
						zeros[i] = xval[i];
					else
						zeros[i] = 0;

				}
				/*
				 * MessageDigest md2 = MessageDigest.getInstance("SHA1");
				 * md2.update(zeros); w_1 = md2.digest();
				 */
				System.out.println("xval in hex before encoding = "
						+ bytesToHex(xval));
				String w1_base64 = encode(bytesToHex(xval));

				// System.out.println("w1_base64 = "+w1_base64);

				try {
					w_1 = base64.decode(w1_base64);
				} catch (Exception e) {
					e.printStackTrace();
				}

				System.out.println("w_1 = " + bytesToHex(w_1));
				/* f */
				/*onesixty_add_mod(sum, xkey, w_1);
				System.out.println("sum in f= " + bytesToHex(sum));
				onesixty_add_mod(xkey, sum, one);
				System.out.println("xkey in f= " + bytesToHex(xkey));*/
				BigInteger xkeyBig1 = new BigInteger(xkey);
				BigInteger w_1Big = new BigInteger(w_1);
				BigInteger result1 = bigIntAdd(xkeyBig1,w_1Big);
				
				
				xkeyBig1 = bigIntAdd(result1,oneBig);
				xkey = xkeyBig1.toByteArray();

				/*
				 * onesixty_add_mod_try(sum, xkey, w_1);
				 * onesixty_add_mod_try(xkey, sum, one);
				 */
				for (int i = 0; i < 20; i++) {
					f[pos + i] = w_0[i];
				}
				pos += 20;
				for (int i = 0; i < 20; i++) {
					f[pos + i] = w_1[i];
				}
				pos += 20;

			}
			return f;
			// System.out.println("big Key = "+bytesToHex(f));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
	public  String asHex(byte[] buf) {
		char[] chars = new char[2 * buf.length];
		for (int i = 0; i < buf.length; ++i) {
			chars[2 * i] = HEX_CHARS[(buf[i] & 0xF0) >>> 4];
			chars[2 * i + 1] = HEX_CHARS[buf[i] & 0x0F];
		}
		return new String(chars);
	}

	/*public static String hexadecimal(String input, String charsetName)
			throws UnsupportedEncodingException {
		if (input == null)
			throw new NullPointerException();
		return asHex(input.getBytes(charsetName));
	}
*/
	public  String bytesToHex(byte[] b) {
		char hexDigit[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'A', 'B', 'C', 'D', 'E', 'F' };
		StringBuffer buf = new StringBuffer();
		for (int j = 0; j < b.length; j++) {
			buf.append(hexDigit[(b[j] >> 4) & 0x0f]);
			buf.append(hexDigit[b[j] & 0x0f]);
		}
		return buf.toString();
	}
	private  int rol(int num, int cnt) {
		return (num << cnt) | (num >>> (32 - cnt));

	}

	public  int fourBytes2Word(byte[] input) {
		int value = ((input[0] & 0xFF) << 24) | ((input[1] & 0xFF) << 16)
				| ((input[2] & 0xFF) << 8) | (input[3] & 0xFF);
		return value;
	}

	
	public  byte[] hexStringToHexByte(String str) {
		
		byte[] bArr = str.getBytes();
		byte[] output = new byte[str.length() / 2];
		for (int i = 0; i < bArr.length; i++) {
			// System.out.println("arr =" + bArr[i]);
			byte b = (byte) (bArr[i] - 0x30);
			byte c = halfOctetByteToDecByte(b);
			if (i % 2 == 0) {
				// System.out.println("c = " + c);
				output[i >> 1] = (byte) (c << 4);
			} else {
				output[i >> 1] |= c;
				// System.out.println("c = " + c);
			}
		}
		return output;
	}

	public static byte halfOctetByteToDecByte(byte b) {
		byte c = 0;
		// A-65 to Z-90
		if (b >= 17 && b <= 42) {
			if (b == 17)
				c = 10;
			else if (b == 18)
				c = 11;
			else if (b == 19)
				c = 12;
			else if (b == 20)
				c = 13;
			else if (b == 21)
				c = 14;
			else if (b == 22)
				c = 15;

		}
		// a-97 to z -122
		else if (b >= 49 && b <= 74) {

			if (b == 49)
				c = 10;
			else if (b == 50)
				c = 11;
			else if (b == 51)
				c = 12;
			else if (b == 52)
				c = 13;
			else if (b == 53)
				c = 14;
			else if (b == 54)
				c = 15;
		} else
			c = b;
		return c;
	}

	public  void intArrToByteArray(int[] value, int intArrNo, byte[] sum) {
		sum = new byte[4 * intArrNo];
		for (int j = 0; j < intArrNo; j++) {
			for (int i = 0; i < 4; i++) {
				int offset = (((sum.length - 1) % (4) - i)) * 8;
				sum[4 * j + i] = (byte) ((value[j] >>> offset) & 0xFF);
			}
		}
	}

	public  BigInteger bigIntAdd(BigInteger a,BigInteger b) {
		 return a.add(b);
		  }
	//modified version of SHA-1 standard algorithm mentioned in FIPS 180-1
	public  String encode(String str) {

		// Convert a string to a sequence of 16-word blocks, stored as an array.
		// Append padding bits and the length, as described in the SHA1 standard

		/*
		 * actual code byte[] x = str.getBytes(); int[] blks = new
		 * int[(((x.length + 8) >> 6) + 1) * 16]; int i;
		 * 
		 * for(i = 0; i < x.length; i++) { blks[i >> 2] |= x[i] << (24 - (i % 4)
		 * * 8); }
		 * 
		 * blks[i >> 2] |= 0x80 << (24 - (i % 4) * 8); blks[blks.length - 1] =
		 * x.length * 8;
		 */

		byte[] x = hexStringToHexByte(str);
		System.out.println("x.length = " + x.length);
		int i = 0;
		int[] blks = new int[16];
		byte[] xPart0 = new byte[4];
		byte[] xPart1 = new byte[4];
		byte[] xPart2 = new byte[4];
		byte[] xPart3 = new byte[4];
		byte[] xPart4 = new byte[4];

		for (int ii = 0; ii < 4; ii++) {
			xPart0[ii] = x[ii];
		}
		for (int ii = 4; ii < 8; ii++) {
			xPart1[ii - 4] = x[ii];
		}
		for (int ii = 8; ii < 12; ii++) {
			xPart2[ii - 8] = x[ii];
		}
		for (int ii = 12; ii < 16; ii++) {
			xPart3[ii - 12] = x[ii];
		}

		for (int ii = 16; ii < 20; ii++) {
			xPart4[ii - 16] = x[ii];
		}

		logger.debug("xPart0 = " + bytesToHex(xPart0));
		logger.debug(" xPart1 = " + bytesToHex(xPart1));
		logger.debug(" xPart2 = " + bytesToHex(xPart2));
		logger.debug(" xPart3 = " + bytesToHex(xPart3));
		logger.debug(" xPart4 = " + bytesToHex(xPart4));

		blks[0] = fourBytes2Word(xPart0);
		blks[1] = fourBytes2Word(xPart1);
		blks[2] = fourBytes2Word(xPart2);
		blks[3] = fourBytes2Word(xPart3);
		blks[4] = fourBytes2Word(xPart4);
		for (i = 5; i < 16; i++)
			blks[i] = 0;

		// for (int j = 0; j < 5; j++)
		// System.out.println(+blks[j]);

		// calculate 160 bit SHA1 hash of the sequence of blocks

		int[] w = new int[80];

		int a = 1732584193;
		int b = -271733879;
		int c = -1732584194;
		int d = 271733878;
		int e = -1009589776;

		for (i = 0; i < blks.length; i += 16) {
			int olda = a;
			int oldb = b;
			int oldc = c;
			int oldd = d;
			int olde = e;

			for (int j = 0; j < 80; j++) {
				w[j] = (j < 16) ? blks[i + j] : (rol(w[j - 3] ^ w[j - 8]
						^ w[j - 14] ^ w[j - 16], 1));

				int t = rol(a, 5)
						+ e
						+ w[j]
						+ ((j < 20) ? 1518500249 + ((b & c) | ((~b) & d))
								: (j < 40) ? 1859775393 + (b ^ c ^ d)
										: (j < 60) ? -1894007588
												+ ((b & c) | (b & d) | (c & d))
												: -899497514 + (b ^ c ^ d));
				e = d;
				d = c;
				c = rol(b, 30);
				b = a;
				a = t;
			}

			a = a + olda;
			b = b + oldb;
			c = c + oldc;
			d = d + oldd;
			e = e + olde;
		}

		// Convert 160 bit hash to base64

		int[] words = { a, b, c, d, e, 0 };
		byte[] base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
				.getBytes();
		byte[] result = new byte[28];
		for (i = 0; i < 27; i++) {
			int start = i * 6;
			int word = start >> 5;
			int offset = start & 0x1f;

			if (offset <= 26) {
				result[i] = base64[(words[word] >> (26 - offset)) & 0x3F];
			} else if (offset == 28) {
				result[i] = base64[(((words[word] & 0x0F) << 2) | ((words[word + 1] >> 30) & 0x03)) & 0x3F];
			} else {
				result[i] = base64[(((words[word] & 0x03) << 4) | ((words[word + 1] >> 28) & 0x0F)) & 0x3F];
			}
		}
		result[27] = '=';

		return new String(result);
	}
	//return k_aut,k_encr,msk,emsk
	public byte[] calculateKAut(byte[]atNonce,byte[]kc,byte[]atIdentity,byte[]selectedVersionList,byte[] versionList,byte[] k_aut,byte[]k_encr,byte[] msk,byte[]emsk) {
		//creating MK key
		try {
		ByteBuffer mkBuff = ByteBuffer.allocate(1024);
		mkBuff.put(atIdentity);
		mkBuff.put(kc);
		mkBuff.put(atNonce);
		mkBuff.put(versionList);
		mkBuff.put(selectedVersionList);
		byte[] sizedInput = new byte[mkBuff.position()];

		for (int i = 0; i < mkBuff.position(); i++) {
			sizedInput[i] = mkBuff.get(i);
		}
		MessageDigest md = MessageDigest.getInstance("SHA1");
		// md.update(mkBuff.array());
		md.update(sizedInput);
		// md.update(mkByte);
		byte[] outputMKKey = md.digest();
		//creating K_aut
		byte[] result_fips186_2prf = fips186_2prf(outputMKKey);	
		
		for (int i = 0; i < 16; i++) {
			k_encr[i] = result_fips186_2prf[i];
		}
		for (int i = 0; i < 16; i++) {
			k_aut[i] = result_fips186_2prf[16 + i];
		}
		for (int i = 0; i < 64; i++) {
			msk[i] = result_fips186_2prf[32 + i];
		}
		for (int i = 0; i < 64; i++) {
			emsk[i] = result_fips186_2prf[96 + i];
		}
		return k_aut;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public byte[] calculateAkaKAut(byte[]ik,byte[]ck,byte[]atIdentity,byte[] k_aut,byte[]k_encr,byte[] msk,byte[]emsk) {
		//creating MK key
		try {
		ByteBuffer mkBuff = ByteBuffer.allocate(1024);
		mkBuff.put(atIdentity);
		mkBuff.put(ik);
		mkBuff.put(ck);
		
		byte[] sizedInput = new byte[mkBuff.position()];

		for (int i = 0; i < mkBuff.position(); i++) {
			sizedInput[i] = mkBuff.get(i);
		}
		MessageDigest md = MessageDigest.getInstance("SHA1");
		// md.update(mkBuff.array());
		md.update(sizedInput);
		// md.update(mkByte);
		byte[] outputMKKey = md.digest();
		//creating K_aut
		byte[] result_fips186_2prf = fips186_2prf(outputMKKey);	
		
		for (int i = 0; i < 16; i++) {
			k_encr[i] = result_fips186_2prf[i];
		}
		for (int i = 0; i < 16; i++) {
			k_aut[i] = result_fips186_2prf[16 + i];
		}
		for (int i = 0; i < 64; i++) {
			msk[i] = result_fips186_2prf[32 + i];
		}
		for (int i = 0; i < 64; i++) {
			emsk[i] = result_fips186_2prf[96 + i];
		}
		return k_aut;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public byte[] calculateAkaAtMac(byte[] eapPacket, byte[] k_aut) {
		//create MAC
				try {
				SecretKeySpec signingKey = new SecretKeySpec(k_aut, "HmacSHA1");

				// get an hmac_sha1 Mac instance and initialize with the signing key
				
				hmacsha1.init(signingKey);
				byte[] rawHmac = new byte[20]; 
				rawHmac = hmacsha1.doFinal(eapPacket);
				return rawHmac;
				} catch(Exception e) {
					e.printStackTrace();
				}
				return null;
				
	}
	
	public byte[] calculateAtMac(byte[] eapPacket, byte[]mtNonce,byte[] k_aut) {
		//create MAC
				try {
				SecretKeySpec signingKey = new SecretKeySpec(k_aut, "HmacSHA1");

				// get an hmac_sha1 Mac instance and initialize with the signing key
				
				hmacsha1.init(signingKey);
				ByteBuffer buffer = ByteBuffer.allocate(512);
				// compute the hmac on input data bytes
				buffer.put(eapPacket);
				buffer.put(mtNonce);
				byte[] data = new byte[buffer.position()];
				logger.debug("data length ="+data.length);
				for(int i=0;i<buffer.position();i++) {
				data[i] = buffer.get(i);	
				}
				
				logger.debug("data = "+bytesToHex(data));
				/*for(int m=0;m<buffer.position();m++) {
					data[m] = buffer.get();
				}*/
				byte[] rawHmac = new byte[16]; 
				rawHmac = hmacsha1.doFinal(data);
				return rawHmac;
				} catch(Exception e) {
					e.printStackTrace();
				}
				return null;
				
	}
}
	


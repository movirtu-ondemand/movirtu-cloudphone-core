package org.tinyradius.dictionary;

public class EAPMsgTypes {
	//type
	public static final byte EAP_AKA = 0x17;
	public static final byte EAP_SIM = 0x12;
	//sub types
	public static final byte AKA_CHALLENGE = 0x01;
	public static final byte AKA_AUTHENTICATION_REJECT = 0x02;
	public static final byte AKA_SYNCHRONIZATION_FAILURE = 0x04;
	public static final byte AKA_IDENTITY = 0x05;
	public static final byte SIM_START = 0x0a;
	public static final byte SIM_CHALLENGE = 0x0b;
	public static final byte SIM_NOTIFICATION = 0x0c;
	public static final byte SIM_REAUTHENTICAION = 0x0d;
	public static final byte SIM_CLIENT_ERROR = 0x0e;
}

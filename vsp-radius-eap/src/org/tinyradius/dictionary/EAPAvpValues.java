package org.tinyradius.dictionary;

public class EAPAvpValues {

	//AT_NOTIFICATION_CODE_VALUES
	public static final int GENERAL_FAILURE_AFTER_AUTHENTICATION_VALUE = 0;
	public static final int USER_TEMPORARILY_DENIED_ACCESS_VALUE = 1026;
	public static final int USER_NOT_SUBSCRIBER_TO_REQUESTED_SERVICE_VALUE = 1031;
	public static final int GENERAL_FAILURE_VALUE = 16384;
	public static final int SUCCESS_VALUE = 32768;
	
	// (15)AT_VERSION_LIST_TYPE VALUES or (16)AT_SELECTEDVERSION_VALUES
	public static final byte EAP_SIM_VERSION_1 = 1;
	
	//(22) AT_CLIENT_ERROR_CODE_TYPE VALUES
	public static final byte GENERAL_ERROR_CODE = 0x00;
	public static final byte UNSUPPORTED_VERSION = 0x01;
	public static final byte INSUFFICIENT_NUMBER_OF_CHALLENGES = 0x02;
	public static final byte RANDS_NOT_FRESH_VALUE = 0x03;
}

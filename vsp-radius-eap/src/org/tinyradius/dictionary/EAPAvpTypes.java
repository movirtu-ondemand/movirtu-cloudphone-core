package org.tinyradius.dictionary;

public class EAPAvpTypes {
	//non skippable attributes 0-127
	public static final byte RESERVED_TYPE = 0x00;
	public static final byte AT_RAND_TYPE = 0x01;
	public static final byte AT_AUTN_TYPE = 0x02;
	public static final byte AT_RES_TYPE = 0x03;
	public static final byte AT_AUTS_TYPE = 0x04;
	public static final byte AT_PADDING_TYPE = 0x06;
	public static final byte AT_NONCE_MT_TYPE = 0x07;
	public static final byte AT_PERMANENT_ID_REQ_TYPE = 0x0a;
	public static final byte AT_MAC_TYPE = 0x0b;
	public static final byte AT_NOTIFICATION_TYPE = 0x0c;
	public static final byte AT_ANY_ID_REQ_TYPE = 0x0d;
	public static final byte AT_IDENTITY_TYPE = 0x0e;
	public static final byte AT_VERSION_LIST_TYPE = 0x0f;
	public static final byte AT_SELECTED_VERSION_TYPE = 0x10;
	public static final byte AT_FULLAUTH_ID_REQ_TYPE = 0x11;
	public static final byte AT_COUNTER_TYPE = 0x13;
	public static final byte AT_COUNTER_TOO_SMALL_TYPE = 0x14;
	public static final byte AT_NONCE_S_TYPE = 0x15;
	public static final byte AT_CLIENT_ERROR_CODE_TYPE = 0x16;
	public static final byte AT_KDF_INPUT_TYPE = 0x17;
	public static final byte AT_KDF_TYPE = 0x18;
	
	//skippable attributes 128-255
	
	public static final byte AT_IV_TYPE = (byte)0x81;
	public static final byte AT_ENCR_DATA_TYPE = (byte)0x82;
	public static final byte AT_NEXT_PSEUDONYM_TYPE = (byte)0x84;
	public static final byte AT_NEXT_REAUTH_ID_TYPE = (byte)0x85;
	public static final byte AT_CHECKCODE_TYPE = (byte)0x86;
	public static final byte AT_RESULT_IND_TYPE = (byte)0x87;
	public static final byte AT_BIDDING = (byte)0x88;
	public static final byte AT_IPMS_IND_TYPE = (byte)0x89;
	public static final byte AT_IPMS_RES_TYPE = (byte)0x8a;
	public static final byte AT_TRUST_IND_TYPE = (byte)0x08b;
	public static final byte AT_SHORT_NAME_FOR_NETWORK_TYPE = (byte)0x8c;
	public static final byte AT_FULL_NAME_FOR_NETWORK = (byte)0x8d;
	public static final byte AT_RQSI_IND_TYPE = (byte)0x8e;
	public static final byte RQSI_RES_TYPE = (byte)0x8f;
	public static final byte AT_TWAN_CONN_MODE_TYPE = (byte)0x90;
	public static final byte AT_VIRTUAL_NETWORK_ID_TYPE = (byte)0x91;
	public static final byte AT_VIRTUAL_NETWORK_REQ_TYPE = (byte)0x92;
	public static final byte AT_CONNECTIVITY_TYPE = (byte)0x93;
	public static final byte AT_HANDOVER_INDICATION_TYPE = (byte)0x94;
	public static final byte AT_HANDOVER_SESSION_ID_TYPE = (byte)0x95;
	public static final byte AT_MN_SERIAL_ID_TYPE =(byte) 0x96;
	
}

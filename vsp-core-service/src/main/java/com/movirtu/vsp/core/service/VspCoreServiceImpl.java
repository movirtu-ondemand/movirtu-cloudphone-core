/**
 * 
 */
package com.movirtu.vsp.core.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.movirtu.map.MapJavaThriftHandler;
import com.movirtu.map.message.LURequest;
import com.movirtu.map.message.SRISMResponse;
import com.movirtu.map.message.SmsRequest;
import com.movirtu.vsp.core.service.iface.VspCoreService;
import com.movirtu.vsp.persistence.constants.VirtualNumberStatusEnum;
import com.movirtu.vsp.persistence.dao.hb.PrimaryMsisdnDao;
import com.movirtu.vsp.persistence.dao.hb.SettingsDao;
import com.movirtu.vsp.persistence.dao.hb.impl.VirtualNumberPoolDaoImpl;
import com.movirtu.vsp.persistence.domain.PrimaryMsisdn;
import com.movirtu.vsp.persistence.domain.VirtualNumberPool;
import com.movirtu.vsp.persistence.exceptions.RecordNotFoundException;

@Component("VspCoreService")
//@DependsOn(value={"settingsDaoImpl","imsiMsrnMappingDaoImpl"})
public class VspCoreServiceImpl implements VspCoreService {

	private static Logger logger = LoggerFactory.getLogger(VspCoreServiceImpl.class);
	// TODO
	//	@Autowired

	@Autowired
	private PrimaryMsisdnDao primaryMsisdnDao;

	@Autowired
	private VirtualNumberPoolDaoImpl virtualNumberPoolDao;

	@Autowired
	private SettingsDao settingsDao;
	
	@Autowired
	BalanceCheckService balanceCheckService;

	@Autowired
	LocationUpdateManager luManager;
	
	/*Need to be commented  Start*/
	private static volatile VspCoreServiceImpl isntance = new VspCoreServiceImpl();

	public VspCoreServiceImpl() {
		MapJavaThriftHandler.getInstance().initializeThriftInterface();
	}

	public static VspCoreServiceImpl getInstance() {
		return isntance;
	}
	/*Need to be commented  END*/
	
	@Override
	public List<String> getPurchaseNumbers() {
		List<String> numberList = new ArrayList<String>();
		List<VirtualNumberPool> list=null;
		int listSize = Integer.parseInt(settingsDao.getValue("vm_list_size"));
		
		try {
			list = virtualNumberPoolDao.findByStatus(VirtualNumberStatusEnum.UNASSIGNED.getStatus(), listSize, true);
			//list = virtualNumberPoolDao.findByStatus(VirtualNumberStatusEnum.UNASSIGNED.getStatus(), listSize, false);
		} catch (RecordNotFoundException e) {	
			logger.error("No free virtual number");
		}

		for(VirtualNumberPool entry:list) {
			logger.debug("NUMBER="+entry.getMsisdn());
			numberList.add(entry.getMsisdn());
		}

		return numberList;
	}

	@Override
	public void purchaseNumber(String purchaseNumber) {
		logger.debug("Inside purchaseNumber="+purchaseNumber);
		VirtualNumberPool poolNumber=null;

		try {
			poolNumber = virtualNumberPoolDao.findByMsisdn(purchaseNumber);
		} catch (RecordNotFoundException e) {
			logger.error("Exception in findByMsisdn ",e);
		}

		poolNumber.setStatus(VirtualNumberStatusEnum.ASSIGNED.getStatus());
		virtualNumberPoolDao.update(poolNumber);

	}

	@Override
	public void performLocationUpdate(String msisdn, String virtualNumber) {
		VirtualNumberPool poolNumber=null;

		try {
			poolNumber = virtualNumberPoolDao.findByMsisdn(virtualNumber);
		} catch (RecordNotFoundException e) {
			logger.error("Exception in findByMsisdn ",e);
		}

		LURequest luRequest = new LURequest(poolNumber.getImsi());
		luRequest.setMsisdn(virtualNumber);
		luManager.sendLocationUpdate(luRequest);
		//MapJavaThriftHandler.getInstance().submit(luRequest);
	}

	@Override
	public void performLocationUpdateWithImsi(String msisdn, String imsi) {
		LURequest luRequest = new LURequest(imsi);
		luRequest.setMsisdn(msisdn);
		luManager.sendLocationUpdate(luRequest);
		//MapJavaThriftHandler.getInstance().submit(luRequest);
	}

	@Override
	public void updatePrinaryMsisdnTable(String msisdn, int prefix, String virtual_msisdn, 
			String display_msisdn, String charge_msisdn, String vimsi, 
			boolean isActive, boolean isPreparid ) {

		PrimaryMsisdn primaryDao = new PrimaryMsisdn();
		primaryDao.setPrimary_msisdn_key(msisdn);
		primaryDao.setPrefix(prefix);
		primaryDao.setVirtual_msisdn(virtual_msisdn);
		primaryDao.setDisplay_msisdn(display_msisdn);
		primaryDao.setCharge_msisdn(charge_msisdn);
		primaryDao.setVimsi(vimsi);
		primaryDao.setIs_active(isActive);
		primaryDao.setIs_prepaid(isPreparid);

		primaryMsisdnDao.addNew(primaryDao);
	}

	//@Override
	public void handleSRISMResponse(SRISMResponse message) {
		logger.debug("Inside handleSRISMResponse");
		PrimaryMsisdn primaryMsisdn = getProfile(message.getMsisdn());
		primaryMsisdn.setVimsi(message.getImsi());
		//		user.setMcc(message.getMcc());
		//		user.setMnc(message.getMnc());
		logger.debug("updating imsi,mcc,mnc to DB");
		primaryMsisdnDao.update(primaryMsisdn);
	}

	@Override
	public double getBalance(String msisdn) {
				
		return balanceCheckService.getBalance(msisdn);
	}

	@Override
	public Map<String, Double> performBalanceQuery(String msisdn) {
		List<PrimaryMsisdn> primaryMsisdnList=null;

		try {
			primaryMsisdnList = primaryMsisdnDao.findByPrimaryMsisdn(msisdn);

		} catch (RecordNotFoundException e) {
			logger.info("No primary number entries found");
			return null;
		}

		if(primaryMsisdnList.size()==0) {
			return null;
		}

		Map<String, Double> balanceMap = new HashMap<String, Double>();

		for(PrimaryMsisdn msisdnObject: primaryMsisdnList) {
			String virtualMsisdn = msisdnObject.getVirtual_msisdn();
			logger.debug("Performing balance query for "+virtualMsisdn);
			double balance = getBalance(virtualMsisdn);
			balanceMap.put(virtualMsisdn, new Double(balance));
		}

		return balanceMap;

	}


	@Override
	public boolean setDefaultNumber(String msisdn) {
		logger.debug("Inside setDefaultNumber with virtual Msisdn="+msisdn);
		
		PrimaryMsisdn primaryMsisdn=null;

		try {
			primaryMsisdn = primaryMsisdnDao.findByVirtualMsisdn(msisdn);
			logger.debug("primaryMsisdn="+primaryMsisdn);
			
		} catch (RecordNotFoundException e) {
			logger.info("No primary number entries found");
			return false;
		}

		if(primaryMsisdn==null) {
			logger.info("primaryMsisdn is null. Returning false");
			return false;
		}


		if(!resetDeafultNumber(primaryMsisdn)) {
			return false;
		}

		if(!setDeafultNumber(primaryMsisdn)) {
			return false; 
		}

		return true;
	}

	private boolean updateActiveStatus(PrimaryMsisdn primaryMsisdn, boolean active) {
		logger.info("inside updateActiveStatus with primaryMsisdn="+primaryMsisdn +" and active="+active);
		primaryMsisdn.setIs_active(active);
		primaryMsisdnDao.update(primaryMsisdn);
		return true;
	}

	private boolean setDeafultNumber(PrimaryMsisdn primaryMsisdn) {
		return updateActiveStatus(primaryMsisdn, true);
	}

	private boolean resetDeafultNumber(PrimaryMsisdn primaryMsisdn) {
		logger.info("inside resetDeafultNumber");
		List<PrimaryMsisdn> primaryMsisdnList=null;

		try {
			primaryMsisdnList = primaryMsisdnDao.findByPrimaryMsisdn(primaryMsisdn.getPrimary_msisdn_key());
		} catch (RecordNotFoundException e) {
			logger.info("No primary number entries found");
			return false;
		}	

		for(PrimaryMsisdn msisdnObject: primaryMsisdnList) {
			logger.info("inside loop="+msisdnObject);
			if(msisdnObject.isIs_active()) {
				logger.info("Changing stauts======"+msisdnObject);
				return updateActiveStatus(msisdnObject, false);
			}
		}

		return false;
	}

	@Override
	public List<String> getAllAssociatedNumber(String primaryMsisdn) {
		List<PrimaryMsisdn> primaryMsisdnList=null;

		try {
			primaryMsisdnList = primaryMsisdnDao.findByPrimaryMsisdn(primaryMsisdn);

		} catch (RecordNotFoundException e) {
			logger.info("No primary number entries found");
			return null;
		}

		List<String> allNumberList = new ArrayList<String>();

		for(PrimaryMsisdn msisdnObject: primaryMsisdnList) {
			allNumberList.add(msisdnObject.getVirtual_msisdn());
		}

		return allNumberList;
	}

	@Override
	public PrimaryMsisdn getProfile(String msisdn) {
		PrimaryMsisdn primaryMsisdn=null;

		try {
			primaryMsisdn = primaryMsisdnDao.findByVirtualMsisdn(msisdn);

		} catch (RecordNotFoundException e) {
			logger.info("No primary number entries found");
			return null;
		}

		return primaryMsisdn;
	}

	@Override
	public PrimaryMsisdn getProfile(String msisdn, int prefix) {
		PrimaryMsisdn primaryMsisdn=null;

		try {
			primaryMsisdn = primaryMsisdnDao.findByPrimaryMsisdnAndPrefix(msisdn, prefix);

		} catch (RecordNotFoundException e) {
			logger.info("No profile found");
			return null;
		}

		return primaryMsisdn;
	}

	@Override
	public Map<String, Boolean> checkStatus(String primaryMsisdn) {
		List<PrimaryMsisdn> primaryMsisdnList=null;

		try {
			primaryMsisdnList = primaryMsisdnDao.findByPrimaryMsisdn(primaryMsisdn);

		} catch (RecordNotFoundException e) {
			logger.info("No primary number entries found");
			return null;
		}

		if(primaryMsisdnList.size()==0) {
			return null;
		}

		Map<String, Boolean> statusMap = new HashMap<String, Boolean>();

		for(PrimaryMsisdn msisdnObject: primaryMsisdnList) {
			String virtualMsisdn = msisdnObject.getVirtual_msisdn();
			statusMap.put(virtualMsisdn, new Boolean(msisdnObject.isIs_active()));
		}

		return statusMap;
	}

	@Override
	public PrimaryMsisdn createSignupProfile( 
			String sim_number, 
			int profile_prefix, 
			String virtual_msisdn, 
			String vimsi,
			boolean isActive,
			boolean isPrepaid ) {
		return createProfile(sim_number, profile_prefix, virtual_msisdn, vimsi, isActive, isPrepaid);
	}

	private PrimaryMsisdn createProfile( 
			String sim_number, 
			int profile_prefix, 
			String virtual_msisdn, 
			String vimsi,
			boolean isActive,
			boolean isPrepaid ) {

		PrimaryMsisdn primaryMsisdn = new PrimaryMsisdn();

		primaryMsisdn.setPrimary_msisdn_key(sim_number);
		primaryMsisdn.setCharge_msisdn(sim_number);
		primaryMsisdn.setDisplay_msisdn(sim_number);
		primaryMsisdn.setPrefix(profile_prefix);
		primaryMsisdn.setVirtual_msisdn(virtual_msisdn);
		primaryMsisdn.setVimsi(vimsi);
		primaryMsisdn.setIs_active(isActive);
		primaryMsisdn.setIs_prepaid(isPrepaid);



		primaryMsisdnDao.addNew(primaryMsisdn);
		//		// Send SriSms request
		//		MapJavaThriftHandler.getInstance().submit(new SRISMRequest(sim_number));

		return primaryMsisdn;
	}

	@Override
	public PrimaryMsisdn createVirtualProfile( 
			String sim_number, 
			int profile_prefix, 
			String virtual_msisdn,
			boolean isActive,
			boolean isPrepaid ) {

		VirtualNumberPool poolNumber=null;

		try {
			poolNumber = virtualNumberPoolDao.findByMsisdn(virtual_msisdn);
		} catch (RecordNotFoundException e) {
			logger.error("Exception in findByMsisdn ",e);
		}

		return createVirtualProfile(sim_number, profile_prefix, virtual_msisdn, poolNumber.getImsi(), isActive, isPrepaid);
	}

	@Override
	public PrimaryMsisdn createVirtualProfile( 
			String sim_number, 
			int profile_prefix, 
			String virtual_msisdn, 
			String vimsi,
			boolean isActive,
			boolean isPrepaid ) {

		return createProfile(sim_number, profile_prefix, virtual_msisdn, vimsi, isActive, isPrepaid);
	}

	@Override
	public void sendSMS(String msisdn, String message) {
		logger.debug("Inside  sendRegisterSMS with msisdn="+msisdn);
		Map<String, String> ext = new HashMap<String, String>();
		ext.put(SmsRequest.MT_KEY, SmsRequest.MT_KEY_VALUE);
		MapJavaThriftHandler.getInstance().submit(new SmsRequest(msisdn, message, ext));

	}
	@Override
	public void deleteProfile(String sim_number, int profile_prefix) {
		PrimaryMsisdn profile = getProfile(sim_number, profile_prefix);
		primaryMsisdnDao.delete(profile);

	}


}

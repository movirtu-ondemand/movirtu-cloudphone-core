package com.movirtu.vsp.core.service;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.movirtu.vsp.persistence.dao.hb.impl.ImsiMsrnMappingDaoImpl;
import com.movirtu.vsp.persistence.dao.hb.impl.SettingsDaoImpl;
import com.movirtu.zte.mml.client.MMLClient;


@Component
public class BalanceCheckService {

	private static Logger logger = LoggerFactory.getLogger(BalanceCheckService.class);

	private String mml_user ;
	private String mml_password;
	private String mml_terminalId;
	private String mml_serviceId;
	private String mml_smpHost;
	private int mml_smpPort;
	private int mml_heartBeatInterval;
	private boolean useZteMmlBalanceService=false;
	private int mml_DivisionFactor;
	
	@Autowired
	SettingsDaoImpl settingsDaoImpl ;
	
	@Autowired
	ImsiMsrnMappingDaoImpl imsiMsrnMapping;
	

	@PostConstruct
	private void initialize() {

		logger.debug("Initiliasizing BalanceCheckService");

		// Initializing ZTE MML parameters
		this.useZteMmlBalanceService = Boolean.parseBoolean(settingsDaoImpl.getValue("useZteMmlBalanceService"));
		this.mml_user = settingsDaoImpl.getValue("mml_user");
		this.mml_password = settingsDaoImpl.getValue("mml_password");
		this.mml_terminalId = settingsDaoImpl.getValue("mml_terminalId");
		this.mml_serviceId = settingsDaoImpl.getValue("mml_serviceId");
		this.mml_smpHost = settingsDaoImpl.getValue("mml_smpHost");
		this.mml_smpPort = Integer.parseInt(settingsDaoImpl.getValue("mml_smpPort"));
		this.mml_heartBeatInterval = Integer.parseInt(settingsDaoImpl.getValue("mml_heartBeatInterval"));
		this.mml_DivisionFactor = Integer.parseInt(settingsDaoImpl.getValue("mml_DivisionFactor"));
		// Initializing ZTE MML balance service
		if(useZteMmlBalanceService) {
			MMLClient.getInstance().initialize(this.mml_user,
					this.mml_password,
					this.mml_terminalId,
					this.mml_serviceId, 
					this.mml_smpHost,
					this.mml_smpPort,
					this.mml_heartBeatInterval,
					this.mml_DivisionFactor);
		}
	}
	
	public double getBalance(String msisdn) {
		
		double balance=0.0;

		if(useZteMmlBalanceService) {
			balance = MMLClient.getInstance().performBalanceQuery(msisdn);
		} else {
			logger.info("ZTE Balance service not enabled");
		}

		return balance;
	}

}

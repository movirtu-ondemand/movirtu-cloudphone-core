package com.movirtu.vsp.core.service;

import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.movirtu.map.MapJavaThriftHandler;
import com.movirtu.map.message.BaseMessage;
import com.movirtu.map.message.LURequest;
import com.movirtu.map.message.LUResponse;
import com.movirtu.vsp.persistence.dao.hb.PrimaryMsisdnDao;
import com.movirtu.vsp.persistence.dao.hb.SettingsDao;
import com.movirtu.vsp.persistence.domain.PrimaryMsisdn;

@Component
@DependsOn("VspCoreService")
public class LocationUpdateManager implements Observer {

	private static Logger logger = LoggerFactory.getLogger(LocationUpdateManager.class);
	private static Map<String, TimerTask> thriftMessageMap;
	private Timer timer;
	private static int luRequestResendInterval = 120;
	private static int luRequestSendInterval = 60;

	@Autowired
	private PrimaryMsisdnDao primaryMsisdnDao;

	@Autowired
	private SettingsDao settingsDao;

	@PostConstruct
	private void initialize() {
		logger.debug("Initiliasizing LocationUpdateManager");
		MapJavaThriftHandler.getInstance().addObserver(this);

		luRequestResendInterval = Integer.parseInt(settingsDao.getValue("luRequestResendInterval"));
		luRequestSendInterval = Integer.parseInt(settingsDao.getValue("luRequestSendInterval"));

		this.timer = new Timer();
		thriftMessageMap = new ConcurrentHashMap<String, TimerTask>();

		List<PrimaryMsisdn> primaryMsisdnList=null;

		primaryMsisdnList = primaryMsisdnDao.findAll();

		if(primaryMsisdnList.size()==0) {
			return;
		}

		for(PrimaryMsisdn msisdnObject: primaryMsisdnList) {
			LURequest luRequest = new LURequest(msisdnObject.getVimsi());
			luRequest.setMsisdn(msisdnObject.getPrimary_msisdn_key());
			sendLocationUpdate(luRequest);
		}

	}

	@Override
	public void update(Observable observer, Object message) {
		logger.debug("Entering update with message = {}"+ message);

		if ((message instanceof LUResponse)) {
			handleLUResponse((LUResponse) message);
		} else {
			logger.warn("This message is not of desired type: {}" + message);
		}

	}

	/**
	 * This method handles Location update response received from mapgw.
	 * @param message LUResponse message received from network.
	 */
	private void handleLUResponse(LUResponse message) {
		logger.debug("Inside handleLUResponse with message="+message);
		LuRequestTimerTask luTimerTask = cancelTimerTask((BaseMessage)message);
		createAndAddLuTimerTaskToMap(luTimerTask.getMessage(), luRequestResendInterval * 1000);
	}

	public void sendLocationUpdate(LURequest luRequest) {
		logger.debug("Inside sendLocationUpdate for luRequest="+luRequest);
		createAndAddLuTimerTaskToMap(luRequest, luRequestSendInterval * 1000);
		MapJavaThriftHandler.getInstance().submit(luRequest);
	}

	private void createAndAddLuTimerTaskToMap(LURequest request, long delay) {
		logger.debug("Inside createAndAddLuTimerTaskToMap for request="+request +" and delay="+delay);
		String mapId = createMapId(request);
		TimerTask timerTask = new LuRequestTimerTask(request);
		thriftMessageMap.put(mapId, timerTask);
		this.timer.schedule(timerTask, delay);
		logger.debug("Leaving createAndAddTimerTaskToMap");
	}

	private LuRequestTimerTask cancelTimerTask(BaseMessage message) {
		logger.debug("Inside cancelTimerTask");
		LuRequestTimerTask timerTask = removeFromTimerMap(message);

		if (timerTask != null) {
			timerTask.cancel();
		}
		logger.debug("Leaving cancelTimerTask");
		return timerTask;		
	}

	private LuRequestTimerTask removeFromTimerMap(BaseMessage message) {
		String mapId = createMapId(message);
		LuRequestTimerTask timerTask = (LuRequestTimerTask) thriftMessageMap.remove(mapId);	
		return timerTask;
	}

	private String createMapId(BaseMessage message) {
		String mapId = "" + message.getServiceId() + "" + message.getSessionId();
		return mapId;
	}

	class LuRequestTimerTask extends TimerTask {

		int serviceId;
		long sessionId;
		LURequest message;

		LuRequestTimerTask(LURequest message) {
			this.serviceId = message.getServiceId();
			this.sessionId = message.getSessionId();
			this.message = message;
		}

		public void run()
		{
			logger.error("Did not receive response for serviceId=[" + this.serviceId + "] and sessionId=[" + this.sessionId + "]");
			removeFromTimerMap(message);
			sendLocationUpdate(message);
		}

		public int getServiceId() {
			return this.serviceId;
		}

		public void setServiceId() {
			this.serviceId = serviceId;
		}

		public long getSessionId() {
			return this.sessionId;
		}

		public void setSessionId() {
			this.sessionId = sessionId;
		}

		public LURequest getMessage() {
			return this.message;
		}

		public void setMessage() {
			this.message = message;
		}
	}

}

package com.movirtu.vsp.persistence.dao.spring;

import static org.junit.Assert.assertTrue;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

@ContextConfiguration(locations={"classpath:**/applicationContext-test.xml"})
public abstract class AbstractDAOTests extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    protected DataSource dataSource;
    protected SimpleJdbcInsert simpleJdbcInsert;

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        simpleJdbcInsert = new SimpleJdbcInsert(dataSource);
    }

    @After
    public void tearDown() {
    }

    protected void removeAll(String table) {
        simpleJdbcTemplate.update("delete from " + table);
    }

    protected boolean hasAtLeastOneRecord(String table) {
        int rows = simpleJdbcTemplate.queryForInt("select count(1) from " + table);
        return rows > 0;
    }

    abstract protected long add();

    protected void cleanUp() {
        String sql = "delete from subscriber_number_time_interval";
        simpleJdbcTemplate.getJdbcOperations().update(sql);
        sql = "delete from time_interval";
        simpleJdbcTemplate.getJdbcOperations().update(sql);
        sql = "delete from subscr_num_manager";
        simpleJdbcTemplate.getJdbcOperations().update(sql);
        sql = "delete from subscriber_number";
        simpleJdbcTemplate.getJdbcOperations().update(sql);
        sql = "delete from subscriber";
        simpleJdbcTemplate.getJdbcOperations().update(sql);
        sql = "delete from supported_languages";
        simpleJdbcTemplate.getJdbcOperations().update(sql);
        sql = "delete from choices";
        simpleJdbcTemplate.getJdbcOperations().update(sql);
        sql = "delete from current_state";
        simpleJdbcTemplate.getJdbcOperations().update(sql);
        sql = "delete from menu";
        simpleJdbcTemplate.getJdbcOperations().update(sql);
        sql = "delete from msrn";
        simpleJdbcTemplate.getJdbcOperations().update(sql);
        sql = "delete from options";
        simpleJdbcTemplate.getJdbcOperations().update(sql);
        sql = "delete from node";
        simpleJdbcTemplate.getJdbcOperations().update(sql);
        sql = "delete from number_activation_history";
        simpleJdbcTemplate.getJdbcOperations().update(sql);
        sql = "delete from recharge_history";
        simpleJdbcTemplate.getJdbcOperations().update(sql);
        sql = "delete from sms";
        simpleJdbcTemplate.getJdbcOperations().update(sql);
        sql = "delete from states";
        simpleJdbcTemplate.getJdbcOperations().update(sql);
        sql = "delete from temp_call_trans";
        simpleJdbcTemplate.getJdbcOperations().update(sql);

    }

    /**
     * Rigourous Test :-)
     */
    @Test
    public void testApp() {
        assertTrue(true);
    }
}

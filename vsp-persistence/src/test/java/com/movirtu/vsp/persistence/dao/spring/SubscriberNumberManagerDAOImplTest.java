package com.movirtu.vsp.persistence.dao.spring;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.annotation.Timed;

import com.movirtu.vsp.persistence.domain.Language;
import com.movirtu.vsp.persistence.domain.Subscriber;
import com.movirtu.vsp.persistence.domain.SubscriberNumber;
import com.movirtu.vsp.persistence.domain.SubscriberNumberManager;
import com.movirtu.vsp.persistence.exceptions.DaoException;
import com.movirtu.vsp.persistence.exceptions.DuplicateRecordException;
import com.movirtu.vsp.persistence.exceptions.RecordNotFoundException;

public class SubscriberNumberManagerDAOImplTest extends AbstractDAOTests {

	@Autowired
	private SubscriberNumberManagerDAO subscriberNumberManagerDAO;
	@Autowired
	private SubscriberNumberDAO subscriberNumberDAO;
	@Autowired
	private LanguageDAO languageDAO;
	@Autowired
	private SubscriberDAO subscriberDAO;
	/**
	 * Test of insert method, of class SubscriberNumberManagerDAO.
	 */
	@Rollback
	////@Test(expected = DuplicateRecordException.class)
	@Timed(millis = 500)
	public void testInsertExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		SubscriberNumberManager subscriberNumberManager = manuallyInsertSubscriberNumberManager();
		
		subscriberNumberManagerDAO.insert(subscriberNumberManager);
	}

	/**
	 * Test of insert method, of class SubscriberNumberManagerDAO.
	 */
	@Rollback
	@Timed(millis = 100)
	////@Test
	public void testInsertNew() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		simpleJdbcTemplate.update(
				"delete from subscr_num_manager where id = ?",
				new Object[] { 1L });
		SubscriberNumberManager subscriberNumberManager = new SubscriberNumberManager();
		subscriberNumberManager.setId(1);
		subscriberNumberManager.setDefaultNumber(false);
		subscriberNumberManager.setIntervalOnStart("10:10:00");
		subscriberNumberManager.setIntervalOnEnd("20:10:00");
		subscriberNumberManager.setIntervalOffStart("10:10:00");
		subscriberNumberManager.setIntervalOffEnd("20:10:00");
		subscriberNumberManager.setActivated(false);
		subscriberNumberManager.setDivertToNumber("abc");
		subscriberNumberManager.setDivertToVoiceMail(false);
		subscriberNumberManager.setDivertToSMS(false);
		subscriberNumberManager.setDivertToMMS(false);
		subscriberNumberManager.setSubscriberNumber(getManuallyInsertSubscriberNumber());
		
		subscriberNumberManagerDAO.insert(subscriberNumberManager);
		SubscriberNumberManager subscriberNumberManager1 = simpleJdbcTemplate
				.queryForObject(
						"select id,default_num,interval_on_start, interval_on_end,interval_off_start,interval_off_end,activated,divert_to_num,divert_to_vc_mail,divert_to_sms,divert_to_mms,subscr_num_id from subscr_num_manager where divert_to_num = ?",
						new SubscriberNumberManagerRowMapper(), "abc");

		assertEquals("abc", subscriberNumberManager1.getDivertToNumber());
	}

	/**
	 * Test of update method, of class SubscriberNumberManagerDAO.
	 */
	@Rollback
	@Timed(millis = 200)
	////@Test
	public void testUpdateExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		SubscriberNumberManager subscriberNumberManager =  manuallyInsertSubscriberNumberManager();
		subscriberNumberManager.setActivated(true);
		subscriberNumberManagerDAO.update(subscriberNumberManager);
		assertNotSame("Updated SubscriberNumberManager should be different",
				false, subscriberNumberManager.isActivated());
	}

	/**
	 * Test of update method, of class SubscriberNumberManagerDAO.
	 */
	@Rollback
	@Timed(millis = 200)
	////@Test
	public void testUpdateNonExistingSubscriberNumberManager() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		SubscriberNumberManager subscriberNumberManager = new SubscriberNumberManager();
		subscriberNumberManager.setId(1);
		subscriberNumberManager.setDefaultNumber(false);
		subscriberNumberManager.setIntervalOnStart("10:10:00");
		subscriberNumberManager.setIntervalOnEnd("20:10:00");
		subscriberNumberManager.setIntervalOffStart("10:10:00");
		subscriberNumberManager.setIntervalOffEnd("20:10:00");
		subscriberNumberManager.setActivated(false);
		subscriberNumberManager.setDivertToNumber("abc");
		subscriberNumberManager.setDivertToVoiceMail(false);
		subscriberNumberManager.setDivertToSMS(false);
		subscriberNumberManager.setDivertToMMS(false);

		subscriberNumberManagerDAO.update(subscriberNumberManager);
		assertTrue("Update does not fail if data does not exist", true);
	}

	@Rollback
	@Timed(millis = 200)
	////@Test(expected = RecordNotFoundException.class)
	public void testDeleteExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		SubscriberNumberManager subscriberNumberManager = manuallyInsertSubscriberNumberManager();
		
		subscriberNumberManagerDAO.delete(subscriberNumberManager);
		simpleJdbcTemplate
				.queryForObject(
						"select id,default_num,interval_on_start, interval_on_end,interval_off_start,interval_off_end,activated,divert_to_num,divert_to_vc_mail,divert_to_sms,divert_to_mms,subscr_num_id from subscr_num_manager where id = ?",
						new SubscriberNumberManagerRowMapper(), 1L);

	}

	@Rollback
	@Timed(millis = 200)
	////@Test(expected = RecordNotFoundException.class)
	public void testDeleteNonExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		SubscriberNumberManager subscriberNumberManager = new SubscriberNumberManager();
		subscriberNumberManager.setId(1);
		subscriberNumberManager.setDefaultNumber(false);
		subscriberNumberManager.setIntervalOnStart("10:10:00");
		subscriberNumberManager.setIntervalOnEnd("20:10:00");
		subscriberNumberManager.setIntervalOffStart("10:10:00");
		subscriberNumberManager.setIntervalOffEnd("20:10:00");
		subscriberNumberManager.setActivated(false);
		subscriberNumberManager.setDivertToNumber("abc");
		subscriberNumberManager.setDivertToVoiceMail(false);
		subscriberNumberManager.setDivertToSMS(false);
		subscriberNumberManager.setDivertToMMS(false);

		subscriberNumberManagerDAO.delete(subscriberNumberManager);
		simpleJdbcTemplate
				.queryForObject(
						"select id,default_num,interval_on_start, interval_on_end,interval_off_start,interval_off_end,activated,divert_to_num,divert_to_vc_mail,divert_to_sms,divert_to_mms,subscr_num_id from subscr_num_manager where id = ?",
						new SubscriberNumberManagerRowMapper(), 1L);
		assertTrue("Should throw RecordNotFoundException", true);

	}

	////@Test
	@Timed(millis = 200)
	@Rollback
	public void testFindSubscriberNumberManagerByIdExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		SubscriberNumberManager subscriberNumberManager = manuallyInsertSubscriberNumberManager();
		SubscriberNumberManager subscriberNumberManager1 = subscriberNumberManagerDAO
				.findById(1L);
		assertEquals(1L, subscriberNumberManager1.getId());
	}

	////@Test(expected = RecordNotFoundException.class)
	@Timed(millis = 200)
	public void testFindSubscriberNumberManagerByIdNonExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		simpleJdbcTemplate.update(
				"delete from subscr_num_manager where id = ?",
				new Object[] { 1L });
		subscriberNumberManagerDAO.findById(1L);
	}

	////@Test
	@Timed(millis = 200)
	@Rollback
	public void testFindAll() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		SubscriberNumberManager subscriberNumberManager = manuallyInsertSubscriberNumberManager();
		List<SubscriberNumberManager> subscriberNumberManagers = subscriberNumberManagerDAO
				.findAll();
		assertTrue(!subscriberNumberManagers.isEmpty());
	}

	////@Test
	@Timed(millis = 200)
	@Rollback
	public void testFindAllButNoData() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		simpleJdbcTemplate.update("delete from subscr_num_manager");
		List<SubscriberNumberManager> subscriberNumberManagers = subscriberNumberManagerDAO
				.findAll();
		assertTrue(subscriberNumberManagers.isEmpty());
	}
	
	private Subscriber getAddedSubscriber() throws DuplicateRecordException, DaoException, RecordNotFoundException{
		Subscriber subscriber = new Subscriber();
		Language language =new Language("enZA", "English-South Africa");
		languageDAO.insert(language);
		subscriber.setName("abc");
		subscriber.setId(1);
		subscriber.setActualMSISDN("abc");
		subscriber.setPin("abc");
		subscriber.setLanguage(language.getCode());

		simpleJdbcTemplate
				.update("insert into subscriber(id,actual_msisdn,subscr_name,pin,lang_code) values(?,?,?,?,?)",
						new Object[] { subscriber.getId(),
								subscriber.getActualMSISDN()+"1",
								subscriber.getName(), subscriber.getPin(),
								subscriber.getLanguage() });
		return subscriber;
	}
	
	private SubscriberNumber getManuallyInsertSubscriberNumber() throws DuplicateRecordException, DaoException, RecordNotFoundException{
		Subscriber subscriber = getAddedSubscriber();
		
		SubscriberNumber subscriberNumber = new SubscriberNumber();
		subscriberNumber.setId(1);
		subscriberNumber.setManyMeNumber("abc");
		subscriberNumber.setDatePurchased(new Date());
		subscriberNumber.setPrepaidType(false);
		subscriberNumber.setPrefix(6);
		subscriberNumber.setSubscriber(subscriber);
		
		simpleJdbcTemplate
		.update("insert into subscriber_number(id,many_me_num,date_purchased,prepaid_type,subscriber_id,prefix) values(?,?,?,?,?,?)",
				new Object[] { 1,subscriberNumber.getManyMeNumber()+"1",
				subscriberNumber.getDatePurchased(),
				subscriberNumber.isPrepaidType(),
				subscriberDAO.findById(1).getId(),
				subscriberNumber.getPrefix()});
		
		return subscriberNumber;
	}
	
	private SubscriberNumberManager manuallyInsertSubscriberNumberManager() throws DuplicateRecordException, DaoException, RecordNotFoundException{
		SubscriberNumberManager subscriberNumberManager = new SubscriberNumberManager();
		subscriberNumberManager.setId(1);
		subscriberNumberManager.setDefaultNumber(false);
		subscriberNumberManager.setIntervalOnStart("10:10:00");
		subscriberNumberManager.setIntervalOnEnd("20:10:00");
		subscriberNumberManager.setIntervalOffStart("10:10:00");
		subscriberNumberManager.setIntervalOffEnd("20:10:00");
		subscriberNumberManager.setActivated(false);
		subscriberNumberManager.setDivertToNumber("abc");
		subscriberNumberManager.setDivertToVoiceMail(false);
		subscriberNumberManager.setDivertToSMS(false);
		subscriberNumberManager.setDivertToMMS(false);
		subscriberNumberManager.setSubscriberNumber(getManuallyInsertSubscriberNumber());

		simpleJdbcTemplate
				.update("insert into subscr_num_manager(id,default_num,interval_on_start, interval_on_end,interval_off_start,interval_off_end,activated,divert_to_num,divert_to_vc_mail,divert_to_sms,divert_to_mms,subscr_num_id) values(?,?,?,?,?,?,?,?,?,?)",
						new Object[] { subscriberNumberManager.getId(),
								subscriberNumberManager.isDefaultNumber(),
								subscriberNumberManager.getIntervalOnStart(),
								subscriberNumberManager.getIntervalOnEnd(),
								subscriberNumberManager.getIntervalOnStart(),
								subscriberNumberManager.getIntervalOnEnd(),
								subscriberNumberManager.isActivated(),
								subscriberNumberManager.getDivertToNumber(),
								subscriberNumberManager.isDivertToVoiceMail(),
								subscriberNumberManager.isDivertToSMS(),
								subscriberNumberManager.isDivertToMMS(),
								subscriberNumberManager.getSubscriberNumber().getId() });
		return subscriberNumberManager;
	}
	
	private Object []getParams(SubscriberNumberManager s) {
		return new Object[]{s.isDefaultNumber(), s.getIntervalOnStart(), s.getIntervalOnEnd(), s.getIntervalOffStart(), s.getIntervalOffEnd(), s.isActivated(), s.getDivertToNumber(), s.isDivertToVoiceMail(), s.isDivertToSMS(), s.isDivertToMMS(), s.getSubscriberNumber().getId()};
	}
	
	private Object []getParamsWithId(SubscriberNumberManager s) {
		List<Object> numberManagers = Arrays.asList(getParams(s));
		numberManagers.add(s.getId());
		return numberManagers.toArray();
	}
	
	private class SubscriberNumberManagerRowMapper implements RowMapper<SubscriberNumberManager>{
		public SubscriberNumberManager mapRow(ResultSet rs, int rows) throws SQLException {
			SubscriberNumberManager subscriberNumberManager = new SubscriberNumberManager();
			SubscriberNumber subscriberNumber = null;
			try {
				subscriberNumber = subscriberNumberDAO.findById(rs.getLong(12));
			} catch (RecordNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			subscriberNumberManager.setId(rs.getLong(1));
			subscriberNumberManager.setDefaultNumber(rs.getBoolean(2));
			subscriberNumberManager.setIntervalOnStart(rs.getString(3));
			subscriberNumberManager.setIntervalOnEnd(rs.getString(4));
			subscriberNumberManager.setIntervalOffStart(rs.getString(5));
			subscriberNumberManager.setIntervalOffEnd(rs.getString(6));
			subscriberNumberManager.setActivated(rs.getBoolean(7));
			subscriberNumberManager.setDivertToNumber(rs.getString(8));
			subscriberNumberManager.setDivertToVoiceMail(rs.getBoolean(9));
			subscriberNumberManager.setDivertToSMS(rs.getBoolean(10));
			subscriberNumberManager.setDivertToMMS(rs.getBoolean(11));
			subscriberNumberManager.setSubscriberNumber(subscriberNumber);

			return subscriberNumberManager;
		}
	}
	
	//@Test
	public void test(){
		assertTrue("Make it work", true);
	}

	@Override
	protected long add() {
		cleanUp();
		return 0;
	}
}

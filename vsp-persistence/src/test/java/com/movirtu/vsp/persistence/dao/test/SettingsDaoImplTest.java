/**
 * 
 */
package com.movirtu.vsp.persistence.dao.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import com.movirtu.vsp.persistence.dao.hb.impl.SettingsDaoImpl;
import com.movirtu.vsp.persistence.domain.Settings;

/**
 * @author Abhishek.Kumar
 *
 */
public class SettingsDaoImplTest {

	
	SettingsDaoImpl hibernate;

	@Before
	public void setUp() {

		hibernate = new SettingsDaoImpl();

	}

	// @Rollback
	@Test
	public void testAddNew() {
		Settings mapping = new Settings();

		mapping.setKey("key7");
		mapping.setValue("value7");
		hibernate.addNew(mapping);
		assertEquals("Wrong value during insertion", "value7",
				mapping.getValue());
	}

	@Test
	public void testUpdate() {
		Settings mapping = new Settings();
		mapping.setKey("key6");
		mapping.setValue("value34");
		hibernate.update(mapping);
		Settings imsimap = hibernate.findById(mapping.getKey());
		assertEquals(mapping.getKey(), imsimap.getKey());

	}

	@Test
	public void testFindById() {
		String id = "key6";
		Settings mapping = new Settings();
		mapping.setKey(id);
		Settings imsimap = hibernate.findById(mapping.getKey());
		assertEquals(id, imsimap.getKey().toString());

	}

	@Test
	public void testFindAll() {
		Settings mapping = new Settings();
		List<Settings> imap = hibernate.findAll();

		assertTrue(!imap.isEmpty());
	}

	@Test
	public void testDelete() {
		Settings mapping = new Settings();
		mapping.setKey("key6");
		hibernate.delete(mapping.getKey());
		/*
		 * Settings imsimap=hibernate.findById(mapping.getKey());
		 * 
		 * assertEquals(mapping.getKey(),imsimap.getKey());
		 */
	}

	/*
	 * //@Test public void testDeleteAll() { PrimaryMsisdn mapping=new
	 * PrimaryMsisdn(); //t.setId(1); hibernate.delete(t); }
	 */
}

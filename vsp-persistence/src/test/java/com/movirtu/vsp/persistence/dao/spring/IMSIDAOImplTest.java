package com.movirtu.vsp.persistence.dao.spring;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import com.movirtu.vsp.persistence.domain.IMSI;
import com.movirtu.vsp.persistence.domain.Language;
import com.movirtu.vsp.persistence.domain.Subscriber;
import com.movirtu.vsp.persistence.domain.SubscriberNumber;
import com.movirtu.vsp.persistence.exceptions.DaoException;
import com.movirtu.vsp.persistence.exceptions.DuplicateImsiNumberException;
import com.movirtu.vsp.persistence.exceptions.DuplicateLanguageException;
import com.movirtu.vsp.persistence.exceptions.DuplicateRecordException;
import com.movirtu.vsp.persistence.exceptions.NoAvailableMsrnException;
import com.movirtu.vsp.persistence.exceptions.RecordNotFoundException;

public class IMSIDAOImplTest extends AbstractDAOTests {

	@Autowired
	private IMSIDAO iMSIDAO;
	@Autowired
	private SubscriberNumberDAO subscriberNumberDAO;

	@Autowired
	private SubscriberDAO subscriberDAO;
	
	@Autowired
	private LanguageDAO languageDAO;

	/**
	 * Test of insert method, of class IMSIDAO.
	 * @throws DaoException 
	 * @throws DuplicateRecordException 
	 */
	@Rollback
	//@Test(expected = DuplicateRecordException.class)
	public void testInsertExisting() throws DuplicateRecordException, DaoException {
		removeAll();
		long id = add();
		IMSI iMSI = new IMSI();
		iMSI.setId(id);
		iMSI.setManyMeNumber("0720000000");
		iMSI.setImsiNumber("111111111111111");
		iMSI.setInUse(false);
		iMSI.setMsrn(null);

		iMSIDAO.insert(iMSI);
		
	}

	/**
	 * Test of insert method, of class IMSIDAO.
	 */
	@Rollback
	//@Test
	public void testInsertNew() throws DuplicateRecordException, DaoException {
		removeAll();
		assertTrue(!hasAtLeastOneRecord());
		IMSI iMSI = new IMSI();
		iMSI.setId(1);
		iMSI.setManyMeNumber("abc");
		iMSI.setImsiNumber("abc");
		iMSI.setInUse(false);
		iMSI.setMsrn("abc");

		iMSIDAO.insert(iMSI);
		

		assertTrue("Must have at least one record", hasAtLeastOneRecord());
	}

	/**
	 * Test of update method, of class IMSIDAO.
	 * @throws RecordNotFoundException 
	 */
	@Rollback
	//@Test
	public void testUpdateExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		removeAll();
		assertTrue(!hasAtLeastOneRecord());
		IMSI iMSI = new IMSI();
		iMSI.setId(1);
		iMSI.setManyMeNumber("abc");
		iMSI.setImsiNumber("abc");
		iMSI.setInUse(false);
		iMSI.setMsrn("abc");

		simpleJdbcTemplate
				.update("insert into msrn(id,imsi,in_use,msrn,many_me_num) values(?,?,?,?,?)",
						new Object[] { iMSI.getId(), iMSI.getImsiNumber(), true,
								iMSI.getMsrn(), iMSI.getManyMeNumber() });
		iMSIDAO.update(iMSI);
		assertNotSame("Updated IMSI should be different", true, iMSI.isInUse());
	}

	/**
	 * Test of update method, of class IMSIDAO.
	 * @throws RecordNotFoundException 
	 */
	@Rollback
	//@Test(expected = RecordNotFoundException.class)
	public void testUpdateNonExistingIMSI() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		removeAll();
		assertTrue(!hasAtLeastOneRecord());
		IMSI iMSI = new IMSI();
		iMSI.setId(1);
		iMSI.setManyMeNumber("abc");
		iMSI.setImsiNumber("abc");
		iMSI.setInUse(false);
		iMSI.setMsrn("abc");

		iMSIDAO.update(iMSI);
		assertTrue("Update does not fail if data does not exist", true);
	}

	@Rollback
	//@Test
	public void testDeleteExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		removeAll();
		assertTrue(!hasAtLeastOneRecord());
		long id = add();
		IMSI imsi = new IMSI();
		imsi.setId(id);
		assertTrue(hasAtLeastOneRecord());
		iMSIDAO.delete(imsi);
		assertTrue(!hasAtLeastOneRecord());
	}

	@Rollback
	//@Test(expected = RecordNotFoundException.class)
	public void testDeleteNonExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		removeAll();
		assertTrue(!hasAtLeastOneRecord());
		IMSI iMSI = new IMSI();
		iMSI.setId(1);
		iMSIDAO.delete(iMSI);
		assertTrue(!hasAtLeastOneRecord());

	}

	//@Test
	@Rollback
	public void testFindIMSIByIdExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		removeAll();
		assertTrue(!hasAtLeastOneRecord());
		long id = add();
		IMSI iMSI1 = iMSIDAO.findById(id);
		assertNotNull(iMSI1.getId());
	}

	//@Test(expected = RecordNotFoundException.class)
	public void testFindIMSIByIdNonExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		simpleJdbcTemplate.update("delete from msrn where id = ?",
				new Object[] { 1L });
		iMSIDAO.findById(1L);
	}

	//@Test
	@Rollback
	public void testFindAll() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		removeAll();
		assertTrue(!hasAtLeastOneRecord());
		add();
		List<IMSI> iMSIs = iMSIDAO.findAll();
		assertTrue(!iMSIs.isEmpty());
	}

	//@Test
	@Rollback
	public void testFindAllButNoData()  throws DuplicateRecordException, DaoException, RecordNotFoundException{
		simpleJdbcTemplate.update("delete from msrn");
		List<IMSI> iMSIs = iMSIDAO.findAll();
		assertTrue(iMSIs.isEmpty());
	}

	////@Test
	public void testFindFirstAvailableMSRN() throws DuplicateRecordException, DaoException, RecordNotFoundException, NoAvailableMsrnException{
		cleanUp();
		
		addSubscriber();
		//add();
		IMSI imsi = iMSIDAO.findFirstAvailableMSRNAndAssign("111111111111111");
		IMSI imsi2 = iMSIDAO.findById(imsi.getId());
		assertNotSame("New IMSI should be marked in use", imsi.isInUse(), imsi2.isInUse());
	}
	
	
	
	private void removeAll(){
		deleteFromTables("msrn");
	}
	
	private boolean hasAtLeastOneRecord(){
		return hasAtLeastOneRecord("msrn");
	}

	@Override
	protected long add() {
		
		IMSI iMSI = new IMSI();
		iMSI.setId(1);
		iMSI.setManyMeNumber("0720000000");
		iMSI.setImsiNumber("111111111111111");
		iMSI.setInUse(false);
		iMSI.setMsrn(null);
		simpleJdbcInsert.setTableName("msrn");
		simpleJdbcInsert.setGeneratedKeyName("id");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("imsi", iMSI.getImsiNumber());
		map.put("in_use", iMSI.isInUse());
		map.put("msrn", iMSI.getMsrn());
		map.put("many_me_num", iMSI.getManyMeNumber());
		
		Number id = simpleJdbcInsert.executeAndReturnKey(map);
		
		return id.longValue();
	}
	
	protected long addSubscriber() {
		SubscriberNumber subscriberNumber = new SubscriberNumber();
		
		Subscriber subscriber = new Subscriber();
		Language language =new Language("enAB", "English-South Africa");
		try {
			languageDAO.insert(language);
		} catch (DuplicateLanguageException e) {
			e.printStackTrace();
		}
		subscriber.setName("0841111111");
		subscriber.setActualMSISDN("0720000000");
		subscriber.setPin("secrete");
		subscriber.setLanguage(language.getCode());
		long id = 0;
		try {
			id = subscriberDAO.insert(subscriber);
		} catch (DuplicateImsiNumberException e) {
			e.printStackTrace();
		} 
		subscriber.setId(id);
		
		subscriberNumber.setManyMeNumber("0841111111");
		subscriberNumber.setDatePurchased(new Date());
		subscriberNumber.setPrepaidType(false);
		subscriberNumber.setPrefix(6);
		subscriberNumber.setSubscriber(subscriber);
		subscriberNumber.setImsi("111111111111111");
		try {
			subscriberNumber.setId(subscriberNumberDAO.insert(subscriberNumber));
		} catch (DuplicateImsiNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("subscriberNumber = " + subscriberNumber);
		return subscriberNumber.getId();
	}
	
}

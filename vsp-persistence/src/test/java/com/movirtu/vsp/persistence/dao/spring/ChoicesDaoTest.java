package com.movirtu.vsp.persistence.dao.spring;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.vsp.persistence.domain.Choices;

public class ChoicesDaoTest extends AbstractDAOTests  {
	
	@Autowired
	private ChoicesDao choiceDao;
	
 
	//@Test
	public void testFindAllChoicesOfState()
	{
		int stateId=1;
		List<Choices> choices=choiceDao.findAllChoicesOfState(stateId);
		 
		System.out.println(choices);
		assertNotNull(choices);
		
	}
 
	public void testFindNextStateAndAction()
	{
		int stateId=1;
		int choiceId=1;
		Choices choice=choiceDao.findNextStateAndAction(stateId, choiceId);
		System.out.println("choice"+choice);
		assertNotNull(choice);
	}
	@Override
	protected long add() {
		// TODO Auto-generated method stub
		return 0;
	}

}

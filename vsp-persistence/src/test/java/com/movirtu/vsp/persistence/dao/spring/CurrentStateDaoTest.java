/**
 * 
 */
package com.movirtu.vsp.persistence.dao.spring;

import static org.junit.Assert.assertNotNull;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.vsp.persistence.domain.CurrentState;

/**
 * @author Anjaneyulu Bikka
 *
 */
public class CurrentStateDaoTest  extends AbstractDAOTests {
	
	@Autowired
	private CurrentStateDao currentStateDao;
 
	public void testFindCurrentState()
	{
		long sessionId=1222;
		CurrentState currentState=currentStateDao.findCurrentState(sessionId);
		 
		System.out.println(currentState);
		assertNotNull(currentState);
		
	}
	
	public void testChangeState()
	{
		long sessionId=1222;
		currentStateDao.changeState(sessionId, 2);
	}
	//@Test
	public void saveState()
	{
		currentStateDao.saveState(1223, 1);
	}


	@Override
	protected long add() {
		// TODO Auto-generated method stub
		return 0;
	}
}

/**
 * 
 */
package com.movirtu.vsp.persistence.dao.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;








import com.movirtu.vsp.persistence.dao.hb.BaseHibernateImpl;
import com.movirtu.vsp.persistence.dao.hb.ImsiMsrnMappingDao;
import com.movirtu.vsp.persistence.dao.hb.impl.ImsiMsrnMappingDaoImpl;
import com.movirtu.vsp.persistence.domain.ImsiMsrnMapping;
import com.movirtu.vsp.persistence.domain.SubscriberInfo;



/**
 * @author Abhishek.Kumar
 *
 */

public class ImsiMsrnMappingDaoImplTest  {
	ImsiMsrnMappingDaoImpl hibernate;
	
	@Before
	public void setUp() {

		hibernate = new ImsiMsrnMappingDaoImpl();

	}

	// @Rollback
	@Test
	public void testAddNew() {
		ImsiMsrnMapping mapping = new ImsiMsrnMapping();
		mapping.setImsi("IMSITEST123");
		mapping.setMsrn("MSRNTEST");
		hibernate.addNew(mapping);
		assertEquals("Wrong value during insertion", "IMSITEST123",
				mapping.getImsi());
	}

	@Test
	public void testUpdate() {
		ImsiMsrnMapping mapping = new ImsiMsrnMapping();
		mapping.setId(3);
		mapping.setImsi("imsi33");
		mapping.setMsrn("MSRN33");
		hibernate.update(mapping);
		ImsiMsrnMapping imsimap = hibernate.findById(mapping.getId());
		assertEquals(mapping.getId(), imsimap.getId());
	}

	@Test
	public void testFindById() {
		Integer id = 5;
		ImsiMsrnMapping mapping = new ImsiMsrnMapping();
		mapping.setId(id);
		ImsiMsrnMapping imsimap = hibernate.findById(mapping.getId());
		System.out.println(imsimap.getMsrn().toString());
	}

	@Test
	public void testFindAll() {
		final ImsiMsrnMapping mapping = new ImsiMsrnMapping();
		List<ImsiMsrnMapping> imap = hibernate.findAll();

		assertTrue(!imap.isEmpty());
	}

	@Test
	public void testDelete() {
		ImsiMsrnMapping t = new ImsiMsrnMapping();
		t.setId(1);
		hibernate.delete(t.getId());
	}
	

	/*//@Test
	public void testDeleteAll() {
		ImsiMsrnMapping t=new ImsiMsrnMapping();
		//t.setId(1);
			hibernate.delete(t);
	}
	*/
}

package com.movirtu.vsp.persistence.dao.spring;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import com.movirtu.vsp.persistence.domain.Number;
import com.movirtu.vsp.persistence.exceptions.DaoException;
import com.movirtu.vsp.persistence.exceptions.DuplicateImsiNumberException;
import com.movirtu.vsp.persistence.exceptions.DuplicateRecordException;
import com.movirtu.vsp.persistence.exceptions.RecordNotFoundException;

public class NumberDAOImplTest extends AbstractDAOTests {

	@Autowired
	private NumberDAO numberDAO;

	/**
	 * Test of insert method, of class NumberDAO.
	 */
	@Rollback
	//@Test(expected = DuplicateImsiNumberException.class)
	public void testInsertExisting()  throws DuplicateImsiNumberException, DaoException{
		cleanUp();
		add();
		Number number = new Number();
		number.setAvailableNumber("0721111111");
		number.setReserved(false);
		number.setImsi("111111111111111");
		
		numberDAO.insert(number);
	}

	/**
	 * Test of insert method, of class NumberDAO.
	 */
	@Rollback
	//@Test
	public void testInsertNew() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		removeAll();
		Number number = new Number();
		number.setAvailableNumber("0721111111");
		number.setReserved(false);
		number.setImsi("111111111111111");
		number.setAssignedTo("1234");
		numberDAO.insert(number);
		
		assertTrue("Must have at least 1 record", hasAtLeastOneRecord());
	}

	/**
	 * Test of update method, of class NumberDAO.
	 */
	@Rollback
	//@Test
	public void testUpdateExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		removeAll();
		assertTrue(!hasAtLeastOneRecord());
		long id = add();
		Number number = new Number();
		number.setId(id);
		number.setAvailableNumber("0721111111");
		number.setReserved(true);
		number.setImsi("111111111111111");
		number.setAssignedTo("1234");
		numberDAO.update(number);
		number = numberDAO.findById(id);
		assertEquals("Updated Number should be same", true, number.isReserved());
	}

	/**
	 * Test of update method, of class NumberDAO.
	 * @throws RecordNotFoundException 
	 */
	@Rollback
	//@Test
	public void testUpdateNonExistingNumber() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		removeAll();
		assertTrue(!hasAtLeastOneRecord());
		long id = add();
		Number number = new Number();
		number.setId(id);
		number.setAvailableNumber("abc");
		number.setReserved(false);
		number.setImsi("111111111111111");
		numberDAO.update(number);
		assertTrue("Update does not fail if data does not exist", true);
	}

	@Rollback
	//@Test
	public void testDeleteExisting()  throws DuplicateRecordException, DaoException, RecordNotFoundException{
		removeAll();
		assertTrue(!hasAtLeastOneRecord());
		Number number = numberDAO.findById(add());
		assertTrue(hasAtLeastOneRecord());
		numberDAO.delete(number);
		assertTrue(!hasAtLeastOneRecord());

	}

	@Rollback
	//@Test(expected = RecordNotFoundException.class)
	public void testDeleteNonExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		removeAll();
		assertTrue(!hasAtLeastOneRecord());
		Number number = new Number();
		number.setId(1);
		number.setAvailableNumber("abc");
		number.setReserved(false);
		number.setImsi("111111111111111");
		numberDAO.delete(number);
		assertTrue("Should throw RecordNotFoundException", true);

	}

	//@Test
	@Rollback
	public void testFindNumberByIdExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		removeAll();
		assertTrue(!hasAtLeastOneRecord());
		long id = add();
		Number number1 = numberDAO.findById(id);
		assertEquals(id, number1.getId());
	}

	//@Test(expected = RecordNotFoundException.class)
	public void testFindNumberByIdNonExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		simpleJdbcTemplate.update("delete from available_number where id = ?",
				new Object[] { 1L });
		numberDAO.findById(1L);
	}

	//@Test
	@Rollback
	public void testFindAll() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		add();
		List<Number> numbers = numberDAO.findAll();
		assertTrue(!numbers.isEmpty());
	}

	//@Test
	@Rollback
	public void testFindAllButNoData() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		simpleJdbcTemplate.update("delete from available_number");
		List<Number> numbers = numberDAO.findAll();
		assertTrue(numbers.isEmpty());
	}
	
	
	protected long add(){
		
		Number number = new Number();
		number.setAvailableNumber("0721111111");
		number.setReserved(false);
		number.setImsi("111111111111111");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("available_num", number.getAvailableNumber());
		params.put("reserved", number.isReserved());
		params.put("imsi", number.getImsi());
		simpleJdbcInsert.setTableName("available_number");
		simpleJdbcInsert.setGeneratedKeyName("id");
		java.lang.Number id = simpleJdbcInsert.executeAndReturnKey(params);
		return id.longValue();
	}
	
	private void removeAll(){
		deleteFromTables("available_number");
	}
	
	private boolean hasAtLeastOneRecord(){
		return hasAtLeastOneRecord("available_number");
	}

}

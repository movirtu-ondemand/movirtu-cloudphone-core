package com.movirtu.vsp.persistence.dao.spring;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.annotation.Rollback;

import com.movirtu.vsp.persistence.domain.Language;
import com.movirtu.vsp.persistence.domain.Subscriber;
import com.movirtu.vsp.persistence.domain.SubscriberNumber;
import com.movirtu.vsp.persistence.exceptions.DaoException;
import com.movirtu.vsp.persistence.exceptions.DuplicateImsiNumberException;
import com.movirtu.vsp.persistence.exceptions.DuplicateLanguageException;
import com.movirtu.vsp.persistence.exceptions.DuplicateRecordException;
import com.movirtu.vsp.persistence.exceptions.RecordNotFoundException;

public class SubscriberNumberDAOImplTest extends AbstractDAOTests {

	private static Logger logger = Logger.getLogger(SubscriberNumberDAOImplTest.class);
	
	@Autowired
	private SubscriberNumberDAO subscriberNumberDAO;

	@Autowired
	private SubscriberDAO subscriberDAO;
	
	@Autowired
	private LanguageDAO languageDAO;

	/**
	 * Test of insert method, of class SubscriberNumberDAO.
	 */
	@Rollback
	//@Test
	public void testInsertNew() throws DuplicateRecordException, DaoException {
		
		SubscriberNumber subscriberNumber = new SubscriberNumber();
		
		Subscriber subscriber = new Subscriber();
		Language language =new Language("enAB", "English-South Africa");
		languageDAO.insert(language);
		subscriber.setName("abc");
		subscriber.setId(1);
		subscriber.setActualMSISDN("abc");
		subscriber.setPin("abc");
		subscriber.setLanguage(language.getCode());
		subscriber.setImsi("0123");
		
		subscriberNumber.setId(1);
		subscriberNumber.setManyMeNumber("abc");
		subscriberNumber.setDatePurchased(new Date());
		subscriberNumber.setPrepaidType(false);
		subscriberNumber.setPrefix(6);
		subscriberNumber.setSubscriber(subscriber);
		subscriberNumber.setImsi("1245");
		
		
		subscriberNumberDAO.insert(subscriberNumber);
		SubscriberNumber subscriberNumber1 = simpleJdbcTemplate
				.queryForObject(
						"select id,many_me_num,date_purchased,prepaid_type,subscriber_id,prefix,bill_this_number from subscriber_number where many_me_num = ?",
						new SubscriberNumberRowMapper(), "abc");

		assertEquals("abc", subscriberNumber1.getManyMeNumber());
	}

	/**
	 * Test of update method, of class SubscriberNumberDAO.
	 */
	@Rollback
	//@Test
	public void testUpdateExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		SubscriberNumber subscriberNumber = subscriberNumberDAO.findById(add());
		String oldNumber = subscriberNumber.getManyMeNumber();
		subscriberNumber.setManyMeNumber(oldNumber.replace("0", "1"));
		subscriberNumberDAO.update(subscriberNumber);
		subscriberNumber = subscriberNumberDAO.findById(subscriberNumber.getId());
		assertNotSame("Updated SubscriberNumber should be different", oldNumber,
				subscriberNumber.getManyMeNumber());
	}

	/**
	 * Test of update method, of class SubscriberNumberDAO.
	 * @throws RecordNotFoundException 
	 */
	//@Test(expected = RecordNotFoundException.class)
	public void testUpdateNonExistingSubscriberNumber() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		SubscriberNumber subscriberNumber = new SubscriberNumber();
		subscriberNumber.setId(1);
		subscriberNumber.setManyMeNumber("abc");
		subscriberNumber.setDatePurchased(new Date());
		subscriberNumber.setPrepaidType(false);
		subscriberNumber.setPrefix(6);

		subscriberNumberDAO.update(subscriberNumber);
		assertTrue("Update does not fail if data does not exist", true);
	}

	//@Test(expected = RecordNotFoundException.class)
	public void testDeleteExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		SubscriberNumber subscriberNumber = getManuallyInsertSubscriberNumber();
		
		subscriberNumberDAO.delete(subscriberNumber);
		simpleJdbcTemplate
				.queryForObject(
						"select id,many_me_num,date_purchased,prepaid_type,subscriber_id,prefix,bill_this_number from subscriber_number where id = ?",
						new SubscriberNumberRowMapper(), 1L);

	}

	//@Test(expected = RecordNotFoundException.class)
	public void testDeleteNonExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		SubscriberNumber subscriberNumber = new SubscriberNumber();
		subscriberNumber.setId(1);
		subscriberNumber.setManyMeNumber("abc");
		subscriberNumber.setDatePurchased(new Date());
		subscriberNumber.setPrepaidType(false);
		subscriberNumber.setPrefix(6);

		subscriberNumberDAO.delete(subscriberNumber);
		simpleJdbcTemplate
				.queryForObject(
						"select id,many_me_num,date_purchased,prepaid_type,subscriber_id,prefix,bill_this_number from subscriber_number where id = ?",
						new SubscriberNumberRowMapper(), 1L);
		assertTrue("Should throw RecordNotFoundException", true);

	}

	//@Test
	public void testFindSubscriberNumberByIdExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		
		SubscriberNumber subscriberNumber = subscriberNumberDAO.findById(add());
		assertTrue("Record should be found", subscriberNumber != null);
	}

	//@Test(expected = RecordNotFoundException.class)
	public void testFindSubscriberNumberByIdNonExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		simpleJdbcTemplate.update("delete from subscriber_number where id = ?",
				new Object[] { 1L });
		subscriberNumberDAO.findById(1L);
	}

	//@Test
	public void testFindAll() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		
		List<SubscriberNumber> subscriberNumbers = subscriberNumberDAO
				.findAll();
		assertTrue(!subscriberNumbers.isEmpty());
	}
	
	//@Test
	@Rollback
	public void testFindByManyMeNumberExists() throws DuplicateRecordException, DaoException, RecordNotFoundException{
		SubscriberNumber subscriberNumber = new SubscriberNumber();
		
		Subscriber subscriber = new Subscriber();
		Language language =new Language("enAB", "English-South Africa");
		languageDAO.insert(language);
		subscriber.setName("abc");
		subscriber.setId(5);
		subscriber.setActualMSISDN("abc");
		subscriber.setPin("abc");
		subscriber.setLanguage(language.getCode());
		subscriber.setImsi("10101919191981");
		simpleJdbcTemplate
				.update("insert into subscriber(id,actual_msisdn,subscr_name,pin,lang_code, imsi) values(?,?,?,?,?,?)",
						new Object[] { subscriber.getId(),
								subscriber.getActualMSISDN()+"1",
								subscriber.getName(), subscriber.getPin(),
								subscriber.getLanguage(), 
								subscriber.getImsi() });
		
		subscriberNumber.setId(1);
		subscriberNumber.setManyMeNumber("abc");
		subscriberNumber.setDatePurchased(new Date());
		subscriberNumber.setPrepaidType(false);
		subscriberNumber.setPrefix(6);
		subscriberNumber.setSubscriber(subscriber);
		subscriberNumber.setImsi("01717171717171");
		simpleJdbcTemplate
				.update("insert into subscriber_number(many_me_num,date_purchased,prepaid_type,subscriber_id,prefix,bill_this_number,imsi) values(?,?,?,?,?,?,?)",
						new Object[] { subscriberNumber.getManyMeNumber(),
						subscriberNumber.getDatePurchased(),
						subscriberNumber.isPrepaidType(),
						subscriberDAO.findById(5).getId(),
						subscriberNumber.getPrefix(),
						subscriberNumber.isBillThisNumber(),
						subscriberNumber.getImsi()});
		
		SubscriberNumber subscriberNumber2 = subscriberNumberDAO.findByManyMeNumber(subscriberNumber.getManyMeNumber());
		assertEquals("They should be the same", "abc", subscriberNumber2.getManyMeNumber());
	}

	private Subscriber getAddedSubscriber() throws DuplicateRecordException, DaoException, RecordNotFoundException{
		
		Subscriber subscriber = new Subscriber();
		Language language =new Language("enAB", "English-South Africa");
		try {
			languageDAO.insert(language);
		} catch (DuplicateLanguageException e) {
			e.printStackTrace();
		} 
		subscriber.setName("Sipho");
		subscriber.setId(2);
		subscriber.setActualMSISDN("0720000000");
		subscriber.setPin("secrete");
		subscriber.setImsi("111111111111111");
		subscriber.setLanguage(language.getCode());
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("actual_msisdn", subscriber.getActualMSISDN());
		map.put("subscr_name", subscriber.getName());
		map.put("pin", subscriber.getPin());
		map.put("lang_code", subscriber.getLanguage());
		map.put("imsi", subscriber.getImsi());
		simpleJdbcInsert.setTableName("subscriber");
		simpleJdbcInsert.setGeneratedKeyName("id");
		Number id = simpleJdbcInsert.executeAndReturnKey(map);
		subscriber.setId(id.longValue());
		return subscriber;
	}
	
	private SubscriberNumber getManuallyInsertSubscriberNumber() throws DuplicateRecordException, DaoException, RecordNotFoundException{
		Subscriber subscriber = getAddedSubscriber();
		
		SubscriberNumber subscriberNumber = new SubscriberNumber();
		subscriberNumber.setId(1);
		subscriberNumber.setManyMeNumber("0821111111");
		subscriberNumber.setDatePurchased(new Date());
		subscriberNumber.setPrepaidType(false);
		subscriberNumber.setPrefix(3);
		subscriberNumber.setSubscriber(subscriber);
		subscriberNumber.setImsi("111111111111111");
		
		simpleJdbcTemplate
		.update("insert into subscriber_number(many_me_num,date_purchased,prepaid_type,subscriber_id,prefix,bill_this_number,imsi) values(?,?,?,?,?,?,?)",
				new Object[] { subscriberNumber.getManyMeNumber()+"1",
				subscriberNumber.getDatePurchased(),
				subscriberNumber.isPrepaidType(),
				subscriberDAO.findById(1).getId(),
				subscriberNumber.getPrefix(),
				subscriberNumber.isBillThisNumber(),
				subscriberNumber.getImsi()});
		
		return subscriberNumber;
	}
	
	private class SubscriberNumberRowMapper implements RowMapper<SubscriberNumber>{
		public SubscriberNumber mapRow(ResultSet rs, int rows) throws SQLException{
			SubscriberNumber subscriberNumber = new SubscriberNumber();
			Subscriber subscriber = null;
			try {
				subscriber = subscriberDAO.findById(rs.getLong(5));
			} catch (RecordNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			subscriberNumber.setId(rs.getLong(1));
			subscriberNumber.setManyMeNumber(rs.getString(2));
			subscriberNumber.setDatePurchased(rs.getDate(3));
			subscriberNumber.setPrepaidType(rs.getBoolean(4));
			subscriberNumber.setSubscriber(subscriber);
			subscriberNumber.setPrefix(rs.getInt(6));
			subscriberNumber.setBillThisNumber(rs.getBoolean(7));
			
			return subscriberNumber;
		}
	}
	
	//@Test
	public void testFindByMSISDNAndPrefixFound() throws DuplicateRecordException, DaoException, RecordNotFoundException{
		SubscriberNumber subscriberNumber =subscriberNumberDAO.findById(add());
		
		String actualMsisdn = subscriberNumber.getSubscriber().getActualMSISDN();
		int prefix = subscriberNumber.getPrefix();
		if (logger.isDebugEnabled()) {
			logger.debug("actualMsisdn = " + actualMsisdn);
			logger.debug("prefix = " + prefix);
		}
		SubscriberNumber foundSubscriberNumber = subscriberNumberDAO.findByMSISDNAndPrefix(actualMsisdn, prefix);
		if (logger.isDebugEnabled()){
			logger.debug("foundSubscriberNumber = " + foundSubscriberNumber);
		}
	}
	
	@Override
	protected long add() {
		cleanUp();
		SubscriberNumber subscriberNumber = new SubscriberNumber();
		
		Subscriber subscriber = new Subscriber();
		Language language =new Language("enAB", "English-South Africa");
		try {
			languageDAO.insert(language);
		} catch (DuplicateLanguageException e) {
			e.printStackTrace();
		}
		subscriber.setName("0841111111");
		subscriber.setActualMSISDN("0720000000");
		subscriber.setPin("secrete");
		subscriber.setLanguage(language.getCode());
		long id = 0;
		try {
			id = subscriberDAO.insert(subscriber);
		} catch (DuplicateImsiNumberException e) {
			e.printStackTrace();
		} 
		subscriber.setId(id);
		
		subscriberNumber.setManyMeNumber("0841111111");
		subscriberNumber.setDatePurchased(new Date());
		subscriberNumber.setPrepaidType(false);
		subscriberNumber.setPrefix(6);
		subscriberNumber.setSubscriber(subscriber);
		subscriberNumber.setImsi("111111111111111");
		try {
			subscriberNumber.setId(subscriberNumberDAO.insert(subscriberNumber));
		} catch (DuplicateImsiNumberException e) {
			e.printStackTrace();
		}
		System.out.println("subscriberNumber = " + subscriberNumber);
		return subscriberNumber.getId();
	}
	
}

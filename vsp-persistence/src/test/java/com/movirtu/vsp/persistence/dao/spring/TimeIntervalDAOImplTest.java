package com.movirtu.vsp.persistence.dao.spring;

import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.vsp.persistence.domain.Language;
import com.movirtu.vsp.persistence.domain.Subscriber;
import com.movirtu.vsp.persistence.domain.SubscriberNumber;
import com.movirtu.vsp.persistence.domain.TimeInterval;
import com.movirtu.vsp.persistence.exceptions.DuplicateImsiNumberException;
import com.movirtu.vsp.persistence.exceptions.DuplicateLanguageException;
import com.movirtu.vsp.persistence.exceptions.DuplicateRecordException;
import com.movirtu.vsp.persistence.exceptions.NoLanguageSetException;
import com.movirtu.vsp.persistence.exceptions.RecordNotFoundException;

public class TimeIntervalDAOImplTest extends AbstractDAOTests {

	protected final static Logger LOGGER = Logger.getLogger(TimeIntervalDAOImplTest.class);
	
	@Autowired
	private TimeIntervalDAO timeIntervalDAO;
	@Autowired
	private LanguageDAO languageDAO;
	@Autowired
	private SubscriberNumberDAO subscriberNumberDAO;
	@Autowired
	private SubscriberDAO subscriberDAO;
	@Autowired
	private SubscriberNumberTimeIntervalDao subscriberNumberTimeIntervalDao;
	
	//@Test
	public void testIsNumberActiveAtThisTimeNoIntervalSet(){
		cleanUp();
		assertTrue("Must not contain any record", !hasAtLeastOneRecord());
		boolean isNumberActiveNow = timeIntervalDAO.isNumberActiveAtThisTime("0841111111");
		assertTrue("Any number should be refelcting active", isNumberActiveNow);
	}

	//@Test
	public void testIsNumberActiveAtThisTimeIntervalSetButNoAssociation(){
		cleanUp();
		assertTrue("Must not contain any record", !hasAtLeastOneRecord());
		add();
		boolean isNumberActiveNow = timeIntervalDAO.isNumberActiveAtThisTime("0841111111");
		assertTrue("Any number should be refelcting active", isNumberActiveNow);
	}
	
	//@Test
	public void testIsNumberActiveAtThisTimeIntervalSetButWithAssociation() throws NoLanguageSetException, RecordNotFoundException{
		cleanUp();
		assertTrue("Must not contain any record", !hasAtLeastOneRecord());
		addAndAssociate();
		boolean isNumberActiveNow = timeIntervalDAO.isNumberActiveAtThisTime("0841111111");
		assertTrue("Any number should be refelcting active", !isNumberActiveNow);
	}
	
	//@Test
	public void testFindAllActiveIntervalsAs12HourFormat() throws RecordNotFoundException{
		cleanUp();
		assertTrue("Must not contain any record", !hasAtLeastOneRecord());
		add();
		List<TimeInterval> allIntervals = timeIntervalDAO.findAllActiveIntervalsAs12HourFormat();
		for(TimeInterval interval : allIntervals) {
			LOGGER.debug("interval = " + interval);
		}
	}
	
	@Override
	protected long add() {
		cleanUp();
		TimeInterval timeInterval = new TimeInterval(0, "00:00:00", "23:59:59", false);
		try {
			timeIntervalDAO.insert(timeInterval);
			timeInterval = new TimeInterval(0, "00:00:00", "23:59:59", true);
			timeIntervalDAO.insert(timeInterval);
			timeInterval = new TimeInterval(0, "08:00:00", "16:59:59", true);
			timeIntervalDAO.insert(timeInterval);
			timeInterval = new TimeInterval(0, "17:00:00", "21:59:59", false);
			timeIntervalDAO.insert(timeInterval);
		} catch (DuplicateRecordException e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
	protected long addAndAssociate() throws NoLanguageSetException, RecordNotFoundException {
		TimeInterval timeInterval = new TimeInterval(0, "00:00:00", "23:59:59", false);
		long id = 0;
		try {
			id = timeIntervalDAO.insert(timeInterval);
			timeInterval = new TimeInterval(0, "00:00:00", "23:59:59", true);
			id = timeIntervalDAO.insert(timeInterval);
			timeInterval = new TimeInterval(0, "08:00:00", "16:59:59", true);
			id = timeIntervalDAO.insert(timeInterval);
			timeInterval = new TimeInterval(0, "17:00:00", "21:59:59", false);
			id = timeIntervalDAO.insert(timeInterval);
			timeInterval.setId(id);
			LOGGER.debug("timeInterval = " + timeInterval);
			System.out.println("timeInterval = " + timeInterval);
			long subscriberNumberId = addSubscruberNumber();
			SubscriberNumber subscriberNumber = subscriberNumberDAO.findById(subscriberNumberId);
			subscriberNumberTimeIntervalDao.setTimeInterval(subscriberNumber.getManyMeNumber(), (int)id);
			//subscriberNumberTimeIntervalDao.insert(subscriberNumberId, id);
		} catch (DuplicateRecordException e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
	protected long addSubscruberNumber() throws DuplicateImsiNumberException, NoLanguageSetException {
		
		SubscriberNumber subscriberNumber = new SubscriberNumber();
		
		Subscriber subscriber = new Subscriber();
		Language language =new Language("enAB", "English-South Africa");
		try {
			languageDAO.insert(language);
		} catch (DuplicateLanguageException e) {
			e.printStackTrace();
		}
		subscriber.setName("0841111111");
		subscriber.setActualMSISDN("0720000000");
		subscriber.setPin("secrete");
		subscriber.setLanguage(language.getCode());
		long id = subscriberDAO.insert(subscriber);
		subscriber.setId(id);
		
		subscriberNumber.setManyMeNumber("0841111111");
		subscriberNumber.setDatePurchased(new Date());
		subscriberNumber.setPrepaidType(false);
		subscriberNumber.setPrefix(6);
		subscriberNumber.setSubscriber(subscriber);
		subscriberNumber.setImsi("111111111111111");
		subscriberNumber.setId(subscriberNumberDAO.insert(subscriberNumber));
		LOGGER.debug("subscriberNumber = " + subscriberNumber);
		System.out.println("subscriberNumber = " + subscriberNumber);
		return subscriberNumber.getId();
	}

	
	private boolean hasAtLeastOneRecord(){
		return hasAtLeastOneRecord("time_interval");
	}

}

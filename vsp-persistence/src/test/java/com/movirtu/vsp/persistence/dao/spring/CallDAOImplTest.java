package com.movirtu.vsp.persistence.dao.spring;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.test.annotation.Rollback;

import com.movirtu.vsp.persistence.exceptions.DaoException;
import com.movirtu.vsp.persistence.exceptions.DuplicateImsiNumberException;
import com.movirtu.vsp.persistence.exceptions.DuplicateRecordException;
import com.movirtu.vsp.persistence.exceptions.RecordNotFoundException;

public class CallDAOImplTest extends AbstractDAOTests {

	@Autowired
	private CallDAO callDAO;

	protected static Log log = LogFactory.getLog(CallDAOImplTest.class);
	/**
	 * Test of insert method, of class CallDAO.
	 * @throws DaoException 
	 * @throws DuplicateRecordException 
	 */
	@Rollback
	//@Test
	public void testInsertNew() throws DuplicateRecordException, DaoException {

		removeAll();
		Call call = new Call();
		call.setId(1);
		call.setCallingNumber("abc");
		call.setCalledNumber("abc");
		call.setManyMeNumber("abc");
		call.setConnectingTime(new java.sql.Timestamp(System.currentTimeMillis()));
		call.setConnectedTime(new java.sql.Timestamp(System.currentTimeMillis()));
		call.setConnectingTime(new java.sql.Timestamp(System.currentTimeMillis()));
		call.setStartBalance(8.0);
		call.setEndBalance(9.0);
		call.setDuration("abc");

		callDAO.insert(call);

		assertTrue("There should be at least 1 call", hasAtLeastOneRecord());
	}

	/**
	 * Test of update method, of class CallDAO.
	 * @throws DaoException 
	 * @throws RecordNotFoundException 
	 * @throws DuplicateRecordException 
	 */
	@Rollback
	//@Test
	public void testUpdateNonExistingCall() throws DuplicateImsiNumberException, DaoException, RecordNotFoundException {
		removeAll();
		assertTrue(!hasAtLeastOneRecord());
		long id = add();
		Call call = new Call();
		call.setId(id);
		call.setCallingNumber("abc");
		call.setCalledNumber("abc");
		call.setManyMeNumber("abc");
		call.setConnectingTime(new java.sql.Timestamp(System.currentTimeMillis()));
		call.setConnectedTime(new java.sql.Timestamp(System.currentTimeMillis()));
		call.setConnectingTime(new java.sql.Timestamp(System.currentTimeMillis()));
		call.setStartBalance(8.0);
		call.setEndBalance(9.0);
		call.setDuration("abc");

		callDAO.update(call);
		assertTrue("Update does not fail if data does not exist", true);
	}

	@Rollback
	//@Test
	public void testDeleteExisting() throws DaoException, RecordNotFoundException {
		cleanUp();
		long id = add();
		callDAO.delete(id);
		int rows = simpleJdbcTemplate.queryForInt("select count(1) from call where id = ?", id);
		assertTrue("There must be no records found", rows == 0);
	}

	@Rollback
	//@Test(expected = RecordNotFoundException.class)
	public void testDeleteNonExisting() throws DaoException, RecordNotFoundException {
		removeAll();

		callDAO.delete(-1);
		int rows = simpleJdbcTemplate.queryForInt("select count(1) from call where id = ?", -1);
		assertTrue("There must be no records found", rows == 0);

	}

	//@Test
	@Rollback
	public void testFindCallByIdExisting() {
		removeAll();
		long id = add();
		Call call1 = null;
		try {
			call1 = callDAO.findById(id);
		} catch (RecordNotFoundException e) {
			log.debug(e.getMessage(), e);
			fail("Should not throu this exception");
		}
		assertEquals(id, call1.getId());
	}

	//@Test(expected = RecordNotFoundException.class)
	public void testFindCallByIdNonExisting() throws RecordNotFoundException{
		removeAll();
		assertTrue(!hasAtLeastOneRecord());

		Call call = callDAO.findById(1L);
		log.debug("found call: " + call);
		assertTrue(call != null);
	}

	//@Test
	@Rollback
	public void testFindAll() throws RecordNotFoundException, DaoException {
		removeAll();
		add();
		List<Call> calls = callDAO.findAll();
		assertTrue(!calls.isEmpty());
	}

	//@Test
	@Rollback
	public void testFindAllButNoData() throws RecordNotFoundException, DaoException {
		simpleJdbcTemplate.update("delete from call");
		List<Call> calls = callDAO.findAll();
		assertTrue(calls.isEmpty());
	}


	protected long add(){
		cleanUp();
		SimpleJdbcInsert insert = new SimpleJdbcInsert(dataSource).withTableName("call").usingGeneratedKeyColumns("id");

		SqlParameterSource params = new MapSqlParameterSource()
		.addValue("calling_num", "0720000000")
		.addValue("called_num", "0830000000")
		.addValue("many_me_num", "0820000000")
		.addValue("connection_time", new java.sql.Timestamp(System.currentTimeMillis()))
		.addValue("connected_time", new java.sql.Timestamp(System.currentTimeMillis() + 3))
		.addValue("disconnected_time", new java.sql.Timestamp(System.currentTimeMillis() + 700))
		.addValue("start_balance", 8.5)
		.addValue("end_balance", 2.5)
		.addValue("duration", 6.0);
		Number id = insert.executeAndReturnKey(params);
		return id.longValue();

	}

	private void removeAll(){
		deleteFromTables("call");
	}

	private boolean hasAtLeastOneRecord(){
		return hasAtLeastOneRecord("call");
	}
	
}

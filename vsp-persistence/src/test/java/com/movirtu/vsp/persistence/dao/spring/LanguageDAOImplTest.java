package com.movirtu.vsp.persistence.dao.spring;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.annotation.Rollback;

import com.movirtu.vsp.persistence.domain.Language;
import com.movirtu.vsp.persistence.exceptions.DaoException;
import com.movirtu.vsp.persistence.exceptions.DuplicateRecordException;
import com.movirtu.vsp.persistence.exceptions.RecordNotFoundException;

public class LanguageDAOImplTest extends AbstractDAOTests {

	@Autowired
	private LanguageDAO languageDAO;

	/**
	 * Test of insert method, of class PersonDAO.
	 */
	//@Test(expected = DuplicateRecordException.class)
	public void testInsertExisting() throws DuplicateRecordException, DaoException {
		add();
		languageDAO.insert(new Language("EN_AB", "English"));
	}

	/**
	 * Test of insert method, of class PersonDAO.
	 * @throws RecordNotFoundException 
	 */
	@Rollback(value=false)
	//@Test
	public void testInsertNew() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		languageDAO.delete(new Language("EN_UK", "English"));
		languageDAO.insert(new Language("EN_UK", "English"));
		Language language = simpleJdbcTemplate
				.queryForObject(
						"select code, description from supported_languages where code = ?",
						new RowMapper<Language>() {

							public Language mapRow(ResultSet rs, int rows)
									throws SQLException {
								Language language = new Language();
								language.setCode(rs.getString(1));
								language.setDescription(rs.getString(2));
								return language;
							}
						}, "EN_UK");

		assertEquals("EN_UK", language.getCode());
	}

	/**
	 * Test of update method, of class PersonDAO.
	 * @throws RecordNotFoundException 
	 */
	@Rollback
	//@Test
	public void testUpdateExistingLanguage() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		simpleJdbcTemplate.update(
				"insert into supported_languages values(?, ?)", new Object[] {
						"EN_AB", "English" });
		Language language = new Language("EN_AB", "English UK");
		languageDAO.update(language);
		assertNotSame("Updated language should be different",
				language.getDescription(), "English");
	}

	/**
	 * Test of update method, of class PersonDAO.
	 * @throws RecordNotFoundException 
	 */
	@Rollback
	//@Test(expected = RecordNotFoundException.class)
	public void testUpdateNonExistingLanguage() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		Language language = new Language("EN_AB", "English UK");
		languageDAO.update(language);
		assertTrue("Update does not fail if data does not exist", true);
	}

	@Rollback
	//@Test(expected = EmptyResultDataAccessException.class)
	public void testDeleteExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		simpleJdbcTemplate.update(
				"insert into supported_languages values(?, ?)", new Object[] {
						"EN_AB", "English" });
		languageDAO.delete(new Language("EN_AB", "English UK"));
		simpleJdbcTemplate
				.queryForObject(
						"select code, description from supported_languages where code = ?",
						new RowMapper<Language>() {

							public Language mapRow(ResultSet rs, int rows)
									throws SQLException {
								Language language = new Language();
								language.setCode(rs.getString(1));
								language.setDescription(rs.getString(2));
								return language;
							}
						}, "EN_AB");
		
	}

	@Rollback
	//@Test(expected = RecordNotFoundException.class)
	public void testDeleteNonExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		languageDAO.delete(new Language("EN_AB", "English UK"));
		simpleJdbcTemplate
				.queryForObject(
						"select code, description from supported_languages where code = ?",
						new RowMapper<Language>() {

							public Language mapRow(ResultSet rs, int rows)
									throws SQLException {
								Language language = new Language();
								language.setCode(rs.getString(1));
								language.setDescription(rs.getString(2));
								return language;
							}
						}, "EN_AB");
		assertTrue("Should throw RecordNotFoundException", true);

	}

	//@Test
	@Rollback
	public void testFindLanguageByIdExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		simpleJdbcTemplate.update(
				"insert into supported_languages values(?, ?)", new Object[] {
						"EN_AB", "English" });
		Language language = languageDAO.findById("EN_AB");
		assertEquals("English", language.getDescription());
	}

	//@Test(expected = RecordNotFoundException.class)
	public void testFindLanguageByIdNonExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		languageDAO.findById("EN_AB");
	}

	//@Test
	@Rollback
	public void testFindAll() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		simpleJdbcTemplate.update(
				"insert into supported_languages values(?, ?)", new Object[] {
						"EN_AB", "English" });
		List<Language> languages = languageDAO.findAll();
		assertTrue(!languages.isEmpty());
	}

	//@Test
	@Rollback
	public void testFindAllButNoData() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		cleanUp();
		List<Language> languages = languageDAO.findAll();
		assertTrue(languages.isEmpty());
	}

	@Override
	protected long add() {
		cleanUp();
		Language language = new Language("EN_AB", "English UK");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", language.getCode());
		map.put("description", language.getDescription());
		simpleJdbcInsert.setTableName("supported_languages");
		return simpleJdbcInsert.execute(map);
	}
	

}

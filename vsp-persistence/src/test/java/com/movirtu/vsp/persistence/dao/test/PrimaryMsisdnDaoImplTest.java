/**
 * 
 */
package com.movirtu.vsp.persistence.dao.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import com.movirtu.vsp.persistence.dao.hb.impl.SubscriberInfoDaoImpl;
import com.movirtu.vsp.persistence.domain.SubscriberInfo;

/**
 * @author Abhishek.Kumar
 *
 */
public class PrimaryMsisdnDaoImplTest {

	SubscriberInfoDaoImpl hibernate;

	@Before
	public void setUp() {

		hibernate = new SubscriberInfoDaoImpl();

	}

	// @Rollback
	@Test
	public void testAddNew() {
		SubscriberInfo mapping = new SubscriberInfo();
		mapping.setCharge_msisdn("charge_msisdnTest");
		mapping.setDisplay_msisdn("display_msisdnTest");
		mapping.setIs_active(true);
		mapping.setIs_prepaid(true);
		mapping.setPrefix("prefixTest");
		mapping.setPrimary_msisdn_key("primary_msisdn_keyTest");
		mapping.setVimsi("vimsiTest");
		mapping.setVirtual_msisdn("virtual_msisdnTest");
		hibernate.addNew(mapping);
		assertEquals("Wrong value during insertion", "prefixTest",
				mapping.getPrefix());
	}

	@Test
	public void testUpdate() {
		SubscriberInfo mapping = new SubscriberInfo();
		mapping.setId(1);
		mapping.setCharge_msisdn("chargeTest1");
		mapping.setDisplay_msisdn("displaTest1");
		mapping.setIs_active(true);
		mapping.setIs_prepaid(true);
		mapping.setPrefix("preTest1");
		mapping.setPrimary_msisdn_key("primaryTest1");
		mapping.setVimsi("vimsiTest1");
		mapping.setVirtual_msisdn("virtTest1");
		hibernate.update(mapping);
		SubscriberInfo imsimap = hibernate.findById(mapping.getId());
		assertEquals(mapping.getId(), imsimap.getId());
	}

	@Test
	public void testFindById() {
		Integer id = 5;
		SubscriberInfo mapping = new SubscriberInfo();
		mapping.setId(id);
		SubscriberInfo imsimap = hibernate.findById(mapping.getId());
		//assertEquals(mapping.getId(), imsimap.getId());
	}

	@Test
	public void testFindAll() {
		SubscriberInfo mapping = new SubscriberInfo();
		List<SubscriberInfo> imap = hibernate.findAll();

		assertTrue(!imap.isEmpty());
	}

	@Test
	public void testDelete() {
		SubscriberInfo mapping = new SubscriberInfo();
		mapping.setId(1);
		hibernate.delete(mapping.getId());
	}
	

	/*//@Test
	public void testDeleteAll() {
		PrimaryMsisdn mapping=new PrimaryMsisdn();
		//t.setId(1);
			hibernate.delete(t);
	}
	*/
}

package com.movirtu.vsp.persistence.dao.spring;

import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.Rollback;

import com.movirtu.vsp.persistence.domain.Language;
import com.movirtu.vsp.persistence.domain.Subscriber;
import com.movirtu.vsp.persistence.exceptions.DaoException;
import com.movirtu.vsp.persistence.exceptions.DuplicateLanguageException;
import com.movirtu.vsp.persistence.exceptions.DuplicateRecordException;
import com.movirtu.vsp.persistence.exceptions.NoLanguageSetException;
import com.movirtu.vsp.persistence.exceptions.RecordNotFoundException;

public class SubscriberDAOImplTest extends AbstractDAOTests {

	protected final static Logger log = Logger.getLogger(SubscriberDAOImplTest.class);
	
	@Autowired
	private SubscriberDAO subscriberDAO;
	@Autowired
	private LanguageDAO languageDAO;

	/**
	 * Test of insert method, of class SubscriberDAO.
	 * @throws RecordNotFoundException 
	 * @throws NoLanguageSetException 
	 */
	@Rollback
	//@Test(expected = DuplicateRecordException.class)
	public void testInsertExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException, NoLanguageSetException {
		
		long id = add();
		Subscriber subscriber = subscriberDAO.findById(id);
		subscriberDAO.insert(subscriber);
	}

	/**
	 * Test of insert method, of class SubscriberDAO.
	 * @throws NoLanguageSetException 
	 */
	@Rollback
	//@Test
	public void testInsertNew() throws DuplicateRecordException, DaoException, RecordNotFoundException, NoLanguageSetException {
		
		Subscriber subscriber = new Subscriber();
		Language language =new Language("enAB", "English-South Africa");
		languageDAO.insert(language);
		subscriber.setName("abc");
		subscriber.setId(1);
		subscriber.setActualMSISDN("abc");
		subscriber.setPin("abc");
		subscriber.setImsi("000001000010001");
		subscriber.setLanguage(language.getCode());
		int initialCount = recordCount();
		subscriberDAO.insert(subscriber);
		int finalCount = recordCount();
		
		assertTrue(finalCount > initialCount);
	}

	/**
	 * Test of update method, of class SubscriberDAO.
	 * @throws RecordNotFoundException 
	 */
	@Rollback
	//@Test
	public void testUpdateExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		Subscriber subscriber = subscriberDAO.findById(add());
		String oldName = subscriber.getName();
		subscriber.setName("newname");
		subscriberDAO.update(subscriber);
		subscriber = subscriberDAO.findById(subscriber.getId());
		assertNotSame("Updated Subscriber should be different", oldName, subscriber.getName());
	}

	/**
	 * Test of update method, of class SubscriberDAO.
	 * @throws RecordNotFoundException 
	 */
	@Rollback
	//@Test(expected = RecordNotFoundException.class)
	public void testUpdateNonExistingSubscriber() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		removeAll();
		Subscriber subscriber = new Subscriber();
		Language language =new Language("enAB", "English-South Africa");
		languageDAO.insert(language);
		subscriber.setName("abc");
		subscriber.setId(2);
		subscriber.setActualMSISDN("abc");
		subscriber.setPin("abc");
		subscriber.setImsi("111111111111111");
		subscriber.setLanguage(language.getCode());

		subscriberDAO.update(subscriber);
		assertTrue("Update does not fail if data does not exist", true);
	}

	//@Test
	public void testDeleteExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		Subscriber subscriber = subscriberDAO.findById(add());
		int initialCount = recordCount();
		subscriberDAO.delete(subscriber);
		int finalCount = recordCount();
		assertTrue(finalCount < initialCount);

	}

	//@Test(expected = RecordNotFoundException.class)
	public void testDeleteNonExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		Subscriber subscriber = new Subscriber();
		subscriber.setId(-1);
		subscriberDAO.delete(subscriber);
		
		assertTrue("Should throw RecordNotFoundException", true);

	}

	//@Test
	public void testFindSubscriberByIdExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		long id = add();

		Subscriber subscriber = subscriberDAO.findById(id);
		assertTrue(subscriber != null);
	}

	//@Test(expected = RecordNotFoundException.class)
	public void testFindSubscriberByIdNonExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		
		subscriberDAO.findById(-1);
	}

	//@Test
	public void testFindAll() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		add();
		List<Subscriber> subscribers = subscriberDAO.findAll();
		assertTrue(!subscribers.isEmpty());
	}

	//@Test
	public void testFindAllButNoData() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		
		try{
			removeAll();
		} catch (DataIntegrityViolationException e){
			assertTrue("It means we could not remove all data", hasAtLeastOneRecord());
			return;
		}
		try{
			List<Subscriber> subscribers = subscriberDAO.findAll();
			assertTrue(subscribers.isEmpty());
		} catch(RecordNotFoundException e){
			assertTrue("Data was removed and therefore, could find record", true);
		}
		
	}
	
	private void removeAll(){
		cleanUp();
	}
	
	private boolean hasAtLeastOneRecord(){
		return hasAtLeastOneRecord("subscriber");
	}

	@Override
	protected long add() {
		cleanUp();
		Subscriber subscriber = new Subscriber();
		Language language =new Language("enAB", "English-South Africa");
		try {
			languageDAO.insert(language);
		} catch (DuplicateLanguageException e) {
			e.printStackTrace();
		} 
		subscriber.setName("Sipho");
		subscriber.setId(2);
		subscriber.setActualMSISDN("0720000000");
		subscriber.setPin("secrete");
		subscriber.setImsi("111111111111111");
		subscriber.setLanguage(language.getCode());
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("actual_msisdn", subscriber.getActualMSISDN());
		map.put("subscr_name", subscriber.getName());
		map.put("pin", subscriber.getPin());
		map.put("lang_code", subscriber.getLanguage());
		map.put("imsi", subscriber.getImsi());
		simpleJdbcInsert.setTableName("subscriber");
		simpleJdbcInsert.setGeneratedKeyName("id");
		Number id = simpleJdbcInsert.executeAndReturnKey(map);
		return id.longValue();
	}

	private int recordCount(){
		String sql = "select count(1) from subscriber";
		return simpleJdbcTemplate.getJdbcOperations().queryForInt(sql);
	}
	
}

package com.movirtu.vsp.persistence.dao.spring;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.annotation.Timed;

import com.movirtu.vsp.persistence.domain.Call;
import com.movirtu.vsp.persistence.domain.IMSI;
import com.movirtu.vsp.persistence.domain.TemporalCallTransaction;
import com.movirtu.vsp.persistence.exceptions.DaoException;
import com.movirtu.vsp.persistence.exceptions.DuplicateRecordException;
import com.movirtu.vsp.persistence.exceptions.RecordNotFoundException;

public class TemporalCallTransactionDAOImplTest extends AbstractDAOTests {

	@Autowired
	private TemporalCallTransactionDAO temporalCallTransactionDAO;
	@Autowired
	private CallDAO callDAO;
	@Autowired
	private IMSIDAO imsidao;

	/**
	 * Test of insert method, of class TemporalCallTransactionDAO.
	 */
	@Rollback
	////@Test(expected = DuplicateRecordException.class)
	@Timed(millis = 200)
	public void testInsertExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		TemporalCallTransaction temporalCallTransaction = new TemporalCallTransaction();
		temporalCallTransaction.setId(1);
		temporalCallTransaction.setTransactionId(2);
		temporalCallTransaction.setTransactionType("abc");

		simpleJdbcTemplate
				.update("insert into temp_call_trans(trans_id,trans_purpose,imsi,call_id) values(?,?,?,?)",
						new Object[] {
								temporalCallTransaction.getTransactionId(),
								temporalCallTransaction.getTransactionType(),
								temporalCallTransaction.getImsi(),
								temporalCallTransaction.getCall() });
		temporalCallTransactionDAO.insert(temporalCallTransaction);
	}

	/**
	 * Test of insert method, of class TemporalCallTransactionDAO.
	 */
	@Rollback
	@Timed(millis = 100)
	////@Test
	public void testInsertNew() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		simpleJdbcTemplate.update("delete from temp_call_trans where id = ?",
				new Object[] { 1L });
		TemporalCallTransaction temporalCallTransaction = new TemporalCallTransaction();
		temporalCallTransaction.setId(1);
		temporalCallTransaction.setTransactionId(2);
		temporalCallTransaction.setTransactionType("abc");

		simpleJdbcTemplate
				.update("insert into temp_call_trans(id,trans_id,trans_purpose,imsi,call_id) values(?,?,?,?,?)",
						new Object[] { temporalCallTransaction.getId(),
								temporalCallTransaction.getTransactionId(),
								temporalCallTransaction.getTransactionType(),
								temporalCallTransaction.getImsi(),
								temporalCallTransaction.getCall() });
		temporalCallTransactionDAO.insert(temporalCallTransaction);
		TemporalCallTransaction temporalCallTransaction1 = simpleJdbcTemplate
				.queryForObject(
						"select id,trans_id,trans_purpose,imsi,call_id from temp_call_trans where id = ?",
						new TemporalCallTransactionRowMapper(), 1L);

		assertEquals(1L, temporalCallTransaction1.getId());
	}

	/**
	 * Test of update method, of class TemporalCallTransactionDAO.
	 */
	@Rollback
	@Timed(millis = 200)
	////@Test
	public void testUpdateExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		TemporalCallTransaction temporalCallTransaction = new TemporalCallTransaction();
		temporalCallTransaction.setId(1);
		temporalCallTransaction.setTransactionId(2);
		temporalCallTransaction.setTransactionType("abc");

		simpleJdbcTemplate
				.update("insert into temp_call_trans(id,trans_id,trans_purpose,imsi,call_id) values(?,?,?,?,?)",
						new Object[] { temporalCallTransaction.getId(),
								temporalCallTransaction.getTransactionId(),
								temporalCallTransaction.getTransactionType(),
								temporalCallTransaction.getImsi(),
								temporalCallTransaction.getCall() });
		temporalCallTransactionDAO.update(temporalCallTransaction);
		assertNotSame("Updated TemporalCallTransaction should be different",
				1L, temporalCallTransaction.getId());
	}

	/**
	 * Test of update method, of class TemporalCallTransactionDAO.
	 */
	@Rollback
	@Timed(millis = 200)
	////@Test
	public void testUpdateNonExistingTemporalCallTransaction() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		TemporalCallTransaction temporalCallTransaction = new TemporalCallTransaction();
		temporalCallTransaction.setId(1);
		temporalCallTransaction.setTransactionId(3);
		temporalCallTransaction.setTransactionType("abc");

		temporalCallTransactionDAO.update(temporalCallTransaction);
		assertTrue("Update does not fail if data does not exist", true);
	}

	@Rollback
	@Timed(millis = 200)
	////@Test(expected = RecordNotFoundException.class)
	public void testDeleteExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		TemporalCallTransaction temporalCallTransaction = new TemporalCallTransaction();
		temporalCallTransaction.setId(1);
		temporalCallTransaction.setTransactionId(3);
		temporalCallTransaction.setTransactionType("abc");

		simpleJdbcTemplate
				.update("insert into temp_call_trans(id,trans_id,trans_purpose,imsi,call_id) values(?,?,?,?,?)",
						new Object[] { temporalCallTransaction.getId(),
								temporalCallTransaction.getTransactionId(),
								temporalCallTransaction.getTransactionType(),
								temporalCallTransaction.getImsi(),
								temporalCallTransaction.getCall() });
		temporalCallTransactionDAO.delete(temporalCallTransaction);
		simpleJdbcTemplate
				.queryForObject(
						"select id,trans_id,trans_purpose,imsi,call_id from temp_call_trans where id = ?",
						new TemporalCallTransactionRowMapper(), 1L);

	}

	@Rollback
	@Timed(millis = 200)
	////@Test(expected = RecordNotFoundException.class)
	public void testDeleteNonExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		TemporalCallTransaction temporalCallTransaction = new TemporalCallTransaction();
		temporalCallTransaction.setId(1);
		temporalCallTransaction.setTransactionId(3);
		temporalCallTransaction.setTransactionType("abc");

		temporalCallTransactionDAO.delete(temporalCallTransaction);
		simpleJdbcTemplate
				.queryForObject(
						"select id,trans_id,trans_purpose,imsi,call_id from temp_call_trans where id = ?",
						new TemporalCallTransactionRowMapper(), 1L);
		assertTrue("Should throw RecordNotFoundException", true);

	}

	////@Test
	@Timed(millis = 200)
	@Rollback
	public void testFindTemporalCallTransactionByIdExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		TemporalCallTransaction temporalCallTransaction = new TemporalCallTransaction();
		temporalCallTransaction.setId(1);
		temporalCallTransaction.setTransactionId(3);
		temporalCallTransaction.setTransactionType("abc");

		simpleJdbcTemplate
				.update("insert into temp_call_trans(id,trans_id,trans_purpose,imsi,call_id) values(?,?,?,?,?)",
						new Object[] { temporalCallTransaction.getId(),
								temporalCallTransaction.getTransactionId(),
								temporalCallTransaction.getTransactionType(),
								temporalCallTransaction.getImsi(),
								temporalCallTransaction.getCall() });
		TemporalCallTransaction temporalCallTransaction1 = temporalCallTransactionDAO
				.findById(1L);
		assertEquals(1L, temporalCallTransaction1.getId());
	}

	////@Test(expected = RecordNotFoundException.class)
	@Timed(millis = 200)
	public void testFindTemporalCallTransactionByIdNonExisting() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		simpleJdbcTemplate.update("delete from temp_call_trans where id = ?",
				new Object[] { 1L });
		temporalCallTransactionDAO.findById(1L);
	}

	////@Test
	@Timed(millis = 200)
	@Rollback
	public void testFindAll() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		TemporalCallTransaction temporalCallTransaction = new TemporalCallTransaction();
		temporalCallTransaction.setId(1);
		temporalCallTransaction.setTransactionId(3);
		temporalCallTransaction.setTransactionType("abc");

		simpleJdbcTemplate
				.update("insert into temp_call_trans(trans_id,trans_purpose,imsi,call_id) values(?,?,?,?)",
						new Object[] {
								temporalCallTransaction.getTransactionId(),
								temporalCallTransaction.getTransactionType(),
								temporalCallTransaction.getImsi(),
								temporalCallTransaction.getCall() });
		List<TemporalCallTransaction> temporalCallTransactions = temporalCallTransactionDAO
				.findAll();
		assertTrue(!temporalCallTransactions.isEmpty());
	}

	////@Test
	@Timed(millis = 200)
	@Rollback
	public void testFindAllButNoData() throws DuplicateRecordException, DaoException, RecordNotFoundException {
		simpleJdbcTemplate.update("delete from temp_call_trans");
		List<TemporalCallTransaction> temporalCallTransactions = temporalCallTransactionDAO
				.findAll();
		assertTrue(temporalCallTransactions.isEmpty());
	}

	private class TemporalCallTransactionRowMapper implements RowMapper<TemporalCallTransaction>{
		public TemporalCallTransaction mapRow(ResultSet rs, int rows) throws SQLException {
			TemporalCallTransaction temporalCallTransaction = new TemporalCallTransaction();
			IMSI imsi = null;
			try {
				imsi = imsidao.findById(rs.getLong(4));
			} catch (RecordNotFoundException e1) {
				e1.printStackTrace();
			}
			Call call = null;
			try {
				call = callDAO.findById(rs.getLong(5));
			} catch (RecordNotFoundException e) {
				e.printStackTrace();
			}
			temporalCallTransaction.setId(rs.getLong(1));
			temporalCallTransaction.setTransactionId(rs.getLong(2));
			temporalCallTransaction.setTransactionType(rs.getString(3));
			temporalCallTransaction.setImsi(imsi);
			temporalCallTransaction.setCall(call);

			return temporalCallTransaction;
		}
	}
	
	@Override
	protected long add() {
		cleanUp();
		return 0;
	}
	
	//@Test
	public void dummyTest(){
		
	}
}

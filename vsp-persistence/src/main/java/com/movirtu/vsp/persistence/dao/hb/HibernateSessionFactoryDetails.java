package com.movirtu.vsp.persistence.dao.hb;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * Class is designed for providing annotation based session factory
 * HibernateDaoImpl class is using this class for creating session factory and
 * getting Hibernate session instance.
 * 
 * @since ManyMe Mobile
 */
public class HibernateSessionFactoryDetails {
	private final static Logger logger = Logger
			.getLogger(BaseHibernateImpl.class);

	// Annotation based configuration
	private static SessionFactory sessionAnnotationFactory;

	/*
	 * Method is used for building annotation base session factory
	 * 
	 * @return SessionFactory
	 */
	private static SessionFactory buildSessionAnnotationFactory() {
		try {
			// Create the SessionFactory from hibernate.cfg.xml
			Configuration configuration = new Configuration();
			configuration.configure("hibernate-annotation.cfg.xml");
			logger.info("Hibernate Annotation Configuration loaded");

			ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
					.applySettings(configuration.getProperties()).build();
			logger.info("Hibernate Annotation serviceRegistry created");

			SessionFactory sessionFactory = configuration
					.buildSessionFactory(serviceRegistry);

			return sessionFactory;
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			logger.error("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	/*
	 * Method is used for getting annotation base session factory using
	 * buildSessionAnnotationFactory method
	 * 
	 * @return SessionFactory
	 */
	public static SessionFactory getSessionAnnotationFactory() {
		if (sessionAnnotationFactory == null)
			sessionAnnotationFactory = buildSessionAnnotationFactory();
		return sessionAnnotationFactory;
	}
}

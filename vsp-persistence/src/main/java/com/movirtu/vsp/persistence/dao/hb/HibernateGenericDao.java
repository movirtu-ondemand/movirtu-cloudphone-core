package com.movirtu.vsp.persistence.dao.hb;

import java.io.Serializable;
import java.util.List;

import com.movirtu.vsp.persistence.exceptions.HibernateException;

/**
 * Class is designed for providing abstraction for HibernateDaoImp 
 * which will implements all the methods available in this interface commonly used by
 * all the hibernate dao classess   
 * 
 *@since ManyMe
 */
public interface HibernateGenericDao {
	 public<T> void  addNew(T t) throws HibernateException;
	 <T> void update(T t)  throws HibernateException;
	 <T, PK extends Serializable> T findById(Class<T> type, PK id)  throws HibernateException;
	 <T> List<T> findAll(Class<T> type)  throws HibernateException;
	 <T> void delete(T t) throws HibernateException;
	 <T, PK extends Serializable> void delete(Class<T> type, PK id)  throws HibernateException;
	 <T, PK extends Serializable> List<T> findByCriteria(Class<T> type,String columnName, PK id)  throws HibernateException;
}

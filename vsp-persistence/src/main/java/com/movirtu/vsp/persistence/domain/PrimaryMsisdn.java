package com.movirtu.vsp.persistence.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="primary_msisdn")
public class PrimaryMsisdn implements Serializable{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7138025297041197581L;
	
	@Id
	@SequenceGenerator(name = "primary_msisdn_id_seq", sequenceName = "primary_msisdn_id_seq", allocationSize=1 )
	@GeneratedValue(generator = "primary_msisdn_id_seq", strategy=GenerationType.SEQUENCE)
	private Integer id;
	
	@Column(name="primary_msisdn_key",nullable=false)
	private String primary_msisdn_key;
	
	@Column(name="prefix",nullable=false)
	private int prefix;
	
	@Column(name="virtual_msisdn",nullable=false)
	private String virtual_msisdn;
	
	@Column(name="display_msisdn",nullable=false)
	private String display_msisdn;
	
	@Column(name="charge_msisdn",nullable=false)
	private String charge_msisdn;
	
	@Column(name="vimsi")
	private String vimsi;
	
	@Column(name="is_active")
	private boolean is_active;
	
	@Column(name="is_prepaid")
	private boolean is_prepaid;


	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}


	/**
	 * @return the primary_msisdn_key
	 */
	public String getPrimary_msisdn_key() {
		return primary_msisdn_key;
	}


	/**
	 * @param primary_msisdn_key the primary_msisdn_key to set
	 */
	public void setPrimary_msisdn_key(String primary_msisdn_key) {
		this.primary_msisdn_key = primary_msisdn_key;
	}


	/**
	 * @return the prefix
	 */
	public int getPrefix() {
		return prefix;
	}


	/**
	 * @param prefix the prefix to set
	 */
	public void setPrefix(int prefix) {
		this.prefix = prefix;
	}


	/**
	 * @return the virtual_msisdn
	 */
	public String getVirtual_msisdn() {
		return virtual_msisdn;
	}


	/**
	 * @param virtual_msisdn the virtual_msisdn to set
	 */
	public void setVirtual_msisdn(String virtual_msisdn) {
		this.virtual_msisdn = virtual_msisdn;
	}


	/**
	 * @return the display_msisdn
	 */
	public String getDisplay_msisdn() {
		return display_msisdn;
	}


	/**
	 * @param display_msisdn the display_msisdn to set
	 */
	public void setDisplay_msisdn(String display_msisdn) {
		this.display_msisdn = display_msisdn;
	}


	/**
	 * @return the charge_msisdn
	 */
	public String getCharge_msisdn() {
		return charge_msisdn;
	}


	/**
	 * @param charge_msisdn the charge_msisdn to set
	 */
	public void setCharge_msisdn(String charge_msisdn) {
		this.charge_msisdn = charge_msisdn;
	}


	/**
	 * @return the vimsi
	 */
	public String getVimsi() {
		return vimsi;
	}


	/**
	 * @param vimsi the vimsi to set
	 */
	public void setVimsi(String vimsi) {
		this.vimsi = vimsi;
	}


	/**
	 * @return the is_active
	 */
	public boolean isIs_active() {
		return is_active;
	}


	/**
	 * @param is_active the is_active to set
	 */
	public void setIs_active(boolean is_active) {
		this.is_active = is_active;
	}


	/**
	 * @return the is_prepaid
	 */
	public boolean isIs_prepaid() {
		return is_prepaid;
	}


	/**
	 * @param is_prepaid the is_prepaid to set
	 */
	public void setIs_prepaid(boolean is_prepaid) {
		this.is_prepaid = is_prepaid;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 83 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final PrimaryMsisdn other = (PrimaryMsisdn) obj;
		if ((this.id == null) ? (other.id != null) : !this.id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "PrimaryMsisdn [id=" + id + ", primary_msisdn_key="
				+ primary_msisdn_key + ", prefix=" + prefix
				+ ", virtual_msisdn=" + virtual_msisdn + ", display_msisdn="
				+ display_msisdn + ", charge_msisdn=" + charge_msisdn
				+ ", vimsi=" + vimsi + ", is_active=" + is_active
				+ ", is_prepaid=" + is_prepaid + "]";
	}
	
}

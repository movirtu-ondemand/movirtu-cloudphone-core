package com.movirtu.vsp.persistence.dao.hb.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import com.movirtu.vsp.persistence.dao.hb.BaseHibernateImpl;
import com.movirtu.vsp.persistence.dao.hb.ImsiMsrnMappingDao;
import com.movirtu.vsp.persistence.domain.ImsiMsrnMapping;
import com.movirtu.vsp.persistence.exceptions.RecordNotFoundException;
@Component
public class ImsiMsrnMappingDaoImpl extends BaseHibernateImpl implements
		ImsiMsrnMappingDao {

	private final static Logger logger = Logger
			.getLogger(ImsiMsrnMappingDaoImpl.class);

	@Override
	public ImsiMsrnMapping getImsiMsrnMappingById(Integer id) {
		return hibernate.findById(ImsiMsrnMapping.class, id);
	}

	@Override
	public List<ImsiMsrnMapping> getAllImsiMsrn()
			throws RecordNotFoundException {

		List<ImsiMsrnMapping> imsiMsrn = hibernate
				.findAll(ImsiMsrnMapping.class);
		return imsiMsrn;
	}

	@Override
	public void addNew(ImsiMsrnMapping t) {
		hibernate.addNew(t);

	}

	@Override
	public void update(ImsiMsrnMapping t) {
		hibernate.update(t);
	}

	@Override
	public ImsiMsrnMapping findById(Integer id) {
		return hibernate.findById(ImsiMsrnMapping.class, id);
	}

	@Override
	public List<ImsiMsrnMapping> findAll() {
		return hibernate.findAll(ImsiMsrnMapping.class);
	}

	@Override
	public void delete(ImsiMsrnMapping t) {
		hibernate.delete(t);
	}

	@Override
	public void delete(Integer id) {
		hibernate.delete(ImsiMsrnMapping.class, id);
	}

	@Override
	public ImsiMsrnMapping findByMsrn(String msrn) throws RecordNotFoundException {
		List<ImsiMsrnMapping> number = hibernate.findByCriteria(ImsiMsrnMapping.class, "msrn", msrn);
		
		if(number.size()==0) {
			return null;
		}
		
		return number.get(0);
	}

	@Override
	public ImsiMsrnMapping findByImsi(String imsi) throws RecordNotFoundException {
		List<ImsiMsrnMapping> number = hibernate.findByCriteria(ImsiMsrnMapping.class, "imsi", imsi);
		
		if(number.size()==0) {
			return null;
		}
		
		return number.get(0);
	}

}

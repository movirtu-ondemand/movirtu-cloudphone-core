package com.movirtu.vsp.persistence.exceptions;

/**
 * Used for any SQLException
 *
 * @author mlungisi
 * @since 1.0
 */
public class DaoException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 5255352918974633719L;

    /**
     * @see Exception#Exception()
     */
    public DaoException() {
    }

    /**
     * @see Exception#Exception(String)
     */
    public DaoException(String message) {
        super(message);
    }

    /**
     * @see Exception#Exception(String, Throwable)
     */
    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }
}

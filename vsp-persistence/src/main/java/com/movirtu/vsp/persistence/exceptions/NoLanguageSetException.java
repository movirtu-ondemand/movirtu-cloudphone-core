package com.movirtu.vsp.persistence.exceptions;

public class NoLanguageSetException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 3734918667114070997L;

    public NoLanguageSetException() {
        super();
    }

    public NoLanguageSetException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public NoLanguageSetException(String arg0) {
        super(arg0);
    }

    public NoLanguageSetException(Throwable arg0) {
        super(arg0);
    }
}

package com.movirtu.vsp.persistence.exceptions;

public class HibernateException extends RuntimeException {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 3922063060436550223L;

	public HibernateException() {
	}

	public HibernateException(String msg) {
		super(msg);
	}

	public HibernateException(Throwable t) {
		super(t);
	}

	public HibernateException(String msg, Throwable t) {
		super(msg, t);
	}

}

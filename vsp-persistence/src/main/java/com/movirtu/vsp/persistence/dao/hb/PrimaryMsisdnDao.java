package com.movirtu.vsp.persistence.dao.hb;

import java.util.List;

import com.movirtu.vsp.persistence.dao.GenericDao;
import com.movirtu.vsp.persistence.domain.PrimaryMsisdn;
import com.movirtu.vsp.persistence.exceptions.RecordNotFoundException;

public interface PrimaryMsisdnDao extends GenericDao<PrimaryMsisdn, Integer> {
	
	PrimaryMsisdn getPrimaryMsisdnById(Integer id) throws RecordNotFoundException;
	
	List<PrimaryMsisdn> getAllPrimaryMsisdn() throws RecordNotFoundException;
	
	List<PrimaryMsisdn> findByPrimaryMsisdn(String primaryMsisdn) throws RecordNotFoundException;
	
	PrimaryMsisdn findByVirtualMsisdn(String virtualMsisdn) throws RecordNotFoundException;

	PrimaryMsisdn findByImsi(String imsi) throws RecordNotFoundException;
	
	PrimaryMsisdn findByPrimaryMsisdnAndPrefix(String primaryMsisdn, int prefix) throws RecordNotFoundException;
	
}

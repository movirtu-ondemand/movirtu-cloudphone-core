package com.movirtu.vsp.persistence.dao.hb.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import com.movirtu.vsp.persistence.dao.hb.BaseHibernateImpl;
import com.movirtu.vsp.persistence.dao.hb.VirtualNumberPoolDao;
import com.movirtu.vsp.persistence.domain.VirtualNumberPool;
import com.movirtu.vsp.persistence.exceptions.RecordNotFoundException;

/**
 * @author Anant.Bhatnagar
 *
 */
 @Component
public class VirtualNumberPoolDaoImpl extends BaseHibernateImpl implements VirtualNumberPoolDao {

	private final static Logger logger = Logger.getLogger(VirtualNumberPoolDaoImpl.class);

	@Override
	public void addNew(VirtualNumberPool t) {
		hibernate.addNew(t);

	}

	@Override
	public void update(VirtualNumberPool t) {
		hibernate.update(t);

	}

	@Override
	public VirtualNumberPool findById(Integer id) {
		return hibernate.findById(VirtualNumberPool.class, id);
	}

	@Override
	public List<VirtualNumberPool> findAll() {
		return hibernate.findAll(VirtualNumberPool.class);
	}

	@Override
	public void delete(VirtualNumberPool t) {
		hibernate.delete(t);

	}

	@Override
	public void delete(Integer id) {
		hibernate.delete(VirtualNumberPool.class, id);

	}

	@Override
	public VirtualNumberPool getPrimaryMsisdnById(Integer id)
			throws RecordNotFoundException {
		return hibernate.findById(VirtualNumberPool.class, id);
	}

	@Override
	public List<VirtualNumberPool> getAllVirtualNumberPool()
			throws RecordNotFoundException {
		return hibernate.findAll(VirtualNumberPool.class);
	}

	@Override
	public List<VirtualNumberPool> findByStatus(int status)
			throws RecordNotFoundException {
		return hibernate.findByCriteria(VirtualNumberPool.class, "status", status);

	}

	@Override
	public List<VirtualNumberPool> findByStatus(int status, int length, boolean random)
			throws RecordNotFoundException {
		if(random) {

			return hibernate.findRandomlyByCriteria(VirtualNumberPool.class, "status", status, length);
		} else {
			return hibernate.findByCriteria(VirtualNumberPool.class, "status", status, length);
		}

	}

	@Override
	public VirtualNumberPool findByImsi(String imsi)
			throws RecordNotFoundException {
		List<VirtualNumberPool> number = hibernate.findByCriteria(VirtualNumberPool.class, "imsi", imsi);
		return number.get(0);
	}

	@Override
	public VirtualNumberPool findByMsisdn(String msisdn)
			throws RecordNotFoundException {
		List<VirtualNumberPool> number = hibernate.findByCriteria(VirtualNumberPool.class, "msisdn", msisdn);
		return number.get(0);
	}

}

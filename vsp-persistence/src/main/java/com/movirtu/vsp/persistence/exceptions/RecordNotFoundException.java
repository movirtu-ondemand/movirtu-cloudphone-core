package com.movirtu.vsp.persistence.exceptions;

/**
 *
 * In case no records found from database
 *
 * @author mlungisi
 * @since 1.0
 *
 */
public class RecordNotFoundException extends Exception {

    /**
     * @see Exception#Exception(String)
     */
    public RecordNotFoundException(String message) {
        super(message);
    }

    /**
     * @see Exception#Exception()
     */
    public RecordNotFoundException() {
        super();
    }

    /**
     * @see Exception#Exception(String, Throwable)
     */
    public RecordNotFoundException(String message, Throwable t) {
        super(message, t);
    }
    /**
     *
     */
    private static final long serialVersionUID = -2900954900528567619L;
}

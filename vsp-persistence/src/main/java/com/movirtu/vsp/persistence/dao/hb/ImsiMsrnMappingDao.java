package com.movirtu.vsp.persistence.dao.hb;

import java.util.List;

import com.movirtu.vsp.persistence.dao.GenericDao;
import com.movirtu.vsp.persistence.domain.ImsiMsrnMapping;
import com.movirtu.vsp.persistence.exceptions.RecordNotFoundException;

public interface ImsiMsrnMappingDao extends GenericDao<ImsiMsrnMapping, Integer> {

	ImsiMsrnMapping getImsiMsrnMappingById(Integer id) throws RecordNotFoundException;

	List<ImsiMsrnMapping> getAllImsiMsrn() throws RecordNotFoundException;

	ImsiMsrnMapping findByMsrn(String msrn) throws RecordNotFoundException;

	ImsiMsrnMapping findByImsi(String imsi) throws RecordNotFoundException;

}

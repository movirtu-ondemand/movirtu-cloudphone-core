package com.movirtu.vsp.persistence.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "virtual_number_pool")
public class VirtualNumberPool implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 8109441193061702012L;

	@SequenceGenerator(name = "virtual_number_pool_id_seq", sequenceName = "virtual_number_pool_id_seq", allocationSize = 1)
	@GeneratedValue(generator = "virtual_number_pool_id_seq", strategy = GenerationType.SEQUENCE)
	@Id
	private Integer id;

	@Column(name = "msisdn", nullable = false)
	private String msisdn;

	@Column(name = "imsi", nullable = false)
	private String imsi;

	@Column(name = "status", nullable = false)
	private int status;
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	/**
	 * @return the imsi
	 */
	public String getImsi() {
		return imsi;
	}

	/**
	 * @param imsi
	 *            the imsi to set
	 */
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VirtualNumberPool other = (VirtualNumberPool) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ImsiMsrnMapping [id=" + id + ", msisdn=" + msisdn + ", imsi="
				+ imsi + "]";
	}

}

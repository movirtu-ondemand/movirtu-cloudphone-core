package com.movirtu.vsp.persistence.dao.hb.impl;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.movirtu.vsp.persistence.dao.hb.HibernateGenericDao;
import com.movirtu.vsp.persistence.dao.hb.HibernateSessionFactoryDetails;
import com.movirtu.vsp.persistence.exceptions.HibernateException;

/**
 * Class is designed  to provide methods for generic CRUD operations
 * 
 * @since ManyMe Mobile
 */
public class HibernateDaoImpl implements HibernateGenericDao {

	/**/
	private static Logger logger = Logger.getLogger(HibernateDaoImpl.class);
	/**/
	protected Session session;
	//SettingsDao setting = new SettingsDaoImpl();

	/**
	 * This method returns new session 
	 * @return Session 
	 */
	private Session openSession(){
		return HibernateSessionFactoryDetails.getSessionAnnotationFactory().openSession();
	}

	/**
	 * adds a new entity to the database
	 * @param generic object type
	 */
	public<T> void addNew(T t) throws HibernateException {
		logger.info("Entering Hibernate addNew ");
		Transaction tx = null;
		try {
			session = openSession();
			tx = session.beginTransaction();
			session.save(t);
			tx.commit();
			session.close();
		} catch (HibernateException ex) {
			logger.error("update sql {} with params {}, failed" +ex);
			tx.rollback();
			session.close();
			throw ex;
		} finally {
		}
	}

	/**
	 * updates an existing entity to the database
	 * @param generic object type
	 */
	public<T> void update(T t) throws HibernateException{
		logger.info("Entering Hibernate update ");
		try {
			session = openSession();
			Transaction tx = session.beginTransaction();
			session.saveOrUpdate(t);
			tx.commit();
			session.close();
		} catch (HibernateException ex) {
			logger.error("update sql {} with params {}, failed" +ex);
			session.close();
			throw ex;
		} finally {
		}
	}

	/**
	 * finds an entity from the database based on id
	 * @param generic object type
	 * @param serializable id
	 * @return entity
	 */
	public<T, PK extends Serializable> T findById(Class<T> type, PK id) throws HibernateException{
		logger.info("Entering Hibernate findById ");
		try {
			logger.info("Opening session");
			session= openSession(); 
			logger.info("session="+session);
			T t = (T) session.get(type, id);
			logger.info("Closing session");
			session.close();
			logger.info("Closed session");
			return t;
		} catch (HibernateException ex) {
			logger.error("findById failed" ,ex);
			session.close();
			throw ex;
		} finally {
			logger.error("In finally:: findById failed");
		}
	}

	/**
	 * find all entities from the database
	 * @param generic object type
	 * @return List of entities
	 */
	public <T> List<T> findAll(final Class<T> type) throws HibernateException {
		logger.info("Entering Hibernate findAll ");
		try {
			session = openSession();
			final Criteria crit = session.createCriteria(type);
			List<T> list = crit.list();
			session.close();
			return list;
		} catch (HibernateException ex) {
			logger.error("update sql {} with params {}, failed" + ex);
			session.close();
			throw ex;
		} finally {
		}
	}

	/**
	 * deletes the provided entity from the database
	 * @param generic object type
	 */
	public <T> void delete(T t) throws HibernateException {
		logger.info("Entering Hibernate delete ");
		try {
			session = openSession();
			Transaction tx = session.beginTransaction();
			session.delete(t);
			tx.commit();
			session.close();
		} catch (HibernateException ex) {
			logger.error("update sql {} with params {}, failed" + ex);
			session.close();
			throw ex;
		} finally {
		}
	}

	/**
	 * deletes an entity from the database based on the id
	 * @param generic object type
	 * @param serializable id
	 */
	public <T, PK extends Serializable> void delete(Class<T> type, PK id) throws HibernateException {
		T t = findById(type, id);
		delete(t);
	}

	public <T, PK extends Serializable> List<T> findByCriteria(Class<T> type, String columnName, PK id) throws HibernateException {
		// TODO done as this code was throwing "org.hibernate.SessionException: Session is closed!" Exception
		synchronized(this) {
			logger.info("Entering Hibernate findById ");
			try {
				session= openSession();
				Criteria criteria = session.createCriteria(type);
				criteria.add(Restrictions.eq(columnName, id));
				List<T> t =  criteria.list();
				session.close();
				return t;
			} catch (HibernateException ex) {
				logger.error("update sql {} with params {}, failed" +ex);
				session.close();
				throw ex;
			} finally {
			}
		}
	}

	public <T, PK extends Serializable> List<T> findByCriteria(Class<T> type, String columnName, PK id, int length) throws HibernateException {

		logger.info("Entering Hibernate findById ");
		try {
			session= openSession();
			Criteria criteria = session.createCriteria(type);
			criteria.setFirstResult(0);
			criteria.setMaxResults(length);
			criteria.add(Restrictions.eq(columnName, id));
			List<T> t =  criteria.list();
			session.close();
			return t;
		} catch (HibernateException ex) {
			logger.error("update sql {} with params {}, failed" +ex);
			session.close();
			throw ex;
		} finally {
		}

	}

	public <T, PK extends Serializable> List<T> findRandomlyByCriteria(Class<T> type, String columnName, PK id, int length) throws HibernateException {

		logger.info("Entering Hibernate findRandomlyByCriteria ");
		try {
			session= openSession();
			Criteria criteria = session.createCriteria(type);
			criteria.add(Restrictions.eq(columnName, id));
			criteria.add(Restrictions.sqlRestriction("1=1 order by random()"));
			criteria.setMaxResults(length);
			List<T> t =  criteria.list();
			session.close();
			return t;
		} catch (HibernateException ex) {
			logger.error("update sql {} with params {}, failed" +ex);
			session.close();
			throw ex;
		} finally {
		}

	}

	//	public <T> List<T> findByRandomCriteria(Class<T> type, int limit) throws HibernateException {
	//
	//		  logger.info("Entering Hibernate findById ");
	//		  try {
	//		   session= openSession();
	//		   Criteria criteria = session.createCriteria(type);
	//		   criteria.add(Restrictions.sqlRestriction("1=1 order by random()"));
	//		   criteria.setMaxResults(limit);
	//		   List<T> t =  criteria.list();
	//		   session.close();
	//		   return t;
	//		  } catch (HibernateException ex) {
	//		   logger.error("update sql {} with params {}, failed" +ex);
	//		   session.close();
	//		   throw ex;
	//		  } finally {
	//		  }
	//
	//		 }

	@Override
	public String toString() {
		return "Hibernate DataSource: "  ;
	}
}

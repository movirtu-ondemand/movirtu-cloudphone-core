package com.movirtu.vsp.persistence.dao.hb;

import java.util.List;

import com.movirtu.vsp.persistence.dao.GenericDao;
import com.movirtu.vsp.persistence.domain.VirtualNumberPool;
import com.movirtu.vsp.persistence.exceptions.RecordNotFoundException;

public interface VirtualNumberPoolDao extends GenericDao<VirtualNumberPool, Integer> {
	
	VirtualNumberPool getPrimaryMsisdnById(Integer id) throws RecordNotFoundException;
	
	List<VirtualNumberPool> getAllVirtualNumberPool() throws RecordNotFoundException;
	
	List<VirtualNumberPool> findByStatus(int status) throws RecordNotFoundException;
	
	VirtualNumberPool findByImsi(String imsi) throws RecordNotFoundException;
	
	VirtualNumberPool findByMsisdn(String msisdn) throws RecordNotFoundException;

	List<VirtualNumberPool> findByStatus(int status, int length, boolean random)
			throws RecordNotFoundException;
	
}

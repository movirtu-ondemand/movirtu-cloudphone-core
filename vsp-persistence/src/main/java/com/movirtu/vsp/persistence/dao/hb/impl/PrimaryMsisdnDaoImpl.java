package com.movirtu.vsp.persistence.dao.hb.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import com.movirtu.vsp.persistence.dao.hb.BaseHibernateImpl;
import com.movirtu.vsp.persistence.dao.hb.PrimaryMsisdnDao;
import com.movirtu.vsp.persistence.domain.PrimaryMsisdn;
import com.movirtu.vsp.persistence.domain.SubscriberInfo;
import com.movirtu.vsp.persistence.exceptions.RecordNotFoundException;

/**
 * @author Anant.Bhatnagar
 *
 */
 @Component
public class PrimaryMsisdnDaoImpl extends BaseHibernateImpl implements PrimaryMsisdnDao {

	private final static Logger logger = Logger.getLogger(PrimaryMsisdnDaoImpl.class);

	@Override
	public PrimaryMsisdn getPrimaryMsisdnById(Integer id) throws RecordNotFoundException {
		return hibernate.findById(PrimaryMsisdn.class, id);
	}

	@Override
	public List<PrimaryMsisdn> getAllPrimaryMsisdn()
			throws RecordNotFoundException {
		return hibernate.findAll(PrimaryMsisdn.class);
	}

	@Override
	public void addNew(PrimaryMsisdn t) {
		hibernate.addNew(t);
	}

	@Override
	public void update(PrimaryMsisdn t) {
		hibernate.update(t);	
	}

	@Override
	public PrimaryMsisdn findById(Integer id) {
		return hibernate.findById(PrimaryMsisdn.class, id);
	}

	@Override
	public List<PrimaryMsisdn> findAll() {
		return hibernate.findAll(PrimaryMsisdn.class);
	}

	@Override
	public void delete(PrimaryMsisdn t) {
		hibernate.delete(t);

	}

	@Override
	public void delete(Integer id) {
		hibernate.delete(PrimaryMsisdn.class, id);
	}

	@Override
	public List<PrimaryMsisdn> findByPrimaryMsisdn(String primaryMsisdn)
			throws RecordNotFoundException {
		return hibernate.findByCriteria(PrimaryMsisdn.class, "primary_msisdn_key", primaryMsisdn);
	}

	@Override
	public PrimaryMsisdn findByVirtualMsisdn(String virtualMsisdn) throws RecordNotFoundException {
		List<PrimaryMsisdn> list = hibernate.findByCriteria(PrimaryMsisdn.class, "virtual_msisdn", virtualMsisdn);

		if(list.size()==0) {
			return null;
		}

		return list.get(0);
	}

	@Override
	public PrimaryMsisdn findByImsi(String imsi) throws RecordNotFoundException {
		List<PrimaryMsisdn> list = hibernate.findByCriteria(PrimaryMsisdn.class, "vimsi", imsi);

		if(list.size()==0) {
			return null;
		}

		return list.get(0);
	}

	@Override
	public PrimaryMsisdn findByPrimaryMsisdnAndPrefix(String primaryMsisdn,
			int prefix) throws RecordNotFoundException {

		List<PrimaryMsisdn> list = hibernate.findByCriteria(PrimaryMsisdn.class, "primary_msisdn_key", primaryMsisdn);

		if(list.size()==0) {
			return null;
		}

		for(PrimaryMsisdn entry:list) {
			if(entry.getPrefix()==prefix) {
				return entry;
			}
		}

		return null;
	}
}

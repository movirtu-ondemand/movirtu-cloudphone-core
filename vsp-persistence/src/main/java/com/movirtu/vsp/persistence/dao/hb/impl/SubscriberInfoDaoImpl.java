package com.movirtu.vsp.persistence.dao.hb.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import com.movirtu.vsp.persistence.dao.hb.BaseHibernateImpl;
import com.movirtu.vsp.persistence.dao.hb.SubscriberInfoDao;
import com.movirtu.vsp.persistence.domain.SubscriberInfo;
import com.movirtu.vsp.persistence.exceptions.RecordNotFoundException;

/**
 * @author Anant.Bhatnagar
 *
 */
@Component
public class SubscriberInfoDaoImpl extends BaseHibernateImpl implements SubscriberInfoDao {

	private final static Logger logger = Logger.getLogger(SubscriberInfoDaoImpl.class);

	@Override
	public SubscriberInfo getPrimaryMsisdnById(Integer id) throws RecordNotFoundException {
		return hibernate.findById(SubscriberInfo.class, id);
	}

	@Override
	public List<SubscriberInfo> getAllSubscriberProfiles()
			throws RecordNotFoundException {
		return hibernate.findAll(SubscriberInfo.class);
	}

	@Override
	public void addNew(SubscriberInfo t) {
		hibernate.addNew(t);
	}

	@Override
	public void update(SubscriberInfo t) {
		hibernate.update(t);	
	}

	@Override
	public SubscriberInfo findById(Integer id) {
		return hibernate.findById(SubscriberInfo.class, id);
	}

	@Override
	public List<SubscriberInfo> findAll() {
		return hibernate.findAll(SubscriberInfo.class);
	}

	@Override
	public void delete(SubscriberInfo t) {
		hibernate.delete(t);

	}

	@Override
	public void delete(Integer id) {
		hibernate.delete(SubscriberInfo.class, id);
	}

	@Override
	public SubscriberInfo findByPrimaryMsisdn(String msisdn) throws RecordNotFoundException {
		List<SubscriberInfo> list = hibernate.findByCriteria(SubscriberInfo.class, "msisdn", msisdn);

		if(list.size()==0) {
			throw new RecordNotFoundException();
		}

		return list.get(0);
	}

	@Override
	public List<SubscriberInfo> findAllByPrimaryMsisdn(String msisdn) throws RecordNotFoundException {
		List<SubscriberInfo> list = hibernate.findByCriteria(SubscriberInfo.class, "msisdn", msisdn);

		if(list.size()==0) {
			throw new RecordNotFoundException();
		}

		return list;
	}

	@Override
	public SubscriberInfo findByProfileNumber(String profile_number) throws RecordNotFoundException {
		List<SubscriberInfo> list = hibernate.findByCriteria(SubscriberInfo.class, "profile_number", profile_number);

		if(list.size()==0) {
			throw new RecordNotFoundException();
		}

		return list.get(0);
	}

	@Override
	public SubscriberInfo findPrimaryProfile(String primaryMsisdn) throws RecordNotFoundException {
		List<SubscriberInfo> list = hibernate.findByCriteria(SubscriberInfo.class, "msisdn", primaryMsisdn);

		if(list.size()==0) {
			throw new RecordNotFoundException();
		}

		SubscriberInfo primarySubscriber=null;

		for(SubscriberInfo subscriber:list) {
			logger.debug("subscriber.isPrimary()="+subscriber.isPrimary());
			if(subscriber.isPrimary()) {
				primarySubscriber = subscriber;
				break;
			}
		}

		if(primarySubscriber==null) {
			throw new RecordNotFoundException(); 
		} else {
			return primarySubscriber;
		}

	}

	@Override
	public SubscriberInfo findByPrimaryMsisdnAndPrefix(String primaryMsisdn,
			int prefix) throws RecordNotFoundException {
		List<SubscriberInfo> list = hibernate.findByCriteria(SubscriberInfo.class, "msisdn", primaryMsisdn);

		if(list.size()==0) {
			throw new RecordNotFoundException();
		}

		for(SubscriberInfo subscriber:list) {
			if(subscriber.getPrefix()==prefix) {
				return subscriber;
			}
		}

		return null;
	}

	@Override
	public SubscriberInfo findByPrimaryMsisdnAndPassword(String primaryMsisdn,
			String cookeePassword) throws RecordNotFoundException {
		List<SubscriberInfo> list = hibernate.findByCriteria(SubscriberInfo.class, "msisdn", primaryMsisdn);

		if(list.size()==0) {
			throw new RecordNotFoundException();
		}

		for(SubscriberInfo subscriber:list) {
			if(subscriber.getCookeepassword()==cookeePassword) {
				return subscriber;
			}
		}

		return null;
	}


	@Override
	public SubscriberInfo findByPrimaryMsisdnAndNumberState(String primaryMsisdn,
			int numb_state) throws RecordNotFoundException {
		List<SubscriberInfo> list = hibernate.findByCriteria(SubscriberInfo.class, "msisdn", primaryMsisdn);

		if(list.size()==0) {
			throw new RecordNotFoundException();
		}

		for(SubscriberInfo subscriber:list) {
			if(subscriber.getNumb_state()==numb_state) {
				return subscriber;
			}
		}

		return null;
	}

	@Override
	public SubscriberInfo findByPrimaryMsisdnAndDefaultStatus(String primaryMsisdn,
			boolean defaultStatus) throws RecordNotFoundException {
		List<SubscriberInfo> list = hibernate.findByCriteria(SubscriberInfo.class, "msisdn", primaryMsisdn);

		if(list.size()==0) {
			throw new RecordNotFoundException();
		}

		for(SubscriberInfo subscriber:list) {
			if(subscriber.isIs_default()==defaultStatus) {
				return subscriber;
			}
		}

		return null;
	}

	@Override
	public List<SubscriberInfo> getAllAssociatedProfiles(String primaryMsisdn)
			throws RecordNotFoundException {
		List<SubscriberInfo> list = hibernate.findByCriteria(SubscriberInfo.class, "msisdn", primaryMsisdn);

		if(list.size()==0) {
			throw new RecordNotFoundException();
		}

		return list;
	}
}
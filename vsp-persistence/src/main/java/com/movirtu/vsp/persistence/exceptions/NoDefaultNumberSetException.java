package com.movirtu.vsp.persistence.exceptions;

/**
 *
 * In case no default number is set and there is no prefix to specify the many
 * me number
 *
 * @author mlungisi
 * @since 1.0
 *
 */
public class NoDefaultNumberSetException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -8481168097368842793L;

    /**
     * @see Exception#Exception()
     */
    public NoDefaultNumberSetException() {
        super();
    }

    /**
     * @see Exception#Exception(String)
     */
    public NoDefaultNumberSetException(String message) {
        super(message);
    }

    /**
     * @see Exception#Exception(String, Throwable)
     */
    public NoDefaultNumberSetException(String message, Throwable e) {
        super(message, e);
    }
}

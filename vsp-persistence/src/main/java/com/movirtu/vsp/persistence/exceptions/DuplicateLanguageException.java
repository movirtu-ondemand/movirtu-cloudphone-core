package com.movirtu.vsp.persistence.exceptions;


public class DuplicateLanguageException extends DuplicateRecordException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public DuplicateLanguageException() {
        super();
    }

    public DuplicateLanguageException(String message, Throwable e) {
        super(message, e);
    }

    public DuplicateLanguageException(String message) {
        super(message);
    }
}

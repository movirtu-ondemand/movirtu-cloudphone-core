package com.movirtu.vsp.persistence.dao.hb;

import org.apache.log4j.Logger;

import com.movirtu.vsp.persistence.dao.hb.impl.HibernateDaoImpl;

public class BaseHibernateImpl {
    
	private final static Logger logger = Logger.getLogger(BaseHibernateImpl.class);
    protected HibernateDaoImpl hibernate;
    
    /**
     * Uses Hibernate Session Factory;
     *
     * Used mainly for testing and web applications with default filename
     */
    public BaseHibernateImpl() {
    	
        logger.debug("Using Hibernate: {} ");
        if (hibernate == null) {
        	hibernate = new HibernateDaoImpl();
        }
    }
   

}

package com.movirtu.vsp.persistence.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "imsi_msrn_mapping")
public class ImsiMsrnMapping implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 8109441193061701549L;

	@SequenceGenerator(name = "imsi_msrn_mapping_id_seq", sequenceName = "imsi_msrn_mapping_id_seq", allocationSize = 1)
	@GeneratedValue(generator = "imsi_msrn_mapping_id_seq", strategy = GenerationType.SEQUENCE)
	@Id
	private Integer id;

	@Column(name = "msrn", nullable = false)
	private String msrn;

	@Column(name = "imsi", nullable = false)
	private String imsi;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the msrn
	 */
	public String getMsrn() {
		return msrn;
	}

	/**
	 * @param msrn
	 *            the msrn to set
	 */
	public void setMsrn(String msrn) {
		this.msrn = msrn;
	}

	/**
	 * @return the imsi
	 */
	public String getImsi() {
		return imsi;
	}

	/**
	 * @param imsi
	 *            the imsi to set
	 */
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImsiMsrnMapping other = (ImsiMsrnMapping) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ImsiMsrnMapping [id=" + id + ", msrn=" + msrn + ", imsi="
				+ imsi + "]";
	}

}

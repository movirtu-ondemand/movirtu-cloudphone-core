/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.movirtu.vsp.persistence.dao.hb.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import com.movirtu.vsp.persistence.dao.hb.BaseHibernateImpl;
import com.movirtu.vsp.persistence.dao.hb.SettingsDao;
import com.movirtu.vsp.persistence.domain.Settings;

/**
 * 
 * @since ManyMe Mobile
 */
 @Component
public class SettingsDaoImpl extends BaseHibernateImpl implements SettingsDao {

	/**/
	private final static Logger logger = Logger.getLogger(SettingsDaoImpl.class);
    
	/**
	  * Method getValue
	  * @param String
	  * @return String
	  */
	@Override
    public String getValue(String key) {
        String value = hibernate.findById(Settings.class, key).getValue();
        logger.debug ("getValue(key: "+key+ ") returns: value:"+value);
        return value;
    }

    /**
	  * getAllSettings
	  * @return List<Settings>
	  */
	@Override
    public List<Settings> getAllSettings() {
        List<Settings> allSettings = hibernate.findAll(Settings.class);
        return allSettings;
    }

    /**
	  * For adding new Entity
	  * @param Setting
	  */
	@Override
    public void addNew(Settings t) {
    	hibernate.addNew(t);
    }

    /**
	  * For adding Setting Entity
	  * @param Setting
	  */
	@Override
    public void update(Settings t) {
    	hibernate.update(t);
    }

    /**
	  * findById
	  * @param String
	  * @return Settings
	  */
    public Settings findById(String id) {
    	return hibernate.findById(Settings.class, id);
    }

    /**
	  * findAll
	  * @return List<Settings>
	  */
    @Override
    public List<Settings> findAll() {
    	return hibernate.findAll(Settings.class);
    }

 	/**
	  * For deleting Setting Entity
	  * @param Settings
	  */
    @Override
	public void delete(Settings t) {
		hibernate.delete(t);
	}

	/**
	  * For deleting Setting Entity
	  * @param String
	  */
    @Override
	public void delete(String id) {
		hibernate.delete(Settings.class, id);
	}
}

package com.movirtu.vsp.persistence.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="subscriber_info", uniqueConstraints=@UniqueConstraint(columnNames={"profile_number"}))
public class SubscriberInfo implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7138025297041197581L;

	@Id
	@SequenceGenerator(name = "subscriber_info_id_seq", sequenceName = "subscriber_info_id_seq", allocationSize=1 )
	@GeneratedValue(generator = "subscriber_info_id_seq", strategy=GenerationType.SEQUENCE)
	private Integer id;

	@Column(name="msisdn",nullable=false)
	private String msisdn;

	@Column(name="prefix",nullable=false)
	private int prefix;

	@Column(name = "profile_name")
	private String profile_name;

	@Column(name = "profile_number")
	private String profile_number;


	@Column(name = "profile_color")
	private String profile_color;

	@Column(name = "profile_icon")
	private int profile_icon;

	@Column(name = "user_state", nullable = false)
	private int user_state;

	@Column(name = "FTN")
	private String FTN;

	@Column(name = "forward_option")
	private String forward_option;

	@Column(name = "pin", nullable = false)
	private String pin;

	@Column(name = "cookeepassword", nullable = false)
	private String cookeepassword;


	@Column(name = "verify_retry_max", nullable = false)
	private int verify_retry_max;

	@Column(name = "profile_max", nullable = false)
	private int profile_max;

	@Column(name="numb_state")
	private int numb_state;

	@Column(name="is_default")
	private boolean is_default;

	@Column(name="code_expiry_time")
	private long code_expiry_time;

	@Column(name="isPrimary")
	private boolean isPrimary;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public int getPrefix() {
		return prefix;
	}

	public void setPrefix(int prefix) {
		this.prefix = prefix;
	}

	public String getProfile_name() {
		return profile_name;
	}

	public String getProfile_number() {
		return profile_number;
	}

	public void setProfile_number(String profile_number) {
		this.profile_number = profile_number;
	}

	public void setProfile_name(String profile_name) {
		this.profile_name = profile_name;
	}

	public String getProfile_color() {
		return profile_color;
	}

	public void setProfile_color(String profile_color) {
		this.profile_color = profile_color;
	}

	public int getProfile_icon() {
		return profile_icon;
	}

	public void setProfile_icon(int profile_icon) {
		this.profile_icon = profile_icon;
	}

	public int getUser_state() {
		return user_state;
	}

	public void setUser_state(int user_state) {
		this.user_state = user_state;
	}

	public String getFTN() {
		return FTN;
	}

	public void setFTN(String fTN) {
		FTN = fTN;
	}

	public String getForward_option() {
		return forward_option;
	}

	public void setForward_option(String forward_option) {
		this.forward_option = forward_option;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getCookeepassword() {
		return cookeepassword;
	}

	public void setCookeepassword(String cookeepassword) {
		this.cookeepassword = cookeepassword;
	}

	public int getVerify_retry_max() {
		return verify_retry_max;
	}

	public void setVerify_retry_max(int verify_retry_max) {
		this.verify_retry_max = verify_retry_max;
	}

	public int getProfile_max() {
		return profile_max;
	}

	public void setProfile_max(int profile_max) {
		this.profile_max = profile_max;
	}

	public int getNumb_state() {
		return numb_state;
	}

	public void setNumb_state(int numb_state) {
		this.numb_state = numb_state;
	}

	public boolean isIs_default() {
		return is_default;
	}

	public void setIs_default(boolean is_default) {
		this.is_default = is_default;
	}

	public long getCode_expiry_time() {
		return code_expiry_time;
	}

	public void setCode_expiry_time(long code_expiry_time) {
		this.code_expiry_time = code_expiry_time;
	}

	public boolean isPrimary() {
		return isPrimary;
	}

	public void setPrimary(boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 83 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final SubscriberInfo other = (SubscriberInfo) obj;
		if ((this.id == null) ? (other.id != null) : !this.id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "PrimaryMsisdn ["
				+ "id=" + id 
				+ ", msisdn=" + msisdn 
				+ ", prefix=" + prefix
				+"]";
	}

}

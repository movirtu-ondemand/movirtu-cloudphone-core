package com.movirtu.vsp.persistence.exceptions;

public class NoAvailableMsrnException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public NoAvailableMsrnException() {
        super();
    }

    public NoAvailableMsrnException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public NoAvailableMsrnException(Throwable arg0) {
        super(arg0);
    }
}

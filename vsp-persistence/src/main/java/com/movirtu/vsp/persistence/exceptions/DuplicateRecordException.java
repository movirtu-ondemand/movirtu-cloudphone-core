package com.movirtu.vsp.persistence.exceptions;

/**
 *
 * In case record already exists in terms of unique keys on database table
 *
 * @author mlungisi
 * @since 1.0
 *
 */
public class DuplicateRecordException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -5900190190258086524L;

    /**
     * @see Exception#Exception()
     */
    public DuplicateRecordException() {
        super();
    }

    /**
     * @see Exception#Exception(String)
     */
    public DuplicateRecordException(String message) {
        super(message);
    }

    /**
     * @see Exception#Exception(String, Throwable)
     */
    public DuplicateRecordException(String message, Throwable e) {
        super(message, e);
    }
}

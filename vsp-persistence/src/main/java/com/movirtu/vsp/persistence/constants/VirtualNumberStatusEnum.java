package com.movirtu.vsp.persistence.constants;

public enum VirtualNumberStatusEnum {

	UNASSIGNED(0),
	INPROGRESS(1),
	ASSIGNED(2);

	private int status;

	VirtualNumberStatusEnum(int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}

package com.movirtu.vsp.persistence.dao.hb;

import java.util.List;

import com.movirtu.vsp.persistence.dao.GenericDao;
import com.movirtu.vsp.persistence.domain.Settings;

public interface SettingsDao extends GenericDao<Settings, String>{

    String getValue(String key) ;
    
    List<Settings> getAllSettings() ;
}

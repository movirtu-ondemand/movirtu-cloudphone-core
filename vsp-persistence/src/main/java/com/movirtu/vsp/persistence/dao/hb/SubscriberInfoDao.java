package com.movirtu.vsp.persistence.dao.hb;

import java.util.List;

import com.movirtu.vsp.persistence.dao.GenericDao;
import com.movirtu.vsp.persistence.domain.SubscriberInfo;
import com.movirtu.vsp.persistence.exceptions.RecordNotFoundException;

public interface SubscriberInfoDao extends GenericDao<SubscriberInfo, Integer> {
	
	SubscriberInfo getPrimaryMsisdnById(Integer id) throws RecordNotFoundException;
	
	List<SubscriberInfo> getAllSubscriberProfiles() throws RecordNotFoundException;
	
	List<SubscriberInfo> getAllAssociatedProfiles(String primaryMsisdn) throws RecordNotFoundException;
	
	SubscriberInfo findByPrimaryMsisdn(String primaryMsisdn) throws RecordNotFoundException;
	
	SubscriberInfo findByPrimaryMsisdnAndPrefix(String primaryMsisdn, int prefix) throws RecordNotFoundException;

	SubscriberInfo findByPrimaryMsisdnAndNumberState(String primaryMsisdn,
			int numb_state) throws RecordNotFoundException;

	SubscriberInfo findByPrimaryMsisdnAndDefaultStatus(String primaryMsisdn,
			boolean defaultStatus) throws RecordNotFoundException;

	SubscriberInfo findByProfileNumber(String profile_number)
			throws RecordNotFoundException;

	SubscriberInfo findPrimaryProfile(String primaryMsisdn)
			throws RecordNotFoundException;

	SubscriberInfo findByPrimaryMsisdnAndPassword(String primaryMsisdn,
			String cookeePassword) throws RecordNotFoundException;

	List<SubscriberInfo> findAllByPrimaryMsisdn(String msisdn)
			throws RecordNotFoundException;

}

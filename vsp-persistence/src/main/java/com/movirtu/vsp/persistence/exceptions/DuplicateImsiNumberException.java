package com.movirtu.vsp.persistence.exceptions;


public class DuplicateImsiNumberException extends DuplicateRecordException {

    public DuplicateImsiNumberException(String message, Throwable e) {
        super(message, e);
    }
    /**
     *
     */
    private static final long serialVersionUID = 9149972285669840932L;
}

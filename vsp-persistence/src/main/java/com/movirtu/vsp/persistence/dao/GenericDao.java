/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.movirtu.vsp.persistence.dao;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Mlungisi Sincuba
 * @since 1.0
 */
public interface GenericDao<T, PK extends Serializable> {
    void addNew(T t);
    void update(T t);
    T findById(PK id);
    List<T> findAll();
    void delete(T t);
    void delete(PK id);
}

﻿CREATE TABLE settings
(
  key character varying(50) NOT NULL,
  value character varying(100),
  comment character varying(300),
  CONSTRAINT settings_pkey PRIMARY KEY (key)
)


CREATE TABLE imsi_msrn_mapping
(
  id serial NOT NULL,
  imsi character varying(50) NOT NULL,
  msrn character varying(50) NOT NULL,
  CONSTRAINT imsi_msrn_mapping_pkey PRIMARY KEY (id)
)

CREATE TABLE primary_msisdn
(
  primary_msisdn_key character varying(50) NOT NULL,
  id serial NOT NULL,
  prefix character varying(10) NOT NULL,
  virtual_msisdn character varying(50) NOT NULL,
  display_msisdn character varying(50) NOT NULL,
  charge_msisdn character varying(50) NOT NULL,
  vimsi character varying(50) NOT NULL,
  is_active boolean,
  is_prepaid boolean,
  CONSTRAINT primary_msisdn_pkey PRIMARY KEY (id)
)
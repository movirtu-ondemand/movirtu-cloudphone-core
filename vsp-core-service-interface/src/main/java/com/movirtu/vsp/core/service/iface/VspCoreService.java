package com.movirtu.vsp.core.service.iface;

import java.util.List;
import java.util.Map;

import com.movirtu.vsp.persistence.domain.PrimaryMsisdn;

public interface VspCoreService {

	List<String> getPurchaseNumbers();

	void purchaseNumber(String number); 

	public double getBalance(String msisdn);

	public Map<String, Double> performBalanceQuery(String msisdn);

	public boolean setDefaultNumber(String msisdn);

	public List<String> getAllAssociatedNumber(String msisdn);

	public Map<String, Boolean> checkStatus(String primaryMsisdn);

	void updatePrinaryMsisdnTable(String primary_msisdn_key, int prefix,
			String virtual_msisdn, String display_msisdn, String charge_msisdn,
			String vimsi, boolean isActive, boolean isPreparid);

	PrimaryMsisdn getProfile(String msisdn);

	void sendSMS(String msisdn, String pin);

	PrimaryMsisdn createSignupProfile(String sim_number, int profile_prefix,
			String virtual_msisdn, String vimsi, boolean isActive,
			boolean isPrepaid);

	PrimaryMsisdn createVirtualProfile(String sim_number, int profile_prefix,
			String virtual_msisdn, String vimsi, boolean isActive,
			boolean isPrepaid);

	PrimaryMsisdn createVirtualProfile(String sim_number, int profile_prefix,
			String virtual_msisdn, boolean isActive, boolean isPrepaid);

	void performLocationUpdate(String msisdn, String virtualNumber);

	PrimaryMsisdn getProfile(String msisdn, int prefix);

	void performLocationUpdateWithImsi(String msisdn, String imsi);

	//void handleSRISMResponse(SRISMResponse message);

	void deleteProfile(String sim_number, int profile_prefix);

}
